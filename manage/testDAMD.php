<?php
	// 未登入則轉至首頁(登入頁)
	if (!isset($_SESSION)) { session_start(); }
	if (!(isset($_SESSION["manageuser"]))) {
		echo '<meta http-equiv=REFRESH CONTENT=1;url=index.php>';		
	} else {
		include_once("../common/connectdb.php");
		include_once("../common/AutoFormClass.php");

		$a = new AutoFormClass("D","testD.php","testDAMD.php","zonedetail","zonedetail",array("ZoneID","item"),"區域基本資料單身(異動)","center","table90");
		$a->setTableTitle("left","Arial18Bold bg_gray");
		$a->setFieldTitle("center","Arial16Bold bg_y");
		$a->setButtonDefaultClass("Arial14Bold");
		$a->setField("ZoneID","區域代碼","left","Arial14","N","N","","","Y","N","Y","N","text",array(array(),""),"");
		$a->setField("item","項目","left","Arial14","N","N","","","Y","Y","Y","Y","date",array(array(),""),"");
		$a->setField("vvv","備註","left","Arial14","N","N","","","Y","Y","Y","Y","text",array(array(),""),"");
		//$a->addBeforeDeleteCheck("community",array("ZoneID","ZoneID"),"已被社區基本資料叫用,不可刪除!");
		$a->showData();
	}
?>