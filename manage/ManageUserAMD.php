<?php
	// 未登入則轉至首頁(登入頁)
	if (!isset($_SESSION)) { session_start(); }
	if (!(isset($_SESSION["manageuser"]))) {
		echo '<meta http-equiv=REFRESH CONTENT=1;url=index.php>';		
	} else {
		include_once("../common/connectdb.php");
		include_once("../common/PublicFunction.php");		
		include_once("../common/AutoFormClass.php");
		
		$a = new AutoFormClass("M","ManageUser.php","ManageUserAMD.php","manage_user","manage_user",array("UserID"),"管理者基本資料單頭(異動)","center","");
		$a->setTableTitle("left","font-18-bold bg_gray");
		$a->setFieldTitle("center","font-16-bold bg_y");
		$a->setButtonDefaultClass("btn btn-warning");
		
		$a->setField("UserID","帳號","left","font-14","N","N","","","Y","Y","Y","N","text",array(array(),""),"","");
		$a->setField("UserName","名稱","left","font-14","N","N","","","Y","Y","Y","Y","text",array(array(),""),"","");
		$a->setField("Password","密碼","left","font-14","N","N","","","Y","Y","Y","Y","password",array(array(),""),"","");
	
		$RoleID_item1 = "SELECT DISTINCT RoleName,RoleID FROM v_manage_roleprogram";
		$a->setField("RoleID","系統角色權限","left","font-14","Y","N","","","Y","Y","Y","Y","select",array(array("無系統權限","none"),$RoleID_item1),"","");

		$CommID_item1 = "SELECT DISTINCT CommName,CommID FROM v_manage_user_comm_program WHERE UserID='".getIdValueFromPost('UserID')."' ORDER BY CommName"; 
		$a->setField("CommID","預設社區","left","font-14","N","N","","","Y","Y","Y","Y","select",array(array("無預設社區",""),$CommID_item1),"","");			
	
		$a->setField("Tel","電話","left","font-14","N","N","","","Y","Y","Y","Y","text",array(array(),""),"");
		$a->setField("Email","電子郵件","left","font-14","N","N","","","Y","Y","Y","Y","text",array(array(),""),"","");			
		$a->setField("Enable","有效","left","font-14","Y","N","","","Y","Y","Y","Y","radio",array(array("啟用","Y","停用","N"),""),"Y","");
		$a->setField("Note","備註","left","font-14","Y","N","","","Y","Y","Y","Y","textarea",array(array(5,100),""),"","");
		$a->setField("ModUser","異動人員","left","font-14","N","N","","","Y","Y","Y","Y","hidden",array(array(),""),"","",array("loginuser","Y","Y"));
		$a->setField("ModDate","異動日期","left","font-14","N","N","","","Y","Y","Y","Y","hidden",array(array(),""),"","",array("datetime","Y","Y"));

		$a->addBeforeDeleteCheck("manage_user_role",array("UserID","UserID"),"已被【使用者社區角色權限維護作業】叫用,不可刪除!");
		$a->showData();
		
		if (isset($_POST["f"]) && ($_POST["f"] == "A" || $_POST["f"] == "M")) {
			// 欄位及規則檢查
?>
			<script language="JavaScript">
				var form = "MasterForm";
				var fieldArray = [];
				fieldArray.push(["UserID","Need","此欄位為必填"]);
				fieldArray.push(["UserName","Need","此欄位為必填"]);
				fieldArray.push(["Password","Need","此欄位為必填"]);				
				checkRule(form,fieldArray);
								
				function otherCheckRule() {
					return true;
				}
			</script>
<?php
		}		
	}
?>
