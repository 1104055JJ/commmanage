<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
	// 未登入則轉至首頁(登入頁)
	session_start();
	if (!(isset($_SESSION["manageuser"]))) {
		echo '<meta http-equiv=REFRESH CONTENT=1;url=index.php>';		
	} else {
?>
		<script type="text/javascript" src="../js/DetailJSB.js"></script>
		<script language="JavaScript">
			function openwindow(l_url) {
				var w = window.open(l_url,"w",config="width=500,height=500");
			}
                </script>
		
<?php
		include("../common/connectdb.php");
		include("../common/AutoDataClassB.php");
		include("../common/AutoPageSwitchClass.php");	

		// 建立及初使化頁數切換
		$a = new AutoPageSwitchClass("D","PA_RequestDetail.php","SELECT COUNT(*) FROM paymentrequestdetail",array(10,20,30,40));			
		
		// 建立及初始化表格
		$b = new AutoDataClass("D","PA_RequestDetail.php","PA_RequestDetailAMD.php",array("CommID","RequestNo","DetailNo","SupplierID"),"","請款單身資料維護作業","center","table90");
		$b->setTableTitle("left","Arial18Bold bg_gray");
		$b->setFieldTitle("center","Arial16 bg_y");	
		$b->setOddRow("","bg_light_blue");
		//$b->setEvenRow("","bg_y");
		// 配合頁數切換,設定表格資料來源
		$b->beginRowsNum = $a->beginRowsNum;
		$b->setQuery("CommID,RequestNo,DetailNo,SupplierID,RequestDate,paymentitemname,suppliername,Amount,UnitPrice,TotalAmount,Paid,Note","v_payment_request_detail","","",$a->beginRowsNum.",".$a->perPageRows);
		// 設定表格欄位顯示
		$b->setAllFieldAlign("center");
		$b->setAllFieldClass("Arial14");
		$b->setFieldDisplayName("DetailNo","請款單明細項目");
		$b->setFieldDisplayName("paymentitemname","支出項目");
                $b->setFieldDisplayName("RequestDate","請購日期");
                $b->setFieldDisplayName("suppliername","供應商");
		$b->setFieldDisplayName("Amount","數量");
                $b->setFieldDisplayName("UnitPrice","單價");
                $b->setFieldDisplayName("Note","備註");
                $b->setFieldDisplayName("TotalAmount","複價");
                $b->setFieldDisplayName("Paid", "已付金額");
                $b->setFieldDisplay("SupplierID", "N");
                $b->setFieldDisplay("DetailNo", "N");
                $b->setFieldDisplay("CommID", "N");
                $b->setFieldDisplay("RequestNo", "N");
                $b->setFieldDisplay("ModUser", "N");
                $b->setFieldDisplay("ModDate", "N");
                
                
                
		// 增加按鈕及觸發事件
		
		// 設定表格底部顯示頁數切換
		$b->setTableBottom($a->getShowData());
		// 顯示表格資料
		$b->showData();
	}
?>
                
              