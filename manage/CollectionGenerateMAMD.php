<?php
	$err = "";
	// 未登入則轉至首頁(登入頁)
	session_start();
	if (!(isset($_SESSION["manageuser"])))// && isset($_SESSION["SubMenu"]) && isset($_SESSION["ProgramItem"])))
  {echo '<meta http-equiv=REFRESH CONTENT=1;url=index.php>';} 
  else
  {
		include("../common/connectdb.php");
		include("../common/PublicFunction.php");
		include("../common/AutoFormClass.php");    

		$a = new AutoFormClass("M","CollectionGenerateM.php","CollectionGenerateMAMD.php","collectionsmaster","collectionsmaster",array("CommID","CollectionsNo"),"收款作業","center","table90");
		$a->setTableTitle("left","Arial18Bold bg_gray");
		$a->setFieldTitle("center","Arial16 bg_y");
 // $a->setField($fieldName        ,$displayName    , $align, $class  ,$wrap,$isNumeric,$beforeHTML,$afterHTML,$A_Display,$A_CanEdit,$M_Display,$M_CanEdit,$inputType, $defaultValue      )
    $a->setField("CollectionsNo"   ,"收款編號"      ,"left" ,"Arial14","N"  ,"N"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"N"       ,"text"    ,array(array(),""),"","",array("autoserial","Y","Y"));
		$FillterHouseHold = "Select HouseHoldID, HouseHoldID From household Where CommID ='".$_SESSION['Community']."'";
    $a->setField("HouseHoldID"     ,"門牌戶號"      ,"left" ,"Arial14","N"  ,"N"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"Y"       ,"select"  ,array(array(),$FillterHouseHold),"", 'required');
		$a->setField("PaymentType"     ,"收款方式"      ,"left" ,"Arial14","N"  ,"N"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"Y"       ,"select"  ,array(array("現金","0","轉帳","1","支票","2"),""),"0",'size="6" maxlength="6" required');
		$FillterIncomeItemID = "Select IncomeItemShow, IncomeItemID From incomeitem Where CommID ='".$_SESSION['Community']."' and IncomeItemID <> '003'and IncomeItemID <> '004'";
    $a->setField("IncomeItemID"    ,"收款用途"      ,"left" ,"Arial14","Y"  ,"N"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"Y"       ,"select"  ,array(array(),$FillterIncomeItemID),"",'size="30" maxlength="30" required');
		$a->setField("BillNo"          ,"票據號碼"      ,"left" ,"Arial14","N"  ,"N"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"Y"       ,"text"    ,array(array(),""),"",'size="12" maxlength="12"');
		$a->setField("BillDate"        ,"收票日期"      ,"left" ,"Arial14","N"  ,"N"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"Y"       ,"date"    ,array(array(),""),"",'size="10" maxlength="10"');
		$a->setField("DueDate"         ,"收款(到期)日"  ,"left" ,"Arial14","N"  ,"N"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"Y"       ,"date"    ,array(array(),""),"",'size="10" maxlength="10" required');
		$a->setField("IncomeAmount"    ,"收款金額"      ,"left" ,"Arial14","Y"  ,"Y"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"Y"       ,"text"    ,array(array(),""),"",'size="10" maxlength="10" Required');
    $a->setField("Refund"          ,"退款共計"      ,"left" ,"Arial14","N"  ,"Y"       ,""         ,"<th align='center' class='Arial16 bg_y'>賬單共計</th> <td align='left' class='Arial14'><input type='text' name='TotalArs' id='TotalArs' size='10' maxlength='10' readonly='true' value='0'></td> <th align='center' class='Arial16 bg_y'>收款餘額</th> <td align='left' class='Arial14'><input type='text' name='TotalRes' id='TotalRes' size='10' maxlength='10' readonly='true' value='0'></td>"        ,"Y"       ,"Y"       ,"Y"       ,"Y"       ,"text"    ,array(array(),""),"",'size="10" maxlength="10"');
		$a->setField("Note"            ,"收款說明"      ,"left" ,"Arial14","N"  ,"N"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"Y"       ,"text"    ,array(array(),""),"",'size="15" maxlength="100"');
//hidden column
    $Comm = "".$_SESSION['Community']."";
    $a->setField("CommID"          ,"社區編號"      ,"left" ,"Arial14","N"  ,"N"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"Y"       ,"Hidden"  ,array(array(),""),"$Comm",'size="7" maxlength="7" required');
    $a->setField("CollectionDate"  ,"收款日期"      ,"left" ,"Arial14","N"  ,"N"       ,""         ,""        ,"N"       ,"Y"       ,"Y"       ,"Y"       ,"Hidden"  ,array(array(),""),"");    
    //$User="".$_SESSION["manageuser"]."";
		$a->setField("ModUser"         ,"異動人員"      ,"left" ,"Arial14","N"  ,"N"       ,""         ,""        ,"Y"       ,"Y"       ,"N"       ,"N"       ,"Hidden"  ,array(array(),""),"","",array("loginuser","Y","Y"));
		$a->setField("ModDate"         ,"異動日期"      ,"left" ,"Arial14","N"  ,"N"       ,""         ,""        ,"Y"       ,"Y"       ,"N"       ,"N"       ,"Hidden"  ,array(array(),""),"","",array("datetime","Y","Y"));
	  $a->setField("TurnToIncomeDate","轉入收入日期"  ,"left" ,"Arial14","N"  ,"N"       ,""         ,""        ,"Y"       ,"Y"       ,"N"       ,"N"       ,"Hidden"  ,array(array(),""),"");
		$a->setField("CleanDeduction"  ,"清潔費扣款"    ,"left" ,"Arial14","N"  ,"Y"       ,""         ,""        ,"Y"       ,"Y"       ,"N"       ,"N"       ,"Hidden"  ,array(array(),""),"0",'size="10" maxlength="10"');
    $a->setField("RepairDeduction" ,"修繕費用扣款"  ,"left" ,"Arial14","N"  ,"Y"       ,""         ,""        ,"Y"       ,"Y"       ,"N"       ,"N"       ,"Hidden"  ,array(array(),""),"0",'size="10" maxlength="10"');
    $a->setField("PunishDeduction" ,"罰款扣款"      ,"left" ,"Arial14","N"  ,"Y"       ,""         ,""        ,"Y"       ,"Y"       ,"N"       ,"N"       ,"Hidden"  ,array(array(),""),"0",'size="10" maxlength="10"');
    $a->setField("ReturnDeduction" ,"回饋金扣款"    ,"left" ,"Arial14","N"  ,"N"       ,""         ,""        ,"Y"       ,"Y"       ,"N"       ,"N"       ,"Hidden"  ,array(array(),""),"0");
    $a->setBeforeForm("Y","./CollectionGenerateMAMDJS.php");
  	$a->showData();		
    //
    //syntax reference:
   	//$a->setField("ModDate"         ,"修改日期"      ,"left" ,"Arial14","N"  ,"N"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"Y"       ,"select"  ,array(array(),"Select CompanyName,CompanyID From Company Where Enable = 'Y'"),"PS1");
	  //$a->setField("ModUser"         ,"修改人員"      ,"left" ,"Arial14","Y"  ,"N"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"Y"       ,"select"  ,array(array(),"Select ZoneName,ZoneID From Zone Where Enable = 'Y'"),"02");
    //$a->setField("DueDate"         ,"收款(到期)日期","left" ,"Arial14","Y"  ,"N"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"Y"       ,"select"  ,array(array(),"Select CodeName,CodeID from unifieddatacode where catagoryid='A02'"),"Y");
	  //$a->setField("IncomeAmount"    ,"收款金額"      ,"left" ,"Arial14","N"  ,"N"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"Y"       ,"select"  ,array(array(),"Select CodeName,CodeID from unifieddatacode where catagoryid='A01'"),"1");    
	}
?>