<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
	// 未登入則轉至首頁(登入頁)
	session_start();
	if (!(isset($_SESSION["manageuser"]))) {
		echo '<meta http-equiv=REFRESH CONTENT=1;url=index.php>';		
	} else {
?>
		<script type="text/javascript" src="../js/MasterJSB.js"></script>
		<!--<script language="JavaScript">refreshDataMAMD('ProgramAMD.php','A','');</script>-->
<?php
		include("../common/connectdb.php");
		include("../common/AutoDataClassB.php");
		include("../common/AutoPageSwitchClass.php");	
		//include("../common/PublicFunction.php");

		// 建立及初使化頁數切換
		$a = new AutoPageSwitchClass("M","CM_UDC.php","SELECT COUNT(Distinct CatagoryID) FROM unifieddatacode",array(5,10,15,20,25));			
		
		// 建立及初始化表格
		$b = new AutoDataClass("M","CM_UDC.php","blank.php",array("CatagoryID","CatagoryName"),"CM_UDCD.php","清單代碼維護作業","center","table90");
		$b->setTableTitle("left","Arial18Bold bg_gray");
		$b->setFieldTitle("center","Arial16 bg_y");	
		$b->setOddRow("","bg_light_blue");
		//$b->setEvenRow("","bg_y");
		// 配合頁數切換,設定表格資料來源
		$b->beginRowsNum = $a->beginRowsNum;
		$b->setQuery("Distinct CatagoryID,CatagoryName","unifieddatacode","","",$a->beginRowsNum.",".$a->perPageRows);
		// 設定表格欄位顯示
		$b->setAllFieldAlign("center");
		$b->setAllFieldClass("Arial14");
		$b->setFieldDisplayName("CatagoryID","代碼類別");
		$b->setFieldDisplayName("CatagoryName","代碼名稱");
		$b->setFAMDButtonDisplay("N", "N", "N", "N", "Y");
		
		$b->addButton("新增udc類別","","addcode","btn btn-info");
		$b->addButton("刪除udc類別","","delcode","btn btn-info");
		// 增加按鈕及觸發事件
		// 設定表格底部顯示頁數切換
		$b->setTableBottom($a->getShowData());
		// 顯示表格資料
		$b->showData();
	}
?>
<div id="dialog_addcode" title="新增udc類別">
	類別代碼<input type="text" id= "cat1" />
	類別名稱<input type="text" id= "cat_name" />
	<br>
	<button id="b_add" name="b_add">新增</button>
	<br>
	<div id = "msg"></div>
</div>
<div id="dialog_delcode" title="刪除項目">
	請輸入來源社區
	<select id="del_cat1" name="del_cat1" class="Arial14">
		<?PHP
			//$where = "community where CommID <> '".$_SESSION['Community']."'";
			SelectOption('unifieddatacode','CatagoryID','CatagoryName','');
		?>
	</select>
	<br>
	<div id ='msg1'>請選擇欲刪除項目</div>
	<br>
	<button id="b_del" >刪除</button>
</div>

<script type="text/javascript">
function showDialog(title,flag) {
    $("#dialog_addcode").dialog({
        autoOpen: false,
        title: title,
        height:550,
      	width:600,
        modal:true
    });
    $("#dialog_delcode").dialog({
        autoOpen: false,
        title: title,
        height:550,
      	width:600,
        modal:true
    });

    //第一次載入時不開啟
    if (flag=='1'){
    	$("#dialog_addcode").dialog("open");
    }
    if (flag=='2'){
    	$("#dialog_delcode").dialog("open");
    }
}
$( "#addcode" ).click(function() {
	showDialog("新增udc類別",'1');
});

$( "#delcode" ).click(function() {
	showDialog("刪除udc類別",'2');
});
$( "#b_add" ).click(function() {
     addCode1();
});
   $( "#b_del" ).click(function() {
		delCode1();
	});
function addCode1(){
  			var catacode = document.getElementById("cat1").value;
  			var cataname = document.getElementById("cat_name").value;
			//alert(catacode);
			//$("#dialog_addcode").dialog( "close" );
			$.ajax({
				url: "CM_UDC_addCatagory.php",
				type:"POST",
				dataType: "text",
				data:{type:'a',catacode:catacode,cataname:cataname},
				//成功執行並返回值
				success: function(data){
					$("#msg").empty().append(data);  
					//alert(data); 
					//refreshDataM('CM_UDC.php',10,10,1,'','','','','');	
				},
				//發送請求之前會執行的函式
				beforeSend:function(){
					
				},
				//請求完成時執行的函式(不論結果是success或error)
				complete:function(){	
					
				},
				error:function(xhr, ajaxOptions, thrownError){ 
					alert(xhr.status);
					alert(thrownError);
				}
			});
			refreshDataM('CM_UDC.php',10,10,1,'','','','','');	
}
function delCode1(){
  			var e = document.getElementById("del_cat1");
  			var catacode = e.options[e.selectedIndex].value;
  			var cataname = " ";
			$.ajax({
				url: "CM_UDC_addCatagory.php",
				type:"POST",
				dataType: "text",
				data:{type:'d',catacode:catacode,cataname:cataname},
				//成功執行並返回值
				success: function(data){
					$("#msg1").empty().append(data);  
					//alert(data); 
					//refreshDataM('CM_UDC.php',10,10,1,'','','','','');	
				},
				//發送請求之前會執行的函式
				beforeSend:function(){
					
				},
				//請求完成時執行的函式(不論結果是success或error)
				complete:function(){	
					
				},
				error:function(xhr, ajaxOptions, thrownError){ 
					alert(xhr.status);
					alert(thrownError);
				}
			});
			refreshDataM('CM_UDC.php',10,10,1,'','','','','');	
}
//第一次執行載入dialog
showDialog("新增udc類別",'0');
</script>
