<?php
	// 未登入則轉至首頁(登入頁)
	if (!isset($_SESSION)) { session_start(); }
	if (!(isset($_SESSION["manageuser"]))) {
		echo '<meta http-equiv=REFRESH CONTENT=1;url=index.php>';		
	} else {
		include("../common/connectdb.php");
		include("../common/AutoFormClass.php");
		
		$a = new AutoFormClass("M","Program.php","ProgramAMD.php","manage_program","manage_program",array("ProgramID"),"程式設定維護作業(異動)","center","");
		$a->setTableTitle("left","font-18-bold bg_gray");
		$a->setFieldTitle("center","font-16-bold bg_y");
		$a->setButtonDefaultClass("btn btn-warning");
		$a->setField("ProgramID","程式代碼","left","font-14","N","N","","","Y","Y","Y","N","text",array(array(),""),"","");
		$a->setField("ProgramName","程式名稱","left","font-14","N","N","","","Y","Y","Y","Y","text",array(array(),""),"","");
		$a->setField("ProgramIcon","圖示","left","font-14","N","N","","","Y","Y","Y","Y","hidden",array(array(),""),"icon_arrow.png","");
		$a->setField("ModuleID","程式模組","left","font-14","N","N","","","Y","Y","Y","Y","select",array(array(),"select ModuleName,ModuleID from manage_module order by ModuleSort"),"","");	
		$a->setField("ProgramSort","排序","left","font-14","Y","Y","","","Y","Y","Y","Y","combobox",array(array(),"SELECT DISTINCT ProgramSort,ProgramSort FROM manage_program ORDER BY ProgramSort"),"");
		$a->setField("ProgramType","程式類型","left","font-14","N","N","","","Y","Y","Y","Y","radio",array(array("系統程式","S","社區程式","C"),""),"C","");
		$a->setField("ProgramFileName","程式檔名","left","font-14","N","N","","","Y","Y","Y","Y","text",array(array(),""),"","");
		$a->setField("ProgramJSON","JSON","left","font-14","N","N","","","Y","Y","Y","Y","hidden",array(array(),""),"","");
		$a->setField("Enable","啟用否","left","font-14","N","N","","","Y","Y","Y","Y","radio",array(array("啟用","Y","停用","N"),""),"Y");			
		$a->addBeforeDeleteCheck("manage_roleprogram",array("ProgramID","ProgramID"),"資料已被【系統程式權限維護作業】調用,不可刪除 !");
		$a->addBeforeDeleteCheck("manage_role_zone",array("ProgramID","ProgramID"),"資料已被【社區程式權限維護作業-依區域】調用,不可刪除 !");
		$a->addBeforeDeleteCheck("manage_role_comm_program",array("ProgramID","ProgramID"),"資料已被【社區程式權限維護作業-依社區】調用,不可刪除 !");
		$a->showData();
		
		if (isset($_POST["f"]) && ($_POST["f"] == "A" || $_POST["f"] == "M")) {
			// 欄位及規則檢查
?>
			<script language="JavaScript">
				var form = "MasterForm";
				var fieldArray = [];
				fieldArray.push(["ProgramID","Need","此欄位為必填"]);
				fieldArray.push(["ProgramName","Need","此欄位為必填"]);
				fieldArray.push(["ProgramSort","Num","此欄位為整數"]);
				fieldArray.push(["ProgramFileName","Need","此欄位為必填"]);				
				checkRule(form,fieldArray);
								
				function otherCheckRule() {
					return true;
				}
			</script>
<?php
		}
	}
?>