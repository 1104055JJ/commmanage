<?php
	$err = "";
	// 未登入則轉至首頁(登入頁)
	session_start();
	if (!(isset($_SESSION["manageuser"])))// && isset($_SESSION["SubMenu"]) && isset($_SESSION["ProgramItem"]))) 
  {
		echo '<meta http-equiv=REFRESH CONTENT=1;url=index.php>';		
	} 
  else
  {
		include("../common/connectdb.php");
		include("../common/PublicFunction.php");
		include("../common/AutoFormClass.php");    
		//$a = new AutoFormClass("M","RM_ARGenerateMaintain.php","RM_ARGenerateMaintainAMD.php","ar",array("CommID","ArNo"),"賬單查詢","center","table90");
    $a = new AutoFormClass("M","RM_ARGenerateMaintain.php","RM_ARGenerateMaintainAMD.php","ar","ar",array("CommID","ArNo"),"賬單查詢","center","table90");
		$a->setTableTitle("left","Arial18Bold bg_gray");
		$a->setFieldTitle("center","Arial16 bg_y");
 // 	$a->setField($fieldName   ,$displayName    ,$align,$class   ,$wrap,$isNumeric,$beforeHTML,$afterHTML,$A_Display,$A_CanEdit,$M_Display,$M_CanEdit,$inputType,$defaultValue)
    	$Comm = "".$_SESSION['Community']."";
		$a->setField("CommID"     ,"社區代碼"      ,"left","Arial14","N"  ,"N"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"Y"       ,"Hidden"  ,array(array(),""),"$Comm", '');
		$FilterBuilding = "Select BuildingName,BuildingID From Building Where CommID='".$_SESSION['Community']."'";
		$a->setField("BuildingID" ,"棟別代碼"      ,"left","Arial14","N"  ,"N"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"N"       ,"select"  ,array(array(),$FilterBuilding),"","");
    	$a->setField("ArNo"       ,"賬單編號"      ,"left","Arial14","N"  ,"N"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"Y"       ,"text"    ,array(array(),""),"","",array("autoserial","Y","Y") );
		$a->setField("Year"       ,"年度"          ,"left","Arial14","N"  ,"Y"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"N"       ,"text"    ,array(array(),""),"",'size="2" maxlength="2" ');
		$a->setField("Month"      ,"月份"          ,"left","Arial14","N"  ,"Y"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"N"       ,"text"    ,array(array(),""),"",'size="2" maxlength="2" max="12" min="1"');
    	$a->setField("ArDate"     ,"應收帳款產生日","left","Arial14","N"  ,"N"       ,""         ,""        ,"N"       ,"N"       ,"N"       ,"N"       ,"Hidden"  ,array(array(),""),"",'size="5" maxlength="5"',array("date","Y","N"));
		$FillterHouseHold = "Select HouseHoldID, HouseHoldID From household Where CommID ='".$_SESSION['Community']."'";
    	$a->setField("HouseHoldID","門牌戶號"      ,"left","Arial14","Y"  ,"N"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"N"       ,"select"  ,array(array(),$FillterHouseHold),"",'size="5" maxlength="5" ');
		$a->setField("HouseFee"   ,"管理費"        ,"left","Arial14","N"  ,"Y"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"Y"       ,"text"    ,array(array(),""),"",'size="15" maxlength="10"');
		$a->setField("CarFee"     ,"車位清潔費"    ,"left","Arial14","N"  ,"Y"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"Y"       ,"text"    ,array(array(),""),"",'size="15" maxlength="10"');
		$a->setField("Discount"   ,"折扣總額"      ,"left","Arial14","N"  ,"Y"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"Y"       ,"text"    ,array(array(),""),"",'size="15" maxlength="10"');
		$a->setField("TotalAmount","總計金額"      ,"left","Arial14","Y"  ,"Y"       ,""         ,""        ,"Y"       ,"N"       ,"Y"       ,"N"       ,"label"    ,array(array(),""),"",'size="15" maxlength="10"');
		$a->setField("Received"   ,"已收金額"      ,"left","Arial14","N"  ,"Y"       ,""         ,""        ,"Y"       ,"N"       ,"Y"       ,"N"       ,"label"    ,array(array(),""),"0",'size="15" maxlength="10"');
		$a->setField("UnReceived" ,"未收金額"      ,"left","Arial14","N"  ,"Y"       ,""         ,""        ,"Y"       ,"N"       ,"Y"       ,"N"       ,"label"    ,array(array(),""),"",'size="15" maxlength="10"');
		$a->setField("Note"       ,"賬單說明"      ,"left","Arial14","Y"  ,"N"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"Y"       ,"text"    ,array(array(),""),"",'size="75" maxlength="100"');
    	$a->setField("Enable"     ,"啟用"          ,"left","Arial14","N"  ,"N"       ,""         ,""        ,"N"       ,"N"       ,"Y"       ,"Y"       ,"Hidden"  ,array(array(),""),"");
		$a->setField("ModDate"    ,"異動日期"      ,"left","labelinput","N"  ,"N"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"Y"       ,"Hidden"  ,array(array(),""),"",'readonly',array("datetime","Y","Y"));
		$a->setField("ModUser"    ,"異動人員"      ,"left","labelinput","N"  ,"N"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"Y"       ,"Hidden"  ,array(array(),""),"",'readonly',array("loginuser","Y","Y"));    
    	$a->setAfterForm("Y","./RM_ARGenerateMaintainAMDJS.php");
		$a->addBeforeDeleteCheck("collectionsdetail",array("CommID","CommID","ArNo","ArNo"),"已有沖銷資料,不可刪除!"); 
		$a->showData();
		if (isset($_POST["f"]) && ($_POST["f"] == "A" || $_POST["f"] == "M")) {
			// 欄位及規則檢查
?>
			<script language="JavaScript">
				var form = "MasterForm";
				var fieldArray = [];
				fieldArray.push(["Year","Need","此欄位為必填"]);
				fieldArray.push(["Month","Need","此欄位為必填"]);
				fieldArray.push(["HouseFee","Need","此欄位為必填"]);
				fieldArray.push(["CarFee","Need","此欄位為必填"]);
				fieldArray.push(["Discount","Need","此欄位為必填"]);
							
				checkRule(form,fieldArray);
				
				function otherCheckRule() {
					var event = true;
					document.forms["MasterForm"].elements["HouseFee"].style.border="";
					document.forms["MasterForm"].elements["CarFee"].style.border="";
   					document.forms["MasterForm"].elements["Discount"].style.border="";
					
					var HouseFee = document.forms["MasterForm"].elements["HouseFee"]; 
					var CarFee = document.forms["MasterForm"].elements["CarFee"]; 
					var Discount = document.forms["MasterForm"].elements["Discount"]; 
					var TotalAmount = document.forms["MasterForm"].elements["TotalAmount"];
					var UnReceived = document.forms["MasterForm"].elements["UnReceived"];
					var Received = document.forms["MasterForm"].elements["Received"];
					TotalAmount.value = parseInt(HouseFee.value)+parseInt(CarFee.value)-parseInt(Discount.value);
					
   					
					return event;
				}
				
mod = document.getElementById("m_f").value;
  var HouseFee = document.getElementById("HouseFee");
  var CarFee = document.getElementById("CarFee");
  var Discount = document.getElementById("Discount");
  var TotalAmount = document.getElementById("TotalAmount");
  var UnReceived = document.getElementById("UnReceived");
  var Received = document.getElementById("Received");
  var ArNo = document.getElementById("ArNo");
  

function calArDate() {
	alert("tes");
  var currentdate = new Date(); 
  var datetime =  currentdate.getFullYear() + "-"
                + (currentdate.getMonth()+1)  + "-" 
                + currentdate.getDate();
  document.forms["MasterForm"].elements["ArDate"].value = datetime;   
  //alert(datetime);
}

function calsum(){   
  TotalAmount.value = HouseFee.value*1 + CarFee.value*1 - Discount.value*1;
  UnReceived.value= TotalAmount.value*1-Received.value*1;

}

function getHouseHoldID(){
	var CommID = document.getElementById("MasterForm").elements.namedItem("CommID");
	var BuildingID = document.getElementById("MasterForm").elements.namedItem("BuildingID");
			$.ajax({
				url: "RM_ARGenerateBack.php",
				type:"POST",
				dataType: "json",
				data:{type:'1',CommID:CommID.value, BuildingID:BuildingID.value },
				//data:$('#MasterForm').serialize(),
				//成功執行並返回值
				success: function(data){
					//$("#hid").empty().append(data); 
					var obj=document.getElementById("HouseHoldID");
  					obj.options.length=0;
  					for (var i=0;i<data.length ;i=i+1)
 					 {
    					obj.options.add(new Option(data[i],data[i]));
  					}           		
				},
				//發送請求之前會執行的函式
				beforeSend:function(){
				},
				//請求完成時執行的函式(不論結果是success或error)
				complete:function(){		
					getHouseHoldData();
					},
				error:function(xhr, ajaxOptions, thrownError){ 
					alert(xhr.status);
					alert(thrownError);
				}
			});
}

function getHouseHoldData(){
	var CommID = document.getElementById("MasterForm").elements.namedItem("CommID");
	var BuildingID = document.getElementById("MasterForm").elements.namedItem("BuildingID");
	var HouseHoldID = document.getElementById("MasterForm").elements.namedItem("HouseHoldID");
			$.ajax({
				url: "RM_ARGenerateBack.php",
				type:"POST",
				dataType: "json",
				data:{type:'2',CommID:CommID.value, BuildingID:BuildingID.value , HouseHoldID:HouseHoldID.value},
				//成功執行並返回值
				success: function(data){
					document.getElementById("HouseFee").value = data["fee"];
        			document.getElementById("Discount").value = data["discount"]; 
					document.getElementById("CarFee").value = data["SUM(carspace.Netfee)"]; 
					calsum();
				},
				//發送請求之前會執行的函式
				beforeSend:function(){		
					
				},
				//請求完成時執行的函式(不論結果是success或error)
				complete:function(){		
					
				},
				error:function(xhr, ajaxOptions, thrownError){ 
					alert(xhr.status);
					alert(thrownError);
				}
			});
}
if (mod=="AW"){
	document.getElementById("MasterForm").elements.namedItem("BuildingID").onchange= function(){
		getHouseHoldID();
	};

	document.getElementById("MasterForm").elements.namedItem("HouseHoldID").onchange = function() {
		getHouseHoldData();
	}
	//載入時取得第一棟各戶清單
	getHouseHoldID();
	//calArDate;
}
if (mod == "MW" && UnReceived.value < TotalAmount.value){
  HouseFee.readOnly = true;
  CarFee.readOnly = true;
  Discount.readOnly = true;
}
if (mod == "MW" ){
	ArNo.readOnly = true;
}
//HouseFee.onchange = function(){calsum()};
//document.getElementById("MasterForm").elements.namedItem("CarFee").onchange = function(){calsum()};
//document.getElementById("MasterForm").elements.namedItem("Discount").onchange = function(){calsum()};
							
			</script>
<?php
		}
	}
?>