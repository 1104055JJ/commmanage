<?php
	$err = "";
	// 未登入則轉至首頁(登入頁)
	session_start();
	if (!(isset($_SESSION["manageuser"]))) //&& isset($_SESSION["SubMenu"]) && isset($_SESSION["ProgramItem"])))
  {echo '<meta http-equiv=REFRESH CONTENT=1;url=index.php>';} 
  else
  {
		include("../common/connectdb.php");
		include("../common/PublicFunction.php");
		include("../common/AutoFormClass.php");    
		$a = new AutoFormClass("M","DepositMaintain.php","DepositMaintainAMD.php","collectionsmaster","collectionsmaster",array("CommID", "CollectionsNo"),"保證金收款","center","table90");
		$a->setTableTitle("left","Arial18Bold bg_gray");
		$a->setFieldTitle("center","Arial16 bg_y");
 // $a->setField($fieldName        , $displayName   ,$align,$class   ,$wrap,$isNumeric,$beforeHTML,$afterHTML,$A_Display,$A_CanEdit,$M_Display,$M_CanEdit,$inputType,$defaultValue      )
    $Comm = "".$_SESSION['Community']."";
		$a->setField("CommID"          ,"社區代碼"      ,"left","Arial14","N"  ,"N"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"N"       ,"Hidden"  ,array(array(),""),"$Comm", 'required');
    $a->setField("CollectionsNo"   ,"收款編號"      ,"left","Arial14","N"  ,"N"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"N"       ,"text"    ,array(array(),""),"",'size="10" maxlength="10" required');
    $a->setField("CollectionDate"  ,""              ,"left","Arial14","N"  ,"N"       ,""         ,""        ,"N"       ,"N"       ,"N"       ,"N"       ,"Hidden"  ,array(array(),""),"");    
		$FillterHouseHold = "Select HouseHoldID, HouseHoldID From household Where CommID ='".$_SESSION['Community']."'";
    $a->setField("HouseHoldID"     ,"門牌戶號"      ,"left","Arial14","N"  ,"N"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"Y"       ,"select"  ,array(array(),$FillterHouseHold),"",'size="5" maxlength="5" required');
	  $a->setField("PaymentType"     ,"收款方式"      ,"left","Arial14","N"  ,"N"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"Y"       ,"select"  ,array(array("現金","0","轉帳","1","支票"),""),"0", 'required');
		$FillterIncomeItemID = "Select IncomeItemShow, IncomeItemID From incomeitem Where IncomeItemId='003' and CommID ='".$_SESSION['Community']."'";
    $a->setField("IncomeItemID"    ,"收款用途"      ,"left","Arial14","Y"  ,"N"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"Y"       ,"select"  ,array(array(),$FillterIncomeItemID),"", 'required');
		$a->setField("BillNo"          ,"票據號碼"      ,"left","Arial14","N"  ,"N"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"Y"       ,"text"    ,array(array(),""),"",'size="12" maxlength="12"');
		$a->setField("BillDate"        ,"收票日期"      ,"left","Arial14","N"  ,"N"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"Y"       ,"date"    ,array(array(),""),"",'size="10" maxlength="10"');
	//$a->setField("DueDate"         ,"收款(到期)日期","left","Arial14","Y"  ,"N"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"Y"       ,"select"  ,array(array(),"Select CodeName,CodeID from unifieddatacode where catagoryid='A02'"),"Y");
	//$a->setField("IncomeAmount"    ,"收款金額"      ,"left","Arial14","N"  ,"N"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"Y"       ,"select"  ,array(array(),"Select CodeName,CodeID from unifieddatacode where catagoryid='A01'"),"1");
		$a->setField("DueDate"         ,"收款(到期)日期","left","Arial14","N"  ,"N"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"Y"       ,"date"    ,array(array(),""),"",'size="10" maxlength="10" required');
	  $a->setField("TurnToIncomeDate","保證金轉收入日","left","Arial14","Y"  ,"N"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"N"       ,"date"    ,array(array(),""),"",'size="10" maxlength="10"');
		$a->setField("IncomeAmount"    ,"收款金額"      ,"left","Arial14","N"  ,"Y"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"Y"       ,"text"    ,array(array(),""),"",'size="10" maxlength="10" required');
		$a->setField("CleanDeduction"  ,"清潔費扣款"    ,"left","Arial14","N"  ,"Y"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"Y"       ,"text"    ,array(array(),""),"0",'size="10" maxlength="10"');
    $a->setField("RepairDeduction" ,"修繕費用扣款"  ,"left","Arial14","N"  ,"Y"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"Y"       ,"text"    ,array(array(),""),"0",'size="10" maxlength="10"');
    $a->setField("PunishDeduction" ,"罰款扣款"      ,"left","Arial14","Y"  ,"Y"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"Y"       ,"text"    ,array(array(),""),"0",'size="10" maxlength="10"');
    $a->setField("ReturnDeduction" ,""              ,"left","Arial14","N"  ,"N"       ,""         ,""        ,"N"       ,"N"       ,"N"       ,"N"       ,"Hidden"  ,array(array(),""),"");
    $a->setField("Refund"          ,"退款共計"      ,"left","Arial14","N"  ,"Y"       ,""         ,"<th align='center' class='Arial16 bg_y'>收款餘額</th> <td align='left' class='Arial14'><input type='text' name='TotalRes' id='TotalRes' size='10' maxlength='10' readonly='true' value='0'></td>"        ,"Y"       ,"Y"       ,"Y"       ,"Y"       ,"text"    ,array(array(),""),"0",'size="10" maxlength="10"');
		$a->setField("Note"            ,"收款說明"      ,"left","Arial14","N"  ,"N"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"Y"       ,"text"    ,array(array(),""),"",'size="15" maxlength="100"');
    $User="".$_SESSION["manageuser"]."";
		$a->setField("ModUser"         ,"修改人員"      ,"left","Arial14","N"  ,"N"       ,""         ,""        ,"Y"       ,"Y"       ,"N"       ,"Y"       ,"Hidden"  ,array(array(),""),"$User", 'required');
		$a->setField("ModDate"         ,"修改日期"      ,"left","Arial14","N"  ,"N"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"Y"       ,"Hidden"  ,array(array(),""),"", 'required');
    $a->setBeforeForm("Y","./DepositMaintainAMDJS.php");  
  	$a->showData();		
	}
?>