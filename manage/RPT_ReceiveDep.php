<?php
include("../common/connectdb.php");	
session_start();


$sql = "select DueDate,collectionsmaster.HouseHoldID,Payer,Owner,Renter,IncomeAmount,collectionsmaster.Note".
	" from collectionsmaster,incomeitem,household ".
	" where ".
	" collectionsmaster.commID='".$_SESSION['Community']."'".
	" and Year(DueDate)=".$_REQUEST['Year'].
	" and Month(DueDate)=".$_REQUEST['Month'].
	" and collectionsmaster.IncomeItemID = '001' ".
	" and collectionsmaster.commID=incomeitem.CommID ".
	" and collectionsmaster.incomeitemID=incomeitem.incomeitemID".
	" and collectionsmaster.commID=household.CommID".
	" and .collectionsmaster.HouseHoldID=household.HouseHoldID".
	" order by 1";
//echo $sql;
$rows = mysql_query($sql) ;

$all_content="";
//$cate=get_contact_cate_all();
$item = '';
while($row = mysql_fetch_array($rows)){
	//以下會產生這些變數： $tel , $email , $name , $gsn , $sn , $birthday , $zip , $county , $city , $addr

	$all_content.="
	<tr>";
	$all_content.="
		<td>".$row["DueDate"]."</td>
		<td>".$row["HouseHoldID"]."</td>";
		if ($row["Payer"] == '1'){
			$all_content.="<td>".$row["Owner"]."</td>";
			}else{$all_content.="<td>".$row["Renter"]."</td>";}
		$all_content.="
		<td>".$row["IncomeAmount"]."</td>
		<td>".$row["Note"]."</td>
	</tr>
	";
	//$item = $row["IncomeItemName"];

}

$html="<h1>管理費收款明細表</h1>
<table border=\"1\" cellpadding=\"4\">
	<tr bgcolor=\"#e6e6fa\" align=\"center\">
		<th width=\"100\">入帳日期</th>
		<th width=\"60\">客戶編號</th>
		<th width=\"80\">客戶名稱</th>
		<th width=\"100\">金額</th>
		<th width=\"200\">備註</th>
	</tr>
	$all_content
</table>";

if ($_REQUEST['FORMAT']=="PDF"){
	
require_once('../tcpdf/config/zho.php');
require_once('../tcpdf/config/tcpdf_config.php');
require_once('../tcpdf/tcpdf.php');
//客製頁首頁尾
// Extend the TCPDF class to create custom Header and Footer
class MYPDF extends TCPDF {

	//Page header
	/*
	public function Header() {
		// Logo
		$image_file = K_PATH_IMAGES.'logo_example.jpg';
		$this->Image($image_file, 10, 10, 15, '', 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);
		// Set font
		$this->SetFont('helvetica', 'B', 20);
		// Title
		$this->Cell(0, 15, '<< TCPDF Example 003 >>', 0, false, 'C', 0, '', 0, false, 'M', 'M');
	}
*/
	// Page footer
	public function Footer() {
		// Position at 15 mm from bottom
		$this->SetY(-15);
		// Set font
		$this->SetFont('droidsansfallback', 'I', 16);
		//$this->setFooterMargin(80);
		// Page number
		$this->Cell(0, 10, '主任委員：　 　     監察委員:               財務委員：         　  現場主任: ', 0, false, 'C', 0, '', 0, false, 'T', 'M');
	}
}

//實體化PDF物件
$pdf = new MYPDF("L", PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
//$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->setPrintHeader(false); //不要頁首
$pdf->setPrintFooter(true); //不要頁尾

// set margins
//$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
//$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);  //設定自動分頁

$pdf->setLanguageArray($l); //設定語言相關字串

$pdf->setFontSubsetting(true); //產生字型子集（有用到的字才放到文件中）

$pdf->SetFont('droidsansfallback', '', 12, '', true); //設定字型

$pdf->AddPage(); //新增頁面

$pdf->setTextShadow(array('enabled'=>false, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));//文字陰影
	
	
	$pdf->writeHTML($html);
	$pdf->Output('contact.pdf', 'I');	
}

if ($_REQUEST['FORMAT']=="HTML"){
	echo '<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="zh-tw" lang="zh-tw">';
	echo '<head>';
	echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
	echo '</head>';
	echo $html;
}


if ($_REQUEST['FORMAT']=="EXCEL"){

require_once '../Excel/PHPExcel.php';    //引入 PHPExcel 物件庫
require_once '../Excel/PHPExcel/IOFactory.php';    //引入 PHPExcel_IOFactory 物件庫
$objPHPExcel = new PHPExcel();  //實體化Excel
//----------內容-----------//

$objPHPExcel->setActiveSheetIndex(0);  //設定預設顯示的工作表
$objActSheet = $objPHPExcel->getActiveSheet(); //指定預設工作表為 $objActSheet
$objActSheet->setTitle("管理費收款明細表");  //設定標題
$objPHPExcel->createSheet(); //建立新的工作表，上面那三行再來一次，編號要改

$objActSheet->getColumnDimension('A')->setWidth(20);
$objActSheet->getColumnDimension('B')->setWidth(20);
$objActSheet->getColumnDimension('C')->setWidth(20);
$objActSheet->getColumnDimension('D')->setWidth(20);
$objActSheet->getColumnDimension('E')->setWidth(20);


//$objPHPExcel->getDefaultStyle()->getFont()->setName('標楷體');
//$objPHPExcel->getDefaultStyle()->getFont()->setSize(16);


$objActSheet-> getStyle('A1:E1')-> getFont()-> setName('SimHei')-> setSize('14');
$objActSheet-> getStyle('A1:E1')-> getFill()-> setFillType(PHPExcel_Style_Fill:: FILL_SOLID)-> getStartColor()-> setARGB('FFC9E3F3');


$objActSheet->setCellValue("A1", '入帳日期')
            ->setCellValue("B1", '客戶編號')
            ->setCellValue("C1", '客戶名稱')
            ->setCellValue("D1", '金額')
            ->setCellValue("E1", '備註');


//$sql = "select * from `".$xoopsDB->prefix("contact")."` ";
//$result = $xoopsDB->query($sql) or redirect_header($_SERVER['PHP_SELF'],3, mysql_error());
$rows = mysql_query($sql) ;
$i=2;
while($row = mysql_fetch_array($rows)){
	
  $objActSheet->setCellValue("A{$i}", $row["DueDate"])
              ->setCellValue("B{$i}", $row["HouseHoldID"]);
              //->setCellValueExplicit("C{$i}", $tel , PHPExcel_Cell_DataType:: TYPE_STRING)
        if ($row["Payer"] == '1'){
        	$objActSheet->setCellValue("C{$i}", $row["Owner"]);
			}else{
			$objActSheet->setCellValue("C{$i}", $row["Renter"]);}
   $objActSheet->setCellValue("D{$i}", $row["IncomeAmount"])
              ->setCellValue("E{$i}", $row["Note"]);
  $i++;
}


$objActSheet->mergeCells("A{$i}:C{$i}")->setCellValue("A{$i}", '本期其他收入共計');
$n=$i-1;
$objActSheet->setCellValue("D{$i}", "=SUM(D2:D{$n})");

//$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
//$objPHPExcel->getActiveSheet()->protectCells("E{$i}", '12345');

header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename='.iconv('UTF-8','Big5','收入明細').'.xls');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->setPreCalculateFormulas(false);
$objWriter->save('php://output');
exit;
}
?>