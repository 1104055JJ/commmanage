<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
	// 未登入則轉至首頁(登入頁)
	if (!isset($_SESSION)) { session_start(); }
	if (!(isset($_SESSION["manageuser"]))) {
		echo '<meta http-equiv=REFRESH CONTENT=1;url=index.php>';		
	}  else {
?>
		<script type="text/javascript" src="../js/MasterJSB.js"></script>
		<!--<script language="JavaScript">refreshDataMAMD('testAMD.php','A','');</script>-->
<?php
		

		include("../common/connectdb.php");
		include("../common/AutoDataClassB.php");
		include("../common/AutoPageSwitchClass.php");	

		// 建立及初使化頁數切換
		$a = new AutoPageSwitchClass("M","PA_Refund.php","SELECT COUNT(*) FROM refund_master where IncomeItemID='003' and CommID=".'"'.$_SESSION['Community']. '"',array(10,20,30,40));			
		$Society = "CommID='".$_SESSION['Community']."'"; 
		// 建立及初始化表格
		$b = new AutoDataClass("M","PA_Refund.php","PA_RefundAMD.php",array("CommID","PaymentNo","IncomeItemID","HouseHoldID"),"PA_RefundDetail.php","退保證金作業","center","table90");
		$b->setTableTitle("left","Arial18Bold bg_gray");
		$b->setFieldTitle("center","Arial16 bg_y");	
		$b->setOddRow("","bg_light_blue");
		//$b->setEvenRow("","bg_y");
		// 配合頁數切換,設定表格資料來源
		$b->beginRowsNum = $a->beginRowsNum;
		$b->setQuery("CommID,PaymentNo,PayDate,IncomeItemID,HouseHoldID,Amount,Acceptance,Note","refund_master","IncomeItemID='003' and ".$Society,"",$a->beginRowsNum.",".$a->perPageRows);
		// 設定表格欄位顯示
		$b->setAllFieldAlign("center");
		$b->setAllFieldClass("Arial14");
                $b->setFieldDisplayName("CommID","社區代號");
		$b->setFieldDisplayName("PaymentNo","付款單號");
                $b->setFieldDisplayName("PayDate","付款日期");
                $b->setFieldDisplayName("HouseHoldID","戶號");
		//$b->setFieldDisplayName("SupplierID","供應商");
		$b->setFieldDisplayName("Amount","總金額");
                $b->setFieldDisplayName("Acceptance","驗收人員");
                $b->setFieldDisplayName("Note","備註");
                $b->setFieldDisplayName("Acceptance","驗收人員");
                $b->setFieldDisplay("CommID", "N");
                $b->setFieldDisplay("IncomeItemID", "N");
                
		// 增加按鈕及觸發事件
		//$b->addButton("開窗","onclick=\"openwindow('http://www.prince.com.tw');\"");
		// 設定表格底部顯示頁數切換
		$b->setTableBottom($a->getShowData());
		// 顯示表格資料
		$b->showData();
	}
?>