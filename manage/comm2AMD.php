﻿<?php
	// 未登入則轉至首頁(登入頁)
	session_start();
	if (!(isset($_SESSION["manageuser"]))) {
		echo '<meta http-equiv=REFRESH CONTENT=1;url=index.php>';		
	} else {
		include("../common/connectdb.php");
		include("../common/AutoFormClass.php");

		$a = new AutoFormClass("M","Comm2.php","Comm2AMD.php","community","community",array("CommID"),"社區基本資料","center","table90");
		$a->setTableTitle("left","Arial18Bold bg_gray");
		$a->setFieldTitle("center","Arial16 bg_y");
		$a->setButtonDefaultClass("Arial16Bold");
       // $a->setField($fieldName, $displayName, $align, $class, $wrap, $isNumeric, $beforeHTML, $afterHTML, $A_Display, $A_CanEdit, $M_Display, $M_CanEdit, $inputType, $defaultValue)
		$a->setField("CommID","社區編號","left","Arial14","N","N","","","Y","Y","Y","N","text",array(array(),""),"");
		$a->setField("CommName","社區名稱","left","Arial14","Y","N","","","Y","Y","Y","Y","text",array(array(),""),"","minlength='2'");
		$a->setField("CompanyID","公司","left","Arial14","N","N","","","Y","Y","Y","Y","select",array(array(),"Select CompanyName,CompanyID From Company Where Enable = 'Y'"),"PS1");
		$a->setField("ZoneID","地區","left","Arial14","Y","N","","","Y","Y","Y","Y","select",array(array(),"Select ZoneName,ZoneID From Zone Where Enable = 'Y'"),"02");
		//$a->setField("CompanyID","公司","left","Arial14","N","N","","","Y","Y","Y","Y","text",array(array(),""),"");
		//$a->setField("ZoneID","地區","left","Arial14","Y","N","","","Y","Y","Y","N","text",array(array(),""),"");
		$a->setField("TEL","電話","left","Arial14","N","N","","","Y","Y","Y","Y","text",array(array(),""),"","type='url'");
		$a->setField("FAX","傳真","left","Arial14","Y","N","","","Y","Y","Y","Y","text",array(array(),""),"","sise=50");
		$a->setField("Address","地址","left","Arial14","Y","N","","","Y","Y","Y","Y","text",array(array(),""),"");
		$a->setField("Note","備註","left","Arial14","Y","N","","","Y","Y","Y","Y","text",array(array(),""),"");
		$a->setField("CashAllowZero","零用金允許為零","left","Arial14","Y","N","","","Y","Y","Y","Y","select",array(array(),"Select CodeName,CodeID from unifieddatacode where catagoryid='A02'"),"Y");
		$a->setField("CoverMethod","帳單核銷順序","left","Arial14","N","N","","","Y","Y","Y","Y","select",array(array(),"Select CodeName,CodeID from unifieddatacode where catagoryid='A01'"),"1");
		//$a->setField("CashAllowZero","零用金允許為零","left","Arial14","Y","Y","","","Y","Y","Y","Y","text",array(array(),""),"");
		//$a->setField("CoverMethod","沖銷順序","left","Arial14","N","N","","","Y","Y","Y","Y","text",array(array(),""),"");			
		$a->setField("ModUser","更新者","left","Arial14","Y","N","","","Y","Y","Y","Y","text",array(array(),""),"");
		$a->setField("ModDate","更新時間","left","Arial14","Y","N","","","Y","Y","Y","Y","text",array(array(),""),"");
		$a->addBeforeDeleteCheck("HouseHold",array("CommID","CommID"),"已被戶別基本資料叫用,不可刪除!");
	//	$a->setBeforeForm("N","");
		$a->setAfterForm("Y","./Comm2JS.php");
		$a->showData();
	}
?>