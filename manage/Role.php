<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
	// 未登入則轉至首頁(登入頁)
	session_start();
	if (!(isset($_SESSION["manageuser"]))) {
		echo '<meta http-equiv=REFRESH CONTENT=1;url=index.php>';		
	} else {
?>
		<script type="text/javascript" src="../js/MasterJS.js"></script>
		<script language="JavaScript">refreshDataMAMD('RoleAMD.php','A','');</script>

		<table align="center" class="table90">
			<tr class="bg_gray">
				<th colspan="5">
					<table width="100%">
						<tr>
							<th class="Arial18Bold align_left">角色基本資料維護作業</th>
							<th class="Arial18Bold align_right">
								<input type="Button" style="font-family: 標楷體 ,Arial, Helvetica, sans-serif;" value="新增" onclick="handleMasterItem('RoleAMD.php','A');">
								<input type="Button" style="font-family: 標楷體 ,Arial, Helvetica, sans-serif;" value="修改" onclick="handleMasterItem('RoleAMD.php','M');">
								<input type="Button" style="font-family: 標楷體 ,Arial, Helvetica, sans-serif;" value="刪除" onclick="handleMasterItem('RoleAMD.php','D');">								
							</th>
						</tr>
					</table>
				</th>
			</tr>
			<tr class="bg_y">
				<th colspan="2" width="75" class="Arial16Bold align_center">項次</th>
				<th class="Arial16Bold">角色代碼</th>
				<th class="Arial16Bold">角色名稱</th>
				<th class="Arial16Bold">有效否</th>
			<tr>
<?php
		include("../common/connectdb.php");
		// 計算筆數及頁數
		$m_TotalCount = 0;
		$m_PageCount = 10;
		$m_PrePageCount = 10;
		$m_Page = 1;
		$m_TotalPage = 1;
		$strSQL = "SELECT COUNT(*) FROM user_role";
		$rows = mysql_query($strSQL);
		list($m_TotalCount) = mysql_fetch_row($rows);
		if (isset($_POST['m_PageCount'])) { $m_PageCount = $_POST['m_PageCount']; }
		if (isset($_POST['m_PrePageCount'])) { $m_PrePageCount = $_POST['m_PrePageCount']; } 		 
		if (isset($_POST['m_Page'])) { $m_Page = $_POST['m_Page']; }
		if ($m_TotalCount % $m_PageCount == 0) { $m_TotalPage = $m_TotalCount/$m_PageCount; } else { $m_TotalPage = intval($m_TotalCount/$m_PageCount) + 1; } 
		$m_Page = intval(((($m_Page-1)*$m_PrePageCount)+1) / $m_PageCount) + 1;
		//

		$i = 0;
		$j = 0;
		$k = ($m_Page-1)*$m_PageCount;
		$strSQL = "SELECT * FROM user_role ORDER BY RoleID LIMIT ".($m_Page-1)*$m_PageCount.','.$m_PageCount;
		$rows = mysql_query($strSQL);
		while ($row = mysql_fetch_array($rows)) {
			$i++;
			$j++;
			$k++;
			if ($i % 2 == 1) {
				echo '<tr id="m_tr_'.$j.'" class="bg_light_blue" onclick="showSelectMasterItem(\'RoleAMD.php\','.$j.',\''.$row['RoleID'].'\');refreshDataD(\'RoleD.php\',\''.$row['RoleID'].'\',10,10,1);">';
			} else {
				echo '<tr id="m_tr_'.$j.'" class="bg_light_gray" onclick="showSelectMasterItem(\'RoleAMD.php\','.$j.',\''.$row['RoleID'].'\');refreshDataD(\'RoleD.php\',\''.$row['RoleID'].'\',10,10,1);">';
			}
			echo '<td width="30" class="Arial14 align_center" id="m_item_'.$j.'"></td>';
			echo '<td width="45" class="Arial14 align_center">'.$k.'</td>';															
			echo '<td align="center" class="Arial14">'.$row['RoleID'].'</td>';
			echo '<td class="Arial14">'.$row['RoleName'].'</td>';
			echo '<td align="center" class="Arial14">'.$row['Valid'].'</td>';
			echo '</tr>';
		}
		
		// 顯示筆數及頁數
		echo '<tr><td colspan="5"><form action="" name="MasterFormPage" id="MasterFormPage" method="POST">
		<br>每頁顯示';
		echo '<select name="m_PageCount" id="m_PageCount" style="font-family: 標楷體 ,Arial, Helvetica, sans-serif;" onchange="refreshDataMPage(\'Role.php\',\'\');">';
		$m_PageCountArray = array("5","5","10","10","15","15","20","20");
		for ($i = 0; $i < sizeof($m_PageCountArray); $i = $i + 2) {
			$selected = "";
			if ($m_PageCount == $m_PageCountArray[$i+1]) {
				$selected = "selected";
			}
			echo '<option value="'.$m_PageCountArray[$i+1].'" '.$selected.'>'.$m_PageCountArray[$i].'</option>';			
		}
		echo '</select>';
		echo '共'.$m_TotalCount.'筆    ';							
		echo '<input type="Button" style="font-family: 標楷體 ,Arial, Helvetica, sans-serif;" value="<<" '.($m_Page==1?"disabled":"").' onclick="refreshDataMPage(\'Role.php\',\'f\');">';
		echo '<input type="Button" style="font-family: 標楷體 ,Arial, Helvetica, sans-serif;" value="<" '.($m_Page==1?"disabled":"").' onclick="refreshDataMPage(\'Role.php\',\'p\');">';
		echo '<input type="Button" style="font-family: 標楷體 ,Arial, Helvetica, sans-serif;" value=">" '.($m_Page==$m_TotalPage?"disabled":"").' onclick="refreshDataMPage(\'Role.php\',\'n\');">';
		echo '<input type="Button" style="font-family: 標楷體 ,Arial, Helvetica, sans-serif;" value=">>" '.($m_Page==$m_TotalPage?"disabled":"").' onclick="refreshDataMPage(\'Role.php\',\'l\');">';
		echo '    跳至';
		echo '<select name="m_Page" id="m_Page" style="font-family: 標楷體 ,Arial, Helvetica, sans-serif;" onchange="refreshDataMPage(\'Role.php\',\'\');">';
		for ($i = 1; $i <= $m_TotalPage; $i++) {
			$selected = "";
			if ($m_Page == $i) {
				$selected = "selected";
			}
			echo '<option value="'.$i.'" '.$selected.'>'.$i.'</option>';			
		}
		echo '</select>';
		echo '共'.$m_TotalPage.'頁';
		echo '<input id="m_PrePageCount" name="m_PrePageCount" value="'.$m_PageCount.'" type="HIDDEN">';
		echo '<input id="m_TotalPage" name="m_TotalPage" value="'.$m_TotalPage.'" type="HIDDEN">';		
		echo '</form></td></tr>';
		//
		
		echo '</table>';
		echo '<input id="m_id" name="m_id" value="" type="HIDDEN">';
		echo '<input id="m_tr_count" name="m_tr_count" value="'.$j.'" type="HIDDEN">';		
	}
?>
