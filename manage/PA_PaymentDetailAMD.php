<?php
	// 未登入則轉至首頁(登入頁)
	session_start();
	if (!(isset($_SESSION["manageuser"]))) {
		echo '<meta http-equiv=REFRESH CONTENT=1;url=index.php>';		
	} else {
		include("../common/connectdb.php");
		include("../common/AutoFormClass.php");

		$a = new AutoFormClass("D","PA_PaymentDetail.php","PA_PaymentDetailAMD.php","paymentdetail","v_payment_detail",array("CommID","PaymentNo","PaymentDetailNo","SupplierID"),"付款單身基本資料","center","table90");
		$a->setTableTitle("left","Arial18Bold bg_gray");
		$a->setFieldTitle("center","Arial16 bg_y");
		$a->setField("CommID","社區","left","Arial14","N","N","","","Y","N","Y","N","label",array(array(),""),$_SESSION['Community']);
		$a->setField("PaymentNo","付款單號","left","Arial14","Y","Y","","","Y","N","Y","N","label",array(array(),""),0);
		$a->setField("PaymentDetailNo","項次","left","Arial14","N","Y","","","Y","Y","Y","N","hidden",array(array(),""),0);
                $a->setField("SupplierID","廠商","left","Arial14","Y","N","","","Y","N","Y","N","hidden",array(array(),""),0);
                $a->setField("PaymentRequestNo","請款單號","left","Arial14","N","Y","","","Y","Y","Y","N","love",array(array("CommID","out","SupplierID","out","PA03","code"),""),0);
                $a->setField("PaymentRequestDetailNo","請款單項次","left","Arial14","N","Y","","","Y","N","Y","N","hidden",array(array(),""),0);
                $a->setField("PaymentItemID","支出項目","left","Arial14","Y","N","","","Y","N","Y","N","text",array(array(),""),"");
		$a->setField("AmountPayable","付款金額","left","Arial14","Y","Y","","","Y","Y","Y","Y","text",array(array(),""),0,"");
                $a->setField("TotalAmount","總金額","left","Arial14","N","Y","","","Y","N","Y","N","text",array(array(),""),0,"");
		$a->setField("Unpaid","未付金額","left","Arial14","Y","Y","","","Y","N","Y","N","text",array(array(),""),0);
                $a->setField("Paid","已付金額","left","Arial14","N","Y","","","N","N","Y","N","text",array(array(),""),0);
                $a->setField("Note","備註","left","Arial14","Y","N","","","Y","Y","Y","Y","text",array(array(),""),0);
                $a->setField("ModUser","修改人員","left","Arial14","N","N","","","N","N","Y","N","text",array(array(),""),$_SESSION['manageuser']);
		$a->setField("ModDate","修改時間","left","Arial14","N","N","","","N","N","Y","N","date",array(array(),""),"","",array("datetime","Y","N"));
		$a->setBeforeForm("Y","./PA_PaymentJS.php");
		//$a->addBeforeDeleteCheck("community",array("ZoneID","ZoneID"),"已被社區基本資料叫用,不可刪除!");
                $a->setAfterWriteToDataBase('PA_update_Request', array('CommID','PaymentNo','PaymentRequestNo','PaymentRequestDetailNo','ModUser'),array("AW","MW","DW"));
		$a->showData();
	}
