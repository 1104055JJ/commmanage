<?php
	// 未登入則轉至首頁(登入頁)
	if (!isset($_SESSION)) { session_start(); }
	if (!(isset($_SESSION["manageuser"]))) {
		echo '<meta http-equiv=REFRESH CONTENT=1;url=index.php>';		
	} else {
		include("../common/connectdb.php");
		include("../common/AutoFormClass.php");
		
		$a = new AutoFormClass("M","Zone.php","ZoneAMD.php","zone","zone",array("ZoneID"),"區域基本資料維護作業(異動)","center","");
		$a->setTableTitle("left","font-18-bold bg_gray");
		$a->setFieldTitle("center","font-16-bold bg_y");
		$a->setButtonDefaultClass("btn btn-warning");
		$a->setField("ZoneID","區域代碼","left","font-14","N","N","","","Y","Y","Y","N","text",array(array(),""),"","");
		$a->setField("ZoneName","區域名稱","left","font-14","N","N","","","Y","Y","Y","Y","text",array(array(),""),"","");
		$a->setField("ZoneSort","排序","left","font-14","N","Y","","","Y","Y","Y","Y","combobox",array(array(),"SELECT DISTINCT ZoneSort,ZoneSort FROM zone ORDER BY ZoneSort"),"");
		$a->setField("Enable","啟用否","left","font-14","N","N","","","Y","Y","Y","Y","radio",array(array("啟用","Y","停用","N"),""),"Y");			
		$a->addBeforeDeleteCheck("community",array("ZoneID","ZoneID"),"已被【社區基本資料維護作業】調用,不可刪除!");
		$a->showData();
		
		if (isset($_POST["f"]) && ($_POST["f"] == "A" || $_POST["f"] == "M")) {
			// 欄位及規則檢查
?>
			<script language="JavaScript">
				var form = "MasterForm";
				var fieldArray = [];
				fieldArray.push(["ZoneID","Need","此欄位為必填"]);
				fieldArray.push(["ZoneName","Need","此欄位為必填"]);
				fieldArray.push(["ZoneSort","Num","此欄位為整數"]);
				checkRule(form,fieldArray);
								
				function otherCheckRule() {
					return true;
				}
			</script>
<?php
		}
	}
?>