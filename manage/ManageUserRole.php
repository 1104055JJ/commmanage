<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
	// 未登入則轉至首頁(登入頁)
	if (!isset($_SESSION)) { session_start(); }
	if (!(isset($_SESSION["manageuser"]))) {
		echo '<meta http-equiv=REFRESH CONTENT=1;url=index.php>';		
	} else {
?>
		<script type="text/javascript" src="../js/MasterJSB.js"></script>
<?php
		include("../common/connectdb.php");
		include("../common/AutoDataClassB.php");
		include("../common/AutoPageSwitchClass.php");	

		// 建立及初使化頁數切換
		$a = new AutoPageSwitchClass("M","ManageUserRole.php","SELECT COUNT(*) FROM manage_user",array(10,15,20,25));			
		
		// 建立及初始化表格
		$b = new AutoDataClass("M","ManageUserRole.php","ManageUserRoleAMD.php",array("UserID"),"","使用者社區角色權限維護作業(列表)","center","");
		$b->setTableTitle("left","font-18-bold bg_gray");
		$b->setFieldTitle("center","font-16-bold bg_y");
		$b->setDefaultFieldAlign("left");
		$b->setDefaultFieldClass("font-14");
		$b->setButtonDefaultClass("btn");		
		// 配合頁數切換,設定表格資料來源
		$b->beginRowsNum = $a->beginRowsNum;
		$b->setQuery("UserID,UserName,Enable","manage_user","","",$a->beginRowsNum.",".$a->perPageRows);
		// 設定表格欄位顯示
		$b->setField("UserID","帳號");
		$b->setField("UserName","名稱");
		$b->setField("Enable","啟用","center");		
		// 增加按鈕及觸發事件
		$b->setFAMDButtonDisplay("N","N","Y","N","Y");
		// 設定表格底部顯示頁數切換
		$b->setTableBottom($a->getShowData());
		// 顯示表格資料
		$b->showData();
	}
?>