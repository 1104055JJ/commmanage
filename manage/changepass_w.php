<div style="text-align:center">
<form id=MasterForm>
<p>變更密碼</p>
<p>請輸入原密碼及欲變更密碼兩次</p>
<br>
原密碼：<input type='password' id='orgpwd' />
<br><br>
新密碼：<input type='password' id='newpwd1' />
<br><br>
新密碼：<input type='password' id='newpwd2' />
<br><br>
<input type='button' id='resetpw' value ='變更' />
<br><br>
<div id="msg"></div>
</form>
</div>

<script type="text/javascript">
document.getElementById("newpwd1").onchange = function(){
	chkpwd();	
}
document.getElementById("newpwd2").onchange = function(){
	chkpwd();
}
function chkpwd(){
	if (document.getElementById("newpwd1").value != document.getElementById("newpwd2").value){
		$("#msg").empty().append("新密碼前後不一致");
		document.getElementById("newpwd1").style.border="2px red dotted";
		document.getElementById("newpwd2").style.border="2px red dotted";
		//alert("新密碼前後不一致");
		
	}else{
		$("#msg").empty().append("");
		document.getElementById("newpwd1").style.border="";
		document.getElementById("newpwd2").style.border="";
	}
}

function changepass(){
			$("msg").empty().append('');
			if (document.getElementById("newpwd1").value=='' || document.getElementById("newpwd2").value==''){
				$("#msg").empty().append("新密碼不得為空白");
				document.getElementById("newpwd1").style.border="2px red dotted";
				document.getElementById("newpwd2").style.border="2px red dotted";
			}else{
  			var oldpwd = document.getElementById("orgpwd").value;
  			var newpwd1 = document.getElementById("newpwd1").value;
  			var newpwd2 = document.getElementById("newpwd2").value;
			//var commid = e.options[e.selectedIndex].value;
			$.ajax({
				url: "changepass.php",
				type:"POST",
				dataType: "text",
				data:{oldpwd:oldpwd,newpwd1:newpwd1,newpwd2:newpwd2},
				//成功執行並返回值
				success: function(data){
					$("#msg").empty().append(data);  
					document.getElementById("MasterForm").reset();	
				},
				//發送請求之前會執行的函式
				beforeSend:function(){
				},
				//請求完成時執行的函式(不論結果是success或error)
				complete:function(){
					
				},
				error:function(xhr, ajaxOptions, thrownError){ 
					alert(xhr.status);
					alert(thrownError);
				}
			});
			}
}
 
$( "#resetpw" ).click(function() {changepass();});
</script>
