<?php
	// 未登入則轉至首頁(登入頁)
	if (!isset($_SESSION)) { session_start(); }
	if (!(isset($_SESSION["manageuser"]))) {
		echo '<meta http-equiv=REFRESH CONTENT=1;url=index.php>';		
	} else {
		include_once("../common/connectdb.php");
		include_once("../common/PublicFunction.php");		
		include_once("../common/AutoFormClass.php");
		
		$a = new AutoFormClass("M","ManageRole.php","ManageRoleAMD.php","manage_role","manage_role",array("RoleID"),"角色代碼維護作業(異動)","center","");
		$a->setTableTitle("left","font-18-bold bg_gray");
		$a->setFieldTitle("center","font-16-bold bg_y");
		$a->setButtonDefaultClass("btn btn-warning");
		
		$a->setField("RoleID","角色代碼","left","font-14","N","N","","","Y","Y","Y","N","text",array(array(),""),"","");
		$a->setField("RoleName","角色名稱","left","font-14","N","N","","","Y","Y","Y","Y","text",array(array(),""),"","");
		$a->setField("Enable","有效","left","font-14","Y","N","","","Y","Y","Y","Y","radio",array(array("啟用","Y","停用","N"),""),"Y","");

		$a->addBeforeDeleteCheck("manage_user",array("RoleID","RoleID"),"資料已被【管理者基本資料維護作業】調用,不可刪除 !");
		$a->addBeforeDeleteCheck("manage_user_role",array("RoleID","RoleID"),"資料已被【社區角色權限維護作業】調用,不可刪除 !");
		$a->addBeforeDeleteCheck("manage_roleprogram",array("RoleID","RoleID"),"資料已被【系統程式權限維護作業】調用,不可刪除 !");
		$a->addBeforeDeleteCheck("manage_role_zone",array("RoleID","RoleID"),"資料已被【社區程式權限維護作業-依區域】調用,不可刪除 !");
		$a->addBeforeDeleteCheck("manage_role_comm_program",array("RoleID","RoleID"),"資料已被【社區程式權限為護作業-依社區】調用,不可刪除 !");
		$a->showData();
		
		if (isset($_POST["f"]) && ($_POST["f"] == "A" || $_POST["f"] == "M")) {
			// 欄位及規則檢查
?>
			<script language="JavaScript">
				var form = "MasterForm";
				var fieldArray = [];
				fieldArray.push(["RoleID","Need","此欄位為必填"]);
				fieldArray.push(["RoleName","Need","此欄位為必填"]);
				checkRule(form,fieldArray);
								
				function otherCheckRule() {
					return true;
				}
			</script>
<?php
		}		
	}
?>