<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
	// 未登入則轉至首頁(登入頁)
	if (!isset($_SESSION)) { session_start(); }
	if (!(isset($_SESSION["manageuser"]))) {
		echo '<meta http-equiv=REFRESH CONTENT=1;url=index.php>';		
	} else {
?>
		<script type="text/javascript" src="../js/DetailJSB.js"></script>
		<script language="JavaScript">
			function openwindow(l_url) {
				var w = window.open(l_url,"w",config="width=500,height=500");
			}
		</script>
		
<?php
		include_once("../common/connectdb.php");
		include_once("../common/AutoDataClassB.php");
		include_once("../common/AutoPageSwitchClass.php");	

		// 建立及初使化頁數切換
		$a = new AutoPageSwitchClass("D","testD.php","SELECT COUNT(*) FROM zonedetail",array(5,10,15,20));			
		
		// 建立及初始化表格
		$b = new AutoDataClass("D","testD.php","testDAMD.php",array("ZoneID","item"),"","區域基本資料單身(列表)","center","table90");
		$b->setTableTitle("left","Arial18Bold bg_gray");
		$b->setFieldTitle("center","Arial16Bold bg_y");	
		$b->setOddRow("","bg_light_blue");
		//$b->setEvenRow("","bg_y");
		// 配合頁數切換,設定表格資料來源
		$b->beginRowsNum = $a->beginRowsNum;
		$b->setQuery("*","zonedetail","","",$a->beginRowsNum.",".$a->perPageRows);
		// 設定表格欄位顯示
		$b->setAllFieldAlign("center");
		$b->setAllFieldClass("Arial14");
		$b->setFieldDisplayName("ZoneID","區域代碼");
		$b->setFieldDisplayName("item","項目");
		$b->setFieldDisplayName("vvv","備註");
		// 增加按鈕及觸發事件
		$b->setButtonDefaultClass("Arial14Bold");		
		$b->addButton("開窗","onclick=\"openwindow('http://www.prince.com.tw');\"");
		// 設定表格底部顯示頁數切換
		$b->setTableBottom($a->getShowData());
		// 顯示表格資料
		$b->showData();
	}
?>