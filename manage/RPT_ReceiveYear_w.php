<?php
		
		include("../common/connectdb.php");
		include("../common/PublicFunction.php");	
		session_start();
?>
<div style="text-align:center">
<p>年度管理費繳交總表<p>
<p>請選擇所要查詢的年度/月份</p>
	<select id="s_year" name="s_year" class="Arial14">
		<option value ="2015">2015年</option>
		<option value ="2016">2016年</option>
	</select>
	<br><br>
	<button id="b_html" >HTML報表</button>
	<button id="b_pdf" >pdf報表</button>
	<button id="b_csv" >excel報表</button>
	<input id="l_comm" name="l_comm" value=<?php echo $_SESSION['Community'] ?> type="HIDDEN">
	<div id = "msg"></div>
</div>
<script type="text/javascript">
	

$( "#b_pdf" ).click(function() {
	call_rpt('PDF');
});

$( "#b_html" ).click(function() {
	callrpt('HTML');
});

$( "#b_csv" ).click(function() {
	call_rpt('EXCEL');
});

function call_rpt(format){
	var commID = document.getElementById("l_comm").value;
   	var e = document.getElementById("s_year");
	var year = e.options[e.selectedIndex].value;
    window.open("RPT_ReceiveYear.php?CommID="+commID+"&Year="+year+"&FORMAT="+format,"w",config="width=1000,height=800");
    $( "#dialogWindow" ).dialog( "close" );
}
function callrpt(format){
	var commID = document.getElementById("l_comm").value;
   	var e = document.getElementById("s_year");
	var year = e.options[e.selectedIndex].value;
			$.ajax({
				url: "RPT_ReceiveYear.php",
				type:"POST",
				dataType: "text",
				data:{commID:commID,Year:year,FORMAT:format},
				//成功執行並返回值
				success: function(data){
					$("#msg").empty().append(data); 
					$( "#dialogWindow" ).dialog( "close" );
				},
				//發送請求之前會執行的函式
				beforeSend:function(){
				},
				//請求完成時執行的函式(不論結果是success或error)
				complete:function(){
				},
				error:function(xhr, ajaxOptions, thrownError){ 
					alert(xhr.status);
					alert(thrownError);
				}
			});
}
</script>
	

	