<html>
<body>
    

        <?php
        require_once("../reportico/reportico.php");         // Include Reportico
        $q = new reportico();                         // Create instance
        $q->access_mode = "FULL";                     // Allows access to all Reportico pages
        $q->initial_execute_mode = "ADMIN";           // Starts user in administration page
        $q->initial_project = "admin";                // Required for access to admin mode
        $q->bootstrap_styles = "3";                   // Set to "3" for bootstrap v3, "2" for V2 or false for no bootstrap
        $q->force_reportico_mini_maintains = true;    // Often required
        $q->bootstrap_preloaded = true;               // true if you dont need Reportico to load its own bootstrap
        $q->clear_reportico_session = true;           // Normally required
        $q->execute();                                // Run Reportico
        ?>

</body>
</html>