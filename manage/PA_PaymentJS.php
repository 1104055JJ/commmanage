<script type="text/javascript">
/* 
 檢查付款程序金額
 */
//自動計算複價

document.forms["DetailForm"].onsubmit = function() {
  
 
    var CommID=document.forms["MasterForm"].elements["CommID"].value;
  
    var PaymentNo= document.forms["DetailForm"].elements["PaymentNo"].value;
    var RDetailNo= document.forms["DetailForm"].elements["PaymentRequestDetailNo"].value;//請款單項次
     var PDetailNo= document.forms["DetailForm"].elements["PaymentDetailNo"].value;//付款單項次
     var AmountPayable= document.forms["DetailForm"].elements["AmountPayable"].value;
     var Status= document.forms["DetailForm"].elements["d_f"].value;//狀態
   
     var bool=checkPayment("PA_CheckPayment.php",CommID,PaymentNo,RDetailNo,PDetailNo,AmountPayable,Status,"payment");
     return bool;
  
    
};

function checkPayment(l_url,CommID,PaymentNo,RDetailNo,PDetailNo,AmountPayable,Status,table) {
        $bool=true;
      
	$.ajax({
        async:false,  
        type: "GET",
        url: l_url,
        data: {"CommID":CommID,"PaymentNo":PaymentNo,"RDetailNo":RDetailNo,"PDetailNo":PDetailNo,"AmountPayable":AmountPayable,"Status":Status,"table":table},
        dataType: "text",
        success: function(msg){
               msg=$.trim(msg);
             
             //  $bool=false;  
            if(msg==="existed")//重復選擇
            {      alert("單身有重複請款單。"); 
                    $bool=false; }
            else{
                var data=msg.split(",");
               
                    if(data[0]==="false")
                    {   
                    alert("付款金額超過請款金額。付款金額為"+data[2]+"元,請款金額為"+data[1]+"元。"); 
                    $bool=false;
            
                    }
                }
           },

        error:function(xhr, ajaxOptions, thrownError){ 
                    alert(xhr.status); 
                    alert(thrownError); 
                 }
   
    });

  return $bool;

 }



</script>