<?php
	// 未登入則轉至首頁(登入頁)
	session_start();
	if (!(isset($_SESSION["manageuser"]))) {
		echo '<meta http-equiv=REFRESH CONTENT=1;url=index.php>';		
	} else {
		include("../common/connectdb.php");
		include("../common/AutoFormClass.php");

		$a = new AutoFormClass("D","PA_Agency_ReceiptDetail.php","PA_Agency_ReceiptDetailAMD.php","refund_detail","refund_detail",array("CommID","PaymentNo","DetailNo","SupplierID"),"付代收款款單身資料","center","table90");
		$a->setTableTitle("left","Arial18Bold bg_gray");
		$a->setFieldTitle("center","Arial16 bg_y");
		$a->setField("CommID","社區","left","Arial14","N","N","","","Y","N","Y","N","text",array(array(),""),$_SESSION['Community']);
		$a->setField("PaymentNo","付款單號","left","Arial14","Y","Y","","","Y","N","Y","N","text",array(array(),""),0);
		$a->setField("DetailNo","項次","left","Arial14","N","Y","","","Y","Y","Y","N","hidden",array(array(),""),0);
                $a->setField("SupplierID","供應商","left","Arial14","Y","N","","","Y","N","Y","N","text",array(array(),""),0);
                $x="select distinct  HouseHoldID,HouseHoldID from collectionsmaster where  incomeitemID='004' and  CommID='".$_SESSION['Community']."'";
                $a->setField("HouseHoldID","戶號","left","Arial14","Y","N","","","Y","Y","Y","Y","select",array(array(),$x),"");
                $a->setField("IncomeItemID","收款類別","left","Arial14","Y","N","","","Y","N","Y","N","hidden",array(array(),""),"004");
                $a->setField("CollectionsNo","收款單號","left","Arial14","N","Y","","","Y","Y","Y","N","love",array(array("CommID","out","HouseHoldID","out","PA05","code"),""),0);
                $a->setField("Amount","代收款金額","left","Arial14","Y","Y","","","Y","N","Y","N","text",array(array(),""),0,"");
		$a->setField("Unpaid","未付金額","left","Arial14","N","Y","","","Y","N","Y","N","text",array(array(),""),0,"");
                $a->setField("Paid","付款金額","left","Arial14","N","Y","","","Y","Y","Y","Y","text",array(array(),""),0);
                $a->setField("Refund","回饋金","left","Arial14","Y","Y","","","Y","Y","Y","Y","text",array(array(),""),0);
                $a->setField("CleanDeduction","X","left","Arial14","Y","Y","","","Y","Y","Y","Y","hidden",array(array(),""),0);
                $a->setField("RepairDeduction","X","left","Arial14","Y","Y","","","Y","Y","Y","Y","hidden",array(array(),""),0);
                $a->setField("SupplierID","SupplierID","left","Arial14","N","N","","","N","N","N","N","hidden",array(array(),""),"");
                $a->setField("PunishDeduction","X","left","Arial14","Y","Y","","","Y","Y","Y","Y","hidden",array(array(),""),0);
                $a->setField("Note","備註","left","Arial14","Y","N","","","Y","Y","Y","Y","text",array(array(),""),0);
                $a->setField("ModUser","修改人員","left","Arial14","N","N","","","N","N","Y","N","label",array(array(),""),$_SESSION['manageuser']);
		$a->setField("ModDate","修改時間","left","Arial14","N","N","","","N","N","Y","N","date",array(array(),""),"","",array("datetime","Y","Y"));
		$a->setBeforeForm("Y","./PA_Agency_ReceiptJS.php");
                $a->setAfterWriteToDataBase('PA_Update_Collection',array('CommID','PaymentNo','CollectionsNo'),array("AW","MW","DW"));
		//$a->addBeforeDeleteCheck("community",array("ZoneID","ZoneID"),"已被社區基本資料叫用,不可刪除!");
		$a->showData();
	}
