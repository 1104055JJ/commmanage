<?php
include("../common/connectdb.php");	
session_start();
$CommID=$_SESSION['Community'];
$year = $_REQUEST['Year'];
$month = $_REQUEST['Month'];
if ($month == 1 ) {
	$lastmonth = 12;
	$lastyear= $year -1;
} else{
	$lastmonth = $month - 1;
	$lastyear= $year;
}
//標題
$sql = "Select CommName From community Where CommID ='".$CommID."'";
$rows=mysql_query($sql);
$row=mysql_fetch_array($rows);
$CommName=$row['CommName'];
$Title = $CommName."社區 ".$year."年 ".$month."月 收支明細表";

//產生資料
if ($_REQUEST['FORMAT']=="HTML" || $_REQUEST['FORMAT']=="PDF"){
//表頭
$html= "
<table border=\"1\" cellpadding=\"4\" align=\"center\">
	<tr bgcolor=\"#e6e6fa\" align=\"center\">
		<th width=\"120\">銀行帳戶明細</th>
		<th width=\"100\">上期結轉</th>
		<th width=\"100\">本期存入</th>
		<th width=\"100\">本期支出</th>
		<th width=\"100\">本期結餘</th>
	</tr>";

//上期帳戶餘額

$sql ="select AccountID,AccountName,Balance+BalanceMod from accountbalance".
	  " where CommID = '".$_SESSION['Community']."' and Year=".$lastyear." and Month=".$lastmonth;

$rows = mysql_query($sql) ;
if (mysql_num_rows($rows) < 1 ) {
	echo "請先產生前月銀行帳戶資料";
	exit;
}
while($row = mysql_fetch_array($rows)){
	if ($row["AccountID"]=="1"){
		$acc1_name = $row["AccountName"];
		$acc1_bal = $row["Balance+BalanceMod"];
	}
	if ($row["AccountID"]=="2"){
		$acc2_name = $row["AccountName"];
		$acc2_bal = $row["Balance+BalanceMod"];
	}
	if ($row["AccountID"]=="3"){
		$acc3_name = $row["AccountName"];
		$acc3_bal = $row["Balance+BalanceMod"];
	}
}

//計算本期存入
	$sql = "select sum(incomeamount) from collectionsmaster where commid='".$CommID."' and Year(duedate)=".$year." and Month(duedate)=".$month." ";
	$rows=mysql_query($sql);
	$row=mysql_fetch_array($rows);
	if ($row['sum(incomeamount)'] == null ){
		$acc1_income = 0;
	}else{
		$acc1_income = $row['sum(incomeamount)'];
	}
	
	
//計算本期支出
	$sql = "select sum(amount) from paymentmaster where commID='".$CommID."' and Year(paydate)=".$year." and Month(paydate)=".$month;
	$rows=mysql_query($sql);
	$row=mysql_fetch_array($rows);
	if ($row['sum(amount)'] == null ){
		$acc1_payment = 0;
	}else{
		$acc1_payment = $row['sum(amount)'];
	}
	
//計算當月撥補零用金
	$sql = "select sum(Amount) from pettycash where commid='".$CommID."' and Year(Belongdate)=".$year." and Month(Belongdate)=".$month;
	$rows=mysql_query($sql);
	$row=mysql_fetch_array($rows);
	if ($row['sum(Amount)'] != null ){
		$acc1_payment = $acc1_payment + $row['sum(Amount)'];
	}

//計算退保證金、代付款
	$sql = "select sum(amount) from refund_master where commID='".$CommID."' and Year(paydate)=".$year." and Month(paydate)=".$month;
	$rows=mysql_query($sql);
	$row=mysql_fetch_array($rows);
	if ($row['sum(amount)'] != null ){
		$acc1_payment = $acc1_payment + $row['sum(amount)'];
	}

//本期結餘
	$acc1_sum = $acc1_bal + $acc1_income - $acc1_payment;

//零用金存入
	$sql = "select sum(Amount) from pettycash where commid='".$CommID."' and Year(Belongdate)=".$year." and Month(Belongdate)=".$month;
	$rows=mysql_query($sql);
	$row=mysql_fetch_array($rows);
	if ($row['sum(Amount)'] == null ){
		$acc3_income = 0;
	}else{
		$acc3_income = $row['sum(Amount)'];
	}
	

	//計算當月支出零用金
	$sql = "select sum(TotalAmount) from paymentrequestmaster where commID='".$CommID."' and Year(requestdate)=".$year." and Month(requestdate)=".$month." and Petty='Y'";
	$rows=mysql_query($sql);
	$row=mysql_fetch_array($rows);
	if ($row['sum(TotalAmount)'] == null ){
		$acc3_payment = 0;
	}else{
		$acc3_payment = $row['sum(TotalAmount)'];
	}
	//定存結餘
	$acc2_sum = $acc2_bal;

	//零用金結餘
	$acc3_sum = $acc3_bal + $acc3_income - $acc3_payment;

	//合計
	$sum_bal = $acc1_bal + $acc2_bal +$acc3_bal;
	$sum_income = $acc1_income + $acc3_income;
	$sum_payment = $acc1_payment + $acc3_payment;
	$sum_total = $acc1_sum + $acc2_sum + $acc3_sum;
	
	//組html
	$html .="
	<tr align=\"right\">
		<td>".$acc1_name."</td>  
		<td>".number_format($acc1_bal, 0, '.', ',')."</td>
		<td>".number_format($acc1_income, 0, '.', ',')."</td>
		<td>".number_format($acc1_payment, 0, '.', ',')."</td>
		<td>".number_format($acc1_sum, 0, '.', ',')."</td>
	</tr>
	<tr align=\"right\">
		<td>".$acc2_name."</td>
		<td>".number_format($acc2_bal, 0, '.', ',')."</td>
		<td></td>
		<td></td>
		<td>".number_format($acc2_sum, 0, '.', ',')."</td>
	</tr>
	<tr align=\"right\">
		<td>".$acc3_name."</td>
		<td>".number_format($acc3_bal, 0, '.', ',')."</td>
		<td>".number_format($acc3_income, 0, '.', ',')."</td>
		<td>".number_format($acc3_payment, 0, '.', ',')."</td>
		<td>".number_format($acc3_sum, 0, '.', ',')."</td>
	</tr>
	<tr align=\"right\">
		<td>合計</td>
		<td>".number_format($sum_bal, 0, '.', ',')."</td>
		<td>".number_format($sum_income, 0, '.', ',')."</td>
		<td>".number_format($sum_payment, 0, '.', ',')."</td>
		<td>".number_format($sum_total, 0, '.', ',')."</td>
	</tr>
	";

$html.="
	<tr bgcolor=\"#e6e6fa\" align=\"center\">
		<th colspan=3 width=\"320\">本期存入明細</th>
		<th colspan=2 width=\"200\">金額</th>
	</tr>";

//前期管理費收入
	$YearMonth = ($year-2000)*12+$month;
	$sql = "select sum(WriteOffAmount) from v_ar_collections where commID='".$CommID."' and Year(DueDate)=".$year." and Month(DueDate)=".$month." and Year*12+Month <".$YearMonth;
	//echo $sql;
	$rows=mysql_query($sql);
	$row=mysql_fetch_array($rows);
	if ($row['sum(WriteOffAmount)'] == null ){
		$income_last = 0;
	}else{
		$income_last = $row['sum(WriteOffAmount)'];
	}
//當期管理費收入
	$sql = "select sum(WriteOffAmount) from v_ar_collections where commID='".$CommID."' and Year(DueDate)=".$year." and Month(DueDate)=".$month." and Year*12+Month =".$YearMonth;
	//echo $sql;
	$rows=mysql_query($sql);
	$row=mysql_fetch_array($rows);
	if ($row['sum(WriteOffAmount)'] == null ){
		$income_curr = 0;
	}else{
		$income_curr = $row['sum(WriteOffAmount)'];
	}
//預收管理費收入($income_remain)
	$sql = "select sum(RemainAmount) from collectionsmaster where commid='".$CommID."' and Year(duedate)=".$year." and Month(duedate)=".$month." and RemainAmount >0 and incomeitemid = '001'";
	$rows=mysql_query($sql);
	//echo $sql;
	$row=mysql_fetch_array($rows);
	if ($row['sum(RemainAmount)'] == null ){
		$income_remain = 0;
	}else{
		$income_remain = $row['sum(RemainAmount)'];
	}
	//加回後期已沖銷管理費
	//找當月收款且後面月份沖銷金額
	$sql = "select sum(WriteOffAmount) from v_ar_collections where commID='".$CommID."' and Year(DueDate)=".$year." and Month(DueDate)=".$month." and Year*12+Month >".$YearMonth;
	//echo $sql;
	$rows=mysql_query($sql);
	$row=mysql_fetch_array($rows);
	if ($row['sum(WriteOffAmount)'] <> null ){
		$income_remain = $income_remain + $row['sum(WriteOffAmount)'];
	}
	
//利息及其他收入
	//$sql = "select incomeamount from collectionsmaster where commid='".$CommID."' and Year(duedate)=".$year." and Month(duedate)=".$month." and incomeitemid > '002'";
	$sql = "select collectionsmaster.incomeitemid,incomeitemName,sum(incomeamount) from collectionsmaster,incomeitem ".
		   " where collectionsmaster.commid = incomeitem.commid and collectionsmaster.incomeitemid = incomeitem.incomeitemid ".
	       " and collectionsmaster.commid='".$CommID."' and Year(duedate)=".$year." and Month(duedate)=".$month." and collectionsmaster.incomeitemid > '002' ".
		   " group by 1 ";
	$rows=mysql_query($sql);
	//$row=mysql_fetch_array($rows);
	$other_sum = 0;
	$other_income_html ="";
	while ($row = mysql_fetch_array($rows)) {
		$other_income_html.="
		<tr align=\"right\">
			<td colspan=3 width=\"320\">".$row['incomeitemName']."</td>
			<td colspan=2 width=\"200\">".number_format($row['sum(incomeamount)'], 0, '.', ',')."</td>
		</tr>";
		$other_sum = $other_sum + $row['sum(incomeamount)'];
	}
	//echo $other_sum;
	
	$income_total = $income_last + $income_curr + $income_remain + $other_sum;
	
//收入合計
$html.="
	<tr align=\"right\">
		<td colspan=3 width=\"320\">前期管理費收入</td>
		<td colspan=2 width=\"200\">".number_format($income_last, 0, '.', ',')."</td>
	</tr>
	<tr align=\"right\">
		<td colspan=3 width=\"320\">當期管理費收入</td>
		<td colspan=2 width=\"200\">".number_format($income_curr, 0, '.', ',')."</td>
	</tr>
	<tr align=\"right\">
		<td colspan=3 width=\"320\">預收管理費收入</td>
		<td colspan=2 width=\"200\">".number_format($income_remain, 0, '.', ',')."</td>
	</tr>";
//其他收入
$html.=$other_income_html;
$html.="
	<tr align=\"right\">
		<td colspan=3 width=\"320\">合計</td>
		<td colspan=2 width=\"200\">".number_format($income_total, 0, '.', ',')."</td>
	</tr>
	";

//支出明細

$html.="
	<tr bgcolor=\"#e6e6fa\" align=\"center\">
		<th colspan=3 width=\"320\">本期支出明細</th>
		<th colspan=2 width=\"200\">金額</th>
	</tr>";

//本期支出
	$sql = "select Note,Amount from paymentmaster where commID='".$CommID."' and Year(paydate)=".$year." and Month(paydate)=".$month;
	$rows=mysql_query($sql);
	//echo $sql;
	$payment_sum = 0;
	$payment_html ="";
	while ($row = mysql_fetch_array($rows)) {
		$payment_html.="
		<tr align=\"right\">
			<td colspan=3 width=\"320\">".$row['Note']."</td>
			<td colspan=2 width=\"200\">".number_format($row['Amount'], 0, '.', ',')."</td>
		</tr>";
		$payment_sum = $payment_sum + $row['Amount'];
	}
	
	//計算當月撥補零用金
	$sql = "select sum(Amount) from pettycash where commid='".$CommID."' and Year(Belongdate)=".$year." and Month(Belongdate)=".$month;
	$rows=mysql_query($sql);
	$row=mysql_fetch_array($rows);
	if ($row['sum(Amount)'] != null ){
		$payment_html.="
		<tr align=\"right\">
			<td colspan=3 width=\"320\">"."零用金撥補"."</td>
			<td colspan=2 width=\"200\">".number_format($row['sum(Amount)'], 0, '.', ',')."</td>
		</tr>";
		$payment_sum = $payment_sum + $row['sum(Amount)'];
	}
//計算退保證金、代付款
	$sql = "select distinct IncomeItemID,IncomeItemName,sum(Paid),sum(Refund) from v_refund where CommID='".$CommID."' and Year(paydate)=".$year." and Month(paydate)=".$month.
			" group by 1 order by 1 ";
	//echo $sql;
	$rows=mysql_query($sql);
	//$row=mysql_fetch_array($rows);
	while ($row = mysql_fetch_array($rows)) {
		
		if ($row['IncomeItemID']=='003'){
		$payment_html.="
		<tr align=\"right\">
			<td colspan=3 width=\"320\">".$row['IncomeItemName']."</td>
			<td colspan=2 width=\"200\">".number_format($row['sum(Paid)'], 0, '.', ',')."</td>
		</tr>";
		$payment_sum = $payment_sum + $row['sum(Paid)'];
		}
		if ($row['IncomeItemID']=='004'){
		$payment_html.="
		<tr align=\"right\">
			<td colspan=3 width=\"320\">".$row['IncomeItemName']."</td>
			<td colspan=2 width=\"200\">".number_format($row['sum(Paid)'], 0, '.', ',')."</td>
		</tr>";
		$payment_sum = $payment_sum + $row['sum(Paid)'];
		}
	}	
	$html.=$payment_html;
	$html.="
	<tr align=\"right\">
		<td colspan=3 width=\"320\">合計</td>
		<td colspan=2 width=\"200\">".number_format($payment_sum, 0, '.', ',')."</td>
	</tr>
	";
$html.="</table>";
//====================應收未收管理費統計==============
//
//計算至當月底所有欠繳管理費
	//$YearMonth = ($year-2000)*12+$month;
	$sql = "select sum(unreceived) from ar where commID='".$CommID."' and Year*12+Month <=".$YearMonth." and unreceived > 0 ";
	//echo $sql;
	$rows=mysql_query($sql);
	$row=mysql_fetch_array($rows);
	if ($row['sum(unreceived)'] == null ){
		$ar_allunreceived = 0;
	}else{
		$ar_allunreceived = $row['sum(unreceived)'];
	}
	//加回後面收款沖銷的管理費
	$sql = "select sum(WriteOffAmount) from v_ar_collections where commID='".$CommID."' and (Year(DueDate)-2000)*12+Month(DueDate)>".$YearMonth." and Year*12+Month <=".$YearMonth;
	//echo $sql;
	$rows=mysql_query($sql);
	$row=mysql_fetch_array($rows);
	if ($row['sum(WriteOffAmount)'] <> null ){
		$ar_allunreceived = $ar_allunreceived + $row['sum(WriteOffAmount)'];
	}


//加：當月應收管理費收入
	//$YearMonth = ($year-2000)*12+$month;
	$sql = "select sum(TotalAmount) from ar where commID='".$CommID."' and Year*12+Month =".$YearMonth;
	//echo $sql;
	$rows=mysql_query($sql);
	$row=mysql_fetch_array($rows);
	if ($row['sum(TotalAmount)'] == null ){
		$ar_total = 0;
	}else{
		$ar_total = $row['sum(TotalAmount)'];
	}


//減：前期預收款沖轉本期管理費
	$sql = "select sum(WriteOffAmount) from v_ar_collections where commID='".$CommID."' and (Year(DueDate)-2000)*12+Month(DueDate)<".$YearMonth." and Year*12+Month =".$YearMonth;
	//echo $sql;
	$rows=mysql_query($sql);
	$row=mysql_fetch_array($rows);
	if ($row['sum(WriteOffAmount)'] == null ){
		$prereceiveWriteOff = 0;
	}else{
		$prereceiveWriteOff = $row['sum(WriteOffAmount)'];
	}

//截至當月底止止應收未收管理費收入 


	$ar_lastunreceived = $ar_allunreceived - $ar_total + $income_curr + $income_last + $prereceiveWriteOff;
	
//HTML	
	$html.="<table border=\"1\" cellpadding=\"4\" align=\"center\">
	<tr bgcolor=\"#e6e6fa\" align=\"center\">
		<th colspan=5 width=\"520\">應收未收管理費統計</th>
	</tr>";
	
	$html.="
		<tr align=\"right\">
			<td colspan=3 width=\"320\" align=\"left\">"."截至".$year."年".$lastmonth."月底欠繳管理費"."</td>
			<td colspan=2 width=\"200\">".number_format($ar_lastunreceived, 0, '.', ',')."</td>
		</tr>";
	$html.="
		<tr align=\"right\">
			<td colspan=3 width=\"320\" align=\"left\">"."加：".$year."年".$month."月應收管理費收入"."</td>
			<td colspan=2 width=\"200\">".number_format($ar_total, 0, '.', ',')."</td>
		</tr>";
//減：本期管理費收入 
	$html.="
		<tr align=\"right\">
			<td colspan=3 width=\"320\" align=\"left\">"."減：".$year."年".$month."月管理費收入"."</td>
			<td colspan=2 width=\"200\">".number_format($income_curr, 0, '.', ',')."</td>
		</tr>";
//減：前期欠繳管理費收入
	$html.="
		<tr align=\"right\">
			<td colspan=3 width=\"320\" align=\"left\">"."減：前期欠繳管理費收入"."</td>
			<td colspan=2 width=\"200\">".number_format($income_last, 0, '.', ',')."</td>
		</tr>";
	$html.="
		<tr align=\"right\">
			<td colspan=3 width=\"320\" align=\"left\">"."減：前期預收款沖轉本期管理費"."</td>
			<td colspan=2 width=\"200\">".number_format($prereceiveWriteOff, 0, '.', ',')."</td>
		</tr>";
	$html.="
		<tr align=\"right\">
			<td colspan=3 width=\"320\" align=\"left\">"."截至".$year."年".$month."月底止應收未收管理費"."</td>
			<td colspan=2 width=\"200\">".number_format($ar_allunreceived, 0, '.', ',')."</td>
		</tr>";
$html.="</table>";
//===============預收管理費明細===================

//截至上月底預收管理費收入 
	$sql = "select sum(incomeamount) from collectionsmaster where commid='".$CommID."' and (Year(DueDate)-2000)*12+Month(DueDate)<".$YearMonth." ";
	$rows=mysql_query($sql);
	$row=mysql_fetch_array($rows);
	if ($row['sum(incomeamount)'] == null ){
		$pre_last = 0;
	}else{
		$pre_last = $row['sum(incomeamount)'];
	}
	
//減：前期預收沖轉本期管理費($prereceiveWriteOff) 

//加：本月預收管理費($income_remain)

//截至本月底止預收管理費收入
	$pre_income = $pre_last - $prereceiveWriteOff + $income_remain;

	$html.="<table border=\"1\" cellpadding=\"4\" align=\"center\">
	<thead>
		<tr bgcolor=\"#e6e6fa\" align=\"center\">
			<th colspan=5 width=\"520\">預收管理費統計</th>
		</tr>
	</thead>";
	$html.="
		<tr align=\"right\">
			<td colspan=3 width=\"320\" align=\"left\">"."截至上月底預收管理費收入"."</td>
			<td colspan=2 width=\"200\">".number_format($pre_last, 0, '.', ',')."</td>
		</tr>";
	$html.="
		<tr align=\"right\">
			<td colspan=3 width=\"320\" align=\"left\">"."減：前期預收沖轉本期管理費"."</td>
			<td colspan=2 width=\"200\">".number_format($prereceiveWriteOff, 0, '.', ',')."</td>
		</tr>";
	$html.="
		<tr align=\"right\">
			<td colspan=3 width=\"320\" align=\"left\">"."加：本月預收管理費"."</td>
			<td colspan=2 width=\"200\">".number_format($income_remain, 0, '.', ',')."</td>
		</tr>";
	$html.="
		<tr align=\"right\">
			<td colspan=3 width=\"320\" align=\"left\">"."截至本月底預收管理費收入"."</td>
			<td colspan=2 width=\"200\">".number_format($pre_income, 0, '.', ',')."</td>
		</tr>";
	$html.="</table>";
//===============預收管理費明細===================


}

if ($_REQUEST['FORMAT']=="HTML"){
	echo '<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="zh-tw" lang="zh-tw">';
	echo '<head>';
	echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
	echo '</head>';
	echo '<h3 align="center">'.$Title.'</h3>';
	echo $html;
}

if ($_REQUEST['FORMAT']=="PDF"){
	
require_once('../tcpdf/config/zho.php');
require_once('../tcpdf/config/tcpdf_config.php');
require_once('../tcpdf/tcpdf.php');
//客製頁首頁尾
// Extend the TCPDF class to create custom Header and Footer
class MYPDF extends TCPDF {
	var $HeaderTitle;
	//Page header
	public function Header() {
		// Logo
		//$image_file = K_PATH_IMAGES.'psam_logo.jpg';
		//$this->Image($image_file, 10, 10, 25, 0, 'jpg', '', 'T', false, 300, '', false, false, 0, false, false, false);
		//$image_file = K_PATH_IMAGES.'logo_example.jpg';
		//$this->Image($image_file, 10, 10, 15, '', 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);
		// Set font
		$this->SetFont('droidsansfallback', 'B', 18);
		//$this->setHeaderMargin(20);
		// Title
		$this->Cell(0,25,$this->HeaderTitle, 0, 0, 'C', 0, '', 0, false, 'B', 'B');
		//$this->SetTopMargin($this->GetY());
		//$this->SetTopMargin(20);
	}

	// Page footer
	public function Footer() {
		// Position at 15 mm from bottom
		$this->SetY(-15);
		// Set font
		$this->SetFont('droidsansfallback', 'I', 14);

		$this->setFooterMargin(25);
		// Page number
		$this->Cell(0, 5, '主任委員：               監察委員:               財務委員：               現場主任:           ', 0, false, 'L', 0, '', 0, false, 'T', 'M');
	}
}

//實體化PDF物件
$pdf = new MYPDF("P", PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
//$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->setPrintHeader(true); //頁首
$pdf->setPrintFooter(true); //頁尾
$pdf->HeaderTitle = $Title;
// set margins
//$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
//$pdf->SetHeaderMargin(55);
//$pdf->SetFooterMargin(25);
//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);  //設定自動分頁

$pdf->setLanguageArray($l); //設定語言相關字串

$pdf->setFontSubsetting(true); //產生字型子集（有用到的字才放到文件中）

$pdf->SetFont('droidsansfallback', '', 12, '', true); //設定字型

$pdf->AddPage(); //新增頁面

$pdf->setTextShadow(array('enabled'=>false, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));//文字陰影
	
	
	$pdf->writeHTML($html);
	$pdf->Output('contact.pdf', 'D');	
}


if ($_REQUEST['FORMAT']=="EXCEL"){

require_once '../Excel/PHPExcel.php';    //引入 PHPExcel 物件庫
require_once '../Excel/PHPExcel/IOFactory.php';    //引入 PHPExcel_IOFactory 物件庫
$objPHPExcel = new PHPExcel();  //實體化Excel
//----------內容-----------//

$objPHPExcel->setActiveSheetIndex(0);  //設定預設顯示的工作表
$objActSheet = $objPHPExcel->getActiveSheet(); //指定預設工作表為 $objActSheet
$objActSheet->setTitle("管理費收款明細表");  //設定標題
$objPHPExcel->createSheet(); //建立新的工作表，上面那三行再來一次，編號要改

$objActSheet->getColumnDimension('A')->setWidth(20);
$objActSheet->getColumnDimension('B')->setWidth(20);
$objActSheet->getColumnDimension('C')->setWidth(20);
$objActSheet->getColumnDimension('D')->setWidth(20);
$objActSheet->getColumnDimension('E')->setWidth(20);


//$objPHPExcel->getDefaultStyle()->getFont()->setName('標楷體');
//$objPHPExcel->getDefaultStyle()->getFont()->setSize(16);


$objActSheet-> getStyle('A1:E1')-> getFont()-> setName('SimHei')-> setSize('12');
$objActSheet-> getStyle('A1:E1')-> getFill()-> setFillType(PHPExcel_Style_Fill:: FILL_SOLID)-> getStartColor()-> setARGB('FFC9E3F3');


$objActSheet->setCellValue("A1", '入帳日期')
            ->setCellValue("B1", '客戶編號')
            ->setCellValue("C1", '客戶名稱')
            ->setCellValue("D1", '金額')
            ->setCellValue("E1", '備註');


//$sql = "select * from `".$xoopsDB->prefix("contact")."` ";
//$result = $xoopsDB->query($sql) or redirect_header($_SERVER['PHP_SELF'],3, mysql_error());
$rows = mysql_query($sql) ;
$i=2;
while($row = mysql_fetch_array($rows)){
	
  $objActSheet->setCellValue("A{$i}", $row["DueDate"])
              ->setCellValue("B{$i}", $row["HouseHoldID"]);
              //->setCellValueExplicit("C{$i}", $tel , PHPExcel_Cell_DataType:: TYPE_STRING)
        if ($row["Payer"] == '1'){
        	$objActSheet->setCellValue("C{$i}", $row["Owner"]);
			}else{
			$objActSheet->setCellValue("C{$i}", $row["Renter"]);}
   $objActSheet->setCellValue("D{$i}", $row["IncomeAmount"])
              ->setCellValue("E{$i}", $row["Note"]);
  $i++;
}


$objActSheet->mergeCells("A{$i}:C{$i}")->setCellValue("A{$i}", '本期其他收入共計');
$n=$i-1;
$objActSheet->setCellValue("D{$i}", "=SUM(D2:D{$n})");

//$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
//$objPHPExcel->getActiveSheet()->protectCells("E{$i}", '12345');

header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename='.iconv('UTF-8','Big5','收入明細').'.xls');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->setPreCalculateFormulas(false);
$objWriter->save('php://output');
exit;
}
?>