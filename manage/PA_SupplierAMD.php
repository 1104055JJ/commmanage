<?php
	// 未登入則轉至首頁(登入頁)

	if (!isset($_SESSION)) { session_start(); }
	if (!(isset($_SESSION["manageuser"]))) {
		echo '<meta http-equiv=REFRESH CONTENT=1;url=index.php>';		
	} else {
		include("../common/connectdb.php");
		include("../common/AutoFormClass.php");
               
                
                //$Button='<Button type="Button" class="fa fa-search-plus" onclick="OpenWindow(\''.$_SESSION['Community'].'\')"></Button><em id=SupplierName> </em>';
                
		$a = new AutoFormClass("M","PA_Supplier.php","PA_SupplierAMD.php","supplier","supplier",array("CommID","SupplierID"),"供應商維護作業","center","table90");
		$a->setTableTitle("left","Arial18Bold bg_gray");
		$a->setFieldTitle("center","Arial16 bg_y");
                $a->setField("ID","AUTO","left","Arial14","Y","Y","","","N","N","N","N","hidden",array(array(),""),"0");
                $a->setField("CommID","社區","left","Arial14","Y","N","","","Y","N","Y","N","label",array(array(),""),$_SESSION['Community']);
		 //自動取號 from 6201
                $Comm = "".$_SESSION['Community']."";
                $strSQL = "SELECT CAST(Max(SupplierID) as UNSIGNED INTEGER) FROM supplier Where CommID ='".$Comm."' AND SupplierID <> 9999 " ;
                $rows = mysql_query($strSQL);
                $row = mysql_fetch_array($rows);
                $serial = $row[0] + 1;
                 if (mysql_num_rows($rows) > 0 and $row[0]>=1001) 
                 {$serial = $row[0] + 1;} 
                 else
                 {$serial= 1001;} 
                
                $a->setField("SupplierID","供應商代碼","left","Arial14","Y","Y","","","Y","N","Y","N","label",array(array(),""),"$serial","maxlength=10");
	        $a->setField("SupplierName","供應商名稱","left","Arial14","N","N","","","Y","Y","Y","Y","text",array(array(),""),"");
                $a->setField("Type","公司/個人","left","Arial14","Y","N","","","Y","Y","Y","Y","radio",array(array("公司","1","個人","2"),""),"");
                $a->setField("IdNo","統編/身分證","left","Arial14","Y","N","","","Y","Y","Y","Y","text",array(array(),""),"","onchange=\"Checktype()\"");
                $a->setField("Boss","公司負責人","left","Arial14","Y","N","","","Y","Y","Y","Y","text",array(array(),""),"","size=15 ");
                $a->setField("Tel","電話","left","Arial14","Y","N","","","Y","Y","Y","Y","text",array(array(),""),"");
                $a->setField("CellPhone","手機","left","Arial14","Y","N","","","Y","Y","Y","Y","text",array(array(),""),"");
                $a->setField("Address","住址","left","Arial14","Y","N","","","Y","Y","Y","Y","text",array(array(),""),"","size=60");
                $a->setField("Enable","啟用","left","Arial14","Y","N","","","Y","Y","Y","Y","radio",array(array("是","Y","否","N"),""),'Y');
                $a->setField("Note","備註","left","Arial14","Y","N","","","Y","Y","Y","Y","text",array(array(),""),"","size=60");
                $a->setField("ModUser","修改人員","left","Arial14","N","N","","","N","N","Y","N","label",array(array(),""),$_SESSION['manageuser'],"size=20");
		$a->setField("ModDate","修改時間","left","Arial14","N","N","","","N","N","Y","N","date",array(array(),""),"","",array("datetime","Y","N"));
                
               // $a->setField($fieldName, $displayName, $align, $class, $wrap, $isNumeric, $beforeHTML, $afterHTML, $A_Display, $A_CanEdit, $M_Display, $M_CanEdit, $inputType);
		//$a->addBeforeDeleteCheck("community",array("ZoneID","ZoneID"),"已被社區基本資料叫用,不可刪除!");
		$a->setBeforeForm("Y","./PA_SupplierJS.php");
                
                $a->showData();
		if (isset($_POST["f"]) && ($_POST["f"] == "A" || $_POST["f"] == "M")) {
			// 欄位及規則檢查
?>
			<script language="JavaScript">
				var form = "MasterForm";
				var fieldArray = [];
				fieldArray.push(["IdNo","TaxId","統一編號或身分證號錯誤"]);
				fieldArray.push(["SupplierName","Need","此欄位為必填"]);				
				checkRule(form,fieldArray);				
				function otherCheckRule() {
					return true;
				}			
			</script>
<?php
		}
	}
?>