<?php
	// 未登入則轉至首頁(登入頁)
	session_start();
	if (!(isset($_SESSION["manageuser"]))) {
		echo '<meta http-equiv=REFRESH CONTENT=1;url=index.php>';		
	} else {
		include("../common/connectdb.php");
		include("../common/AutoFormClass.php");
		
		$a = new AutoFormClass("M","HouseHold.php","HouseHoldAMD.php","HouseHold","HouseHold",array("CommID","BuildingID","HouseHoldID"),"門牌戶號基本資料","center","table90");
		$a->setTableTitle("left","Arial18Bold bg_gray");
		$a->setFieldTitle("center","Arial16 bg_y");
	  //$a->setField($fieldName   ,$displayName		  ,$align,$class   ,$wrap,$isNumeric,$beforeHTML,$afterHTML,$A_Display,$A_CanEdit,$M_Display,$M_CanEdit,$inputType,$defaultValue)
	  //$a->setField("UnitPrice","單價","left","Arial14","N","Y","","","Y","Y","Y","Y","text",array(array(),""),0,"onchange=\"CheckNum('Float','DetailForm','UnitPrice','需為數字')\"");
		$a->setField("CommID"          ,"社區代碼"     ,"left","Arial14","N","N","","","Y","Y","Y","N","select",array(array(),"Select CommName,CommID From community"),"9BA0001");
		$a->setField("BuildingID"      ,"棟別代碼"     ,"left","Arial14","N","N","","","Y","Y","Y","Y","text",array(array(),""),"");
		$a->setField("HouseHoldID"     ,"戶別代碼"     ,"left","Arial14","Y","N","","","Y","Y","Y","Y","text",array(array(),""),"");
		$a->setField("Address1"        ,"地址-號"      ,"left","Arial14","N","N","","","Y","Y","Y","Y","text",array(array(),""),"","onchange=\"CheckNum('Need','MasterForm','Address1','此欄位為必填')\"");
		$a->setField("Address2"        ,"地址-樓"      ,"left","Arial14","N","N","","","Y","Y","Y","Y","text",array(array(),""),"","onchange=\"CheckNum('Need','MasterForm','Address2','此欄位為必填')\"");
		$a->setField("Address3"        ,"地址-之"      ,"left","Arial14","Y","N","","","Y","Y","Y","Y","text",array(array(),""),"","onchange=\"CheckNum('Need','MasterForm','Address3','此欄位為必填')\"");
		$a->setField("BankCoverCode"   ,"銀行銷帳碼"   ,"left","Arial14","N","N","","","Y","Y","Y","Y","text",array(array(),""),"");
		$a->setField("CStoreCoverCode" ,"超商銷帳碼"   ,"left","Arial14","N","N","","","Y","Y","Y","Y","text",array(array(),""),"");
		$a->setField("Email"		   ,"電郵"		  ,"left","Arial14","Y","N","","","Y","Y","Y","Y","text",array(array(),""),"");
		$a->setField("Payer"	       ,"繳款人"		  ,"left","Arial14","N","N","","","Y","Y","Y","Y","text",array(array(),""),"","onchange=\"CheckNum('Need','MasterForm','Payer','此欄位為必填')\"");
		$a->setField("Owner"		   ,"所有權人"	  ,"left","Arial14","N","N","","","Y","Y","Y","Y","text",array(array(),""),"","onchange=\"CheckNum('Need','MasterForm','Owner','此欄位為必填')\"");
		$a->setField("Renter"		   ,"承租人"		  ,"left","Arial14","Y","N","","","Y","Y","Y","Y","text",array(array(),""),"");
		$a->setField("Area"			   ,"坪數"		  ,"left","Arial14","N","N","","","Y","Y","Y","Y","text",array(array(),""),"");
		$a->setField("Fee"		  	   ,"管理費"		  ,"left","Arial14","N","N","","","Y","Y","Y","Y","text",array(array(),""),"");
		$a->setField("Discount"		   ,"折扣"		  ,"left","Arial14","Y","N","","","Y","Y","Y","Y","text",array(array(),""),"0");
		$a->setField("NetFee"		   ,"應繳金額"	  ,"left","Arial14","N","N","","","Y","N","Y","N","text",array(array(),""),"");
		$a->setField("BillCycle"	   ,"繳費週期"     ,"left","Arial14","N","N","","","Y","Y","Y","Y","select",array(array(),"Select CodeName,CodeName from unifieddatacode where CatagoryName='繳費週期'"),"","onchange=\"CheckNum('Need','MasterForm','BillCycle','此欄位為必填')\"");
		$a->setField("FeePerMonths"	   ,"繳費月份"	  ,"left","Arial14","Y","N","","","Y","Y","Y","Y","text",array(array(),""),"","onchange=\"CheckNum('Need','MasterForm','FeePerMonths','此欄位為必填')\"");
		$a->setField("ModUser"		   ,"資料更新人員" ,"left","Arial14","N","N","","","Y","N","Y","N","text",array(array(),""),"");
		$a->setField("ModDate"		   ,"資料更新時間" ,"left","Arial14","N","N","","","Y","N","Y","N","text",array(array(),""),"");
		//$a->setField("Enable","啟用否","left","Arial14","N","N","","","Y","Y","Y","Y","checkbox",array(array("啟用","Y","停用","N"),"SELECT CompanyName,CompanyID FROM company WHERE Enable='Y'"),"Y");			
		//$a->addBeforeDeleteCheck("community",array("ZoneID","ZoneID"),"已被社區基本資料叫用,不可刪除!");
		$a->setbeforeForm("Y","./HouseHoldjs.php");
		$a->showData();
	}
?>