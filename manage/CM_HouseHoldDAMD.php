<?php
	// 未登入則轉至首頁(登入頁)
	session_start();
	if (!(isset($_SESSION["manageuser"]))) {
		echo '<meta http-equiv=REFRESH CONTENT=1;url=index.php>';		
	} else {
		include("../common/connectdb.php");
		include("../common/AutoFormClass.php");

		$a = new AutoFormClass("D","CM_HouseHoldD.php","CM_HouseHoldDAMD.php","carspace","carspace",array("CommID","BuildingID","HouseHoldID","CarID"),"車位基本資料","center","table90");
		$a->setTableTitle("left","Arial18Bold bg_gray");
		$a->setFieldTitle("center","Arial16 bg_y");
		$a->setField("CommID","社區代碼","left","Arial14","N","N","","","Y","N","Y","N","text",array(array(),""),"");
		$a->setField("BuildingID","棟別代碼","left","Arial14","N","N","","","Y","N","Y","N","date",array(array(),""),"");
		$a->setField("HouseHoldID","戶別代碼","left","Arial14","N","N","","","Y","N","Y","N","text",array(array(),""),"");
		$a->setField("CarID","車位代碼","left","Arial14","Y","N","","","Y","Y","Y","N","text",array(array(),""),"","");
		$a->setField("Area","坪數","left","Arial14","N","N","","","Y","Y","Y","Y","text",array(array(),""),"");
		$a->setField("Fee","管理費","left","Arial14","N","N","","","Y","Y","Y","Y","text",array(array(),""),"");
		$a->setField("Discount","折扣","left","Arial14","N","N","","","Y","Y","Y","Y","text",array(array(),""),"0");
		$a->setField("NetFee","應收管理費","left","Arial14","Y","N","","","Y","N","Y","N","text",array(array(),""),"");
		 $Incomeitem_s = "Select IncomeItemName,IncomeItemID From incomeitem Where IncomeitemId <'100' and IncomeItemID <> '003' and IncomeItemID <> '004' and CommID ='".$_SESSION['Community']."'";
		$a->setField("IncomeItemID","收入項目","left","Arial14","Y","N","","","Y","Y","Y","Y","select",array(array(),$Incomeitem_s),"","");
		$a->setField("BillCycle"	   ,"繳費週期"     ,"left","Arial14","N","N","","","N","N","N","N","select",array(array(),"Select CodeName,CodeName from unifieddatacode where CatagoryName='繳費週期'"),"");
		$a->setField("FeePerMonths","繳費月份","left","Arial14","N","N","","","N","N","N","N","text",array(array(),""),"");
		$a->setField("ModUser","資料更新人員","left","Arial14","N","N","","","N","N","N","N","text",array(array(),""),"","",array("loginuser","Y","Y"));
		$a->setField("ModDate","資料更新時間","left","Arial14","N","N","","","N","N","N","N","text",array(array(),""),"","",array("datetime","Y","Y"));
		$a->addBeforeDeleteCheck("ar",array("CommID","CommID","HouseHoldID","HouseHoldID"),"已產生管理費資料,不可刪除!");
		//$a->setbeforeForm("Y","./CM_HouseHoldDjs.php");
		$a->showData();
		
		if (isset($_POST["f"]) && ($_POST["f"] == "A" || $_POST["f"] == "M")) {
			// 欄位及規則檢查
?>
			<script language="JavaScript">
				var form = "DetailForm";
				var fieldArray = [];
				fieldArray.push(["CarID","Need","此欄位為必填"]);
				fieldArray.push(["Fee","",""]);
				fieldArray.push(["Discount","",""]);				
				checkRule(form,fieldArray);
				
				function otherCheckRule() {
						var event = true;
						document.forms["DetailForm"].elements["Fee"].style.border="";
   						document.forms["DetailForm"].elements["Discount"].style.border="";
    					document.forms["DetailForm"].elements["NetFee"].style.border="";
					    var Fee = document.forms["DetailForm"].elements["Fee"]; 
    					var re = /^\d+$/;   
   						if (Fee.value == ""||Fee.value<0||!re.test(Fee.value)) { 
    							Fee.value = "";
    							$('#Fee', document.forms["DetailForm"]).attr("placeholder","請輸入正確數字");
    							document.forms["DetailForm"].elements["Fee"].style.border="2px red dotted";
    							event = false;
  						}

  						 var Discount = document.forms["DetailForm"].elements["Discount"];
  						 if (!re.test(document.forms["DetailForm"].Discount.value)){Discount.value = "";}   
  						 if (Discount.value == ""||Discount.value<0) { 
  						  	Discount.value = "0";
   						 }
   						  
  						 var NetFee = document.forms["DetailForm"].elements["NetFee"];   
  						 if(parseInt(Fee.value)>=parseInt(Discount.value)){
  						  	NetFee.value = parseInt(Fee.value)-parseInt(Discount.value);    	
 						 }
  						 else{
    						NetFee.value ="";
    						$('#NetFee', document.forms["DetailForm"]).attr("placeholder","管理費或折扣金額錯誤");
    						document.forms["DetailForm"].elements["Discount"].style.border="2px red dotted";
    						document.forms["DetailForm"].elements["Fee"].style.border="2px red dotted";
    						document.forms["DetailForm"].elements["NetFee"].style.border="2px red dotted";
    						event = false;    	
  						 }
  						return event;		 
				}			
			</script>
<?php
		}
	}
?>