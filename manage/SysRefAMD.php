<?php
	$err = "";
	// 未登入則轉至首頁(登入頁)
	session_start();
	if (!(isset($_SESSION["manageuser"]))) {
		echo '<meta http-equiv=REFRESH CONTENT=1;url=index.php>';		
	} else {
		include("../common/connectdb.php");
		//判斷要執行之功能 : Q->顯示資料  A->新增  AW->新增寫入  M->修改  MW->修改寫入  D->刪除  DW->刪除寫入  L->列表
		$f = "";
		$id = "";
		
		if (isset($_POST['f'])) {
			if ($_POST['f'] == "Q" || $_POST['f'] == "A" || $_POST['f'] == "M" || $_POST['f'] == "D" || $_POST['f'] == "AW" || $_POST['f'] == "MW" || $_POST['f'] == "DW") {
				$f = $_POST['f'];
			} else {
				$f = "L";
			}
		} else {
			$f = "L";
		}
		
		if ($f == "Q" || $f == "M" || $f == "D") {
			if (!isset($_POST['id'])) {
				$f = "L";
			} else {
				$id = $_POST['id'];
			}
		}
?>

<?php if ($f == "Q" || $f == "A" || $f == "M" || $f == "D") {
	if ($f != "A") {
		$strSQL = "SELECT COUNT(*) FROM SysRef ";
		$rows = mysql_query($strSQL);
		list($i) = mysql_fetch_row($rows);
		if ($i == 0) {
			$err = "資料錯誤或不存在,請檢查後再執行 !";
		} else {
			$strSQL = "SELECT * FROM SysRef ";
			$rows = mysql_query($strSQL);
			$row = mysql_fetch_array($rows);
		}
	}
	$fW = "";
	$fS = "";
	$fSS = "";
	$fV = "";
	if ($f == "M") { $fW = "MW"; $fS = "修改"; $fSS = "儲存"; }
	if ($err != "") {
		echo $err;
	} else {	
?>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<form action="SysRefAMD.php" name="MasterForm" id="MasterForm" method="POST">
		<table id="MasterFormMainTable" align="center" class="table90">
			<tr class="bg_gray">
				<th colspan="6">
					<table width="100%">
						<tr>
							<th class="Arial18Bold align_left">系統參數資料設定<input name="f" id="f" value="<?php echo $fW; ?>" type="HIDDEN"></th>			
							<th class="Arial18Bold align_right">
							<?php
								if ($f != "Q") {

										echo '<input type="Button" name="submit" value="'.$fSS.'" style="background-color: #FF8888;" class="Arial16Bold" onclick="refreshDataMaster(\'SysRef.php\',\'SysRefAMD.php\');">';
									
								}
							?>
							</th>
						</tr>
					</table>
				</th>
			</tr>
			<tr>
				<th class="Arial16Bold_Right">Logo圖片：</th>
				<td class="Arial16"><input name="LogoImage" type="text" id="LogoImage" <?php if ($f == "Q" || $f == "D") { echo "readonly"; } ?> value="<?php if ($f != "A") { echo $row['LogoImage']; } ?>"></td>
				<th class="Arial16Bold_Right">Logo連結網址：</th>
				<td class="Arial16"><input name="LogoLink" type="text" id="LogoLink" size="35" <?php if ($f == "Q" || $f == "D") { echo "readonly"; } ?> value="<?php if ($f != "A") { echo $row['LogoLink']; } ?>"></td>				
				<th class="Arial16Bold_Right">LDAP功能：</th>				
				<td class="Arial16"><input name="IsLDAP" type="checkbox" id="IsLDAP" <?php if ($f == "Q" || $f == "D") { echo "disabled"; } ?> value="Y" <?php if ($f != "A") { echo ($row['IsLDAP']=="Y" ? "checked" : ""); } ?>></td>								
			</tr>
			<tr>	
				<th class="Arial16Bold_Right">網域：</th>
				<td class="Arial16"><input name="Domain" type="text" id="Domain" <?php if ($f == "Q" || $f == "D") { echo "readonly"; } ?> value="<?php if ($f != "A") { echo $row['Domain']; } ?>"></td>				
				<th class="Arial16Bold_Right">LDAP主機位置：</th>
				<td class="Arial16"><input name="Host" type="text" id="Host" size="35" <?php if ($f == "Q" || $f == "D") { echo "readonly"; } ?> value="<?php if ($f != "A") { echo $row['Host']; } ?>"></td>
				<th class="Arial16Bold_Right">DN：</th>
				<td class="Arial16"><input name="DN" type="text" id="DN" size="35" <?php if ($f == "Q" || $f == "D") { echo "readonly"; } ?> value="<?php if ($f != "A") { echo $row['DN']; } ?>"></td>
			</tr>
			<tr>
				<th colspan="6" class="Arial18Bold"><font color="red"><?php if ($err != "") { echo $err; } ?></font></th>
			</tr>
		</table>
	</form>
<?php }} ?>
				
<?php		
		
		// 新增或修改儲存
		if ($f == "MW") {
			$IsLDAP = "N";
			if (isset($_POST['IsLDAP']) && $_POST['IsLDAP'] == "Y") {
				$IsLDAP = "Y";
			}
					
			
					$strSQL = "UPDATE SysRef SET LogoImage='".$_POST['LogoImage']."',LogoLink='".$_POST['LogoLink']."',IsLDAP='".$IsLDAP."',Host='".$_POST['Host']."',Domain='".$_POST['Domain']."',DN='".$_POST['DN']."'";
					$msg="修改成功";
					echo $msg;
					mysql_query($strSQL);					
					
				}
		else { 
		$f = "L";

		}
		// L : 回列表 
		if ($f == "L") {
			echo $err;
		}
	}
?>