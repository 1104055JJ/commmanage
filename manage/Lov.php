<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>智能化社區管理系統-小幫手</title>
		<link type="text/css" href="../css/icms.css" rel="stylesheet">		
		<link type="text/css" href="../css/jquery.datepick.css" rel="stylesheet">
	</head>
	<script type="text/javascript" src="../plugin/jquery/jquery.min.js"></script>
	<script type="text/javascript" src="../plugin/jquery/jquery.plugin.js"></script>	
	<script type="text/javascript" src="../plugin/jquery/jquery.datepick.js"></script>
	<script type="text/javascript" src="../plugin/jquery/jquery.datepick-zh-TW.js"></script>
	<script type="text/javascript" src="../js/ManageHomeJS.js"></script>
	<body leftmargin=0 topmargin=0>
		<input id="jsonID" name="jsonID" value="" type="HIDDEN">
		<input id="formID" name="formID" value="" type="HIDDEN">
		<div align="left">
			<table width="100%" border=0 cellpadding=0 cellspacing=0>
				<tr align="left" valign="top">
					<!--  CenterZone  -->
					<td width="100%">
						<table width="100%" border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td id="tr_CenterZoneMAMD" align="left" valign="top" style="display:;">
									<div id="CenterZoneMAMD"></div>
								</td>
							</tr>
							<tr>
								<td id="tr_CenterZoneM" align="left" valign="top" style"display:;">
									<div id="CenterZoneM">
										<script language="JavaScript">refreshDataM('LovM.php',10,10,1,'','','','<?php echo (isset($_GET["jsonID"]) ? $_GET["jsonID"] : ""); ?>','<?php echo (isset($_GET["formID"]) ? $_GET["formID"] : ""); ?>');</script>
									</div>	
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
	</body>
</html>