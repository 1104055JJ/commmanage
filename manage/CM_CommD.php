<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
	// 未登入則轉至首頁(登入頁)
	session_start();
	if (!(isset($_SESSION["manageuser"]))) {
		echo '<meta http-equiv=REFRESH CONTENT=1;url=index.php>';		
	} else {
?>
		<script type="text/javascript" src="../js/DetailJSB.js"></script>
		<script language="JavaScript">
			function openwindow(l_url) {
				var w = window.open(l_url,"w",config="width=500,height=500");
			}
		</script>
		
<?php
		include("../common/connectdb.php");
		include("../common/AutoDataClassB.php");
		include("../common/AutoPageSwitchClass.php");	

		// 建立及初使化頁數切換
		$a = new AutoPageSwitchClass("D","CM_CommD.php","SELECT COUNT(*) FROM building",array(3,6,9,12));			
		
		// 建立及初始化表格
		$b = new AutoDataClass("D","CM_CommD.php","CM_CommDAMD.php",array("CommID","BuildingID"),"","社區棟別資料維護作業","center","table90");
		$b->setTableTitle("left","Arial18Bold bg_gray");
		$b->setFieldTitle("center","Arial16 bg_y");	
		$b->setOddRow("","bg_light_blue");
		//$b->setEvenRow("","bg_y");
		// 配合頁數切換,設定表格資料來源
		$b->beginRowsNum = $a->beginRowsNum;
		$b->setQuery("*","Building","","",$a->beginRowsNum.",".$a->perPageRows);
		// 設定表格欄位顯示
		$b->setAllFieldAlign("center");
		$b->setAllFieldClass("Arial14");
		$b->setFieldDisplayName("CommID","社區代碼");
		$b->setFieldDisplayName("BuildingID","棟別代碼");
		$b->setFieldDisplayName("BuildingName","名  稱");
		$b->setFieldDisplayName("Enable","有效否");
		$b->setFieldDisplayName("Note","備註");
		// 增加按鈕及觸發事件
		//$b->addButton("開窗","onclick=\"openwindow('http://www.prince.com.tw');\"");
		// 設定表格底部顯示頁數切換
		$b->setTableBottom($a->getShowData());
		// 顯示表格資料
		$b->showData();
	}
?>