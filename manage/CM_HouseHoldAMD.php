<?php
	// 未登入則轉至首頁(登入頁)
	session_start();
	if (!(isset($_SESSION["manageuser"]))) {
		echo '<meta http-equiv=REFRESH CONTENT=1;url=index.php>';		
	} else {
		include("../common/connectdb.php");
		include("../common/AutoFormClass.php");
		
		$a = new AutoFormClass("M","CM_HouseHold.php","CM_HouseHoldAMD.php","HouseHold","HouseHold",array("CommID","BuildingID","HouseHoldID"),"門牌戶號基本資料","center","table90");
		$a->setTableTitle("left","Arial18Bold bg_gray");
		$a->setFieldTitle("center","Arial16 bg_y");
		$FilterComm = "Select CommName,CommID From Community Where CommID='".$_SESSION['Community']."'"; 
		$a->setField("CommID"          ,"社區代碼"     ,"left","Arial14","N","N","","","Y","Y","Y","N","select",array(array(),$FilterComm),"");
		$FilterBuilding = "Select BuildingName,BuildingID From Building Where CommID='".$_SESSION['Community']."'";
		$a->setField("BuildingID"      ,"棟別代碼"     ,"left","Arial14","N","N","","","Y","Y","Y","N","select",array(array(),$FilterBuilding),"");
		$a->setField("HouseHoldID"     ,"戶別代碼"     ,"left","Arial14","Y","N","","","Y","Y","Y","Y","text",array(array(),""),"");
		$a->setField("Address1"        ,"地址-號"      ,"left","Arial14","N","N","","","Y","Y","Y","Y","text",array(array(),""),"","");
		$a->setField("Address2"        ,"地址-樓"      ,"left","Arial14","N","N","","","Y","Y","Y","Y","text",array(array(),""),"","");
		$a->setField("Address3"        ,"地址-之"      ,"left","Arial14","Y","N","","","Y","Y","Y","Y","text",array(array(),""),"","");
		$a->setField("BankCoverCode"   ,"銀行銷帳碼"   ,"left","Arial14","N","N","","","Y","Y","Y","Y","text",array(array(),""),"");
		$a->setField("CStoreCoverCode" ,"超商銷帳碼"   ,"left","Arial14","N","N","","","Y","Y","Y","Y","text",array(array(),""),"");
		$a->setField("Email"		   ,"電郵"		  ,"left","Arial14","Y","N","","","Y","Y","Y","Y","text",array(array(),""),"");
		$a->setField("Payer"	       ,"繳款人"		  ,"left","Arial14","N","N","","","Y","Y","Y","Y","select",array(array(),"Select CodeName,CodeID from unifieddatacode where catagoryid='A03'"),"","");
		$a->setField("Owner"		   ,"所有權人"	  ,"left","Arial14","N","N","","","Y","Y","Y","Y","text",array(array(),""),"","");
		$a->setField("Renter"		   ,"承租人"		  ,"left","Arial14","Y","N","","","Y","Y","Y","Y","text",array(array(),""),"");
		$a->setField("Area"			   ,"坪數"		  ,"left","Arial14","N","N","","","Y","Y","Y","Y","text",array(array(),""),"");
		$a->setField("Fee"		  	   ,"管理費"		  ,"left","Arial14","N","N","","","Y","Y","Y","Y","text",array(array(),""),"");
		$a->setField("Discount"		   ,"折扣"		  ,"left","Arial14","Y","N","","","Y","Y","Y","Y","text",array(array(),""),"0");
		$a->setField("NetFee"		   ,"應繳金額"	  ,"left","Arial14","N","N","","","Y","N","Y","N","text",array(array(),""),"");
		$a->setField("BillCycle"	   ,"繳費週期"     ,"left","Arial14","N","N","","","Y","Y","Y","Y","select",array(array(),"Select CodeName,CodeID from unifieddatacode where catagoryid='A04'"),"","");
		$a->setField("FeePerMonths"	   ,"繳費月份"	  ,"left","Arial14","Y","N","","","Y","Y","Y","Y","combobox",array(array(),"Select CodeName,CodeID from unifieddatacode where catagoryid='A041'"),"","");
		$a->setField("ModUser"		   ,"資料更新人員" ,"left","Arial14","N","N","","","Y","N","Y","N","text",array(array(),""),"","",array("loginuser","Y","Y"));
		$a->setField("ModDate"		   ,"資料更新時間" ,"left","Arial14","N","N","","","Y","N","Y","N","text",array(array(),""),"","",array("datetime","Y","Y"));
		//$a->setField("Enable","啟用否","left","Arial14","N","N","","","Y","Y","Y","Y","checkbox",array(array("啟用","Y","停用","N"),"SELECT CompanyName,CompanyID FROM company WHERE Enable='Y'"),"Y");			
		$a->addBeforeDeleteCheck("ar",array("CommID","CommID","HouseHoldID","HouseHoldID"),"已產生管理費資料,不可刪除!");
		$a->setbeforeForm("Y","./CM_HouseHoldjs.php");
		$a->showData();
		if (isset($_POST["f"]) && ($_POST["f"] == "A" || $_POST["f"] == "M")) {
			// 欄位及規則檢查
?>
			<script language="JavaScript">
				var form = "MasterForm";
				var fieldArray = [];
				//fieldArray.push(["BankCoverCode","TaxId","統一編號錯誤"]);	//測試用
				fieldArray.push(["Address1","Need","此欄位為必填"]);
				fieldArray.push(["Address2","Need","此欄位為必填"]);
				fieldArray.push(["Address3","Need","此欄位為必填"]);
				fieldArray.push(["Payer","Need","此欄位為必填"]);
				fieldArray.push(["Owner","Need","此欄位為必填"]);
				fieldArray.push(["FeePerMonths","Need","此欄位為必填"]);
				fieldArray.push(["HouseHoldID","Need","此欄位為必填"]);				
				fieldArray.push(["Fee","",""]);
				fieldArray.push(["Discount","",""]);				
				checkRule(form,fieldArray);
				
				function otherCheckRule() {
						var event = true;
						document.forms["MasterForm"].elements["Fee"].style.border="";
   						document.forms["MasterForm"].elements["Discount"].style.border="";
   						document.forms["MasterForm"].elements["NetFee"].style.border="";	

					    var Fee = document.forms["MasterForm"].elements["Fee"]; 
					    var re = /^\d+$/;   
 					    if (Fee.value == ""||Fee.value<0||!re.test(document.forms["MasterForm"].Fee.value)) { 
 					    	Fee.value = "";
					    	$('#Fee', document.forms["MasterForm"]).attr("placeholder","請輸入正確數字");
					    	document.forms["MasterForm"].elements["Fee"].style.border="2px red dotted";
  						   	event = false;    	
 					   	} 
 
					    var Discount = document.forms["MasterForm"].elements["Discount"];
					    if (!re.test(document.forms["MasterForm"].Discount.value)){Discount.value = "";}   
 					    if (Discount.value == ""||Discount.value<0) { 
 						   	Discount.value = "0";
 					   	} 
       
 					   var NetFee = document.forms["MasterForm"].elements["NetFee"];   
 					   if(parseInt(Fee.value)>=parseInt(Discount.value)){
 					    	NetFee.value = parseInt(Fee.value)-parseInt(Discount.value);    	 
 					   }
  					  else{
				       	  NetFee.value ="";
                          $('#NetFee', document.forms["MasterForm"]).attr("placeholder","管理費或折扣金額錯誤");
                          document.forms["MasterForm"].elements["Discount"].style.border="2px red dotted";
    	                  document.forms["MasterForm"].elements["Fee"].style.border="2px red dotted";
    	                  document.forms["MasterForm"].elements["NetFee"].style.border="2px red dotted";
    	                  event = false;    	
                      }

					 return event;
				}
							
			</script>
<?php
		}
	}
?>