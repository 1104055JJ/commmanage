<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
	// 未登入則轉至首頁(登入頁)
	session_start();
	if (!(isset($_SESSION["manageuser"])))// && isset($_SESSION["SubMenu"]) && isset($_SESSION["ProgramItem"])))  
  {
		echo '<meta http-equiv=REFRESH CONTENT=1;url=index.php>';		
	} else {
?>
		<script type="text/javascript" src="../js/DetailJSB.js"></script>
		<script language="JavaScript">
			function openwindow(l_url) 
      {
        var w = window.open(l_url,"w",config="width=500,height=500");
			}
		</script>
    
<?php
		include("../common/connectdb.php");
		include("../common/AutoDataClassB.php");
		include("../common/AutoPageSwitchClass.php");	

    //$words="SELECT COUNT(*) FROM v_collectionsdetail where CommID='".$_SESSION['Community']."'";
    //echo $words; 
		// 建立及初使化頁數切換
		$c = new AutoPageSwitchClass("D","CollectionGenerateD.php","SELECT COUNT(*) FROM v_collectionsdetail ",array(3,6,9,12));
		
		// 建立及初始化表格
		$d = new AutoDataClass("D","CollectionGenerateD.php","CollectionGenerateDAMD.php",array("CommID","CollectionsNo","DetailNo"),"","收款明細維護","center","table90");
		$d->setTableTitle("left","Arial18Bold bg_gray");
		$d->setFieldTitle("center","Arial16 bg_y");	
		$d->setOddRow("","bg_light_blue");
		//$b->setEvenRow("","bg_y");
		// 配合頁數切換,設定表格資料來源
		$d->beginRowsNum = $c->beginRowsNum;
		//$b->setQuery("collectionsdetail.CommID, collectionsdetail.CollectionsNo, collectionsdetail.DetailNo, ar.ArNo, collectionsdetail.ArNo, ar.Year, ar.Month, ar.Note, ar.HouseFee, ar.CarFee, ar.Discount, ar.Unreceived, collectionsdetail.WriteOffAmount, ar.ModUser, ar.ModDate","collectionsdetail, ar","ar.ArNo=collectionsdetail.ArNo and collectionsdetail.CommID='9BA0001'","",$a->beginRowsNum.",".$a->perPageRows);
    //$b->setQuery("collectionsdetail.CommID as CommID, collectionsdetail.CollectionsNo as CollectionsNo, collectionsdetail.DetailNo as DetailNo, ar.ArNo as ArNo, collectionsdetail.ArNo as ArNo2, ar.Year as Year, ar.Month as Month, ar.Note as Note, ar.HouseFee as HouseFee, ar.CarFee as CarFee, ar.Discount as Discount, ar.Unreceived as Unreceived, collectionsdetail.WriteOffAmount as WriteOffAmount, ar.ModUser as ModUser, ar.ModDate as ModDate", "collectionsdetail , ar","ar.ArNo=collectionsdetail.ArNo","",$a->beginRowsNum.",".$a->perPageRows);
    $d->setQuery("*","v_collectionsdetail","CommID='".$_SESSION['Community']."'","",$c->beginRowsNum.",".$c->perPageRows);
    //function setQuery($SELECT,$FROM,$WHERE,$ORDERBY,$LIMIT) 		
    
    // 設定表格欄位顯示
		$d->setAllFieldAlign("center");
		$d->setAllFieldClass("Arial14");
    $d->setFieldDisplay("CommID","N");
    $d->setFieldDisplay("CollectionsNo","N");
  	$d->setFieldDisplayName("DetailNo","明細項次");
  	$d->setFieldDisplay("DetailNo","N");
		$d->setFieldDisplayName("ArNo","賬單編號");
		$d->setFieldDisplayName("Year","年份");
		$d->setFieldDisplayName("Month","月份");
		$d->setFieldDisplayName("Note","收款備註");
    $d->setFieldDisplayName("HouseFee","管理費");
    $d->setFieldDisplayName("CarFee","車位清潔費");
    $d->setFieldDisplayName("Discount","折扣總額");
    $d->setFieldDisplayName("TotalAmount","總計金額");
    $d->setFieldDisplayName("Received","已收金額");
    $d->setFieldDisplayName("UnReceived","未收金額");
    $d->setFieldDisplayName("WriteOffAmount","沖銷金額");
    $d->setFieldDisplayName("ModUser","修改人員");
    $d->setFieldDisplayName("ModDate","修改日期");        
		// 增加按鈕及觸發事件
		//$b->addButton("開窗","onclick=\"openwindow('http://www.prince.com.tw');\"");
		// 設定表格底部顯示頁數切換
		$d->setTableBottom($c->getShowData());
		// 顯示表格資料
		$d->showData();
	}
?>