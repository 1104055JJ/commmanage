<?php
	$err = "";
	// 未登入則轉至首頁(登入頁)
	session_start();
	if (!(isset($_SESSION["manageuser"]))) {
		echo '<meta http-equiv=REFRESH CONTENT=1;url=index.php>';		
	} else {
		include("../common/connectdb.php");
		//判斷要執行之功能 : Q->顯示資料  A->新增  AW->新增寫入  M->修改  MW->修改寫入  D->刪除  DW->刪除寫入  L->列表
		$f = "";
		$id = "";
		
		if (isset($_POST['f'])) {
			if ($_POST['f'] == "Q" || $_POST['f'] == "A" || $_POST['f'] == "M" || $_POST['f'] == "D" || $_POST['f'] == "AW" || $_POST['f'] == "MW" || $_POST['f'] == "DW") {
				$f = $_POST['f'];
			} else {
				$f = "L";
			}
		} else {
			$f = "L";
		}
		
		if ($f == "Q" || $f == "M" || $f == "D") {
			if (!isset($_POST['id'])) {
				$f = "L";
			} else {
				$id = $_POST['id'];
			}
		}
?>

<?php if ($f == "Q" || $f == "A" || $f == "M" || $f == "D") {
	if ($f != "A") {
		$strSQL = "SELECT COUNT(*) FROM user_role WHERE RoleID='".$id."'";
		$rows = mysql_query($strSQL);
		list($i) = mysql_fetch_row($rows);
		if ($i == 0) {
			$err = "資料錯誤或不存在,請檢查後再執行 !";
		} else {
			$strSQL = "SELECT * FROM user_role WHERE RoleID='".$id."'";
			$rows = mysql_query($strSQL);
			$row = mysql_fetch_array($rows);
		}
	}
	$fW = "";
	$fS = "";
	$fSS = "";
	$fV = "";
	if ($f == "Q") { $fW = "Q"; }
	if ($f == "A") { $fW = "AW"; $fS = "新增"; $fSS = "儲存"; }
	if ($f == "M") { $fW = "MW"; $fS = "修改"; $fSS = "儲存"; }
	if ($f == "D") { $fW = "DW"; $fS = "刪除"; $fSS = "刪除"; }
	if ($err != "") {
		echo $err;
	} else {	
?>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<form action="RoleAMD.php" name="MasterForm" id="MasterForm" method="POST">
		<table align="center" class="table90">
			<tr class="bg_gray">
				<th colspan="6">
					<table width="100%">
						<tr>
							<th class="Arial18Bold align_left"><?php echo '<font color="red">'.$fS.'</font>'; ?>角色基本資料<input name="f" id="f" value="<?php echo $fW; ?>" type="HIDDEN"></th>			
							<th class="Arial18Bold align_right">
							<?php
								if ($f != "Q") {
									if ($f == "D") {				
										//檢查是否已被使用
										$strTemp = "SELECT COUNT(*) FROM user_roles WHERE RoleID='".$id."'";
										$rowsTemp = mysql_query($strTemp);
										list($countTemp) = mysql_fetch_row($rowsTemp);
										if ($countTemp > 0) {
											$err = "資料已被使用者角色權限調用,不可刪除 !";
										}
									}
									if ($err == "") {
										echo '<input type="Button" name="submit" value="'.$fSS.'" style="font-family: 標楷體 ,Arial, Helvetica, sans-serif;background-color: #FF8888;" class="Arial16Bold" onclick="refreshDataMaster(\'Role.php\',\'RoleAMD.php\');">';
									}
								}
							?>
							</th>
						</tr>
					</table>
				</th>
			</tr>
			<tr>
				<th class="Arial16Bold_Right">角色代碼：</th>
				<td class="Arial16"><input name="RoleID" type="text" id="RoleID" style="font-family: 標楷體 ,Arial, Helvetica, sans-serif;" <?php if ($f != "A") { echo "readonly"; } ?> value="<?php if ($f != "A") { echo $row['RoleID']; } ?>"></td>			
				<th class="Arial16Bold_Right">角色名稱：</th>
				<td class="Arial16"><input name="RoleName" type="text" id="RoleName" style="font-family: 標楷體 ,Arial, Helvetica, sans-serif;"<?php if ($f == "Q" || $f == "D") { echo "readonly"; } ?> value="<?php if ($f != "A") { echo $row['RoleName']; } ?>"></td>	
				<th class="Arial16Bold_Right">有效否：</th>
				<td class="Arial16"><input name="Valid" type="checkbox" id="Valid" style="font-family: 標楷體 ,Arial, Helvetica, sans-serif;" <?php if ($f == "Q" || $f == "D") { echo "disabled"; } ?> value="Y" <?php if ($f != "A") { echo ($row['Valid']=="Y" ? "checked" : ""); } ?>></td>	
			</tr>
			<tr>
				<th colspan="6" class="Arial18Bold"><font color="red"><?php if ($err != "") { echo $err; } ?></font></th>
			</tr>
		</table>
	</form>
<?php }} ?>
				
<?php		
		//刪除資料
		if ($f == "DW") {
			if (isset($_POST['RoleID'])) {
				// 刪除Table:user_role_zone
				$strSQL = "DELETE FROM user_role_zone WHERE RoleID='".$_POST['RoleID']."'";
				mysql_query($strSQL);
				// 刪除Table:user_role_site
				$strSQL = "DELETE FROM user_role_site WHERE RoleID='".$_POST['RoleID']."'";
				mysql_query($strSQL);
				// 刪除Table:user_role				
				$strSQL = "DELETE FROM user_role WHERE RoleID='".$_POST['RoleID']."'";
				mysql_query($strSQL);
			} else {
				$err = "資料錯誤或不存在 ,請檢查後再執行!";
			}
			$f = "L";			
		}
		
		// 新增或修改儲存
		if ($f == "AW" || $f == "MW") {
			// 檢查新增或更改時之輸入值是否正確
			if (!isset($_POST['RoleID']) || $_POST['RoleID'] == "") {
				$err = $err.'角色代碼不存在或輸入值不正確 !'.chr(13);
			}
			if (!isset($_POST['RoleName']) || $_POST['RoleName'] == "") {
				$err = $err.'角色名稱不存在或輸入值不正確 !'.chr(13);
			}
			$Valid = "N";
			if (isset($_POST['Valid']) && $_POST['Valid'] == "Y") {
				$Valid = "Y";
			}
			// 新增或修改->寫入資料庫
			if ($err == "") {
				if ($f == "AW") {
					// 檢查Key值是否重覆
					$strSQL = "SELECT COUNT(*) FROM user_role WHERE RoleID='".$_POST['RoleID']."'";
					$rows = mysql_query($strSQL);
					list($i) = mysql_fetch_row($rows);
					if ($i > 0) {
						$err = "角色代碼已存在,請檢查後再執行 !";
					} else {
						$strSQL = "INSERT INTO user_role (RoleID,RoleName,Valid) VALUES ('".$_POST['RoleID']."','".$_POST['RoleName']."','".$Valid."')";
						mysql_query($strSQL);
					}
				} else {
					$strSQL = "UPDATE user_role SET RoleID='".$_POST['RoleID']."',RoleName='".$_POST['RoleName']."',Valid='".$Valid."' WHERE RoleID='".$_POST['RoleID']."'";
					mysql_query($strSQL);
				}
			}
			$f = "L";
		}

		// L : 回列表 
		if ($f == "L") {
			echo $err;
		}
	}
?>