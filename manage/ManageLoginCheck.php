<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
	if (!isset($_SESSION)) { session_start(); }
	include_once("../common/connectdb.php");
	include_once("../common/PublicFunction.php");
	// 檢查SQL Injection
	checkInjection();
	
	//設定欲認證的帳號名稱及密碼
	$manageuser = "";
	$managepassword = "";
	$msg = "";
	$success = false;
	$manageusername = "";
	
	if (isset($_POST["manageuser"]) && $_POST["manageuser"] != "" && isset($_POST["managepassword"]) && $_POST["managepassword"] != "") {
		$manageuser = $_POST["manageuser"];
		$managepassword = $_POST["managepassword"];
		
		//檢查登入之帳號是否存在於manage_user之Table中
		$strSQL = "SELECT UserID,UserName FROM manage_user WHERE UserID='".$manageuser."' AND Enable='Y'";
		$rows = mysql_query($strSQL);
		if (mysql_num_rows($rows) == 1) {
			$row = mysql_fetch_array($rows);
			if ($manageuser = $row['UserID']) {
				$manageusername = $row['UserName'];                        
             
				$strSQL = "SELECT * FROM sysref";
				$rows = mysql_query($strSQL);
				$row = mysql_fetch_array($rows);
		
				if ($row['IsLDAP'] == "Y") {								
					//設定網域名稱
					$domain = $row['Domain'];
					$host = $row['Host'];
					$dn = $row['DN'];

					// 使用 ldap bind
					$ldaprdn = $manageuser."@".$domain; 
					// ldap rdn or dn 
					$ldappass = $managepassword; // 設定密碼

					// 連接至網域控制站
					//$ldapconn = ldap_connect($domain);
					// 如果要特別指定某一部網域主控站(DC)來作認證則上面改寫為
					$ldapconn = ldap_connect($host) or die("無法連接至認證伺服器!"); 
					// 或 
					// $ldapconn = ldap_connect('dc2.domain.com') or die("無法連接至 dc2.domain.com"); 

					if ($ldapconn) {
						//以下兩行務必加上，否則 Windows AD 無法在不指定 OU 下，作搜尋的動作
						ldap_set_option($ldapconn, LDAP_OPT_PROTOCOL_VERSION, 3);
						ldap_set_option($ldapconn, LDAP_OPT_REFERRALS, 0);
				
 						// binding to ldap server
 						//$ldapbind = ldap_bind($ldapconn, $user, $managepassword);
 						$ldapbind = ldap_bind($ldapconn, $ldaprdn, $ldappass);
 						//$ldapbind = ldap_bind($ldapconn);     
 						// verify binding
 						if ($ldapbind) {
	  						$filter = "(sAMAccountName=".$manageuser.")";
  							$result = @ldap_search($ldapconn, $dn, $filter);        
  							if ($result) { 
 								//認證成功,取出帳號的所有資訊             
   								//$entries = ldap_get_entries($ldapconn, $result);
								$data = ldap_get_entries($ldapconn, $result);
								//$manageusername = $data[0][$data[0][1]][0];
								$success = true;
								//header("location:index.php");
 							} else {
 								$msg = "您輸入之帳號或密碼錯誤或該使用者無使用權限,請重新輸入(1)!";
 							}
 						} else {   
  							$msg = "您輸入之帳號或密碼錯誤,請重新輸入!";
 						}
					} else {
						$msg = "無法連接至認證主機!";
					}
		
					//關閉LDAP連結
					ldap_close($ldapconn);
				} else {
					// 檢查本地端帳號密碼
					$strSQL = "SELECT UserID,Password FROM manage_user WHERE UserID='".$manageuser."' AND Password='".encrypt($managepassword,"E")."' AND Enable='Y'";
					$rows = mysql_query($strSQL);
					if (mysql_num_rows($rows) == 1) {
						$row = mysql_fetch_array($rows);
						if ($row["UserID"] != $manageuser || $row["Password"] != encrypt($managepassword,"E")) {
							$msg = "您輸入之帳號或密碼錯誤或該使用者無使用權限,請重新輸入(2)!";						
						} else {
							$success = true;
						}
					} else {
						$msg = "您輸入之帳號或密碼錯誤或該使用者無使用權限,請重新輸入(3)!";
					}
				}
			} else {
				$msg = "您輸入之帳號或密碼錯誤或該使用者無使用權限,請重新輸入(4)!"; 
			}
		} else {
			$msg = "您輸入之帳號或密碼錯誤或該使用者無使用權限,請重新輸入(5)!"; 
		}
	} else {
		$msg = "請輸入正確之帳號及密碼!";
	}

	//新增 COMM SESSION  並為預設社區代碼
	$Community = "";
	if ($success) {
		$strSQL = "SELECT DISTINCT a.CommID FROM manage_user AS a,v_manage_user_comm_program AS b WHERE a.UserID='".$manageuser."' AND a.UserID=b.UserID AND a.CommID=b.CommID";
		$rows = mysql_query($strSQL);
		if (mysql_num_rows($rows) > 0) {
			$row = mysql_fetch_array($rows);
			$Community = $row["CommID"];
		}
	}

	if ($success) {
		$_SESSION["manageuser"] = $manageuser;
		$_SESSION["manageusername"] = $manageusername;
		$_SESSION["Community"] = $Community;		
		//echo "登入成功!";
	} else {
		unset($_SESSION['manageuser']);		
		unset($_SESSION['manageusername']);
		unset($_SESSION['Community']);
		//echo $msg;
	}
	if ($success){
		//登入記錄
		$date = date("Y-m-d H:i:s");
		$file = './logs/login.txt';
		$current = file_get_contents($file);
		$current .= $date."  ".$manageuser." Login In\n";
		file_put_contents($file, $current);		
	}
	echo '<meta http-equiv=REFRESH CONTENT=1;url=index.php>';
?>