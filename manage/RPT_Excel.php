<?php
	// 未登入則轉至首頁(登入頁)
	if (!isset($_SESSION)) { session_start(); }
	if (!(isset($_SESSION["manageuser"]))) {
		echo '<meta http-equiv=REFRESH CONTENT=1;url=index.php>';		
	} else {
		include_once("../common/connectdb.php");
		include_once("../common/PublicFunction.php");
		include_once("../Excel/PHPExcel.php");				//引入 PHPExcel 物件庫
		include_once("../Excel/PHPExcel/IOFactory.php");	//引入 PHPExcel_IOFactory 物件庫
		
		function getCell($s,$x) {
			// 假設A~Z對應1~26
			if ($x >= 1 && $x <= 26) {
				return $s.chr($x + 64);
			} else {
				$j = floor($x / 26);
				$k = $x % 26;
				return $s.getCell("",$j).chr($k + 64);
			}
		}

		// 檢查$_POST值
		$title = (isset($_REQUEST["t"]) ? encrypt($_REQUEST["t"],'D') : "");
		$fields = explode('^A',(isset($_REQUEST["f"]) ? encrypt($_REQUEST["f"],'D') : ""));
		$strSQL = (isset($_REQUEST["q"]) ? encrypt($_REQUEST["q"],'D') : "");
		
		$rows = mysql_query($strSQL);
		if (!$rows) {
			global $mysql_link;
			$err = '匯出Excel失敗 ( '.mysql_errno($mysql_link).' : '.mysql_error($mysql_link).' )';
		?>
			<html>
				<head>
					<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
					<title></title>
					<script language="JavaScript">
						function goBack(){
        					alert('<?php echo $err; ?>');
        					history.go(-1);
						}
					</script>
				</head>
				<body onload="goBack()"></body>
			</html>			
		<?php
		} else {
			// 建立並指定Excel工作表
			$objPHPExcel = new PHPExcel();					//實體化Excel
			$objPHPExcel->setActiveSheetIndex(0);			//設定預設顯示的工作表
			$objActSheet = $objPHPExcel->getActiveSheet();	//指定預設工作表為 $objActSheet
			$objActSheet->setTitle($title);					//設定標題
			//$objPHPExcel->createSheet();					//建立新的工作表，上面那三行再來一次，編號要改
	
			// 設定預設字體及大小
			$objPHPExcel->getDefaultStyle()->getFont()->setName('標楷體');
			$objPHPExcel->getDefaultStyle()->getFont()->setSize(12);
			
			$y = 1;
			$j = 0;
			// 設定欄位標題  
			for ($i = 0; $i < count($fields); $i++) {
				$field = explode(",",$fields[$i]);
				if ($field[2] == "Y") {
					$j++;
					// 粗體字
					$objActSheet->getStyle(getCell("",$j).$y.":".getCell("",$j).$y)->getFont()->setBold(true);
					// 自動調整大小
					$objActSheet->getColumnDimension(getCell("",$j))->setAutoSize(true);
					// 填色
					$objActSheet->getStyle(getCell("",$j).$y.":".getCell("",$j).$y)->getFill()->setFillType(PHPExcel_Style_Fill:: FILL_SOLID)->getStartColor()->setARGB('FFC9E3F3');								
					// 塞值
					$objActSheet->setCellValue(getCell("",$j).$y, $field[1]);				
				}			
			}
	
			// 將攔位值填入
			while ($row = mysql_fetch_array($rows)) {
				$y++;
				$j = 0;
				for ($x = 0; $x < mysql_num_fields($rows); $x++) {
					$type = mysql_field_type($rows,$x);
					for ($i = 0; $i < count($fields); $i++) {
						$field = explode(",",$fields[$i]); 
						if (mysql_field_name($rows, $x) == $field[0]) {
							if ($field[2] == "Y") {
								$j++;						
								if ($type == "string") {
									// 數字類型之字串完整呈現,例如顯示02而非顯示2
									$objActSheet->getStyle(getCell("",$j).$y)->getNumberFormat()->setFormatCode('@');
									$objActSheet->getCell(getCell("",$j).$y)->setValueExplicit($row[$x],PHPExcel_Cell_DataType::TYPE_STRING);
								} else {
									$objActSheet->setCellValue(getCell("",$j).$y,$row[$x]);
								}							
							}
						}
					}
				}
			}
	
			// 輸出Excel
			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment;filename='.iconv('UTF-8','Big5',$title).'.xls');	
			header('Cache-Control: max-age=0');
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
			$objWriter->setPreCalculateFormulas(false);
			$objWriter->save('php://output');
		}
		exit;
	}
?>