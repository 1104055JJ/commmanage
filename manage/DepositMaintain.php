<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
	// 未登入則轉至首頁(登入頁)
	session_start();
	if (!(isset($_SESSION["manageuser"])))
  {echo '<meta http-equiv=REFRESH CONTENT=1;url=index.php>';} 
  else 
  {
    ?>
		<script type="text/javascript" src="../js/MasterJS.js"></script>
		<!--<script language="JavaScript">refreshDataMAMD('DepositMaintainAMD.php','A','');</script>-->
    <?php
		// 記錄點選了那個SubMenu中之程式
		$society = "CommID='".$_SESSION['Community']."'"; 

		include("../common/connectdb.php");		
		include("../common/AutoDataClassB.php");
		include("../common/AutoPageSwitchClass.php");	

		// 建立及初使化頁數切換
		$a = new AutoPageSwitchClass("M","DepositMaintain.php","SELECT COUNT(*) FROM collectionsmaster where IncomeItemID='003' and CommID='".$_SESSION['Community']."'",array(5,10,15,20,25));			
		
		// 建立及初始化表格
		$b = new AutoDataClass("M","DepositMaintain.php","DepositMaintainAMD.php",array("CommID","CollectionsNo"),"","保證金維護","center","table90");
		$b->setTableTitle("left","Arial18Bold bg_gray");
		$b->setFieldTitle("center","Arial16 bg_y");	
		$b->setOddRow("","bg_light_blue");
		//$b->setEvenRow("","bg_y");
		// 配合頁數切換,設定表格資料來源
		$b->beginRowsNum = $a->beginRowsNum;
		$b->setQuery("CommID, CollectionsNo,HouseHoldID, PaymentType, IncomeItemID, BillNo, BillDate,DueDate, TurnToIncomeDate,IncomeAmount, Refund, CleanDeduction, RepairDeduction, PunishDeduction, Note,ModDate, ModUser","collectionsmaster","IncomeItemID='003' and CommID='".$_SESSION['Community']."'","",$a->beginRowsNum.",".$a->perPageRows);
		// 設定表格欄位顯示
		$b->setAllFieldAlign("center");
		$b->setAllFieldClass("Arial14");
		$b->setFieldDisplay("CommID","N");
    $b->setFieldDisplayName("CollectionsNo","收款編號");
		$b->setFieldDisplayName("HouseHoldID","門牌戶號");
		$b->setFieldDisplayName("PaymentType","收款方式");		
    $b->setFieldDisplayName("IncomeItemID","收款用途");
    $b->setFieldDisplayName("BillNo","票據號碼");
    $b->setFieldDisplayName("BillDate","收票日期");
    $b->setFieldDisplayName("DueDate","收款(到期)日期");
    $b->setFieldDisplayName("IncomeAmount","收款金額");
    $b->setFieldDisplayName("TurnToIncomeDate","轉收入日期");
    $b->setFieldDisplayName("Refund","退款共計");
    $b->setFieldDisplayName("CleanDeduction","清潔費扣款");
    $b->setFieldDisplayName("RepairDeduction","修繕費用扣款");
    $b->setFieldDisplayName("PunishDeduction","罰款扣款");
    $b->setFieldDisplayName("Note","收款說明");
    $b->setFieldDisplayName("ModUser","修改人員");
    $b->setFieldDisplayName("ModDate","修改日期");
    

		// 增加按鈕及觸發事件
		// 設定表格底部顯示頁數切換
		$b->setTableBottom($a->getShowData());
		// 顯示表格資料
		$b->showData();
  }
?>
