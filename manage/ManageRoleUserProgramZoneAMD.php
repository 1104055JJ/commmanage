<?php
	$err = "";
	// 未登入則轉至首頁(登入頁)
	session_start();
	if (!(isset($_SESSION["manageuser"]))) {
		echo '<meta http-equiv=REFRESH CONTENT=1;url=index.php>';		
	} else {
		include("../common/connectdb.php");
		//判斷要執行之功能 : Q->顯示資料  A->新增  AW->新增寫入  M->修改  MW->修改寫入  D->刪除  DW->刪除寫入  L->列表
		$f = "";
		$id = "";
		$id1 = "";
		$id2 = "";
		$idArray = array();
		
		if (isset($_POST['f'])) {
			if ($_POST['f'] == "Q" || $_POST['f'] == "A" || $_POST['f'] == "M" || $_POST['f'] == "D" || $_POST['f'] == "AW" || $_POST['f'] == "MW" || $_POST['f'] == "DW") {
				$f = $_POST['f'];
			} else {
				$f = "L";
			}
		} else {
			$f = "L";
		}
		
		if ($f == "Q" || $f == "A" || $f == "M" || $f == "D") {
			if (!isset($_POST['id'])) {
				$f = "L";
			} else {
				$idArray = explode("^A", $_POST['id']);
				for ($i = 0; $i < count($idArray); $i += 3) { 
					if ($idArray[$i] == "RoleID") { $id = $idArray[$i+1]; }
					if ($idArray[$i] == "CompanyID") { $id1 = $idArray[$i+1]; }
					if ($idArray[$i] == "ZoneID") { $id2 = $idArray[$i+1]; }					
				}
			}
		}
?>

<?php if ($f == "Q" || $f == "A" || $f == "M" || $f == "D") {
	if ($f != "A") {
		$strSQL = "SELECT COUNT(*) FROM manage_role_zone WHERE RoleID='".$id."' AND CompanyID='".$id1."' AND ZoneID='".$id2."'";
		$rows = mysql_query($strSQL);
		list($i) = mysql_fetch_row($rows);
		if ($i == 0) {
			$err = "資料錯誤或不存在,請檢查後再執行 !";
		}
	}
	$fW = "";
	$fS = "";
	$fSS = "";
	$fV = "";
	if ($f == "Q") { $fW = "Q"; }
	if ($f == "A") { $fW = "AW"; $fS = "新增"; $fSS = "儲存"; }
	if ($f == "M") { $fW = "MW"; $fS = "修改"; $fSS = "儲存"; }
	if ($f == "D") { $fW = "DW"; $fS = "刪除"; $fSS = "刪除"; }
	if ($err != "") {
		echo $err;
	} else {	
?>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<form action="ManageRoleUserProgramZoneAMD.php" name="MasterForm" id="MasterForm" method="POST">
		<table id="MasterFormMainTable" align="center" class="table table-bordered table-condensed table-width-90 table-margin-0">
			<tr class="bg_gray">
				<td colspan="6">
					<table width="100%">
						<tr>
							<td class="font-18-bold align_left">
								<img id="m_FormTableImage" src="../images/icon/NT-Collapse.gif" onclick="foldTable('m_FormTable','m_FormTableDisplay','m_FormTableImage');">								
								<?php echo '<font color="red">'.$fS.'</font>'; ?>設區程式權限維護作業-依區域(異動)
								<input name="f" id="f" value="<?php echo $fW; ?>" type="HIDDEN">
							</td>			
							<td class="font-18-bold align_right">
							<?php
								if ($f != "Q") {
									if ($f == "D") {				
										//檢查是否已被使用
										$strCheck = "SELECT COUNT(*) FROM manage_user_role WHERE RoleID='".$id."'";
										$rowsCheck = mysql_query($strCheck);
										$rowCheck = mysql_fetch_array($rowsCheck);
										if ($rowCheck[0] > 0) {
											$err .= "資料已被【使用者社區角色權限維護作業】調用,不可刪除 !";
										}
									}
									if ($err == "") {
										echo '<input type="Button" name="submit" value="'.$fSS.'" class="btn btn-danger" onclick="refreshDataMaster(\'ManageRoleUserProgramZone.php\',\'ManageRoleUserProgramZoneAMD.php\',\'MW\',\'N\');">';
									}
								}
							?>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr><td><div id="m_FormTable" style="display:;"><table class="table table-bordered table-condensed table-width-100 table-margin-0">			
				<tr>
					<td class="font-16-bold bg_y align_center">角色</td>
					<td class="font-16">
						<select name="RoleID" id="RoleID" <?php if ($f == "Q") { echo "disabled"; } ?>>
						<?php
							$strTemp = "SELECT * FROM manage_role ORDER BY RoleName";
							$rowsTemp = mysql_query($strTemp);
							while ($rowTemp = mysql_fetch_array($rowsTemp)) {
								$selected = "";
								if ($f != "A") {
									if ($id == $rowTemp['RoleID']) {
										$selected = "selected";
										echo '<option value="'.$rowTemp['RoleID'].'" '.$selected.'>'.$rowTemp['RoleName'].'</option>';
									}								
								} else {
									echo '<option value="'.$rowTemp['RoleID'].'" '.$selected.'>'.$rowTemp['RoleName'].'</option>';
								}
							} 
						?>
						</select>
					</td>
	
					<td class="font-16-bold bg_y align_center">公司</td>
					<td class="font-16">
						<select name="CompanyID" id="CompanyID" <?php if ($f == "Q") { echo "disabled"; } ?>>
						<?php
							$strTemp = "SELECT * FROM company ORDER BY CompanyName";
							$rowsTemp = mysql_query($strTemp);
							while ($rowTemp = mysql_fetch_array($rowsTemp)) {
								$selected = "";
								if ($f != "A") {
									if ($id1 == $rowTemp['CompanyID']) {
										$selected = "selected";
										echo '<option value="'.$rowTemp['CompanyID'].'" '.$selected.'>'.$rowTemp['CompanyName'].'</option>';
									}								
								} else {
									echo '<option value="'.$rowTemp['CompanyID'].'" '.$selected.'>'.$rowTemp['CompanyName'].'</option>';
								}
							} 
						?>
						</select>
					</td>				
									
					<td class="font-16-bold bg_y align_center">區域</td>
					<td class="font-16">
						<select name="ZoneID" id="ZoneID"  <?php if ($f == "Q") { echo "disabled"; } ?>>
						<?php
							$strTemp = "SELECT * FROM zone ORDER BY ZoneName";
							$rowsTemp = mysql_query($strTemp);
							while ($rowTemp = mysql_fetch_array($rowsTemp)) {
								$selected = "";
								if ($f != "A") {
									if ($id2 == $rowTemp['ZoneID']) {
										$selected = "selected";
										echo '<option value="'.$rowTemp['ZoneID'].'" '.$selected.'>'.$rowTemp['ZoneName'].'</option>';
									}								
								} else {
									echo '<option value="'.$rowTemp['ZoneID'].'" '.$selected.'>'.$rowTemp['ZoneName'].'</option>';
								}
							} 
						?>
						</select>
					</td>
				</tr>
				<tr>				
					<td class="font-16-bold bg_y align_center">程式權限</td>
					<td colspan="5" align="left" class="font-16">
						<table class="table table-bordered table-condensed table-width-100 table-margin-0">
						<?php
							$strModule = "SELECT DISTINCT a.ModuleID,a.ModuleName FROM manage_module AS a,manage_program AS b WHERE a.Enable='Y' AND a.ModuleID=b.ModuleID AND b.ProgramType='C' ORDER BY a.ModuleSort";
							$rowsM = mysql_query($strModule);
							while (list($ModuleID,$ModuleName) = mysql_fetch_row($rowsM)) {
								echo '<tr><td class="font-16-bold align_center" bgcolor="#E8F3FF">'.$ModuleName.'</td><td><table class="table-condensed table-width-100 table-margin-0">';
								$j = 0;
								$strTemp = "SELECT * FROM manage_program WHERE ModuleID='".$ModuleID."' AND Enable='Y' AND ProgramType='C' ORDER BY ProgramSort";
								$rowsF = mysql_query($strTemp);
								while ($rowF = mysql_fetch_array($rowsF)) {
									$j = $j + 1;
									$strTemp = "SELECT RW FROM manage_role_zone WHERE RoleID='".$id."' AND CompanyID='".$id1."' AND ZoneID='".$id2."' AND ProgramID='".$rowF['ProgramID']."'";
									$rowsTemp = mysql_query($strTemp);
									$i = mysql_num_rows($rowsTemp);
									$RW = "";
									if ($i > 0) {
										list($RW) = mysql_fetch_row($rowsTemp);
									}
									
									$html = "";
									if ($j % 4 == 1) { $html = $html.'<tr>'; }
									$html = '<td width="25%"><input name="ProgramID[]" type="checkbox" id="ProgramID" ';
									if ($f == "Q") { $html = $html." disabled "; }
									$html .= ' value="'.$rowF['ProgramID'].'" ';
									if ($i > 0) { $html = $html." checked "; }
									$html .= '>'.$rowF['ProgramName'].'<br>';
									// 是否可讀寫
									$html .= '<input name="RW[]" type="checkbox" id="RW" ';
									if ($f == "Q") { $html = $html." disabled "; }
									$html .= ' value="'.$rowF['ProgramID'].'" ';
									if ($RW == "W") { $html = $html." checked "; }
									$html .= '>可讀寫';
									$html .= '</td>';
									if ($j % 4 == 0) { $html = $html.'</tr>'; }
									echo $html;
								}
								// 補完剩下空的<td></td>
								if ($j % 4 != 0) {
									for ($k = 1; $k <= (4 - ($j % 4)) ; $k++) { 
										echo '<td></td>';
									}
									echo '</tr>';
								}
								echo '</table></td></tr>';
							}
						?>
						</table>
					</td>								
				</tr>
				<tr>
					<td colspan="6" class="font-16-bold align_center"><font color="red"><?php if ($err != "") { echo $err; } ?></font></td>
				</tr>
			</table></div></td></tr>				
		</table>
	</form>
	<input id="m_FormTableDisplay" name="m_FormTableDisplay" value="Y" type="HIDDEN">	
<?php }} ?>
				
<?php	
		global $mysql_link;
		//刪除資料
		if ($f == "DW") {
			if (isset($_POST['RoleID']) && isset($_POST['CompanyID']) && isset($_POST['ZoneID'])) {
				$strSQL = "DELETE FROM manage_role_zone WHERE RoleID='".$_POST['RoleID']."' AND CompanyID='".$_POST['CompanyID']."' AND ZoneID='".$_POST['ZoneID']."'";
				mysql_query($strSQL);
				if (mysql_errno($mysql_link) != 0) {
					$err .= "刪除資料時發生錯誤 !".chr(13);
					$err .= mysql_errno($mysql_link).": ".mysql_error($mysql_link).chr(13);
				}
			} else {
				$err = "資料錯誤或不存在 ,請檢查後再執行!";
			}
			$f = "L";
		}
		
		// 新增或修改儲存
		if ($f == "AW" || $f == "MW") {
			// 檢查新增或更改時之輸入值是否正確
			if (!isset($_POST['RoleID']) || $_POST['RoleID'] == "") {
				$err = $err.'角色代碼不存在或輸入值不正確 !'.chr(13);
			}
			if (!isset($_POST['CompanyID']) || $_POST['CompanyID'] == "") {
				$err = $err.'公司代碼不存在或輸入值不正確 !'.chr(13);
			}
			if (!isset($_POST['ZoneID']) || $_POST['ZoneID'] == "") {
				$err = $err.'區域代碼不存在或輸入值不正確 !'.chr(13);
			}
			
			// 新增或修改->寫入資料庫
			if ($err == "") {
				if ($f == "AW") {
					// 檢查Key值是否重覆
					$strSQL = "SELECT COUNT(*) FROM manage_role_zone WHERE RoleID='".$_POST['RoleID']."' AND CompanyID='".$_POST['CompanyID']."' AND ZoneID='".$_POST['ZoneID']."'";
					$rows = mysql_query($strSQL);
					list($i) = mysql_fetch_row($rows);
					if ($i > 0) {
						$err = "資料已存在,請檢查後再執行 !";
					}
				}
								
				if ($err == "") {
					// 變更Table:manage_role_zone資料(該區域可用 之program)
					// 先刪除
					$strTemp = "DELETE FROM manage_role_zone WHERE RoleID='".$_POST['RoleID']."' AND CompanyID='".$_POST['CompanyID']."' AND ZoneID='".$_POST['ZoneID']."'";
					mysql_query($strTemp);
					if (mysql_errno($mysql_link) != 0) {
						$err .= "刪除資料時發生錯誤 !".chr(13);
						$err .= mysql_errno($mysql_link).": ".mysql_error($mysql_link).chr(13);
					} else {
						// 再新增
						if (isset($_POST['ProgramID']) && sizeof($_POST['ProgramID']) > 0) {
							for ($i = 0; $i < sizeof($_POST['ProgramID']) ; $i++) {
								$strTemp = "SELECT COUNT(*) FROM manage_program WHERE ProgramID='".$_POST['ProgramID'][$i]."'";
								$rowsTemp = mysql_query($strTemp);
								list($count) = mysql_fetch_row($rowsTemp);
								if ($count > 0) {
									$RW = "R";
									if (isset($_POST['RW'])) {
										for ($j = 0; $j < sizeof($_POST['RW']); $j++) { 
											if ($_POST['RW'][$j] == $_POST['ProgramID'][$i]) {
												$RW = "W";
												break;
											}
										}
									}								
									$strTemp = "INSERT INTO manage_role_zone (RoleID,CompanyID,ZoneID,ProgramID,RW) VALUES ('".$_POST['RoleID']."','".$_POST['CompanyID']."','".$_POST['ZoneID']."','".$_POST['ProgramID'][$i]."','".$RW."')";
									mysql_query($strTemp);
									if (mysql_errno($mysql_link) != 0) {
										$err .= "新增資料時發生錯誤 !".chr(13);
										$err .= mysql_errno($mysql_link).": ".mysql_error($mysql_link).chr(13);
									}									
								}
							}
						}
					}
				}		
			}
			$f = "L";
		}

		// L : 回列表 
		if ($f == "L") {
			echo $err;
		}
	}
?>