<?php
	// 未登入則轉至首頁(登入頁)
	session_start();
	if (!(isset($_SESSION["manageuser"]))) {
		echo '<meta http-equiv=REFRESH CONTENT=1;url=index.php>';		
	} else {
		include("../common/connectdb.php");
		include("../common/AutoFormClass.php");

		$a = new AutoFormClass("D","HouseHoldD.php","HouseHoldDAMD.php","carspace","carspace",array("CommID","BuildingID","HouseHoldID","CarID"),"車位基本資料","center","table90");
		$a->setTableTitle("left","Arial18Bold bg_gray");
		$a->setFieldTitle("center","Arial16 bg_y");
		$a->setField("CommID","社區代碼","left","Arial14","N","N","","","Y","N","Y","N","text",array(array(),""),"");
		$a->setField("BuildingID","棟別代碼","left","Arial14","N","N","","","Y","N","Y","N","date",array(array(),""),"");
		$a->setField("HouseHoldID","戶別代碼","left","Arial14","Y","N","","","Y","N","Y","N","text",array(array(),""),"");
		$a->setField("CarID","車位代碼","left","Arial14","N","N","","","Y","N","Y","N","text",array(array(),""),"");
		$a->setField("Payer","繳款人","left","Arial14","N","N","","","Y","Y","Y","Y","text",array(array(),""),"");
		$a->setField("Owner","所有權人","left","Arial14","N","N","","","Y","Y","Y","Y","text",array(array(),""),"");
		$a->setField("Renter","承租人","left","Arial14","Y","N","","","Y","Y","Y","Y","text",array(array(),""),"");
		$a->setField("Area","坪數","left","Arial14","N","N","","","Y","Y","Y","Y","text",array(array(),""),"");
		$a->setField("Fee","管理費","left","Arial14","N","N","","","Y","Y","Y","Y","text",array(array(),""),"");
		$a->setField("Discount","折扣","left","Arial14","N","N","","","Y","Y","Y","Y","text",array(array(),""),"0");
		$a->setField("NetFee","應收管理費","left","Arial14","Y","N","","","Y","Y","Y","Y","text",array(array(),""),"");
		//$a->setField("BillCycle","繳費周期","left","Arial14","N","N","","","Y","Y","Y","Y","text",array(array(),""),"");
		$a->setField("BillCycle"	   ,"繳費週期"     ,"left","Arial14","N","N","","","Y","Y","Y","Y","select",array(array(),"Select CodeName,CodeName from unifieddatacode where CatagoryName='繳費週期'"),"");
		$a->setField("FeePerMonths","繳費月份","left","Arial14","N","N","","","Y","Y","Y","Y","text",array(array(),""),"");
		$a->setField("ModUser","資料更新人員","left","Arial14","N","N","","","Y","Y","Y","Y","text",array(array(),""),"");
		$a->setField("ModDate","資料更新時間","left","Arial14","N","N","","","Y","Y","Y","Y","text",array(array(),""),"");
		//$a->setField("UnitPrice","單價","left","Arial14","N","Y","","","Y","Y","Y","Y","text",array(array(),""),0,"onchange=\"CheckNum('Float','DetailForm','UnitPrice','需為數字')\"");
		
		//$a->addBeforeDeleteCheck("community",array("ZoneID","ZoneID"),"已被社區基本資料叫用,不可刪除!");
		$a->setAfterForm("Y","./HouseHoldDjs.php");
		$a->showData();
		
	}
?>