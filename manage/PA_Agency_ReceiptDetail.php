<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
	// 未登入則轉至首頁(登入頁)
	session_start();
	if (!(isset($_SESSION["manageuser"]))) {
		echo '<meta http-equiv=REFRESH CONTENT=1;url=index.php>';		
	} else {
?>
		<script type="text/javascript" src="../js/DetailJSB.js"></script>
		<script language="JavaScript">
			function openwindow(l_url) {
				var w = window.open(l_url,"w",config="width=500,height=500");
			}
		</script>
		
<?php
		include("../common/connectdb.php");
		include("../common/AutoDataClassB.php");
		include("../common/AutoPageSwitchClass.php");	

		// 建立及初使化頁數切換
		$a = new AutoPageSwitchClass("D","PA_Agency_ReceiptDetail.php","SELECT COUNT(*) FROM refund_detail",array(10,20,30,40));			
		
		// 建立及初始化表格
		$b = new AutoDataClass("D","PA_Agency_ReceiptDetail.php","PA_Agency_ReceiptDetailAMD.php",array("CommID","PaymentNo","DetailNo","SupplierID"),"","付代收款款單身資料維護作業","center","table90");
		$b->setTableTitle("left","Arial18Bold bg_gray");
		$b->setFieldTitle("center","Arial16 bg_y");	
		$b->setOddRow("","bg_light_blue");
		//$b->setEvenRow("","bg_y");
		// 配合頁數切換,設定表格資料來源
		$b->beginRowsNum = $a->beginRowsNum;
		$b->setQuery("*","refund_detail","","",$a->beginRowsNum.",".$a->perPageRows);
		// 設定表格欄位顯示
		$b->setAllFieldAlign("center");
		$b->setAllFieldClass("Arial14");
		$b->setFieldDisplayName("PaymentNo","付款單號");
		$b->setFieldDisplayName("IncomeItemID","收入項目");
                $b->setFieldDisplayName("CollectionsNo","收款單號");
                $b->setFieldDisplayName("HouseHoldID","戶號");
		$b->setFieldDisplayName("Amount","代收款金額");
                $b->setFieldDisplayName("Unpaid","未付金額");
                $b->setFieldDisplayName("Paid","已付金額");
                $b->setFieldDisplayName("Refund","回饋金");
                $b->setFieldDisplay("CommID","N");
                $b->setFieldDisplay("DetailNo","N");
                $b->setFieldDisplay("CleanDeduction","N");
                $b->setFieldDisplay("RepairDeduction","N");
                $b->setFieldDisplay("PunishDeduction","N");
                $b->setFieldDisplay("SupplierID","N");
                
                $b->setFieldDisplay("Note","N");
                $b->setFieldDisplay("ModDate","N");
                $b->setFieldDisplay("ModUser","N");
		// 增加按鈕及觸發事件
		
		// 設定表格底部顯示頁數切換
		$b->setTableBottom($a->getShowData());
		// 顯示表格資料
		$b->showData();
	}
?>