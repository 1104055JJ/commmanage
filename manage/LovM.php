<?php
	// 未登入則轉至首頁(登入頁)
	if (!isset($_SESSION)) { session_start(); }
	if (!(isset($_SESSION["manageuser"]))) {
		echo '<meta http-equiv=REFRESH CONTENT=1;url=index.php>';
	} else {
?>
		<script type="text/javascript" src="../js/MasterJSB.js"></script>
<?php
	if (isset($_POST["jsonID"])) {
		include("../common/connectdb.php");
		include("../common/AutoDataClassB.php");
		include("../common/AutoPageSwitchClass.php");	

		$strSQL = "SELECT JSON FROM lov WHERE LovID='".$_POST["jsonID"]."'";
		$rows = mysql_query($strSQL);
		if (mysql_num_rows($rows) > 0) {
			list($json) = mysql_fetch_row($rows);
			$json = json_decode($json);
			
			// 建立及初使化頁數切換
			$a = new AutoPageSwitchClass("M","LovM.php","SELECT COUNT(*) FROM ".$json->data->tableName,array(5,10,15,20));			
		
			// 建立及初始化表格
			//print_r($json->data->key);
			$b = new AutoDataClass("M","LovM.php","LovMAMD.php",$json->data->key,"",$json->data->tableTitle,"center","table90");
			$b->setTableTitle("left","Arial18Bold bg_gray");
			$b->setButtonDefaultClass("Arial14Bold");		
			$b->setFieldTitle("center","Arial16 bg_y");	
			$b->setOddRow("","bg_light_blue");
			// 配合頁數切換,設定表格資料來源
			$b->beginRowsNum = $a->beginRowsNum;
			$fields = "";
			$fieldsArray = $json->data->fields;
			for ($i = 0; $i < count($fieldsArray); $i++) { 
				$fields .= ($i == 0 ? "" : ",").$fieldsArray[$i]->fieldName;
			}
			$b->setQuery($fields,$json->data->tableName,$json->data->where,$json->data->orderby,$a->beginRowsNum.",".$a->perPageRows);
			// 設定表格欄位顯示
			$b->setAllFieldAlign("center");
			$b->setAllFieldClass("Arial14");
			for ($i = 0; $i < count($fieldsArray); $i++) { 
				$b->setField($fieldsArray[$i]->fieldName,$fieldsArray[$i]->displayName,"center","Arial14",$fieldsArray[$i]->orderby,$fieldsArray[$i]->display,"","","");
			}
			// 增加按鈕及觸發事件
			$b->setFAMDButtonDisplay("Y","N","N","N","Y");
			// 設定表格底部顯示頁數切換
			$b->setTableBottom($a->getShowData());
			// 顯示表格資料
			$b->showData();
		}
	}
}
?>