<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
	// 未登入則轉至首頁(登入頁)
	if (!isset($_SESSION)) { session_start(); }
	if (!(isset($_SESSION["manageuser"]))) {
		echo '<meta http-equiv=REFRESH CONTENT=1;url=index.php>';		
	} else {
?>
		<script type="text/javascript" src="../js/MasterJSB.js"></script>
<?php
		include_once("../common/connectdb.php");
		include_once("../common/AutoDataClassB.php");
		include_once("../common/AutoPageSwitchClass.php");	

		// 建立及初使化頁數切換
		$a = new AutoPageSwitchClass("M","ManageRoleUserProgramZone.php","SELECT COUNT(*) FROM v_manage_role_zone",array(10,15,20,25));			
		
		// 建立及初始化表格
		$b = new AutoDataClass("M","ManageRoleUserProgramZone.php","ManageRoleUserProgramZoneAMD.php",array("RoleID","CompanyID","ZoneID"),"","社區程式權限維護作業-依區域(列表)","center","");
		$b->setTableTitle("left","font-18-bold bg_gray");
		$b->setFieldTitle("center","font-16-bold bg_y");
		$b->setDefaultFieldAlign("left");
		$b->setDefaultFieldClass("font-14");
		$b->setButtonDefaultClass("btn");
		// 配合頁數切換,設定表格資料來源
		$b->beginRowsNum = $a->beginRowsNum;
		$b->setQuery("*","v_manage_role_zone","","",$a->beginRowsNum.",".$a->perPageRows);
		// 設定表格欄位顯示
		$b->setField("RoleID","角色代碼");
		$b->setField("RoleName","角色名稱");
		$b->setField("CompanyID","公司代碼");
		$b->setField("CompanyName","公司名稱");
		$b->setField("ZoneID","區域代碼");
		$b->setField("ZoneName","區域名稱");		
		// 增加按鈕及觸發事件
		$b->setFAMDButtonDisplay("N","Y","Y","Y","Y");
		// 設定表格底部顯示頁數切換
		$b->setTableBottom($a->getShowData());
		// 顯示表格資料
		$b->showData();
	}
?>
