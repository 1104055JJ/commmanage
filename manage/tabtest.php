<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>jQuery UI Tabs - Default functionality</title>
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link type="text/css" href="../css/icms.css" rel="stylesheet">    
    <link type="text/css" href="../css/jquery.datepick.css" rel="stylesheet">
    <link rel="stylesheet" href="../css/jquery-ui.min.css">
  <script type="text/javascript" src="../plugin/jquery/jquery.min.js"></script> 
  <script type="text/javascript" src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="../plugin/jquery/jquery.plugin.js"></script>  
  <script type="text/javascript" src="../plugin/jquery/jquery.datepick.js"></script>
  <script type="text/javascript" src="../plugin/jquery/jquery.datepick-zh-TW.js"></script>
  <script type="text/javascript" src="../js/ManageHomeJS.js"></script>
    <script type="text/javascript" src="../plugin/jquery/jquery-ui.min.js"></script>
    <script type="text/javascript" src="../plugin/jquery/datepicker-zh-TW.js"></script>
  <script>
$(function() {
  /*
    $( "#tabs" ).tabs({
      beforeLoad: function( event, ui ) {
        ui.jqXHR.fail(function() {
          ui.panel.html(
            "Couldn't load this tab. We'll try to fix this as soon as possible. " +
            "If this wouldn't be a demo." );
        });
      }
    });
*//*
  var tabTitle = $( "#tab_title" ),
      tabContent = $( "#tab_content" ),
      tabTemplate = "<li><a href='#{href}'>#{label}</a> <span class='ui-icon ui-icon-close' role='presentation'>Remove Tab</span></li>",
      tabCounter = 2;
      */
  var tabs = $( "#tabs" ).tabs({ 
    beforeLoad: function( event, ui ) {
        if ( ui.tab.data( "loaded" ) ) {
            event.preventDefault();
            return;
        }
 
        ui.jqXHR.success(function() {
            ui.tab.data( "loaded", true );
        });
    }
  });
      /*tabs.find( ".ui-tabs-nav" ).sortable({
      axis: "x",
      stop: function() {
        tabs.tabs( "refresh" );
      }})*/
// actual addTab function: adds new tab using the input from the form above
/*
  function addTab() {
    var label = tabTitle.val() || "Tab " + tabCounter,
      id = "tabs-" + tabCounter,
        li = $( tabTemplate.replace( /#\{href\}/g, "#" + id ).replace( /#\{label\}/g, label ) ),
        tabContentHtml = tabContent.val() || "Tab " + tabCounter + " content.";
 
      tabs.find( ".ui-tabs-nav" ).append( li );
      tabs.append( "<div id='" + id + "'><p>" + tabContentHtml + "</p></div>" );
      tabs.tabs( "refresh" );
      tabCounter++;
    }
*/
 /*
    // addTab button: just opens the dialog
    $("#add_tab").live("click", function(){
        var getImageID = $('.imageID',$(this).parent()).val();
            tabs.tabs('add', 'CM_Comm2.php' , '社區資料');
            //tabs.tabs('add', 'ajax.php?content=read&id=' + getImageID, 'Read Topic');
    });
*/

    $( "#add_tab" )
      .button()
      .click(function() {
        var num_tabs = $("div#tabs ul li").length ;
        //alert(num_tabs);
        $("div#tabs ul").append(
            //"<li><a href='#tab" + num_tabs + "'>#" + num_tabs + "</a><span class='ui-icon ui-icon-close' role='presentation'>Remove Tab</span></li>"
            "<li><a href='RPT_ReceiveYear_w.php'>收支</a><span class='ui-icon ui-icon-close' role='presentation'>Remove Tab</span></li>"

        );
        //$("div#tabs").append(
        //    "<div id='tab" + num_tabs + "'>#" + num_tabs + "test</div>"
        //);
        $("div#tabs").tabs("refresh");
        //var index = $('#tabs a[href="#RPT_ReceiveYear_w.php"]').parent().index();
        $("div#tabs").tabs("option", "active", num_tabs);
      });
 
     // close icon: removing the tab on click
    tabs.delegate( "span.ui-icon-close", "click", function() {
      var panelId = $( this ).closest( "li" ).remove().attr( "aria-controls" );
      $( "#" + panelId ).remove();
      tabs.tabs( "refresh" );
    });
 
    tabs.bind( "keyup", function( event ) {
      if ( event.altKey && event.keyCode === $.ui.keyCode.BACKSPACE ) {
        var panelId = tabs.find( ".ui-tabs-active" ).remove().attr( "aria-controls" );
        $( "#" + panelId ).remove();
        tabs.tabs( "refresh" );
      }
    });

  });
  </script>
</head>
<body>
<button id="add_tab">Add Tab</button>
<div id="dialog" title="Tab data" style="display:none">
  <form>
    <fieldset class="ui-helper-reset">
      <label for="tab_title">Title</label>
      <input type="text" name="tab_title" id="tab_title" value="Tab Title" class="ui-widget-content ui-corner-all">
      <label for="tab_content">Content</label>
      <textarea name="tab_content" id="tab_content" class="ui-widget-content ui-corner-all">Tab content</textarea>
    </fieldset>
  </form>
</div>

<div id="tabs">
  <ul>
    <li><a href="CM_Comm2.php">社區基本資料</a><span class="ui-icon ui-icon-close" role="presentation">Remove Tab</span></li>
    <li><a href="CM_HouseHold.php">住戶基本資料</a><span class="ui-icon ui-icon-close" role="presentation">Remove Tab</span></li>
    <li><a href="RPT_ReceiveYear_w.php">年度管理費總表</a><span class="ui-icon ui-icon-close" role="presentation">Remove Tab</span></li>
    <li><a href="RPT_Redistatement_w.php">收支明細表</a><span class="ui-icon ui-icon-close" role="presentation">Remove Tab</span></li>
  </ul>
</div>
 
 
</body>
</html>