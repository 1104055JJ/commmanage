<?php
	// 未登入則轉至首頁(登入頁)

	if (!isset($_SESSION)) { session_start(); }
	if (!(isset($_SESSION["manageuser"]))) {
		echo '<meta http-equiv=REFRESH CONTENT=1;url=index.php>';		
	} else {
		include("../common/connectdb.php");
		include("../common/AutoFormClass.php");
                
                //$Button='<Button type="Button" class="fa fa-search-plus" onclick="OpenWindow(\''.$_SESSION['Community'].'\')"></Button><em id=SupplierName> </em>';
                
		$a = new AutoFormClass("M","PA_Refund.php","PA_RefundAMD.php","refund_master","refund_master",array("CommID","PaymentNo","HouseHoldID"),"單頭基本資料","center","table90");
		$a->setTableTitle("left","Arial18Bold bg_gray");
		$a->setFieldTitle("center","Arial16 bg_y");
                $a->setField("CommID","社區","left","Arial14","N","N","","","N","N","N","N","text",array(array(),""),$_SESSION['Community']);
		$a->setField("PaymentNo","退款單號","left","Arial14","N","N","","","N","N","Y","N","text",array(array(),""),"","",array("autoserial","Y","N"));
		
                $a->setField("PayDate","退款日期","left","Arial14","Y","N","","","Y","Y","Y","Y","date",array(array(),""),"");
                $a->setField("IncomeItemID","收款類別","left","Arial14","Y","N","","","Y","N","Y","N","hidden",array(array(),""),"003");
                $x="select distinct  HouseHoldID,HouseHoldID from collectionsmaster where  incomeitemID='003' and  CommID='".$_SESSION['Community']."'";
                $a->setField("HouseHoldID","戶號","left","Arial14","Y","N","","","Y","Y","Y","N","select",array(array(),$x),"");
                $a->setField("SupplierID","供應商","left","Arial14","Y","N","","","Y","Y","Y","N","hidden",array(array(""),""),"");                                                                                                   //都要與TABLE欄位名稱相同//LOVName 為寫死於autodataclass button 後的text id,
                
                
                //$a->setField("PaymentItemID","支出項目","left","Arial14","Y","N","","<em id=PaymentItemName></em>","Y","Y","Y","Y","love",array(array("CommID","out","PaymentItemName","in","PaymentItemID","in","PA02","code"),""),"");
                $a->setField("Amount","退款(單身合計)金額","left","Arial14","N","Y","","","N","N","Y","N","text",array(array(),""),0);
                $a->setField("Rfee","社區負擔匯費","left","Arial14","Y","Y","","","Y","Y","Y","Y","text",array(array(),""),"");
                $a->setField("Acceptance","狀態","left","Arial14","Y","N","","","Y","N","Y","N","hidden",array(array(),""),"");
                $a->setField("Note","備註","left","Arial14","N","N","","","Y","Y","Y","Y","text",array(array(),""),"");
                $a->setField("Void","有效否","left","Arial14","Y","N","","","N","N","Y","Y","radio",array(array("是","Y","否","N"),""),"Y");
                $a->setField("ModUser","修改人員","left","Arial14","N","N","","","N","N","Y","N","text",array(array(),""),$_SESSION['manageuser']);
                $a->setField("ModDate","修改時間","left","Arial14","N","N","","","N","N","Y","N","date",array(array(),""),"","",array("datetime","Y","Y"));
               // $a->setField($fieldName, $displayName, $align, $class, $wrap, $isNumeric, $beforeHTML, $afterHTML, $A_Display, $A_CanEdit, $M_Display, $M_CanEdit, $inputType);
		//$a->addBeforeDeleteCheck("community",array("ZoneID","ZoneID"),"已被社區基本資料叫用,不可刪除!");
		//$a->setBeforeForm("Y","./PA_SearchFunction.php");
                $a->setBeforeForm("Y","./PA_RefundAMDJS.php");
                $a->showData();
	}
?>


