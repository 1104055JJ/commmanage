<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
	// 未登入則轉至首頁(登入頁)
	if (!isset($_SESSION)) { session_start(); }
	if (!(isset($_SESSION["manageuser"]))) {
		echo '<meta http-equiv=REFRESH CONTENT=1;url=index.php>';		
	}  else {
?>           
		<script type="text/javascript" src="../js/MasterJSB.js"></script>
             
               
		
<?php
		

		include("../common/connectdb.php");
		include("../common/AutoDataClassB.php");
		include("../common/AutoPageSwitchClass.php");	

		// 建立及初使化頁數切換
		$a = new AutoPageSwitchClass("M","PA_Request.php","SELECT COUNT(*) FROM paymentrequestmaster where CommID=".'"'.$_SESSION['Community']. '"',array(10,20,30,40));			
		$Society = "CommID='".$_SESSION['Community']."'"; 
		// 建立及初始化表格
		$b = new AutoDataClass("M","PA_Request.php","PA_RequestAMD.php",array("CommID","RequestNo","SupplierID"),"PA_RequestDetail.php","廠商請款作業","center","table90");
		$b->setTableTitle("left","Arial18Bold bg_gray");
		$b->setFieldTitle("center","Arial16 bg_y");	
		$b->setOddRow("","bg_light_blue");
		//$b->setEvenRow("","bg_y");
		// 配合頁數切換,設定表格資料來源
		$b->beginRowsNum = $a->beginRowsNum;
		$b->setQuery("CommID,RequestNo,SupplierID,SupplierName,TotalAmount,Petty,FlowStatus","v_payment_request_master","$Society","",$a->beginRowsNum.",".$a->perPageRows);
		// 設定表格欄位顯示
		$b->setAllFieldAlign("center");
		$b->setAllFieldClass("Arial14");
                $b->setFieldDisplayName("CommID","社區代號");
                $b->setFieldDisplay("CommID", "N");
		$b->setFieldDisplayName("RequestNo","請款單號");
                $b->setFieldDisplayName("SupplierID","供應商");
                $b->setFieldDisplayName("SupplierName","供應商名稱");
		$b->setFieldDisplayName("TotalAmount","請款金額");
                $b->setFieldDisplayName("Petty","零用金");
                $b->setFieldDisplayName("FlowStatus","狀態");
		// 增加按鈕及觸發事件
                $b->addButton("複製請款單","onclick=\"showMsgBox();\"","copy","btn btn-info");
		$b->addButton("請款單","onclick=\"openreport(1);\"","report","btn btn-info");
                $b->addButton("支出傳票","onclick=\"openreport(2);\"","report2","btn btn-info");
              //  $b->addButton($name, $event, $id, $class)
		// 設定表格底部顯示頁數切換
		$b->setTableBottom($a->getShowData());
		// 顯示表格資料
		$b->showData();
	}
?>

                <script type="text/javascript"> 
        function  openreport(c){
  //alert(document.forms["MasterForm"].elements["RequestNo"].value);
            var commID=document.forms["MasterForm"].elements["CommID"].value;
            var RequestNo=document.forms["MasterForm"].elements["RequestNo"].value;
            if(c==1)  {window.open("PA_RequestRpt.php?CommID="+commID+"&RequestNo="+RequestNo,"w",config="width=1000,height=500");}
            if(c==2)  {window.open("RPT_RequestVoucher_w.php?CommID="+commID+"&RequestNo="+RequestNo,"w",config="left=300,top=100,width=800,height=800");}
        }
            
	function showMsgBox() {
            $.msgBox({
            title: "複製單據",
            content: "確定依此請款單新增單據?",
            type: "confirm",
            buttons: [{ value: "是" }, { value: "否" }],
            success: function (result) {
            if (result == "是") {
            //alert("One cup of coffee coming right up!");
               var CommID=document.forms["MasterForm"].elements["CommID"].value;
               var RequestNo=document.forms["MasterForm"].elements["RequestNo"].value;
               
              $.ajax({
				url: "PA_Copy.php",
				type:"GET",
				dataType: "text",
				data:{CommID:CommID,RequestNo:RequestNo},
				//成功執行並返回值
				success: function(msg){
                                    var data=msg.split(",");
                                    if(data[0]==1)
                                    {
                                      alert("新增單據成功，"+"單號為"+data[1]);
                                      refreshDataM('PA_Request.php',10,10,1,'','','','',''); 
                                    }
                                    else
                                    {alert("新增單據失敗!");
                                    }
                                
				},
				//發送請求之前會執行的函式
				beforeSend:function(){
				},
				//請求完成時執行的函式(不論結果是success或error)
				complete:function(){
				},
				error:function(xhr, ajaxOptions, thrownError){ 
					alert(xhr.status);
					alert(thrownError);
				}
			});


                        }
                        }});
                           }	
    
               </script>   