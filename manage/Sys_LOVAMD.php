<?php
	// 未登入則轉至首頁(登入頁)
	if (!isset($_SESSION)) { session_start(); }
	if (!(isset($_SESSION["manageuser"]))) {
		echo '<meta http-equiv=REFRESH CONTENT=1;url=index.php>';		
	} else {
		include("../common/connectdb.php");
		include("../common/AutoFormClass.php");
		
		$a = new AutoFormClass("M","Sys_LOV.php","Sys_LOVAMD.php","syslov","syslov",array("code"),"區域基本資料","center","table90");
		$a->setTableTitle("left","Arial18Bold bg_gray");
		$a->setFieldTitle("center","Arial16 bg_y");
		$a->setButtonDefaultClass("Arial16Bold");
		$a->setField("code","代碼","left","Arial14","Y","N","","","Y","Y","Y","N","text",array(array(),""),"",' size="10" maxlength="10"');
		$a->setField("selectStr","select","left","Arial14","Y","N","","","Y","Y","Y","Y","textarea",array(array(),""),"",'size="60" ');
		$a->setField("whereStr","where","left","Arial14","Y","N","","","Y","Y","Y","Y","textarea",array(array(),""),"");
		$a->setField("note","note","left","Arial14","Y","N","","","Y","Y","Y","Y","text",array(array(),""),"");			
		//$a->addBeforeDeleteCheck("community",array("ZoneID","ZoneID"),"已被社區基本資料叫用,不可刪除!");
		//$a->setBeforeForm("N","測試Form之前要插入的程式碼");
		//$a->setAfterForm("N","AfterAutoform");		
		$a->showData();
	}
?>