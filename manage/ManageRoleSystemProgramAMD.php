<?php
	$err = "";
	// 未登入則轉至首頁(登入頁)
	session_start();
	if (!(isset($_SESSION["manageuser"]))) {
		echo '<meta http-equiv=REFRESH CONTENT=1;url=index.php>';		
	} else {
		include("../common/connectdb.php");
		//判斷要執行之功能 : Q->顯示資料  M->修改  MW->修改寫入  L->列表
		$f = "";
		$id = "";
		
		if (isset($_POST['f'])) {
			if ($_POST['f'] == "Q" || $_POST['f'] == "M" || $_POST['f'] == "MW" || $_POST['f'] == "L") {
				$f = $_POST['f'];
			} else {
				$f = "L";
			}
		} else {
			$f = "L";
		}
		
		if ($f == "Q" || $f == "M") {
			if (!isset($_POST['id'])) {
				$f = "L";
			} else {
				$idArray = explode("^A", $_POST['id']);
				$id = $idArray[1];
			}
		}
?>

<?php if ($f == "Q" || $f == "M") {
	$RoleName = "";
	$Enable = "";
	
	$strSQL = "SELECT * FROM manage_roleprogram WHERE RoleID='".$id."'";
	$rows = mysql_query($strSQL);
	$row = mysql_fetch_array($rows);
	
	global $mysql_link;
	if (mysql_errno($mysql_link) != 0) {
		$err .= mysql_errno($mysql_link).": ".mysql_error($mysql_link).chr(13);
	} else {
		$strSQL = "SELECT * FROM manage_role WHERE RoleID='".$id."'";
		$rolerows = mysql_query($strSQL);
		$rolerow = mysql_fetch_array($rolerows);
		$RoleName = $rolerow["RoleName"];
		$Enable = $rolerow["Enable"];
		if (mysql_errno($mysql_link) != 0) {
			$err .= mysql_errno($mysql_link).": ".mysql_error($mysql_link).chr(13);
		}
	}

	$fW = "";
	$fS = "";
	$fSS = "";
	$fV = "";
	if ($f == "Q") { $fW = "Q"; }
	if ($f == "M") { $fW = "MW"; $fS = "修改"; $fSS = "儲存"; }
	if ($err != "") {
		echo $err;
	} else {
?>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<form action="ManageRoleSystemProgramAMD.php" name="MasterForm" id="MasterForm" method="POST">
		<table id="MasterFormMainTable" align="center" class="table table-bordered table-condensed table-width-90 table-margin-0">
			<tr class="bg_gray">
				<td colspan="6">
					<table width="100%">
						<tr>
							<td class="font-18-bold align_left">
								<img id="m_FormTableImage" src="../images/icon/NT-Collapse.gif" onclick="foldTable('m_FormTable','m_FormTableDisplay','m_FormTableImage');">
								<?php echo '<font color="red">'.$fS.'</font>'; ?>系統程式權限維護作業(異動)
								<input name="f" id="f" value="<?php echo $fW; ?>" type="HIDDEN">
							</td>			
							<td class="font-18-bold align_right">
							<?php
								if ($f != "Q") {
									echo '<input type="Button" name="submit" value="'.$fSS.'" class="btn btn-danger" onclick="refreshDataMaster(\'ManageRoleSystemProgram.php\',\'ManageRoleSystemProgramAMD.php\',\'MW\',\'N\');">';
								}
							?>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr><td><div id="m_FormTable" style="display:;"><table class="table table-bordered table-condensed table-width-100 table-margin-0">
				<tr>
					<td class="font-16-bold bg_y align_center">角色代碼</td>
					<td class="font-16"><input name="RoleID" type="text" id="RoleID" value="<?php echo $id; ?>" readonly></td>
					<td class="font-16-bold bg_y align_center">角色名稱</td>
					<td class="font-16"><input name="RoleName" type="text" id="RoleName" value="<?php echo $RoleName; ?>" readonly></td>
					<td class="font-16-bold bg_y align_center">啟用</td>
					<td class="font-16"><input name="Enable" type="text" id="Enable" value="<?php echo $Enable; ?>" readonly></td>
				</tr>
				<tr>
					<td class="font-16-bold bg_y text-align-center">程式權限</td>
					<td colspan="5" align="left" class="font-16">
						<table class="table table-bordered table-condensed table-width-100 table-margin-0">
						<?php
							$strModule = "SELECT DISTINCT a.ModuleID,a.ModuleName FROM manage_module AS a,manage_program AS b WHERE a.Enable='Y' AND a.ModuleID=b.ModuleID AND b.ProgramType='S' ORDER BY a.ModuleSort";
							$rowsM = mysql_query($strModule);
							while (list($ModuleID,$ModuleName) = mysql_fetch_row($rowsM)) {
								echo '<tr><td class="font-16-bold align_center" bgcolor="#E8F3FF">'.$ModuleName.'</td><td><table class="table-condensed table-width-100 table-margin-0">';
								$j = 0;
								$strTemp = "SELECT * FROM manage_program WHERE ModuleID='".$ModuleID."' AND Enable='Y' AND ProgramType='S' ORDER BY ProgramSort";
								$rowsF = mysql_query($strTemp);
								while ($rowF = mysql_fetch_array($rowsF)) {
									$j = $j + 1;
									$strTemp = "SELECT RW FROM manage_roleprogram WHERE RoleID='".$id."' AND ProgramID='".$rowF['ProgramID']."'";
									$rowsTemp = mysql_query($strTemp);
									$i = mysql_num_rows($rowsTemp);
									$RW = "";
									if ($i > 0) {
										list($RW) = mysql_fetch_row($rowsTemp);
									}
									
									$html = "";
									if ($j % 4 == 1) { $html = $html.'<tr>'; }
									$html = '<td width="25%"><input name="ProgramID[]" type="checkbox" id="ProgramID" ';
									if ($f == "Q") { $html = $html." disabled "; }
									$html .= ' value="'.$rowF['ProgramID'].'" ';
									if ($i > 0) { $html = $html." checked "; }
									$html .= '>'.$rowF['ProgramName'].'<br>';
									// 是否可讀寫
									$html .= '<input name="RW[]" type="checkbox" id="RW" ';
									if ($f == "Q") { $html = $html." disabled "; }
									$html .= ' value="'.$rowF['ProgramID'].'" ';
									if ($RW == "W") { $html = $html." checked "; }
									$html .= '>可讀寫';
									$html .= '</td>';
									if ($j % 4 == 0) { $html = $html.'</tr>'; }
									echo $html;
								}
								// 補完剩下空的<td></td>
								if ($j % 4 != 0) {
									for ($k = 1; $k <= (4 - ($j % 4)) ; $k++) { 
										echo '<td></td>';
									}
									echo '</tr>';
								}
								echo '</table></td></tr>';
							}
						?>
						</table>
					</td>								
				</tr>
			</table></div></td></tr>
		</table>
	</form>
	<input id="m_FormTableDisplay" name="m_FormTableDisplay" value="Y" type="HIDDEN">	
<?php }} ?>
				
<?php		
		// 修改儲存
		if ($f == "MW") {
			// 變更Table:manage_roleprogram資料(該管理者可用 之程式)
			// 先刪除
			$strTemp = "DELETE FROM manage_roleprogram WHERE RoleID='".$_POST['RoleID']."'";
			mysql_query($strTemp);
			global $mysql_link;
			if (mysql_errno($mysql_link) != 0) {
				$err .= "刪除資料時發生錯誤 !".chr(13);
				$err .= mysql_errno($mysql_link).": ".mysql_error($mysql_link).chr(13);
			} else {
				// 再新增
				if (isset($_POST['ProgramID']) && sizeof($_POST['ProgramID']) > 0) {
					for ($i = 0; $i < sizeof($_POST['ProgramID']); $i++) {
						$strTemp = "SELECT COUNT(*) FROM manage_program WHERE ProgramID='".$_POST['ProgramID'][$i]."'";
						$rowsTemp = mysql_query($strTemp);
						list($count) = mysql_fetch_row($rowsTemp);
						if ($count > 0) {
							$RW = "R";
							if (isset($_POST['RW'])) {
								for ($j = 0; $j < sizeof($_POST['RW']); $j++) { 
									if ($_POST['RW'][$j] == $_POST['ProgramID'][$i]) {
										$RW = "W";
										break;
									}
								}
							}
							$strTemp = "INSERT INTO manage_roleprogram (RoleID,ProgramID,RW) VALUES ('".$_POST['RoleID']."','".$_POST['ProgramID'][$i]."','".$RW."')";
							mysql_query($strTemp);
							if (mysql_errno($mysql_link) != 0) {
								$err .= "新增資料時發生錯誤 !".chr(13);
								$err .= mysql_errno($mysql_link).": ".mysql_error($mysql_link).chr(13);
							}							
						}
					}
				}
			}
			$f = "L";
		}

		// L : 回列表 
		if ($f == "L") {
			echo $err;
		}
	}
?>