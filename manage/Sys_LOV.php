<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
	// 未登入則轉至首頁(登入頁)
	if (!isset($_SESSION)) { session_start(); }
	if (!(isset($_SESSION["manageuser"]))) {
		echo '<meta http-equiv=REFRESH CONTENT=1;url=index.php>';		
	} else {
?>
		<script type="text/javascript" src="../js/MasterJSB.js"></script>
		<!--<script language="JavaScript">refreshDataMAMD('testAMD.php','A','');</script>-->
<?php
	

		include("../common/connectdb.php");
		include("../common/AutoDataClassB.php");
		include("../common/AutoPageSwitchClass.php");	

		// 建立及初使化頁數切換
		$a = new AutoPageSwitchClass("M","Sys_LOV.php","SELECT COUNT(*) FROM syslov",array(5,10,15,20));			
		
		// 建立及初始化表格
		$b = new AutoDataClass("M","Sys_LOV.php","Sys_LOVAMD.php",array("code"),"Sys_LOV.php","小幫手維護作業","center","table90");
		$b->setTableTitle("left","Arial18Bold bg_gray");
		$b->setFieldTitle("center","Arial16 bg_y");	
		$b->setOddRow("","bg_light_blue");
		//$b->setEvenRow("","bg_y");
		// 配合頁數切換,設定表格資料來源
		$b->beginRowsNum = $a->beginRowsNum;
		$b->setQuery("*","syslov","","",$a->beginRowsNum.",".$a->perPageRows);
		// 設定表格欄位顯示
		$b->setAllFieldAlign("center");
		$b->setAllFieldClass("Arial14");
		$b->setField("code","代碼","center","Arial14","Y","Y","","","");
		//$b->setFieldDisplayName("ZoneID","區域代碼");
		$b->setFieldDisplayName("selectStr","select");
		$b->setFieldDisplayName("whereStr","where");
		$b->setFieldDisplayName("note","note");		
		// 增加按鈕及觸發事件
		$b->setDefaultFieldClass("Arial16Bold");
		// 設定表格底部顯示頁數切換
		$b->setTableBottom($a->getShowData());
		// 顯示表格資料
		$b->showData();
	}
?>