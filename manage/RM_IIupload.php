<?php
	session_start();
 	include("../common/connectdb.php");
?>
<HTML>
  <head>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <META http-equiv="refresh" content="60">
  <title>檔案彙入</title>
		<link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" media="screen">
		<link type="text/css" href="../css/icms.css" rel="stylesheet">		
		<link type="text/css" href="../css/jquery.datepick.css" rel="stylesheet">
  </head>
	<script type="text/javascript" src="../plugin/jquery/jquery.min.js"></script>	
	<script type="text/javascript" src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="../plugin/jquery/jquery.plugin.js"></script>	
	<script type="text/javascript" src="../plugin/jquery/jquery.datepick.js"></script>
	<script type="text/javascript" src="../plugin/jquery/jquery.datepick-zh-TW.js"></script>
	<script type="text/javascript" src="../js/ManageHomeJS.js"></script>
  <BODY leftmargin=0 topmargin=0>
		<input id="jsonID" name="jsonID" value="" type="HIDDEN">
		<input id="formID" name="formID" value="" type="HIDDEN">		
		<div align="left">  
			<div align="center" style="background-color: #337ab7">
				<p></p>
				<?php include("TopMenu.php"); ?>
			</div>		
			<hr align="center" width="100%">
			<table width="100%" border=0 cellpadding=0 cellspacing=0>
				<tr align="left" valign="top">
					<!--  CenterZone  -->
					<td width="100%">
						<table width="100%" border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td id="tr_CenterZoneF" align="left" valign="top" style"display:;">
									<div id="CenterZoneMAMD"></div>
								</td>
							</tr>
							<tr>
								<td id="tr_CenterZoneM" align="left" valign="top" style"display:;">
									<div id="CenterZoneM">
										<?php include("RM_IncomeItemupload.php"); ?>									
									</div>	
								</td>
							</tr>              
						</table>
						<br>            
						<table width="100%" border=0 cellpadding=0 cellspacing=0>
							<tr id="tr_CenterZoneDAMD" align="left" valign="top" style="display:;">
								<td>
									<div id="CenterZoneDAMD"></div>
								</td>
							</tr>														
							<tr id="tr_CenterZoneD" align="left" valign="top" style"display:;">
								<td>
									<div id="CenterZoneD"></div>
								</td>
							</tr>
						</table>
					</td>
					<!--  CenterZone  -->
  			</tr>
			</table>
		</div>
 	</BODY>
</HTML>
