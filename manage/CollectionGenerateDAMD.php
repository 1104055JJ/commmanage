<?php
	//$err = "";
	// 未登入則轉至首頁(登入頁)
	session_start();
	if (!(isset($_SESSION["manageuser"]))) {
		echo '<meta http-equiv=REFRESH CONTENT=1;url=index.php>';		
	} else {
		include("../common/connectdb.php");
 		include("../common/PublicFunction.php");
		include("../common/AutoFormClass.php");

  //function __construct($MasterOrDetail,$dataFile                ,$formFile                   ,$tableName           ,$tableKey                     ,$table_Title  ,$table_Align,$table_Class)
  //$a = new AutoFormClass("D"            ,"CollectionGenerateD.php","CollectionGenerateDAMD.php","collectionsdetail"  ,array("CommID","CollectionsNo","DetailNo"),"收款內容明細","center"    ,"table90");
    $a = new AutoFormClass("D"            ,"CollectionGenerateD.php","CollectionGenerateDAMD.php","collectionsdetail","collectionsdetail",array("CommID","CollectionsNo","DetailNo"),"收款內容明細","center"    ,"table90");
		$a->setTableTitle("left","Arial18Bold bg_gray");
		$a->setFieldTitle("center","Arial16 bg_y");
//150119    
 // $a->setField($fieldName      , $displayName,$align, $class  ,$wrap,$isNumeric,$beforeHTML,$afterHTML,$A_Display,$A_CanEdit,$M_Display,$M_CanEdit,$inputType,$defaultValue       )
    $Comm = "".$_SESSION['Community']."";
    $a->setField("CommID"        ,"社區代號"   ,"left","Arial14","N"  ,"N"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"N"       ,"Hidden"  ,array(array(),""),"$Comm", 'required');
    $a->setField("CollectionsNo" ,"收款編號"   ,"left","Arial14","N"  ,"N"       ,""         ,""        ,"Y"       ,"N"       ,"Y"       ,"N"       ,"Hidden"  ,array(array(),""),"",'required');
    $a->setField("DetailNo"      ,"明細項次"   ,"left","Arial14","N"  ,"Y"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"N"       ,"Hidden"  ,array(array(),""),"",'required');
    $FillterArNo = "Select ArNo, ArNo From ar where UnReceived>0 and CommID ='".$_SESSION['Community']."'";
    $a->setField("ArNo"          ,"賬單編號"   ,"left","Arial14","N"  ,"N"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"Y"       ,"select"  ,array(array(),$FillterArNo),"",'size="10" maxlength="10" required');
  //$a->setField("Year"          ,"年份"       ,"left","Arial14","N"  ,"Y"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"Y"       ,"text"    ,array(array(),""),"",'size="10" maxlength="10"');
  //$a->setField("Month"         ,"月份"       ,"left","Arial14","N"  ,"Y"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"Y"       ,"text"    ,array(array(),""),"",'size="10" maxlength="10"');
    $a->setField("HouseFee"      ,"管理費"     ,"left","Arial14","N"  ,"Y"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"Y"       ,"text"    ,array(array(),""),"",'size="10" maxlength="10"');
    $a->setField("CarFee"        ,"車位清潔費" ,"left","Arial14","N"  ,"Y"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"Y"       ,"text"    ,array(array(),""),"",'size="10" maxlength="10"');
    $a->setField("Discount"      ,"折扣總額"   ,"left","Arial14","N"  ,"Y"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"Y"       ,"text"    ,array(array(),""),"",'size="10" maxlength="10"');
    $a->setField("TotalAmount"   ,"總計金額"   ,"left","Arial14","N"  ,"Y"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"Y"       ,"text"    ,array(array(),""),"",'size="10" maxlength="10"');
    $a->setField("Received"      ,"已收金額"   ,"left","Arial14","N"  ,"Y"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"Y"       ,"text"    ,array(array(),""),"",'size="10" maxlength="10"');
    $a->setField("UnReceived"    ,"未收金額"   ,"left","Arial14","N"  ,"Y"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"Y"       ,"text"    ,array(array(),""),"",'size="10" maxlength="10"');
    $a->setField("Note"          ,"收款備註"   ,"left","Arial14","N"  ,"N"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"Y"       ,"Hidden"  ,array(array(),""),"");
		$a->setField("WriteOffAmount","沖銷金額"   ,"left","Arial14","N"  ,"Y"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"Y"       ,"text"    ,array(array(),""),"",'size="10" maxlength="10"');
		$a->setField("ModDate"       ,"修改日期"   ,"left","Arial14","N"  ,"N"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"Y"       ,"Hidden"  ,array(array(),""),"",'required');
    $User="".$_SESSION["manageuser"]."";
		$a->setField("ModUser"       ,"修改人員"   ,"left","Arial14","N"  ,"N"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"Y"       ,"Hidden"  ,array(array(),""),"$User", 'required');                        
 		$a->setBeforeForm("Y","./CollectionGenerateDAMDJS.php");
    $a->setAfterForm("Y","./CollectionGenerateMAMDJS.php");
		//$a->addBeforeDeleteCheck("community",array("ZoneID","ZoneID"),"已被社區基本資料叫用,不可刪除!");
		$a->showData();
	}
?>