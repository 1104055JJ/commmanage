<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
	// 未登入則轉至首頁(登入頁)
	if (!isset($_SESSION)) { session_start(); }
	if (!(isset($_SESSION["manageuser"]))) {
		echo '<meta http-equiv=REFRESH CONTENT=1;url=index.php>';		
	}  else {
?>
		<script type="text/javascript" src="../js/MasterJSB.js"></script>
		<!--<script language="JavaScript">refreshDataMAMD('testAMD.php','A','');</script>-->
<?php
		

		include("../common/connectdb.php");
		include("../common/AutoDataClassB.php");
		include("../common/AutoPageSwitchClass.php");	

		// 建立及初使化頁數切換
		$a = new AutoPageSwitchClass("M","PA_Supplier.php","SELECT COUNT(*) FROM supplier where CommID=".'"'.$_SESSION['Community']. '"',array(10,20,30,40));			
		$Society = "CommID='".$_SESSION['Community']."'"; 
		// 建立及初始化表格
		$b = new AutoDataClass("M","PA_Supplier.php","PA_SupplierAMD.php",array("CommID","SupplierID"),"","供應商維護作業","center","table90");
		$b->setTableTitle("left","Arial18Bold bg_gray");
		$b->setFieldTitle("center","Arial16 bg_y");	
		$b->setOddRow("","bg_light_blue");
		//$b->setEvenRow("","bg_y");
		// 配合頁數切換,設定表格資料來源
		$b->beginRowsNum = $a->beginRowsNum;
		$b->setQuery("CommID,SupplierID,SupplierName,IdNo,Tel,CellPhone","supplier","$Society","",$a->beginRowsNum.",".$a->perPageRows);
		// 設定表格欄位顯示
		$b->setAllFieldAlign("center");
		$b->setAllFieldClass("Arial14");
                $b->setFieldDisplayName("CommID","社區代號");
              //  $b->setFieldDisplay("CommID", "N");
		$b->setFieldDisplayName("SupplierID","供應商代碼");
                $b->setFieldDisplayName("SupplierName","供應商名稱");
                //$b->setFieldDisplay("Type","N");
                $b->setFieldDisplayName("IdNo","統編/身分證");
                //$b->setFieldDisplay("Boss","N");
		$b->setFieldDisplayName("Tel","電話");
                $b->setFieldDisplayName("CellPhone","手機");
                /*$b->setFieldDisplay("Address","N");
                $b->setFieldDisplay("Enable","N");
                $b->setFieldDisplay("Note","N");
                $b->setFieldDisplay("ModUser","N");
                $b->setFieldDisplay("ModDate","N");
		 增加按鈕及觸發事件*/
		
                $b->addButton("匯入Excel","onclick=\"openDialogWindow('匯入Excel','./upload.php?url=PA_Supplierupload.php');\"","upexcel","btn btn-info");
                $b->addButton("供應商報表","onclick=\"openreport();\"","report","btn btn-info");
// 設定表格底部顯示頁數切換
		$b->setTableBottom($a->getShowData());
		// 顯示表格資料
		$b->showData();
	}
?>