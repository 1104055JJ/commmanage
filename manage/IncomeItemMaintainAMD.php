<?php
	//$err = "";
	// 未登入則轉至首頁(登入頁)
	session_start();
	if (!(isset($_SESSION["manageuser"]))) //&& isset($_SESSION["SubMenu"]) && isset($_SESSION["ProgramItem"]))) 
  {
		echo '<meta http-equiv=REFRESH CONTENT=1;url=index.php>';		
	} else {
		include("../common/connectdb.php");
		include("../common/PublicFunction.php");
		include("../common/AutoFormClass.php");    
    
		$a = new AutoFormClass("M","IncomeItemMaintain.php","IncomeItemMaintainAMD.php","incomeitem","incomeitem",array("CommID","IncomeItemID"),"收入項目維護","center","table90");
		$a->setTableTitle("left","Arial18Bold bg_gray");
		$a->setFieldTitle("center","Arial16 bg_y");
 		//$a->setButtonDefaultClass("Arial16Bold");
 // $a->setField($fieldName      , $displayName , $align, $class  ,$wrap,$isNumeric,$beforeHTML,$afterHTML,$A_Display,$A_CanEdit,$M_Display,$M_CanEdit,$inputType,$defaultValue)
    //$FilterComm = "Select CommName,CommID From Community Where CommID='".$_SESSION['Community']."'";
    $Comm = "".$_SESSION['Community']."";
		$a->setField("CommID"        ,"社區代碼"    ,"left" ,"Arial14","N"  ,"N"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"N"       ,"Hidden"  ,array(array(),""),"$Comm", 'required');
		$a->setField("IncomeItemID"  ,"收入代號"    ,"left" ,"Arial14","N"  ,"N"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"N"       ,"text"    ,array(array(),""),"",'size="3" maxlength="3" required');
		$a->setField("IncomeItemName","收入項目名稱","left" ,"Arial14","N"  ,"N"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"N"       ,"text"    ,array(array(),""),"",'size="20" maxlength="45" required');
		$a->setField("IncomeItemShow","收入顯示名稱","left" ,"Arial14","Y"  ,"N"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"Y"       ,"text"    ,array(array(),""),"",'size="20" maxlength="45" required');
		$a->setField("Enable"        ,"使用狀態"    ,"left" ,"Arial14","N"  ,"N"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"Y"       ,"radio"   ,array(array("啟用","Y","停用","N"),""),"",'required');
		$a->setField("Note"          ,"備註"        ,"left" ,"Arial14","Y"  ,"N"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"Y"       ,"text"    ,array(array(),""),"",'size="87" maxlength="100" ');
    $a->setAfterForm("Y","./IncomeItemMaintainAMDJS.php");
    		//$a->setAfterForm("Y","./CM_Comm2JS.php");
		//$a->setField("TEL","電話","left","Arial14","N","N","","","Y","Y","Y","Y","text",array(array(),""),"");
		//$a->setField("FAX","傳真","left","Arial14","Y","N","","","Y","Y","Y","Y","text",array(array(),""),"");
		//$a->setField("Address","地址","left","Arial14","Y","N","","","Y","Y","Y","Y","text",array(array(),""),"");
		//$a->setField("Note","備註","left","Arial14","Y","N","","","Y","Y","Y","Y","text",array(array(),""),"");
		//$a->setField("CashAllowZero","零用金允許為零","left","Arial14","Y","N","","","Y","Y","Y","Y","select",array(array(),"Select CodeID,CodeName from unifieddatacode where catagoryid='A02'"),"Y");
		//$a->setField("CoverMethod","沖銷順序","left","Arial14","N","N","","","Y","Y","Y","Y","select",array(array("先房後車","1","先車後房","2"),""),"1");
		//$a->setField("CashAllowZero","零用金允許為零","left","Arial14","Y","Y","","","Y","Y","Y","Y","text",array(array(),""),"");
		//$a->setField("CommID","社區代碼","left","Arial14","N","N","","","Y","Y","Y","N","text",array(array(),""),"");
	//	$a->addBeforeDeleteCheck("HouseHold",array("CommID","CommID"),"已被戶別基本資料叫用,不可刪除!");
		$a->showData();
	}
?>