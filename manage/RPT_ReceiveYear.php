<?php
include("../common/connectdb.php");	
session_start();
$CommID=$_SESSION['Community'];
$year = $_REQUEST['Year'];
for($i=1;$i<=12;$i++){
	$sum_received[$i]=0;
	$sum_unreceived[$i]=0;
}
$sum_area=0;
$sum_fee=0;
$sum_carfee=0;
$sum_allfee=0;
//echo $sum_reveived[1];

//標題
$sql = "Select CommName From community Where CommID ='".$CommID."'";
$rows=mysql_query($sql);
$row=mysql_fetch_array($rows);
$CommName=$row['CommName'];
$Title = $CommName."社區".$year."年度管理費繳交總表";
/*
//找出當年度最多管理費月份
$sql = "select max(month) from ar ".
	   " where commid='".$_SESSION['Community']."' and year =".$year."-2000";
$rows = mysql_query($sql) ;
$row = mysql_fetch_array($rows);
//echo $row["max(month)"];
*/

//$html = "<h2 align=\"center\">".$row['CommName']."社區".$year."年度管理費繳交總表</h2>";
$html ="
<table border=\"1\" cellpadding=\"4\" align=\"center\">
<thead>
	<tr bgcolor=\"#e6e6fa\" align=\"center\">
		<td width=\"40\">戶別</td>
		<td width=\"50\">繳款人</td>
		<td width=\"40\">坪數</td>
		<td width=\"40\">管理費</td>
		<td width=\"40\">車位<br>編號</td>
		<td width=\"40\">車位<br>清潔費</td>
		<td width=\"40\">應收<br>總計</td>";
	for ($i=1;$i<=12;$i++){
		$html.="<td width=\"40\">".$i."月</td>";
	}
	$html.="</tr></thead>";

//住戶
$sql = "select HouseHoldID,Payer,Owner,Renter,Area,Fee,Discount,NetFee from household".
	   " where commid='".$_SESSION['Community']."'";
//echo $sql;
$rows = mysql_query($sql) ;
while($row = mysql_fetch_array($rows)){

	$html.="
	<tr>
		<td width=\"40\">".$row["HouseHoldID"]."</td>";
		if ($row["Payer"] == '1'){
			$html.="<td width=\"50\">".$row["Owner"]."</td>";
			}else{$html.="<td width=\"50\">".$row["Renter"]."</td>";}
		$html.="
		<td align=\"right\" width=\"40\">".$row["Area"]."</td>
		<td align=\"right\" width=\"40\">".number_format($row["NetFee"], 0, '.', ',')."</td>";
		$sum_area=$sum_area+$row["Area"];
		$sum_fee +=$row["NetFee"];
	//車位
	$sql_car = "select CarID,NetFee from carspace ".
	   " where commid='".$_SESSION['Community']."'".
	   " and HouseHoldID ='".$row["HouseHoldID"]."'";
	//echo $sql_car;
	$rows_car = mysql_query($sql_car) ;
	$car_id ="";
	$car_fee="";
	$count = 1;
	$car_fee_total = 0;
	while($row_car = mysql_fetch_array($rows_car)){
		if ($count == 1) {
			$car_id .= $row_car["CarID"];
			$car_fee .= number_format($row_car["NetFee"], 0, '.', ',');
			$car_fee_total = $car_fee_total + $row_car["NetFee"];
		}else{
		$car_id .= "<br>".$row_car["CarID"];
		$car_fee .= "<br>".number_format($row_car["NetFee"], 0, '.', ',');
		$car_fee_total = $car_fee_total + $row_car["NetFee"];
		}

		$count = $count +1;
	}
	$Fee_total = $car_fee_total + $row["Fee"];
	$sum_carfee += $car_fee_total;
	$sum_allfee += $Fee_total;
	$html.="
		<td align=\"left\" width=\"40\">".$car_id."</td>
		<td align=\"right\" width=\"40\">".$car_fee."</td>
		<td align=\"right\" width=\"40\">".number_format($Fee_total, 0, '.', ',')."</td>";	
	//各月管理費
	for ($i=1;$i<=12;$i++){
		$sql_ar = "select Month,Received,UnReceived from ar ".
	   	" where commid='".$_SESSION['Community']."' and Year = ".$year."-2000".
	   	" and HouseHoldID ='".$row["HouseHoldID"]."' and Month=".$i;
		if ($rows_ar = mysql_query($sql_ar)){
			$row_num = mysql_num_rows($rows_ar);
			if($row_num > 0) {
				$row_ar = mysql_fetch_array($rows_ar);
				$html.="<td align=\"right\" width=\"40\">".number_format($row_ar["Received"], 0, '.', ',')."</td>";
				$sum_received[$i] = $sum_received[$i] + $row_ar["Received"];
				$sum_unreceived[$i] = $sum_unreceived[$i] + $row_ar["UnReceived"];
			}else{
				$html.="<td align=\"right\" width=\"40\">0</td>";
			}
		}else{
			$html.="<td align=\"right\" width=\"40\">0</td>";
		}
	}
	$html.="</tr>";
}
//已收合計
$html.="
   <tr bgcolor=\"#e6e6fa\">
   	<td>合計</td>
   	<td></td>
   	<td align=\"right\">".number_format($sum_area, 2, '.', ',')."</td>
   	<td align=\"right\">".number_format($sum_fee, 0, '.', ',')."</td>
   	<td></td>
   	<td align=\"right\">".number_format($sum_carfee, 0, '.', ',')."</td>
   	<td align=\"right\">".number_format($sum_allfee, 0, '.', ',')."</td>
   ";
for ($i=1;$i<=12;$i++){
	$html.="<td align=\"right\">".number_format($sum_received[$i], 0, '.', ',')."</td>";
}
$html.="</tr>";
 //應收未收合計
$html.="
   <tr bgcolor=\"#e6e6fa\">
   	<td width=\"90\">預收款合計</td>
   	<td width=\"40\" align=\"right\"></td>
   	<td></td>
   	<td></td>
   	<td  width=\"80\" COLSPAN=3>應收未收合計</td>";
for ($i=1;$i<=12;$i++){
	$html.="<td align=\"right\">".number_format($sum_unreceived[$i], 0, '.', ',')."</td>";
}
$html.="</tr>";  
$html.="</table>";

if ($_REQUEST['FORMAT']=="HTML"){
	echo '<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="zh-tw" lang="zh-tw">';
	echo '<head>';
	echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
	echo '</head>';
	echo '<h3 align="center">'.$Title.'</h3>';
	echo $html;
}

if ($_REQUEST['FORMAT']=="PDF"){
	
require_once('../tcpdf/config/zho.php');
require_once('../tcpdf/config/tcpdf_config.php');
require_once('../tcpdf/tcpdf.php');
//客製頁首頁尾
// Extend the TCPDF class to create custom Header and Footer
class MYPDF extends TCPDF {
	var $HeaderTitle;
	//Page header
	function Header() {
		// Logo
		//$image_file = K_PATH_IMAGES.'logo_example.jpg';
		//$this->Image($image_file, 10, 10, 15, '', 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);
		// Set font
		$this->SetFont('droidsansfallback', 'B', 18);
		// Title
		$this->Cell(0, 15,$this->HeaderTitle, 0, false, 'C', 0, '', 0, false, 'M', 'T');
		$this->SetTopMargin($this->GetY());
	}

	// Page footer
	function Footer() {
		// Position at 15 mm from bottom
		$this->SetY(-15);
		// Set font
		$this->SetFont('droidsansfallback', 'I', 14);
		//$this->setFooterMargin(80);
		// Page number
		$this->Cell(0, 10, '主任委員：　 　                 監察委員:　 　                財務委員：　 　                現場主任:　 　                ', 0, false, 'C', 0, '', 0, false, 'T', 'M');
	}
}

//實體化PDF物件
$pdf = new MYPDF("L", PDF_UNIT,'A4', true, 'UTF-8', false);
//$pdf = new TCPDF("L", PDF_UNIT,"A4", true, 'UTF-8', false);
$pdf->setPrintHeader(true); //頁首
$pdf->setPrintFooter(true); //頁尾
$pdf->HeaderTitle = $Title;
// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(15);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
//$pdf->SetHeaderData("", "", $Title, "", array(0,64,255), array(0,64,128));
//$pdf->SetHeaderFont(Array('droidsansfallback', 'B', 14));
//$pdf->SetFooterFont(Array('droidsansfallback', 'B', 14));

$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);  //設定自動分頁

$pdf->setLanguageArray($l); //設定語言相關字串

$pdf->setFontSubsetting(true); //產生字型子集（有用到的字才放到文件中）

$pdf->SetFont('droidsansfallback', '', 10, '', true); //設定字型

$pdf->AddPage(); //新增頁面

$pdf->setTextShadow(array('enabled'=>false, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));//文字陰影
	
	
	$pdf->writeHTML($html);
	$pdf->Output('contact.pdf', 'I');	
}


if ($_REQUEST['FORMAT']=="EXCEL"){

require_once '../Excel/PHPExcel.php';    //引入 PHPExcel 物件庫
require_once '../Excel/PHPExcel/IOFactory.php';    //引入 PHPExcel_IOFactory 物件庫
$objPHPExcel = new PHPExcel();  //實體化Excel
//----------內容-----------//

$objPHPExcel->setActiveSheetIndex(0);  //設定預設顯示的工作表
$objActSheet = $objPHPExcel->getActiveSheet(); //指定預設工作表為 $objActSheet
$objActSheet->setTitle("管理費收款明細表");  //設定標題
$objPHPExcel->createSheet(); //建立新的工作表，上面那三行再來一次，編號要改

$objActSheet->getColumnDimension('A')->setWidth(20);
$objActSheet->getColumnDimension('B')->setWidth(20);
$objActSheet->getColumnDimension('C')->setWidth(20);
$objActSheet->getColumnDimension('D')->setWidth(20);
$objActSheet->getColumnDimension('E')->setWidth(20);


//$objPHPExcel->getDefaultStyle()->getFont()->setName('標楷體');
//$objPHPExcel->getDefaultStyle()->getFont()->setSize(16);


$objActSheet-> getStyle('A1:E1')-> getFont()-> setName('SimHei')-> setSize('14');
$objActSheet-> getStyle('A1:E1')-> getFill()-> setFillType(PHPExcel_Style_Fill:: FILL_SOLID)-> getStartColor()-> setARGB('FFC9E3F3');


$objActSheet->setCellValue("A1", '入帳日期')
            ->setCellValue("B1", '客戶編號')
            ->setCellValue("C1", '客戶名稱')
            ->setCellValue("D1", '金額')
            ->setCellValue("E1", '備註');


//$sql = "select * from `".$xoopsDB->prefix("contact")."` ";
//$result = $xoopsDB->query($sql) or redirect_header($_SERVER['PHP_SELF'],3, mysql_error());
$rows = mysql_query($sql) ;
$i=2;
while($row = mysql_fetch_array($rows)){
	
  $objActSheet->setCellValue("A{$i}", $row["DueDate"])
              ->setCellValue("B{$i}", $row["HouseHoldID"]);
              //->setCellValueExplicit("C{$i}", $tel , PHPExcel_Cell_DataType:: TYPE_STRING)
        if ($row["Payer"] == '1'){
        	$objActSheet->setCellValue("C{$i}", $row["Owner"]);
			}else{
			$objActSheet->setCellValue("C{$i}", $row["Renter"]);}
   $objActSheet->setCellValue("D{$i}", $row["IncomeAmount"])
              ->setCellValue("E{$i}", $row["Note"]);
  $i++;
}


$objActSheet->mergeCells("A{$i}:C{$i}")->setCellValue("A{$i}", '本期其他收入共計');
$n=$i-1;
$objActSheet->setCellValue("D{$i}", "=SUM(D2:D{$n})");

//$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
//$objPHPExcel->getActiveSheet()->protectCells("E{$i}", '12345');

header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename='.iconv('UTF-8','Big5','收入明細').'.xls');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->setPreCalculateFormulas(false);
$objWriter->save('php://output');
exit;
}
?>