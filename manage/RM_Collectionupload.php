<?php
 	include("../common/connectdb.php");
	include("../common/PublicFunction.php");
	session_start();
	//取得預設值
    $society = $_SESSION['Community'];
	  $user = $_SESSION['manageuser'];
	  $datetime = date ("Y-m-d H:i:s"); 
	  $chk_error = FALSE;
	  $err_msg=''; 
    $SuccessFlag=1; 
  
      require_once '../Excel/reader.php';
	    $dir = './uploads/';      
	    $filename=$_REQUEST['filename'];
	    $filename = $dir.iconv("utf-8", "big5", $filename);

      $data = new Spreadsheet_Excel_Reader();
      $data->setOutputEncoding('UTF-8');
      $data->read($filename);
      error_reporting(E_ALL ^ E_NOTICE);      
	    for ($i = 1; $i <= $data->sheets[0]['numRows']; $i++) 
	    { $SumERR=0;
        $ERR00=0;
        $ERR10=0;
        $ERR20=0;
        $ERR30=0;
        $ERR40=0;
        $ERR50=0;
        $ERR51=0;
        $ERR60=0;
        $ERR70=0;
        $ERR80=0;
        $ERR90=0;
        $ERRA0=0;
        $ERRB0=0;
        $ERRC0=0;                        
		    if($i!=1)
		    {
           $CommID=$data->sheets[0]['cells'][$i][2];
           $CheckSQL = "SELECT CoverMethod FROM community Where CommID ='".$CommID."'";
           $Checkresults = mysql_query($CheckSQL);           
           if (mysql_num_rows($Checkresults) <= 0)
           {$ERR00=1;}
           $serial = date("ym");
           $strSQL = "SELECT Max(CollectionsNo) FROM collectionsmaster Where CommID ='".$CommID."'";
           $rows = mysql_query($strSQL);
           if (mysql_num_rows($rows) > 0) 
           {			$row = mysql_fetch_array($rows);
									if (substr($row[0],0,4) == $serial) 
                  { // 不換年月最大號+1
                    $serial = $row[0] + 1;
									} 
                  else 
                  {	// 換年月重新給號
										$serial .= "0001";}
					 } 
           else
           {$serial .= "0001";}
          
           $CollectionsNo= $serial;
           $CollectionDate=date("Y-m-d");
           $HouseHoldID=$data->sheets[0]['cells'][$i][3];
           //檢查是否資料結束
           if ($CommID=="" & $HouseHoldID=="")
           {break;}
           //檢查門牌戶號是否正確           
           if ($HouseHoldID=="")
           {$ERR10=1;}             
           $CheckSQL = "SELECT Payer FROM household Where CommID ='".$CommID."' and HouseHoldID ='".$HouseHoldID."'";
           $Checkresults = mysql_query($CheckSQL);           
           if (mysql_num_rows($Checkresults) <= 0)
           {$ERR10=1;}           
           $PaymentType=$data->sheets[0]['cells'][$i][4];
           if ($PaymentType=="")
           {$PaymentType=0;}               
           //檢查收入代號是否正確  
           $IncomeItemID=$data->sheets[0]['cells'][$i][5];
           if ($IncomeItemID=="")
           {$ERR20=1;} 
           if(is_numeric($IncomeItemID)==True and $IncomeItemID< 100)
           {$IncomeItemID=str_pad((string)$IncomeItemID,3,"0",STR_PAD_LEFT);}
           $CheckSQL = "SELECT IncomeItemName FROM incomeitem Where CommID ='".$CommID."' and IncomeItemID ='".$IncomeItemID."'";
           $Checkresults = mysql_query($CheckSQL); 
           if (mysql_num_rows($Checkresults) <=0)
           {$ERR20=1;}                      
           $BillNo=$data->sheets[0]['cells'][$i][6];
           if ($PaymentType=="2" & $BillNo=="")
           {$ERR30=1;}
           if ($PaymentType<>"2" & $BillNo<>"")
           {$ERR30=1;}
           $BillDate=excelDate($data->sheets[0]['cells'][$i][7]);
           if ($PaymentType=="2" & $BillDate=="")
           {$ERR40=1;}
           if ($PaymentType<>"2" & $BillDate<>"")
           {$ERR40=1;}   
		   /*
           if($PaymentType=="2")
           {if(substr($BillDate,4,1)<>'-' or substr($BillDate,7,1)<>'-')
            {$ERR40=1;}
            else
            {$BD=substr($BillDate,0,4).substr($BillDate,5,2).substr($BillDate,8,2);
             $BD=(int)$BD;
            }
           } */                   
           $DueDate=excelDate($data->sheets[0]['cells'][$i][8]);
           //echo $DueDate;
           /*
           if ($DueDate=="")
           {$ERR50=1;}   
           //echo substr($DueDate,4,1).substr($DueDate,7,1);//日期格式檢查驗證
           if(substr($DueDate,4,1)<>'-' or substr($DueDate,7,1)<>'-')
           {$ERR50=1;} 
           else
           {$DD=substr($DueDate,0,4).substr($DueDate,5,2).substr($DueDate,8,2);
            $DD=(int)$DD;
           }
		    */
           if($PaymentType=="2" & $BD >$DD)
           {$ERR51=1;}      
           $IncomeAmount=$data->sheets[0]['cells'][$i][9];//收款金額
           if ($IncomeAmount==0 or $IncomeAmount=="")
           {$ERR60=1;}             
           if(is_numeric($IncomeAmount)==false)
           {$ERR60=1;}
           $CleanDeduction=$data->sheets[0]['cells'][$i][10]; //清潔費扣款
           if(is_numeric($CleanDeduction)==false)
           {$ERR70=1;}           
           $RepairDeduction=$data->sheets[0]['cells'][$i][11]; //修繕費用扣款
           if(is_numeric($RepairDeduction)==false)
           {$ERR80=1;}                                      
           $PunishDeduction=$data->sheets[0]['cells'][$i][12];//罰款扣款
           if(is_numeric($PunishDeduction)==false)
           {$ERR90=1;}              
           $ReturnDeduction=$data->sheets[0]['cells'][$i][13];//回饋金扣款
           if(is_numeric($ReturnDeduction)==false)
           {$ERRA0=1;}              
           $Refund=$data->sheets[0]['cells'][$i][14];//退款金額
           if(is_numeric($Refund)==false)
           {$ERRB0=1;}             
           $RemainAmount=$IncomeAmount-$CleanDeduction-$RepairDeduction-$PunishDeduction-$ReturnDeduction-$Refund;//剩餘可沖銷金額
           if ($RemainAmount<0)
           {$ERRC0=1;} 
           $Note=$data->sheets[0]['cells'][$i][15];
           $lnid=$i-1;
           $SumERR=$ERR00+$ERR10+$ERR20+$ERR30+$ERR40+$ERR50+$ERR51+$ERR60+$ERR70+$ERR90+$ERRA0+$ERRB0+$ERRC0;
           if ($SumERR==0)
           {
           //$sql="INSERT INTO collectionsmaster (CommID, CollectionsNo, CollectionDate, HouseHoldID, PaymentType, IncomeItemID, BillNo, BillDate, DueDate, TurnToIncomeDate, IncomeAmount, RemainAmount, CleanDeduction, RepairDeduction, PunishDeduction, ReturnDeduction, Refund, Note, ModUser, ModDate) VALUES ('$CommID','$CollectionsNo','$CollectionDate', '$HouseHoldID', '$PaymentType', '$IncomeItemID', '$BillNo', '$BillDate', '$DueDate', '0000-00-00','$IncomeAmount', '$RemainAmount','$CleanDeduction', '$RepairDeduction', '$PunishDeduction', '$ReturnDeduction', '$Refund', '$Note', '$user', '$datetime' )" or die("insert error");
           }
           else
           {
            if($SuccessFlag==1)
            {echo '<font color="RED">匯入已中止!!請更正以下資料後重新上傳!!</font><br>';
             $SuccessFlag=0;}
            echo '第'.$lnid.'筆：';
            if ($ERR00==1)
            {echo '社區"'.$CommID.'"不存在!!';}
            else
            {
              if ($ERR10==1)
              {echo '戶號"'.$HouseHoldID.'"不存在!!';}
              if ($ERR20==1)
              {echo '收款用途"'.$IncomeItemID.'"未定義!!';}
              else
              {
                if ($ERR30==1)
                {echo '收款方式與票號"'.$BillNo.'"不一致!!';} 
                else
                {
                  if ($ERR40==1)
                  {echo '收款方式與票據日"'.$BillDate.'"格式不符!!';}
                  else
                  {
                    if ($ERR50==1)
                    {echo '收款到期日格式不符(YYYY-MM-DD)!!';} 
                    {
                      if ($ERR51==1)
                      {echo '收票日'.$BillDate.'晚於到期日'.$DueDate.'!!';} 
                      if ($ERR60==1)
                      {echo '收款金額"'.$IncomeAmount.'"有誤!!';}
                      if ($ERR70==1)
                      {echo '清潔費扣款"'.$CleanDeduction.'"有誤!!';}
                      if ($ERR80==1)
                      {echo '修繕費用扣款"'.$RepairDeduction.'"有誤!!';}
                      if ($ERR90==1)
                      {echo '罰款扣款"'.$PunishDeduction.'"有誤!!';}
                      if ($ERRA0==1)
                      {echo '回饋金扣款"'.$ReturnDeduction.'"有誤!!';}
                      if ($ERRB0==1)
                      {echo '退款金額"'.$Refund.'"有誤!!';}
                      if ($ERRC0==1)
                      {echo '收款餘額不得小於0零!!';}
                    }                
                  }   
                }              
              }
            }
            echo "<br>";             
           }
           //mysql_query($sql);
        }
//        else
//        {echo "社區代號,收款編號,收款日期,戶別代碼,收款方式,收款用途,票據號碼,票據日期,收款(收票到期日),轉入收入日期,收款金額,清潔費扣款,修繕費用扣款,罰款扣款,回饋金扣款,退款金額,備註";}
        //echo "<br>";
      }

//正式匯入
  if($SuccessFlag==1)
  {
	    for ($i = 1; $i <= $data->sheets[0]['numRows']; $i++) 
	    {                     
		    if($i!=1)
		    {
           $CommID=$data->sheets[0]['cells'][$i][2];
           $serial = date("ym");
           $strSQL = "SELECT Max(CollectionsNo) FROM collectionsmaster Where CommID ='".$CommID."'";
           $rows = mysql_query($strSQL);
           if (mysql_num_rows($rows) > 0) 
           {			$row = mysql_fetch_array($rows);
									if (substr($row[0],0,4) == $serial) 
                  { // 不換年月最大號+1
                    $serial = $row[0] + 1;
									} 
                  else 
                  {	// 換年月重新給號
										$serial .= "0001";}
					 } 
           else
           {$serial .= "0001";}
           $CollectionsNo= $serial;
           $CollectionDate=date("Y-m-d");
           $HouseHoldID=$data->sheets[0]['cells'][$i][3];
           //檢查是否資料結束
           if ($CommID=="" & $HouseHoldID=="")
           {break;}
           //檢查門牌戶號是否正確           
           $PaymentType=$data->sheets[0]['cells'][$i][4];
           $IncomeItemID=$data->sheets[0]['cells'][$i][5];
           if(is_numeric($IncomeItemID)==True and $IncomeItemID< 100)
           {$IncomeItemID=str_pad((string)$IncomeItemID,3,"0",STR_PAD_LEFT);}           
           $BillNo=$data->sheets[0]['cells'][$i][6];
           $BillDate=excelDate($data->sheets[0]['cells'][$i][7]);
		   if ($BillDate == ''){$BillDate = 'null';}else{$BillDate = "'".$BillDate."'";}
           $DueDate=excelDate($data->sheets[0]['cells'][$i][8]);
		   if ($DueDate == ''){$DueDate = 'null';}else{$DueDate = "'".$DueDate."'";}
           $IncomeAmount=$data->sheets[0]['cells'][$i][9];//收款金額
           $CleanDeduction=$data->sheets[0]['cells'][$i][10]; //清潔費扣款
           $RepairDeduction=$data->sheets[0]['cells'][$i][11]; //修繕費用扣款
           $PunishDeduction=$data->sheets[0]['cells'][$i][12];//罰款扣款
           $ReturnDeduction=$data->sheets[0]['cells'][$i][13];//回饋金扣款
           $Refund=$data->sheets[0]['cells'][$i][14];//退款金額
           $RemainAmount=$IncomeAmount-$CleanDeduction-$RepairDeduction-$PunishDeduction-$ReturnDeduction-$Refund;//剩餘可沖銷金額           
           $Note=$data->sheets[0]['cells'][$i][15];
           $sql="INSERT INTO collectionsmaster VALUES ('$CommID','$CollectionsNo','$CollectionDate', '$HouseHoldID', '$PaymentType', '$IncomeItemID', '$BillNo', $BillDate, $DueDate, null,'$IncomeAmount','$CleanDeduction', '$RepairDeduction', '$PunishDeduction', '$ReturnDeduction', '$Refund','$RemainAmount', '$Note', '$user', '$datetime' )" or die("insert error");
           //echo $sql;
           mysql_query($sql);
        }
//        else
//        {echo "社區代號,收款編號,收款日期,戶別代碼,收款方式,收款用途,票據號碼,票據日期,收款(收票到期日),轉入收入日期,收款金額,清潔費扣款,修繕費用扣款,罰款扣款,回饋金扣款,退款金額,備註";}
        //echo "<br>";
      }      
      
      echo '資料彙入成功!!';  
  }     
?>