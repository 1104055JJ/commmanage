<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
	// 未登入則轉至首頁(登入頁)
	session_start();
	if (!(isset($_SESSION["manageuser"]))) {
		echo '<meta http-equiv=REFRESH CONTENT=1;url=index.php>';		
	} else {
?>
		<script type="text/javascript" src="../js/DetailJSB.js"></script>		
<?php
		include("../common/connectdb.php");
		include("../common/AutoDataClassB.php");
		include("../common/AutoPageSwitchClass.php");	

		// 建立及初使化頁數切換
		$a = new AutoPageSwitchClass("D","HouseHoldD.php","SELECT COUNT(*) FROM carspace",array(3,6,9,12));			
		
		// 建立及初始化表格
		$b = new AutoDataClass("D","HouseHoldD.php","HouseHoldDAMD.php",array("CommID","BuildingID","HouseHoldID","CarID"),"","車位資料維護作業","center","table90");
		$b->setTableTitle("left","Arial18Bold bg_gray");
		$b->setFieldTitle("center","Arial16 bg_y");	
		$b->setOddRow("","bg_light_blue");
		//$b->setEvenRow("","bg_y");
		// 配合頁數切換,設定表格資料來源
		$b->beginRowsNum = $a->beginRowsNum;
		$b->setQuery("*","carspace","","",$a->beginRowsNum.",".$a->perPageRows);
		// 設定表格欄位顯示
		$b->setAllFieldAlign("center");
		$b->setAllFieldClass("Arial14");
		$b->setFieldDisplayName("CommID","社區代碼");
		$b->setFieldDisplayName("BuildingID","棟別代碼");
		$b->setFieldDisplayName("HouseHoldID","戶別代碼");
		$b->setFieldDisplayName("CarID","車位代碼");
		$b->setFieldDisplayName("Payer","繳款人");
		$b->setFieldDisplayName("Owner","所有權人");
		$b->setFieldDisplayName("Renter","承租人");
		$b->setField("Area","坪數","center","Arial14","Y","N","","","");
		$b->setFieldDisplayName("Fee","管理費");
		$b->setFieldDisplayName("Discount","折扣");
		$b->setFieldDisplayName("NetFee","應收金額");
		$b->setFieldDisplayName("BillCycle","繳費周期");
		$b->setField("FeePerMonths","繳費月份","center","Arial14","Y","N","","","");
		$b->setField("ModUser","資料更新人員","center","Arial14","Y","N","","","");
		$b->setField("ModDate","資料更新時間","center","Arial14","Y","N","","","");
		
		// 增加按鈕及觸發事件
		
		// 設定表格底部顯示頁數切換
		$b->setTableBottom($a->getShowData());
		// 顯示表格資料
		$b->showData();
	}
?>