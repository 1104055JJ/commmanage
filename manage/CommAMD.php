﻿<?php
	$err = "";
	// 未登入則轉至首頁(登入頁)
	session_start();
	if (!(isset($_SESSION["manageuser"]))) {
		echo '<meta http-equiv=REFRESH CONTENT=1;url=index.php>';		
	} else {
		include("../common/connectdb.php");
		include("../common/PublicFunction.php");
		//判斷要執行之功能 : Q->顯示資料  A->新增  AW->新增寫入  M->修改  MW->修改寫入  D->刪除  DW->刪除寫入  L->列表
		list($f,$id) = check_m_f();
?>

<?php if ($f == "Q" || $f == "A" || $f == "M" || $f == "D") {
	//檢查$id是否存在,並傳回資料列
	if ($f != "A") {
		list($err,$row) = check_id_exist("Community","CommID='".$id."'");		
	}
	//依要執行之功能給定變數值
	list($fW,$fS,$fSS) = set_fW($f);
	
	if ($err != "") {
		echo $err;
	} else {	
?>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<form action="CommAMD.php" name="MasterForm" id="MasterForm" method="POST">
		<table align="center" class="table90">
			<tr class="bg_gray">
				<th colspan="6">
					<table width="100%">
						<tr>
							<th class="Arial18Bold align_left"><?php echo '<font color="red">'.$fS.'</font>'; ?>社區基本資料<input name="f" id="f" value="<?php echo $fW; ?>" type="HIDDEN"></th>			
							<th class="Arial18Bold align_right">
							<?php
								if ($f != "Q") {
									if ($f == "D") {				
										//檢查是否已被使用,若已被使用則不可刪除
										$err = check_can_delete("HouseHold","CommID='".$id."'");
										if ($err == "") {
											$err = check_can_delete("user_role_zone","CommID='".$id."'");
										}
									}
									if ($err == "") {
										//設定submit按鈕
										set_submit_button($fSS,"Comm.php","CommAMD.php");
									}
								}
							?>
							</th>
						</tr>
					</table>
				</th>
			</tr>
			<tr>
				<th class="Arial16Bold_Right">社區代碼：</th>
				<td class="Aria116"><input name="CommID" type="text" id="CommID" style="font-family: Microsoft JhengHei ,Arial, Helvetica, sans-serif;" <?php if ($f != "A") { echo "readonly"; } ?> value="<?php if ($f != "A") { echo $row['CommID']; } ?>"></td>				
				<th class="Arial16Bold_Right">社區名稱：</th>
				<td class="Arial16"><input name="CommName" type="text" id="CommName" style="font-family: Microsoft JhengHei ,Arial, Helvetica, sans-serif;"<?php if ($f == "Q" || $f == "D") { echo "readonly"; } ?> value="<?php if ($f != "A") { echo $row['CommName']; } ?>"></td> 
			</tr>
			<tr>
				<th class="Arial16Bold_Right">公司：</th>
				<td class="Arial16"><select name="CompanyID"  id="CompanyID" style="font-family: Microsoft JhengHei ,Arial, Helvetica, sans-serif;" <?php if ($f == "Q" || $f == "D") { echo "disabled"; } ?> > <?php SelectOption("Company","CompanyID","CompanyName",$row['CompanyID']) ?> </select></td> 
			<th class="Arial16Bold_Right">區域：</th>
				<td class="Arial16"><select name="ZoneID"  id="ZoneID" style="font-family: Microsoft JhengHei ,Arial, Helvetica, sans-serif;" <?php if ($f == "Q" || $f == "D") { echo "disabled"; } ?> > <?php SelectOption("Zone","ZoneID","ZoneName",$row['ZoneID']) ?> </select></td> 
			</tr>	
			<tr>
        		<th class="Arial16Bold_Right">電話：</th>
				<td class="Arial16"><input name="TEL" type="text" id="TEL" style="font-family: Microsoft JhengHei ,Arial, Helvetica, sans-serif;"<?php if ($f == "Q" || $f == "D") { echo "readonly"; } ?> value="<?php if ($f != "A") { echo $row['TEL']; } ?>"></td>
				<th class="Arial16Bold_Right">傳真：</th>
				<td class="Arial16"><input name="FAX" type="text" id="FAX" style="font-family: Microsoft JhengHei ,Arial, Helvetica, sans-serif;"<?php if ($f == "Q" || $f == "D") { echo "readonly"; } ?> value="<?php if ($f != "A") { echo $row['FAX']; } ?>"></td>
			</tr>
			<tr>
				<th class="Arial16Bold_Right">地址：</th>
				<td class="Arial16"><input name="Address" type="text" id="Address" style="font-family: Microsoft JhengHei ,Arial, Helvetica, sans-serif;"<?php if ($f == "Q" || $f == "D") { echo "readonly"; } ?> value="<?php if ($f != "A") { echo $row['Address']; } ?>"></td>

			</tr>
			<tr>
				<th class="Arial16Bold_Right">備註：</th>
				<td class="Arial16"><input name="Note" type="textfiled" id="Note" style="font-family: Microsoft JhengHei ,Arial, Helvetica, sans-serif;"<?php if ($f == "Q" || $f == "D") { echo "readonly"; } ?> value="<?php if ($f != "A") { echo $row['Note']; } ?>"></td> 
			</tr>	
			<tr>
				<th class="Arial16Bold_Right">沖銷順序：</th>				
				<td class="Arial16"><select name="CoverMethod"  id="CoverMethod" style="font-family: Microsoft JhengHei ,Arial, Helvetica, sans-serif;" <?php if ($f == "Q" || $f == "D") { echo "disabled"; } ?> > <?php SelectOption_udc("A01",$row['CoverMethod']) ?> </select></td>								
				<th class="Arial16Bold_Right">零用金允許<br>小於零：</th>				
				<td class="Arial16"><select name="CashAllowZero"  id="CashAllowZero" style="font-family: Microsoft JhengHei ,Arial, Helvetica, sans-serif;" <?php if ($f == "Q" || $f == "D") { echo "disabled"; } ?> > <?php SelectOption_udc("A02",$row['CashAllowZero']) ?> </select></td>								
			</tr>	
			<tr>
				<th colspan="6" class="Arial18Bold"><font color="red"><?php if ($err != "") { echo $err; } ?></font></th>
			</tr>
		</table>
	</form>
<?php }} ?>
				
<?php		
		//刪除資料
		if ($f == "DW") {
			if (isset($_POST['CommID'])) {
				$strSQL = "DELETE FROM Community WHERE CommID='".$_POST['CommID']."'";
				mysql_query($strSQL);
			} else {
				$err = "資料錯誤或不存在 ,請檢查後再執行!";
			}
			$f = "L";
		}
		
		// 新增或修改儲存
		if ($f == "AW" || $f == "MW") {
			// 檢查新增或更改時之輸入值是否正確
			if (!isset($_POST['CommID']) || $_POST['CommID'] == "") {
				$err = $err.'社區代碼不存在或輸入值不正確 !'.chr(13);
			}
			//if (!isset($_POST['IncomeItemShow']) || $_POST['IncomeItemShow'] == "") {
			//	$err = $err.'收入顯示名稱不存在或輸入值不正確 !'.chr(13);
			//}
			//if (!isset($_POST['CompanySort']) || $_POST['CompanySort'] == "") {
			//	$err = $err.'公司代碼排序值不存在或輸入值不正確 !'.chr(13);
			//}
			//$Enable = "N";
			//if (isset($_POST['Enable']) && $_POST['Enable'] == "Y") {
			//	$Enable = "Y";
			//}
			// 檢查是否重覆,新增或修改->寫入資料庫
			if ($err == "") {
				//一組欄位:欄位名稱,欄位值,是否含單引號(如字串,日期等)位
				//$today = new DateTime();
				$today = new DateTime('NOW', new DateTimeZone('Asia/Taipei'));
				
				$field = array("CommID",$_POST['CommID'],"'");
				array_push($field,"CommName",$_POST['CommName'],"'");
				array_push($field,"CompanyID",$_POST['CompanyID'],"'");
				array_push($field,"ZoneID",$_POST['ZoneID'],"'");
				array_push($field,"TEL",$_POST['TEL'],"'");
				array_push($field,"FAX",$_POST['FAX'],"'");
				array_push($field,"Address",$_POST['Address'],"'");
				array_push($field,"Note",$_POST['Note'],"'");
				array_push($field,"CoverMethod",$_POST['CoverMethod'],"'");
				array_push($field,"CashAllowZero",$_POST['CashAllowZero'],"'");
				array_push($field,"ModDate",$today->format('Y-m-d H:i:s'),"'");
				$err = insert_update_data($f, "Community", "CommID='".$_POST['CommID']."'", $field);
			}
			$f = "L";
		}

		// L : 回列表 
		if ($f == "L") {
			echo $err;
		}
	}
?>