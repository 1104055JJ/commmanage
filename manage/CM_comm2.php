﻿<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
	// 未登入則轉至首頁(登入頁)
	session_start();
	if (!(isset($_SESSION["manageuser"]))) {
		echo '<meta http-equiv=REFRESH CONTENT=1;url=index.php>';
	} else {
?>
		<script type="text/javascript" src="../js/MasterJSB.js"></script>
		<!--<script language="JavaScript">refreshDataMAMD('ProgramAMD.php','A','');</script>-->
		
<?php

		$where = "CommID='".$_SESSION['Community']."'"; 
		$countSQL="SELECT COUNT(*) FROM community where CommiD='".$_SESSION['Community']."'";
		
		include("../common/connectdb.php");
		include("../common/AutoDataClassB.php");
		include("../common/AutoPageSwitchClass.php");	
		//include("../common/PublicFunction.php");

		// 建立及初使化頁數切換
		$a = new AutoPageSwitchClass("M","CM_Comm2.php",$countSQL,array(5,10,15,20,25));			
		
		// 建立及初始化表格
		$b = new AutoDataClass("M","CM_Comm2.php","CM_Comm2AMD.php",array("CommID"),"CM_Comm2D.php","社區基本資料維護作業","center","table90");
		$b->setTableTitle("left","Arial18Bold bg_gray");
		$b->setFieldTitle("center","Arial16 bg_y");	
		$b->setOddRow("","bg_light_blue");
		//$b->setEvenRow("","bg_y");
		// 配合頁數切換,設定表格資料來源
		$b->beginRowsNum = $a->beginRowsNum;
		//$b ->setQuery($SELECT, $FROM, $WHERE, $ORDERBY, $LIMIT)
		$b->setQuery("CommID,CommName,TEL,FAX,Address","community",$where,"",$a->beginRowsNum.",".$a->perPageRows);
		// 設定表格欄位顯示
		$b->setAllFieldAlign("center");
		$b->setAllFieldClass("Arial14");
		$b->setFieldDisplayName("CommID","社區代碼");
		$b->setFieldDisplayName("CommName","社區名稱");
		$b->setFieldDisplayName("TEL","電話");
		$b->setFieldDisplayName("FAX","傳真");
		$b->setFieldDisplayName("Address","地址");
		$b->setFAMDButtonDisplay("N", "N", "Y", "N", "Y");
		// 增加按鈕及觸發事件-社區管理員不需使用
		//$b->addButton("匯入Excle","onclick=\"openDialogWindow('匯入Excel','./upload.php?url=CM_Comm2upload.php');\"","upexcel","btn btn-info");
		//$b->addButton("產生社區基礎資料","onclick=\"openDialogWindow('產生社區基礎資料','./CM_Comm2BasicGen_w.php');\"","GenBasicData","btn btn-info");
		// 設定表格底部顯示頁數切換
		$b->setTableBottom($a->getShowData());
		// 顯示表格資料
		$b->showData();
	}
?>