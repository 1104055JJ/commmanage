<?php
	// 未登入則轉至首頁(登入頁)
	session_start();
	if (!(isset($_SESSION["manageuser"]))) {
		echo '<meta http-equiv=REFRESH CONTENT=1;url=index.php>';		
	} else {
		include("../common/connectdb.php");
		include("../common/AutoFormClass.php");

		$a = new AutoFormClass("D","Comm2D.php","Comm2DAMD.php","building","building",array("CommID","BuildingID"),"棟別代碼基本資料","center","table90");
		$a->setTableTitle("left","Arial18Bold bg_gray");
		$a->setFieldTitle("center","Arial16 bg_y");
	  	$a->setField("CommID","社區編號","left","Arial14","N","N","","","Y","N","Y","N","text",array(array(),""),"");
		$a->setField("BuildingID","棟別代碼","left","Arial14","N","N","","","Y","Y","Y","Y","text",array(array(),""),"");
		$a->setField("BuildingName","棟別名稱","left","Arial14","N","N","","","Y","Y","Y","Y","text",array(array(),""),"","onchange=\"CheckNum('Need','DetailForm','BuildingName','此欄位為必填')\"");
		
		$a->setField("Enable","有效否","left","Arial14","N","N","","","Y","Y","Y","Y","select",array(array("有效","Y","無效","N"),""),"Y");
		$a->setField("Note","備註","left","Arial14","N","N","","","Y","Y","Y","Y","text",array(array(),""),"");
		//$a->addBeforeDeleteCheck("community",array("ZoneID","ZoneID"),"已被社區基本資料叫用,不可刪除!");
		//$a->setbeforeForm("Y","./ManageHomeJS.php");
		$a->showData();
	}
?>