<?php
	session_start();
 	include("../common/connectdb.php");
?>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>智能化社區管理系統-系統管理</title>
		<link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" media="screen">
		<link type="text/css" href="../css/icms.css" rel="stylesheet">		
		<link type="text/css" href="../css/jquery.datepick.css" rel="stylesheet">
		<link rel="stylesheet" href="../css/jquery-ui.min.css">
                <link href="../css/msgBoxLight.css" rel="stylesheet" type="text/css">
	</head>  
	<script type="text/javascript" src="../plugin/jquery/jquery.min.js"></script>	
	<script type="text/javascript" src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="../plugin/jquery/jquery.plugin.js"></script>	
	<script type="text/javascript" src="../plugin/jquery/jquery.datepick.js"></script>
	<script type="text/javascript" src="../plugin/jquery/jquery.datepick-zh-TW.js"></script>
	<script type="text/javascript" src="../js/ManageHomeJS.js"></script>
  	<script type="text/javascript" src="../plugin/jquery/jquery-ui.min.js"></script>
  	<script type="text/javascript" src="../plugin/jquery/datepicker-zh-TW.js"></script>     
        <script type="text/javascript" src="../plugin/jquery/jquery.msgbox.js"></script>     
	<body leftmargin=0 topmargin=0>
		<input id="jsonID" name="jsonID" value="" type="HIDDEN">
		<input id="formID" name="formID" value="" type="HIDDEN">		
		<div align="left">
			<div id="TopMenu" align="center" style="background-color: #337ab7">
				<script>refreshDataTopMenu('TopMenu.php','<?php echo (isset($_SESSION["Community"]) ? $_SESSION["Community"] : ""); ?>');</script>
			</div>
			<div id="ManageLogin">
				<?php include("ManageLogin.php"); ?>									
			</div>						
			<hr align="center" width="100%">
			<table width="100%" border=0 cellpadding=0 cellspacing=0>
				<tr align="left" valign="top">
					<!--  CenterZone  -->
					<td width="100%">
						<table width="100%" border=0 cellpadding=0 cellspacing=0>
							<tr><td><div id="dialogWindow"></div></td></tr>
						</table>
						<table width="100%" border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td id="tr_CenterZoneMAMD" align="left" valign="top" style="display:;">
									<div id="CenterZoneMAMD"></div>
								</td>
							</tr>
							<tr>
								<td id="tr_CenterZoneM" align="left" valign="top" style="display:;">
									<div id="CenterZoneM">
									</div>	
								</td>
							</tr>
						</table>
						<br>
						<table width="100%" border=0 cellpadding=0 cellspacing=0>
							<tr id="tr_CenterZoneDAMD" align="left" valign="top" style="display:;">
								<td>
									<div id="CenterZoneDAMD"></div>
								</td>
							</tr>														
							<tr id="tr_CenterZoneD" align="left" valign="top" style="display:;">
								<td>
									<div id="CenterZoneD"></div>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
	</body>
</html>
