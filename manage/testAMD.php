<?php
	// 未登入則轉至首頁(登入頁)
	if (!isset($_SESSION)) { session_start(); }
	if (!(isset($_SESSION["manageuser"]))) {
		echo '<meta http-equiv=REFRESH CONTENT=1;url=index.php>';		
	} else {
		include_once("../common/connectdb.php");
		include_once("../common/PublicFunction.php");		
		include_once("../common/AutoFormClass.php");
		
		$a = new AutoFormClass("M","test.php","testAMD.php","zone","zone",array("ZoneID"),"區域基本資料單頭(異動)","center","table90");
		$a->setTableTitle("left","Arial18Bold bg_gray");
		$a->setFieldTitle("center","Arial16Bold bg_y");
		$a->setButtonDefaultClass("Arial14Bold");
		$a->setField("ZoneID","區域代碼","left","Arial14","Y","N","","","Y","Y","Y","N","text",array(array(),""),"",' size="60" maxlength="10"');
		$a->setField("ZoneName","區域名稱","left","Arial14","Y","N","","","Y","Y","Y","N","label",array(array(),"Zone-1"),"test",'size="60" ');
		$a->setField("ZoneSort","排序","left","Arial14","N","Y","","","Y","Y","Y","Y","text",array(array(),""),"");
		$a->setField("Enable","啟用否","left","Arial14","N","N","","","Y","Y","Y","Y","radio",array(array("啟用","Y","停用","N"),""),"Y");			
		$a->addBeforeDeleteCheck("community",array("ZoneID","ZoneID"),"已被社區基本資料叫用,不可刪除!");
		$a->setBeforeForm("N","<script language=\"JavaScript\">function openAlert(){alert(\"test\");}</script>");
		//$a->setAfterForm("N","AfterAutoform");
		// 設定AMDForm按鈕執行後要不要秀出AMDForm	
		$a->showData();
	}
?>