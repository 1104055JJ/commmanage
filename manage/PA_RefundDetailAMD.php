<?php
	// 未登入則轉至首頁(登入頁)
	session_start();
	if (!(isset($_SESSION["manageuser"]))) {
		echo '<meta http-equiv=REFRESH CONTENT=1;url=index.php>';		
	} else {
		include("../common/connectdb.php");
		include("../common/AutoFormClass.php");

		$a = new AutoFormClass("D","PA_RefundDetail.php","PA_RefundDetailAMD.php","refund_detail","refund_detail",array("CommID","PaymentNo","DetailNo","HouseHoldID"),"單身基本資料","center","table90");
		$a->setTableTitle("left","Arial18Bold bg_gray");
		$a->setFieldTitle("center","Arial16 bg_y");
		$a->setField("CommID","社區","left","Arial14","N","N","","","Y","N","Y","N","label",array(array(),""),$_SESSION['Community']);
		$a->setField("PaymentNo","退款單號","left","Arial14","Y","Y","","","Y","N","Y","N","label",array(array(),""),0);
		$a->setField("DetailNo","項次","left","Arial14","N","Y","","","Y","Y","Y","N","hidden",array(array(),""),0);
                $a->setField("HouseHoldID","戶號","left","Arial14","Y","N","","","Y","N","Y","N","label",array(array(),""),0);
                $a->setField("IncomeItemID","收款類別","left","Arial14","Y","N","","","Y","N","Y","N","hidden",array(array(),""),"003");
                $a->setField("CollectionsNo","收款單號","left","Arial14","N","Y","","","Y","Y","Y","N","love",array(array("CommID","out","HouseHoldID","out","PA04","code"),""),0);
                $a->setField("Amount","保證金金額","left","Arial14","Y","Y","","","Y","N","Y","N","text",array(array(),""),"");
                $a->setField("Paid","退款金額","left","Arial14","Y","Y","","","Y","Y","Y","Y","text",array(array(),""),0);
                $a->setField("Unpaid","未退金額","left","Arial14","Y","Y","","","N","N","Y","N","text",array(array(),""),0);
		$a->setField("Refund","X","left","Arial14","N","Y","","","Y","Y","Y","Y","hidden",array(array(),""),0,"");
		$a->setField("CleanDeduction","清潔費用","left","Arial14","N","Y","","","Y","Y","Y","Y","text",array(array(),""),0,"");
                $a->setField("RepairDeduction","修繕費用","left","Arial14","N","Y","","","Y","Y","Y","Y","text",array(array(),""),0);
                $a->setField("PunishDeduction","罰款","left","Arial14","Y","Y","","","Y","Y","Y","Y","text",array(array(),""),0);
                $a->setField("Note","備註","left","Arial14","Y","N","","","Y","Y","Y","Y","text",array(array(),""),0);
                $a->setField("SupplierID","SupplierID","left","Arial14","N","N","","","N","N","N","N","hidden",array(array(),""),"");
                $a->setField("ModUser","修改人員","left","Arial14","N","N","","","N","N","Y","N","text",array(array(),""),$_SESSION['manageuser']);
		$a->setField("ModDate","修改時間","left","Arial14","N","N","","","N","N","Y","N","date",array(array(),""),"","",array("datetime","Y","Y"));
		$a->setBeforeForm("Y","./PA_RefundJS.php");
                            //跑store procedure 要改
                $a->setAfterWriteToDataBase('PA_Update_Collection',array('CommID','PaymentNo','CollectionsNo'),array("AW","MW","DW"));
		//$a->addBeforeDeleteCheck("community",array("ZoneID","ZoneID"),"已被社區基本資料叫用,不可刪除!");
		
               
                 $a->showData();
	}
