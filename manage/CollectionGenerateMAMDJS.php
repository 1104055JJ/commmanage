<script type="text/javascript">

  var IncomeAmount = document.forms["MasterForm"].elements["IncomeAmount"];
  var Refund = document.forms["MasterForm"].elements["Refund"];
  var CleanDeduction = document.forms["MasterForm"].elements["CleanDeduction"];
  var RepairDeduction = document.forms["MasterForm"].elements["RepairDeduction"];  
  var PunishDeduction = document.forms["MasterForm"].elements["PunishDeduction"];
  var ReturnDeduction = document.forms["MasterForm"].elements["ReturnDeduction"];  
  var TotalRes = document.forms["MasterForm"].elements["TotalRes"];
  TotalRes.value = IncomeAmount.value*1 - Refund.value*1 - CleanDeduction.value*1 - RepairDeduction.value*1 - PunishDeduction.value*1 - ReturnDeduction.value*1;  
  
//document.getElementById("MasterForm").elements.namedItem("CollectionsNo").onchange = function() {NewMFunction()};
document.getElementById("MasterForm").elements.namedItem("DueDate").onchange = function() {RecalMFunction()};
document.getElementById("MasterForm").elements.namedItem("IncomeAmount").onchange = function() {RecalMFunction()};
document.getElementById("MasterForm").elements.namedItem("Refund").onchange = function(){RecalMFunction()};
document.getElementById("MasterForm").elements.namedItem("CleanDeduction").onchange = function(){RecalMFunction()};
document.getElementById("MasterForm").elements.namedItem("RepairDeduction").onchange = function(){RecalMFunction()};
document.getElementById("MasterForm").elements.namedItem("PunishDeduction").onchange = function(){RecalMFunction()};
document.getElementById("MasterForm").elements.namedItem("ReturnDeduction").onchange = function(){RecalMFunction()};
document.getElementById("MasterForm").elements.namedItem("Note").onchange = function(){RecalMFunction()};
document.getElementById("DetailForm").elements.namedItem("WriteOffAmount").onchange = function(){RecalMFunction()};


function RecalMFunction()
{   
  //var ModUser = document.forms["MasterForm"].elements["ModUser"];
  //ModUser.value = "1111057";
  //var ModDate = document.forms["MasterForm"].elements["ModDate"];
  var currentdate = new Date(); 
  var datetime =  currentdate.getFullYear() + "-"
                + (currentdate.getMonth()+1)  + "-" 
                + currentdate.getDate() + " "  
                + currentdate.getHours() + ":"  
                + currentdate.getMinutes() + ":" 
                + currentdate.getSeconds();
  ModDate.value = datetime; 
  var IncomeAmount = document.forms["MasterForm"].elements["IncomeAmount"];
  var Refund = document.forms["MasterForm"].elements["Refund"];
  var CleanDeduction = document.forms["MasterForm"].elements["CleanDeduction"];
  var RepairDeduction = document.forms["MasterForm"].elements["RepairDeduction"];  
  var PunishDeduction = document.forms["MasterForm"].elements["PunishDeduction"];
  var ReturnDeduction = document.forms["MasterForm"].elements["ReturnDeduction"];  
  var WriteOffAmount = document.forms["DetailForm"].elements["WriteOffAmount"];
  TotalArs.value= WriteOffAmount.value*1;
  TotalRes.value = IncomeAmount.value*1 - WriteOffAmount.value*1 - Refund.value*1 - CleanDeduction.value*1 - RepairDeduction.value*1 - PunishDeduction.value*1 - ReturnDeduction.value*1;

}


</script>

<script src="../js/jquery.validate.js" type="text/javascript"></script>
<script src="../js/message_tw.js" type="text/javascript"></script>
<script type="text/javascript">
$(function()
{
	//須與form表單ID名稱相同
	$("#DetailForm").validate
  (
    {
		  rules: 
      {
	 			DueDate:
        {Required: true},
  			IncomeAmount: 
        {	digits: true,
          min:0,
          Required: true
				},       
        Refund:   
        {	digits: true,
          //max:IncomeAmount-TotalArs
          max:IncomeAmount.value*1,
          min:0,
          Required: true 
				},  
        TotalArs:   
        {	digits: true,
          max:IncomeAmount.value*1-Refund.value*1, 
          min:0 
				},  
        TotalRes:   
        {	digits: true,
          max:IncomeAmount.value*1,        
          min:0 
				},
  		},
	  }
	);
});

function padLeft(str, len) 
{
     str = '' + str;
          if (str.length >= len)
          {return str;}
          else
          {return padLeft("0" + str, len);} 
} 

</script>