<?php
		include("../common/connectdb.php");
		include("../common/PublicFunction.php");	
		session_start();
?>
<div style="text-align:center">
<p>管理費欠繳明細表(依月份)<p>
<p>請選擇所要查詢的年度/月份</p>
	<select id="s_year" name="s_year" class="Arial14">
		<option value ="2015">2015年</option>
		<option value ="2016">2016年</option>
	</select>
	<select id="s_month" name="s_month" class="Arial14">
		<option value ='1'>1月</option>
		<option value ='2'>2月</option>
		<option value ='3'>3月</option>
		<option value ='4'>4月</option>
		<option value ='5'>5月</option>
		<option value ='6'>6月</option>
		<option value ='7'>7月</option>
		<option value ='8'>8月</option>
		<option value ='9'>9月</option>
		<option value ='10'>10月</option>
		<option value ='11'>11月</option>
		<option value ='12'>12月</option>
	</select>
	<br><br>
	<button id="b_html" >HTML報表</button>
	<button id="b_pdf" >PDF報表</button>
	<button id="b_csv" >EXCEL報表</button>
	<input id="l_comm" name="l_comm" value=<?php echo $_SESSION['Community'] ?> type="HIDDEN">
	<div id = "msg"></div>
</div>
<script type="text/javascript">
	

$( "#b_pdf" ).click(function() {
	call_rpt('PDF');
});

$( "#b_html" ).click(function() {
	call_rpt('HTML');
});

$( "#b_csv" ).click(function() {
	call_rpt('EXCEL');
});

function call_rpt(format){
	var commID = document.getElementById("l_comm").value;
   	var e = document.getElementById("s_year");
	var year = e.options[e.selectedIndex].value;
  	var f = document.getElementById("s_month");
	var month = f.options[f.selectedIndex].value;
        
         if (format == 'PDF' || format == 'EXCEL'){ 
             window.open("RPT_UnreceivedARbyMonth.php?CommID="+commID+"&Year="+year+"&Month="+month+"&FORMAT="+format,"w",config="width=1000,height=800");
            $( "#dialogWindow" ).dialog( "close" );
            }
     else{
         
         
     }
}
</script>
	

	