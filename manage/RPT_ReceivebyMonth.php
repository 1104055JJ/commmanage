<?php
include("../common/connectdb.php");	
session_start();
$CommID=$_SESSION['Community'];
$year = $_REQUEST['Year'];
$month = $_REQUEST['Month'];
$Current_YearMonth=$year*100+$month*1;
$LastMonth=$month*1-1;
//標題
$sql = "Select CommName From community Where CommID ='".$CommID."'";
$rows=mysql_query($sql);
$row=mysql_fetch_array($rows);
/*
//找出當年度最多管理費月份
$sql = "select max(month) from ar ".
	   " where commid='".$_SESSION['Community']."' and year =".$year."-2000";
$rows = mysql_query($sql) ;
$row = mysql_fetch_array($rows);
//echo $row["max(month)"];
*/
$CommName=$row['CommName'];

$html = "<h2 align=\"center\">".$CommName."社區".$year."年".$month."月管理費收入統計表</h2>";
$html .="
<table border=\"1\" cellpadding=\"4\" align=\"center\">
<thead>
	<tr bgcolor=\"#e6e6fa\" align=\"center\">
		<td width=\"80\">入帳<br>日期</td>
		<td width=\"50\">門牌<br>戶號</td>		
    <td width=\"50\">繳<br>款人</td>";
  $Total_Pre_Amount=0;
  $Total_Last_Amount=0;
  $Total_Current_Amount=0;
  $Total_Advance_Payment=0;
  $Total_Total_Amount=0;
  $Total_TotalCarFee_Amount=0;
  
  if($LastMonth*1==0)//前期管理費
  {$LastMonth='12';
   $html .="
    <td width=\"60\">".$LastMonth."月前<br>管理費</td>
		<td width=\"60\">".$LastMonth."月<br>管理費</td>";
   $Last_YearMonth=($year*1-1)*100+12;  
  }
  else
  {$html .="
    <td width=\"60\">".$LastMonth."月前<br>管理費</td>
		<td width=\"60\">".$LastMonth."月<br>管理費</td>";
    $Last_YearMonth=$year*100+$LastMonth*1; 
  }
$html .="
		<td width=\"60\">".$month."月<br>管理費</td>
    <td width=\"60\">預收<br>管理費</td>
    <td width=\"70\">已核銷車<br>位清潔費</td>    
		<td width=\"80\">合計<br>金額</td>
		<td width=\"200\">備註</td></tr></thead>";

$sql = "select DueDate, collectionsmaster.HouseHoldID, CollectionsNo, IncomeAmount, collectionsmaster.Note as Note, Refund, Payer, Owner, Renter, CoverMethod from collectionsmaster, household, community where collectionsmaster.CommID='".$_SESSION['Community']."' and collectionsmaster.CommID=household.CommID and community.CommID=household.CommID and collectionsmaster.HouseHoldID=household.HouseHoldID and Year(DueDate)='".$_REQUEST['Year']."' and Month(DueDate)='".$_REQUEST['Month']."' and IncomeItemID < '003' order by DueDate";
//echo $sql;
$rows = mysql_query($sql) ;
while($row = mysql_fetch_array($rows))//while收款項目
{
	$html.="
	<tr>
		<td width=\"80\">".$row["DueDate"]."</td>";
    $html.="<td width=\"50\">".$row["HouseHoldID"]."</td>";//戶別代號
		if ($row["Payer"] == '1')
    {$html.="<td width=\"50\">".$row["Owner"]."</td>";}
    else
    {$html.="<td width=\"50\">".$row["Renter"]."</td>";}
  
  $Pre_Amount=0;
  $PreHouseFee_Amount=0;
  $Last_Amount=0;
  $LastHouseFee_Amount=0;
  $Current_Amount=0;
  $CurrentHouseFee_Amount=0;
  $Advance_Payment=0;
  $AdvanceHouseFee_Amount=0;
  $Total_Amount=$row["IncomeAmount"]*1-$row["Refund"]*1;
  $TotalHouseFee_Amount=0;

	//沖款明細
	$sql_detail = "select collectionsdetail.ArNo, Month, Year, HouseFee, CarFee, WriteOffAmount from collectionsdetail, ar where collectionsdetail.commid='".$_SESSION['Community']."' and collectionsdetail.commid= ar.commid and collectionsdetail.ArNo=ar.ArNo and CollectionsNo='".$row["CollectionsNo"]."'";
	//echo $sql_car;
	$rows_detail = mysql_query($sql_detail) ;
	while($row_detail = mysql_fetch_array($rows_detail))//while沖消項目
  {
		$WriteOff_YearMonth=(2000+$row_detail ["Year"])*100+$row_detail ["Month"];

    if ($WriteOff_YearMonth < $Last_YearMonth) 
    {	  $Pre_Amount=$Pre_Amount+$row_detail["WriteOffAmount"]*1;
      	if ($row["CoverMethod"] == '1') 
        { if ($row_detail["WriteOffAmount"]*1> $row_detail["HouseFee"])
          {$PreHouseFee_Amount=$PreHouseFee_Amount+$row_detail["HouseFee"]*1;}
          else
          {$PreHouseFee_Amount=$PreHouseFee_Amount+$row_detail["WriteOffAmount"]*1;}
        }
        else
        { if ($row_detail["WriteOffAmount"]*1> $row_detail["CarFee"])
          {$PreHouseFee_Amount=$PreHouseFee_Amount+$row_detail["WriteOffAmount"]*1-$row_detail["CarFee"];}
          else
          {$PreHouseFee_Amount=$PreHouseFee_Amount+0;}
        }
    }
    else
    {
		  if ($WriteOff_YearMonth == $Last_YearMonth)
      { $Last_Amount=$Last_Amount+$row_detail["WriteOffAmount"]*1;
      	if ($row["CoverMethod"] == '1') 
        { if ($row_detail["WriteOffAmount"]*1> $row_detail["HouseFee"])
          {$LastHouseFee_Amount=$LastHouseFee_Amount+$row_detail["HouseFee"]*1;}
          else
          {$LastHouseFee_Amount=$LastHouseFee_Amount+$row_detail["WriteOffAmount"]*1;}
        }
        else
        { if ($row_detail["WriteOffAmount"]*1> $row_detail["CarFee"])
          {$LastHouseFee_Amount=$LastHouseFee_Amount+($row_detail["WriteOffAmount"]*1-$row_detail["CarFee"]);}
          else
          {$LastHouseFee_Amount=$LastHouseFee_Amount+0;}
        }      
      }
      else
      { if($WriteOff_YearMonth == $Current_YearMonth)
        {   $Current_Amount=$Current_Amount+$row_detail["WriteOffAmount"]*1;
      	    if ($row["CoverMethod"] == '1') 
            { if ($row_detail["WriteOffAmount"]*1> $row_detail["HouseFee"])
              {$CurrentHouseFee_Amount=$CurrentHouseFee_Amount+$row_detail["HouseFee"]*1;}
              else
              {$CurrentHouseFee_Amount=$CurrentHouseFee_Amount+$row_detail["WriteOffAmount"]*1;}
            }
            else
            { if ($row_detail["WriteOffAmount"]*1> $row_detail["CarFee"])
              {$CurrentHouseFee_Amount=$CurrentHouseFee_Amount+($row_detail["WriteOffAmount"]*1-$row_detail["CarFee"]);}
              else
              {$CurrentHouseFee_Amount=$CurrentHouseFee_Amount+0;}
            }            
        }
      }
		}
    
    $AdvanceHouseFee_Amount= $Total_Amount*1 - $Current_Amount*1- $Last_Amount*1- $Pre_Amount*1;//預付款(所有-用掉)
    $TotalHouseFee_Amount=$PreHouseFee_Amount*1+$LastHouseFee_Amount*1+$CurrentHouseFee_Amount*1+$AdvanceHouseFee_Amount*1;
    $TotalCarFee_Amount=$Total_Amount*1-$TotalHouseFee_Amount*1;
    //$WriteOffLog.=$WriteOff_YearMonth;    
  }//end while 沖消項目
	$html.="
		<td align=\"right\" width=\"60\">".$PreHouseFee_Amount."</td>
		<td align=\"right\" width=\"60\">".$LastHouseFee_Amount."</td>
		<td align=\"right\" width=\"60\">".$CurrentHouseFee_Amount."</td>
		<td align=\"right\" width=\"60\">".$AdvanceHouseFee_Amount."</td>            
		<td align=\"right\" width=\"70\">".$TotalCarFee_Amount."</td> 
		<td align=\"right\" width=\"80\">".$Total_Amount."</td>
    <td align=\"left\" width=\"200\">".$row["Note"]."</td>";	
    $Total_Pre_Amount=$Total_Pre_Amount+$PreHouseFee_Amount;
    $Total_Last_Amount=$Total_Last_Amount+$LastHouseFee_Amount;
    $Total_Current_Amount=$Total_Current_Amount+$CurrentHouseFee_Amount;
    $Total_Advance_Payment=$Total_Advance_Payment+$AdvanceHouseFee_Amount;
    $Total_TotalCarFee_Amount=$Total_TotalCarFee_Amount+$TotalCarFee_Amount;
    $Total_Total_Amount=$Total_Total_Amount+$Total_Amount;

	$html.="</tr>";
}//end while 收款項目

 $html.="
    <tr bgcolor=\"#e6e6fa\" align=\"center\">
		<td width=\"80\"></td>
		<td width=\"50\" align=\"right\"></td>		
    <td width=\"50\" align=\"right\">總計</td>
    <td width=\"60\" align=\"right\">".$Total_Pre_Amount."</td>
		<td width=\"60\" align=\"right\">".$Total_Last_Amount."</td>
    <td width=\"60\" align=\"right\">".$Total_Current_Amount."</td>
    <td width=\"60\" align=\"right\">".$Total_Advance_Payment."</td>
    <td width=\"70\" align=\"right\">".$Total_TotalCarFee_Amount."</td>
    <td width=\"80\" align=\"right\">".$Total_Total_Amount."</td>
    <td width=\"200\" align=\"right\"></td></tr>";            

$html.="</table>";

if ($_REQUEST['FORMAT']=="HTML"){
	echo '<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="zh-tw" lang="zh-tw">';
	echo '<head>';
	echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
	echo '</head>';
	echo $html;
}

if ($_REQUEST['FORMAT']=="PDF"){
	
require_once('../tcpdf/config/zho.php');
require_once('../tcpdf/config/tcpdf_config.php');
require_once('../tcpdf/tcpdf.php');
//客製頁首頁尾
// Extend the TCPDF class to create custom Header and Footer
class MYPDF extends TCPDF {

	//Page header
	/*
	public function Header() {
		// Logo
		$image_file = K_PATH_IMAGES.'logo_example.jpg';
		$this->Image($image_file, 10, 10, 15, '', 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);
		// Set font
		$this->SetFont('helvetica', 'B', 20);
		// Title
		$this->Cell(0, 15, '<< TCPDF Example 003 >>', 0, false, 'C', 0, '', 0, false, 'M', 'M');
	}  */

	// Page footer
	public function Footer() {
	/*	// Position at 15 mm from bottom
		$this->SetY(-15);
		// Set font
		$this->SetFont('droidsansfallback', 'I', 14);
		//$this->setFooterMargin(80);
		// Page number
		$this->Cell(0, 10, '主任委員：　 　                 監察委員:　 　                財務委員：　 　                現場主任:　 　                ', 0, false, 'C', 0, '', 0, false, 'T', 'M');
    */}       
}

//實體化PDF物件
$pdf = new MYPDF("L", PDF_UNIT,'A4', true, 'UTF-8', false);
//$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->setPrintHeader(false); //不要頁首
$pdf->setPrintFooter(true); //頁尾

// set margins
//$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
//$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);  //設定自動分頁

$pdf->setLanguageArray($l); //設定語言相關字串

$pdf->setFontSubsetting(true); //產生字型子集（有用到的字才放到文件中）

$pdf->SetFont('droidsansfallback', '', 10, '', true); //設定字型

$pdf->AddPage(); //新增頁面

$pdf->setTextShadow(array('enabled'=>false, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));//文字陰影
	
	
	$pdf->writeHTML($html);
	$pdf->Output('contact.pdf', 'I');	
}

if ($_REQUEST['FORMAT']=="EXCEL")
{
require_once '../Excel/PHPExcel.php';    //引入 PHPExcel 物件庫
require_once '../Excel/PHPExcel/IOFactory.php';    //引入 PHPExcel_IOFactory 物件庫
$objPHPExcel = new PHPExcel();  //實體化Excel
//----------內容-----------//
$objPHPExcel->setActiveSheetIndex(0);  //設定預設顯示的工作表
$objActSheet = $objPHPExcel->getActiveSheet(); //指定預設工作表為 $objActSheet
$objActSheet->setTitle("管理費收入統計表");  //設定標題
$objPHPExcel->createSheet(); //建立新的工作表，上面那三行再來一次，編號要改

$objActSheet->getColumnDimension('A')->setWidth(12);
$objActSheet->getColumnDimension('B')->setWidth(10);
$objActSheet->getColumnDimension('C')->setWidth(10);
$objActSheet->getColumnDimension('D')->setWidth(15);
$objActSheet->getColumnDimension('E')->setWidth(15);
$objActSheet->getColumnDimension('F')->setWidth(15);
$objActSheet->getColumnDimension('G')->setWidth(15);
$objActSheet->getColumnDimension('H')->setWidth(20);
$objActSheet->getColumnDimension('I')->setWidth(15);
$objActSheet->getColumnDimension('J')->setWidth(40);

$objActSheet-> getStyle('A1:J1')-> getFont()-> setName('SimHei')-> setSize('16');
$objActSheet-> getStyle('A1:J1')-> getFill()-> setFillType(PHPExcel_Style_Fill:: FILL_SOLID)-> getStartColor()-> setARGB('FFC9E3F3');
$objActSheet->mergeCells("A1:J1")->setCellValue("A1", $CommName."社區".$year."年".$month."月管理費收入統計表");

$objActSheet-> getStyle('A2:J2')-> getFont()-> setName('SimHei')-> setSize('12');
$objActSheet-> getStyle('A2:J2')-> getFill()-> setFillType(PHPExcel_Style_Fill:: FILL_SOLID)-> getStartColor()-> setARGB('FFC9E3F3');

$objActSheet->setCellValue("A2", '入帳日期')
            ->setCellValue("B2", '門牌戶號')
            ->setCellValue("C2", '繳款人')
            ->setCellValue("D2", $LastMonth."月前管理費")
            ->setCellValue("E2", $LastMonth."月管理費")
            ->setCellValue("F2", $month."月管理費")
            ->setCellValue("G2", '預收管理費')
            ->setCellValue("H2", '已核銷車位清潔費')
            ->setCellValue("I2", '合計金額')                                    
            ->setCellValue("J2", '備註');

    $Total_Pre_Amount=0;
    $Total_Last_Amount=0;
    $Total_Current_Amount=0;
    $Total_Advance_Payment=0;
    $Total_Total_Amount=0;
    $Total_TotalCarFee_Amount=0;    
//$sql = "select * from `".$xoopsDB->prefix("contact")."` ";
//$result = $xoopsDB->query($sql) or redirect_header($_SERVER['PHP_SELF'],3, mysql_error());
$rows = mysql_query($sql) ;
$i=3;
while($row = mysql_fetch_array($rows))
{
	
  $objActSheet->setCellValue("A{$i}", $row["DueDate"])
              ->setCellValue("B{$i}", $row["HouseHoldID"]);
              //->setCellValueExplicit("C{$i}", $tel , PHPExcel_Cell_DataType:: TYPE_STRING)
      if ($row["Payer"] == '1')
      {$objActSheet->setCellValue("C{$i}", $row["Owner"]);}
      else
      {$objActSheet->setCellValue("C{$i}", $row["Renter"]);}
//check to here 20150427//

  $Pre_Amount=0;
  $PreHouseFee_Amount=0;
  $Last_Amount=0;
  $LastHouseFee_Amount=0;
  $Current_Amount=0;
  $CurrentHouseFee_Amount=0;
  $Advance_Payment=0;
  $AdvanceHouseFee_Amount=0;
  $Total_Amount=$row["IncomeAmount"]*1-$row["Refund"]*1;
  $TotalHouseFee_Amount=0;

	//沖款明細
	$sql_detail = "select collectionsdetail.ArNo, Month, Year, HouseFee, CarFee, WriteOffAmount from collectionsdetail, ar where collectionsdetail.commid='".$_SESSION['Community']."' and collectionsdetail.commid= ar.commid and collectionsdetail.ArNo=ar.ArNo and CollectionsNo='".$row["CollectionsNo"]."'";
	$rows_detail = mysql_query($sql_detail) ;
	while($row_detail = mysql_fetch_array($rows_detail))//while沖消項目
  {
		$WriteOff_YearMonth=(2000+$row_detail ["Year"])*100+$row_detail ["Month"];

    if ($WriteOff_YearMonth < $Last_YearMonth) 
    {	$Pre_Amount=$Pre_Amount+$row_detail["WriteOffAmount"]*1;
      	if ($row["CoverMethod"] == '1') 
        { if ($row_detail["WriteOffAmount"]*1> $row_detail["HouseFee"])
          {$PreHouseFee_Amount=$PreHouseFee_Amount+$row_detail["HouseFee"]*1;}
          else
          {$PreHouseFee_Amount=$PreHouseFee_Amount+$row_detail["WriteOffAmount"]*1;}
        }
        else
        { if ($row_detail["WriteOffAmount"]*1> $row_detail["CarFee"])
          {$PreHouseFee_Amount=$PreHouseFee_Amount+$row_detail["WriteOffAmount"]*1-$row_detail["CarFee"];}
          else
          {$PreHouseFee_Amount=$PreHouseFee_Amount+0;}
        }   
    }
    else
    {
		  if ($WriteOff_YearMonth == $Last_YearMonth)
      {$Last_Amount=$Last_Amount+$row_detail["WriteOffAmount"]*1;
      	if ($row["CoverMethod"] == '1') 
        { if ($row_detail["WriteOffAmount"]*1> $row_detail["HouseFee"])
          {$LastHouseFee_Amount=$LastHouseFee_Amount+$row_detail["HouseFee"]*1;}
          else
          {$LastHouseFee_Amount=$LastHouseFee_Amount+$row_detail["WriteOffAmount"]*1;}
        }
        else
        { if ($row_detail["WriteOffAmount"]*1> $row_detail["CarFee"])
          {$LastHouseFee_Amount=$LastHouseFee_Amount+($row_detail["WriteOffAmount"]*1-$row_detail["CarFee"]);}
          else
          {$LastHouseFee_Amount=$LastHouseFee_Amount+0;}
        }            
      }
      else
      { if($WriteOff_YearMonth == $Current_YearMonth)
        {$Current_Amount=$Current_Amount+$row_detail["WriteOffAmount"]*1;
      	    if ($row["CoverMethod"] == '1') 
            { if ($row_detail["WriteOffAmount"]*1> $row_detail["HouseFee"])
              {$CurrentHouseFee_Amount=$CurrentHouseFee_Amount+$row_detail["HouseFee"]*1;}
              else
              {$CurrentHouseFee_Amount=$CurrentHouseFee_Amount+$row_detail["WriteOffAmount"]*1;}
            }
            else
            { if ($row_detail["WriteOffAmount"]*1> $row_detail["CarFee"])
              {$CurrentHouseFee_Amount=$CurrentHouseFee_Amount+($row_detail["WriteOffAmount"]*1-$row_detail["CarFee"]);}
              else
              {$CurrentHouseFee_Amount=$CurrentHouseFee_Amount+0;}
            }            
        }
      }
		}
    $AdvanceHouseFee_Amount= $Total_Amount*1 - $Current_Amount*1- $Last_Amount*1- $Pre_Amount*1;//預付款(所有-用掉)
    $TotalHouseFee_Amount=$PreHouseFee_Amount*1+$LastHouseFee_Amount*1+$CurrentHouseFee_Amount*1+$AdvanceHouseFee_Amount*1;
    $TotalCarFee_Amount=$Total_Amount*1-$TotalHouseFee_Amount*1;
  }//end while 沖消項目

    $objActSheet->setCellValue("D{$i}", $PreHouseFee_Amount)
                ->setCellValue("E{$i}", $LastHouseFee_Amount)
                ->setCellValue("F{$i}", $CurrentHouseFee_Amount)
                ->setCellValue("G{$i}", $AdvanceHouseFee_Amount)
                ->setCellValue("H{$i}", $TotalCarFee_Amount)
                ->setCellValue("I{$i}", $Total_Amount)
                ->setCellValue("J{$i}", $row["Note"]);                
	
    $Total_Pre_Amount=$Total_Pre_Amount+$Pre_Amount;
    $Total_Last_Amount=$Total_Last_Amount+$Last_Amount;
    $Total_Current_Amount=$Total_Current_Amount+$Current_Amount;
    $Total_Advance_Payment=$Total_Advance_Payment+$Advance_Payment;
    $Total_Total_Amount=$Total_Total_Amount+$Total_Amount;

  $i++;
}

$objActSheet-> getStyle("A{$i}:J{$i}")-> getFill()-> setFillType(PHPExcel_Style_Fill:: FILL_SOLID)-> getStartColor()-> setARGB('FFC9E3F3');
$objActSheet->mergeCells("A{$i}:C{$i}")->setCellValue("A{$i}", '總計');
$n=$i-1;
$objActSheet->setCellValue("D{$i}", "=SUM(D3:D{$n})")
            ->setCellValue("E{$i}", "=SUM(E3:E{$n})")
            ->setCellValue("F{$i}", "=SUM(F3:F{$n})")
            ->setCellValue("G{$i}", "=SUM(G3:G{$n})")
            ->setCellValue("H{$i}", "=SUM(H3:H{$n})")
            ->setCellValue("I{$i}", "=SUM(I3:I{$n})");

header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename='.iconv('UTF-8','Big5','收入明細').'.xls');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->setPreCalculateFormulas(false);
$objWriter->save('php://output');
exit;
}
?>