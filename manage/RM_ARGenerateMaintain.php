<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
	// 未登入則轉至首頁(登入頁)
	session_start();
	if (!(isset($_SESSION["manageuser"]))) 
  {echo '<meta http-equiv=REFRESH CONTENT=1;url=index.php>';} 
  else
  {
?>
		<script type="text/javascript" src="../js/MasterJSB.js"></script>
		<!--<script language="JavaScript">refreshDataMAMD('RM_ARGenerateMaintainAMD.php','A','');</script>-->


<?php
		// 記錄點選了那個SubMenu中之程式
		//$_SESSION['SubMenu'] = $_POST['SubMenu'];
		//$_SESSION['ProgramItem'] = $_POST['ProgramItem'];
 		$society = "CommID='".$_SESSION['Community']."'"; 

		include("../common/connectdb.php");
		include("../common/AutoDataClassB.php");
		include("../common/AutoPageSwitchClass.php");
		//include("../common/PublicFunction.php");	

		// 建立及初使化頁數切換
		$a = new AutoPageSwitchClass("M","RM_ARGenerateMaintain.php","SELECT COUNT(*) FROM ar where CommID='".$_SESSION['Community']."'",array(10,20,30,40,50));			
		
		// 建立及初始化表格
    $b = new AutoDataClass("M","RM_ARGenerateMaintain.php","RM_ARGenerateMaintainAMD.php",array("CommID","ArNo"),"","賬單維護","center","table90");
	$b->setTableTitle("left","Arial18Bold bg_gray");
	$b->setFieldTitle("center","Arial16 bg_y");	
	$b->setOddRow("","bg_light_blue");
		//$b->setEvenRow("","bg_y");
		// 配合頁數切換,設定表格資料來源
	$b->beginRowsNum = $a->beginRowsNum;
	$b->setQuery(" CommID, ArNo, Year, Month, BuildingID, HouseHoldID, Note, HouseFee, CarFee, Discount, TotalAmount, Received, UnReceived, ModUser, ModDate","ar","$society","",$a->beginRowsNum.",".$a->perPageRows);
	// 設定表格欄位顯示
	$b->setAllFieldAlign("center");
	$b->setAllFieldClass("Arial14");
	//$b->setFieldDisplayName("CommID","社區代碼");
    $b->setFieldDisplay("CommID","N");
    $b->setFieldDisplayName("BuildingID","棟別代碼");
    //$b->setFieldDisplay("BuildingID","N");
	$b->setFieldDisplayName("ArNo","賬單編號");
	$b->setFieldDisplayName("Year","年度");
	$b->setFieldDisplayName("Month","月份");
	$b->setFieldDisplayName("HouseHoldID","門牌戶號");
    $b->setFieldDisplayName("Note","賬單說明");
    $b->setFieldDisplayName("HouseFee","管理費");
    $b->setFieldDisplayName("CarFee","車位清潔費");
    $b->setFieldDisplayName("Discount","折扣總額");
	$b->setFieldDisplayName("TotalAmount","總計金額");
    $b->setFieldDisplayName("Received","已收金額");
    $b->setFieldDisplayName("UnReceived","未收金額");
    $b->setFieldDisplayName("ModDate","修改日期");
	$b->setFieldDisplayName("ModUser","修改人員");
   // $b->addButton("Excel載入批次賬單","onclick=\"openwindow('RM_ARupload.php');\"");
	// 增加按鈕及觸發事件
	$b->addButton("匯入管理費","onclick=\"openDialogWindow('匯入管理費','./upload.php?url=RM_AR_Upload.php');\"","upexcel","btn btn-info");
	$b->addButton("整批產生管理費","onclick=\"openDialogWindow('整批產生管理費','./RM_AR_BatchGenA.php');\"","GroupGen","btn btn-info");
	// 設定表格底部顯示頁數切換
	$b->setTableBottom($a->getShowData());
	// 顯示表格資料
	$b->showData();
	}
?>
