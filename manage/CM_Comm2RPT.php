<?php
include("../common/connectdb.php");	
require_once('../tcpdf/config/zho.php');
require_once('../tcpdf/config/tcpdf_config.php');
require_once('../tcpdf/tcpdf.php');
session_start();
//實體化PDF物件
$pdf = new TCPDF("L", PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
//$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->setPrintHeader(false); //不要頁首
$pdf->setPrintFooter(false); //不要頁尾

$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);  //設定自動分頁

$pdf->setLanguageArray($l); //設定語言相關字串

$pdf->setFontSubsetting(true); //產生字型子集（有用到的字才放到文件中）

$pdf->SetFont('droidsansfallback', '', 12, '', true); //設定字型

$pdf->AddPage(); //新增頁面

$pdf->setTextShadow(array('enabled'=>false, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));//文字陰影



$sql = "select * from community";
$rows = mysql_query($sql) ;

$all_content="";
//$cate=get_contact_cate_all();

while($row = mysql_fetch_array($rows)){
	//以下會產生這些變數： $tel , $email , $name , $gsn , $sn , $birthday , $zip , $county , $city , $addr
/*
	$commID = $row["CommID"];
$commName = $row["CommName"];
$CompanyID = $row["CompanyID"];
$ZoneID = $row["ZoneID"];
$TEL = $row["TEL"];
$FAX= $row["FAX"];
$Address = $row["Address"];
$Note = $row["Note"];
$CoverMethod = $row["CoverMethod"];
$CashAllowZero = $row["CashAllowZero"];
$ModUser = $row["ModUser"];
$ModDate = $row["ModDate"];
*/
	$all_content.="
	<tr>
		<td>".$row["CommID"]."</td>
		<td>".$row["CommName"]."</td>
		<td>".$row["TEL"]."</td>
		<td>".$row["FAX"]."</td>
		<td>".$row["Address"]."</td>
	</tr>
	";
}


$html="<h1>社區列表</h1>
<table border=\"1\" cellpadding=\"4\">
	<tr bgcolor=\"#FFFF66\" align=\"center\">
		<th width=\"80\">社區代碼</th>
		<th>社區名稱</th>
		<th>電話</th>
		<th>傳真</th>
		<th width=\"200\">地址</th>
	</tr>
	$all_content
</table>";

if ($_REQUEST['type']=="PDF"){
	$pdf->writeHTML($html);
	$pdf->Output('contact.pdf', 'I');	
}else{
	echo $html;
}
?>