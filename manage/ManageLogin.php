<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php 
	if (!(isset($_SESSION["manageuser"]))) {
?>

<div class="container">
	<div class="row">
		<div class="col-md-4 col-md-offset-4">
			<div class="login-panel panel panel-default">        
				<div class="panel-body">
					<form action="ManageLoginCheck.php" id="manageLoginForm" name="manageLoginForm" method="POST" role="form">
						<table border="1" align="center" class="table">
							<tr class="bg_gray">
								<td align="center" colspan="2" class="Arial18Bold">請輸入管理者帳號及密碼</td>
							</tr>
							<tr>
								<td width="100" class="Arial16Bold bg_y" >帳  號：</td>
								<td class="Arial16"><input class="form-control" name="manageuser" type="text" id="manageuser" size="30"></td>
							</tr>
							<tr>
								<td width="100" class="Arial16Bold bg_y">密  碼：</td>
								<td class="Arial16"><input class="form-control" name="managepassword" type="password" id="managepassword" size="30"></td>
							</tr>
							<tr class="bg_gray">
								<td align="center" colspan="2"><input type="submit" name="submit" value="登入" class="btn btn-lg btn-success btn-block"></td>
							</tr>
							<tr>
								<td align="center" colspan="2"><div id="manageLoginMessage"></div></td>
							</tr>
						</table>
            		</form>
				</div>
			</div>
		</div>
	</div>
</div>          

<?php } ?>