<?php
	$err = "";
	// 未登入則轉至首頁(登入頁)
	session_start();
	if (!(isset($_SESSION["manageuser"]))) {
		echo '<meta http-equiv=REFRESH CONTENT=1;url=index.php>';		
	} else {
		include("../common/connectdb.php");
		//判斷要執行之功能 : Q->顯示資料  A->新增  AW->新增寫入  M->修改  MW->修改寫入  D->刪除  DW->刪除寫入  L->列表
		$f = "";
		$id = "";
		$id1 = "";
		$id2 = "";
		
		if (isset($_POST['f'])) {
			if ($_POST['f'] == "Q" || $_POST['f'] == "A" || $_POST['f'] == "M" || $_POST['f'] == "D" || $_POST['f'] == "AW" || $_POST['f'] == "MW" || $_POST['f'] == "DW") {
				$f = $_POST['f'];
			} else {
				$f = "L";
			}
		} else {
			$f = "L";
		}
		
		if ($f == "Q" || $f == "A" || $f == "M" || $f == "D") {
			if (!isset($_POST['id']) || !isset($_POST['id1']) || !isset($_POST['id2'])) {
				$f = "L";
			} else {
				$id = $_POST['id'];
				$id1 = $_POST['id1'];
				$id2 = $_POST['id2'];				
			}
		}
?>

<?php if ($f == "Q" || $f == "A" || $f == "M" || $f == "D") {
	if ($f != "A") {
		//$strSQL = "SELECT COUNT(*) FROM site_cctv WHERE SiteID='".$id."' AND CompanyID='".$id1."' AND ZoneID='".$id2."'";
		$strSQL = "SELECT COUNT(*) FROM site_cctv WHERE SiteID='".$id."' AND Host='".$id1."'";
		$rows = mysql_query($strSQL);
		list($i) = mysql_fetch_row($rows);
		if ($i == 0) {
			$err = "資料錯誤或不存在,請檢查後再執行 !";
		} else {
			$strSQL = "SELECT DISTINCT SiteID,Host,Port,Type FROM site_cctv WHERE SiteID='".$id."' AND Host='".$id1."'";
			$rows = mysql_query($strSQL);
			$row = mysql_fetch_array($rows);			
		}
	}
	$fW = "";
	$fS = "";
	$fSS = "";
	$fV = "";
	if ($f == "Q") { $fW = "Q"; }
	if ($f == "A") { $fW = "AW"; $fS = "新增"; $fSS = "儲存"; }
	if ($f == "M") { $fW = "MW"; $fS = "修改"; $fSS = "儲存"; }
	if ($f == "D") { $fW = "DW"; $fS = "刪除"; $fSS = "刪除"; }
	if ($err != "") {
		echo $err;
	} else {	
?>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<form action="SiteDAMD.php" name="DetailForm" id="DetailForm" method="POST">
		<table align="center" class="table90">
			<tr class="bg_gray">
				<th colspan="5">
					<table width="100%">
						<tr>
							<th class="Arial18Bold align_left"><?php echo '<font color="red">'.$fS.'</font>'; ?>攝影機維護作業<input name="f" id="f" value="<?php echo $fW; ?>" type="HIDDEN"></th>			
							<th class="Arial18Bold align_right">
							<?php
								if ($f != "Q") {
									if ($f == "D") {				
										//檢查是否已被使用
										//$err = "資料已被調用,不可刪除 !";
									}
									if ($err == "") {
										echo '<input type="Button" name="submit" value="'.$fSS.'" style="font-family: 標楷體 ,Arial, Helvetica, sans-serif;background-color: #FF8888;" class="Arial16Bold" onclick="refreshDataDetail(\'SiteD.php\',\'SiteDAMD.php\',\''.$id.'\');">';
									}
								}
							?>
							</th>
						</tr>
					</table>
				</th>
			</tr>
			<tr>
				<th class="Arial16Bold_Right">工地代碼：</th>
				<td class="Arial16"><input name="SiteID" type="text" id="SiteID" style="font-family: 標楷體 ,Arial, Helvetica, sans-serif;" readonly value="<?php echo $id; ?>"></td>
				<td rowspan="4" align="left" class="Arial16">
				<?php
					$html = '<table width="100%">';				
					for ($x = 1; $x <= 16; $x++) {	
						if ($x % 4 == 1) { $html .= '<tr>'; }
						$html .= '<td><input name="Camera[]" type="checkbox" id="Camera" style="font-family: 標楷體 ,Arial, Helvetica, sans-serif;" ';
						if ($f == "Q" || $f == "D") { $html .= " disabled "; }
						$html .= ' value="'.$x.'" ';
						if ($f != "A") {
							$strTemp = "SELECT COUNT(*) FROM site_cctv WHERE SiteID='".$id."' AND Host='".$id1."' AND Camera=".$x;
							$rowsTemp = mysql_query($strTemp);
							list($i) = mysql_fetch_row($rowsTemp);
							if ($i > 0) { $html .= " checked "; }
						}
						$html .= '>攝影機_'.$x.'</td>';
						if ($x % 4 == 0) { $html.='</tr>'; }
					}
					echo $html .= '</table>';
				?>
				</td>							
			</tr>
			<tr>	
				<th class="Arial16Bold_Right">主機位置：</th>				
				<td class="Arial16"><input name="Host" type="text" id="Host" style="font-family: 標楷體 ,Arial, Helvetica, sans-serif;" <?php if ($f != "A") { echo "readonly"; } ?> value="<?php if ($f != "A") { echo $id1; } ?>"></td>
				<td rowspan="1" align="left" class="Arial16">
			</tr>
			<tr>				
				<th class="Arial16Bold_Right">連接埠號：</th>
				<td class="Arial16"><input name="Port" type="text" id="Port" style="font-family: 標楷體 ,Arial, Helvetica, sans-serif;" <?php if ($f == "Q" || $f == "D") { echo "readonly"; } ?> value="<?php if ($f != "A") { echo $row['Port'];} ?>"></td>
				
				<td rowspan="1" align="left" class="Arial16">
			</tr>
			<tr>				
				<th class="Arial16Bold_Right">類型：</th>
<td class="Arial16">
					<select name="Type" id="Type" style="font-family: 標楷體 ,Arial, Helvetica, sans-serif;" <?php if ($f == "Q") { echo "disabled"; } ?>>
					<?php
						$Type = array("DVR","DVR","CARD","CARD");
						for ($i = 0; $i < sizeof($Type); $i = $i + 2) {
							$selected = "";
							if ($f != "A") {
								if ($row['Type'] == $Type[$i]) {
									$selected = "selected";
									if ($f == "Q" || $f == "D") {
										echo '<option value="'.$Type[$i].'" '.$selected.'>'.$Type[$i+1].'</option>';
									}
								}
							}										
							if ($f == "A" || $f == "M") {
								echo '<option value="'.$Type[$i].'" '.$selected.'>'.$Type[$i+1].'</option>';
							}
						} 
					?>
					</select>					
				</td>

				<td rowspan="1" align="left" class="Arial16">
			</tr>
			
			<tr>
				<th colspan="3" class="Arial18Bold"><font color="red"><?php if ($err != "") { echo $err; } ?></font></th>
			</tr>
		</table>
	</form>
<?php }} ?>
				
<?php		
		//刪除資料
		if ($f == "DW") {
			if (isset($_POST['SiteID']) && isset($_POST['Host'])) {
				$strSQL = "DELETE FROM Site_cctv WHERE SiteID='".$_POST['SiteID']."' AND Host='".$_POST['Host']."'";
				mysql_query($strSQL);
			} else {
				$err = "資料錯誤或不存在 ,請檢查後再執行!";
			}
			$f = "L";
		}
		
		// 新增或修改儲存
		if ($f == "AW" || $f == "MW") {
			// 檢查新增或更改時之輸入值是否正確
			if (!isset($_POST['Host']) || $_POST['Host'] == "") {
				$err = $err.'主機位置不存在或輸入值不正確 !'.chr(13);
			}

			// 新增或修改->寫入資料庫
			if ($err == "") {
				if ($f == "AW") {
					// 檢查Key值是否重覆
					$strSQL = "SELECT COUNT(*) FROM Site_cctv WHERE SiteID='".$_POST['SiteID']."' AND Host='".$_POST['Host']."'";
					$rows = mysql_query($strSQL);
					list($i) = mysql_fetch_row($rows);
					if ($i > 0) {
						$err = "資料已存在,請檢查後再執行 !";
					}
				}
								
				if ($err == "") {
					// 變更Table:site_function資料(該工地可用 之Funcction)
					// 先刪除
					$strTemp = "DELETE FROM Site_cctv WHERE SiteID='".$_POST['SiteID']."' AND Host='".$_POST['Host']."'";
					mysql_query($strTemp);
					// 再新增
					if (isset($_POST['Camera']) && sizeof($_POST['Camera']) > 0) {
						for ($i = 0; $i < sizeof($_POST['Camera']) ; $i++) {
							//$strTemp = "SELECT COUNT(*) FROM function WHERE FunctionID='".$_POST['FunctionID'][$i]."'";
							//$rowsTemp = mysql_query($strTemp);
							//list($count) = mysql_fetch_row($rowsTemp);
							//if ($count > 0) { 
								$strTemp = "INSERT INTO Site_cctv (SiteID,Host,Port,Type,Camera) VALUES ('".$_POST['SiteID']."','".$_POST['Host']."','".$_POST['Port']."','".$_POST['Type']."','".$_POST['Camera'][$i]."')";
								mysql_query($strTemp);
							//}
						}
					}
				}		
			}
			$f = "L";
		}

		// L : 回列表 
		if ($f == "L") {
			echo $err;
		}
	}
?>