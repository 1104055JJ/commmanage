<script type="text/javascript">
document.getElementById("DetailForm").elements.namedItem("Fee").onchange = function() {DFunction()};
document.getElementById("DetailForm").elements.namedItem("Renter").onchange = function() {DFunction()};
document.getElementById("DetailForm").elements.namedItem("Discount").onchange = function() {DFunction()};
document.forms["DetailForm"].onsubmit = function() { return DFunction()};


function DFunction() {
	var event = true;
	document.forms["DetailForm"].elements["Fee"].style.border="";
    document.forms["DetailForm"].elements["Discount"].style.border="";
    document.forms["DetailForm"].elements["NetFee"].style.border="";
    document.forms["DetailForm"].elements["Payer"].style.border="";
    document.forms["DetailForm"].elements["Owner"].style.border="";
    document.forms["DetailForm"].elements["BillCycle"].style.border="";
    document.forms["DetailForm"].elements["FeePerMonths"].style.border="";	
	
	
    var Fee = document.forms["DetailForm"].elements["Fee"]; 
    x = CheckNum("Rec","DetailForm","Renter","錯誤");
    //alert(x);
    var re = /^\d+$/;   
    if (Fee.value == ""||Fee.value<=0||!re.test(document.forms["DetailForm"].Fee.value)) { 
    	Fee.value = "";
    	$('#Fee', document.forms["DetailForm"]).attr("placeholder","請輸入正確數字");
    	document.forms["DetailForm"].elements["Fee"].style.border="1px red dotted";
    	event = false;
    	} 
    	
    var Discount = document.forms["DetailForm"].elements["Discount"];
    if (!re.test(document.forms["DetailForm"].Discount.value)){Discount.value = "";}   
    if (Discount.value == ""||Discount.value<0) { 
    	Discount.value = "0";
    	} 
    //if (Discount.value == "") { $('#Discount', document.forms["DetailForm"]).attr("placeholder","請輸入正確數字");document.forms["DetailForm"].elements["Discount"].style.border="1px red dotted";}   
    var NetFee = document.forms["DetailForm"].elements["NetFee"];   
    if(parseInt(Fee.value)>=parseInt(Discount.value)){
    	NetFee.value = parseInt(Fee.value)-parseInt(Discount.value); 
    }
    else
    {
    	NetFee.value ="";
    	$('#NetFee', document.forms["DetailForm"]).attr("placeholder","管理費或折扣金額錯誤");
    	document.forms["DetailForm"].elements["Discount"].style.border="1px red dotted";
    	document.forms["DetailForm"].elements["Fee"].style.border="1px red dotted";
    	document.forms["DetailForm"].elements["NetFee"].style.border="1px red dotted";
    	event = false;
    }
    
        var Payer = document.forms["DetailForm"].elements["Payer"];
    if (Payer.value == "") {$('#Payer', document.forms["DetailForm"]).attr("placeholder","請輸入繳款人");document.forms["DetailForm"].elements["Payer"].style.border="1px red dotted"; event = false;}
    
	var Owner = document.forms["DetailForm"].elements["Owner"];
    if (Owner.value == "") {$('#Owner', document.forms["DetailForm"]).attr("placeholder","請輸入所有權人");document.forms["DetailForm"].elements["Owner"].style.border="1px red dotted"; event = false;}
    
	var BillCycle = document.forms["DetailForm"].elements["BillCycle"];
    if (BillCycle.value == "") {$('#BillCycle', document.forms["DetailForm"]).attr("placeholder","請選擇繳費週期");document.forms["DetailForm"].elements["BillCycle"].style.border="1px red dotted"; event = false;}
            
	var FeePerMonths = document.forms["DetailForm"].elements["FeePerMonths"];
    if (FeePerMonths.value == "") {$('#FeePerMonths', document.forms["DetailForm"]).attr("placeholder","請選擇繳費月份");document.forms["DetailForm"].elements["FeePerMonths"].style.border="1px red dotted"; event = false;}
    
    
    
    var ModUser = document.forms["DetailForm"].elements["ModUser"];
    ModUser.value = "1201082";
    var now = new Date();
 	var ModDate = document.forms["DetailForm"].elements["ModDate"];
 	var currentdate = new Date(); 
	var datetime =  currentdate.getFullYear() + "-"
                + (currentdate.getMonth()+1)  + "-" 
                + currentdate.getDate() + " "  
                + currentdate.getHours() + ":"  
                + currentdate.getMinutes() + ":" 
                + currentdate.getSeconds();
 	ModDate.value = datetime;
 	return event;
}


</script>



