<?php
include("../common/connectdb.php");	
include_once("../common/PublicFunction.php");
?>	
	<p>請選擇結轉帳戶總類、年度、月份</p>
	<select id="s_acc" name="s_acc" class="Arial14">
		<?PHP
			$where = "unifieddatacode where CatagoryID = 'A06'";
			SelectOption($where,'CodeID','CodeName','');
		?>
	</select>
	年度
	<select id="s_year" name="s_year" class="Arial14">
		<option value =2015>2015年</option>
		<option value =2016>2016年</option>
	</select>
	月
	<select id="s_month" name="s_month" class="Arial14">
		<option value =1>1月</option>
		<option value =2>2月</option>
		<option value =3>3月</option>
		<option value =4>4月</option>
		<option value =5>5月</option>
		<option value =6>6月</option>
		<option value =7>7月</option>
		<option value =8>8月</option>
		<option value =9>9月</option>
		<option value =10>10月</option>
		<option value =11>11月</option>
		<option value =12>12月</option>
	</select>
	<br>
	<button id="b_gen" >產生</button>
	<br>
	<div id="msg">註：本程式產生餘額為每月底之餘額</div>
<script type="text/javascript">

$( "#b_gen" ).click(function() {
	closeAcc();
});
function closeAcc(){
 			var e = document.getElementById("s_acc");
 			var f = document.getElementById("s_year");
 			var g = document.getElementById("s_month");
			var accid = e.options[e.selectedIndex].value;
			var year = f.options[f.selectedIndex].value;
			var month = g.options[g.selectedIndex].value;
			$.ajax({
				url: "CM_Bank_closeAcc.php",
				type:"POST",
				dataType: "text",
				data:{accid:accid,year:year,month:month},
				//成功執行並返回值
				success: function(data){
					$("#msg").empty().append(data);  
					//alert(data); 
					$("#dialogWindow").dialog( "option", "height", 400 );
					refreshDataM('CM_Bank.php',10,10,1,'','','','',''); 
				},
				//發送請求之前會執行的函式
				beforeSend:function(){
				},
				//請求完成時執行的函式(不論結果是success或error)
				complete:function(){
				},
				error:function(xhr, ajaxOptions, thrownError){ 
					alert(xhr.status);
					alert(thrownError);
				}
			});
}
</script>