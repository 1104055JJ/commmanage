<?php
	// 未登入則轉至首頁(登入頁)
	session_start();
	if (!(isset($_SESSION["manageuser"]))) {
		echo '<meta http-equiv=REFRESH CONTENT=1;url=index.php>';		
	} else {
		include("../common/connectdb.php");
		include("../common/AutoFormClass.php");

		$a = new AutoFormClass("D","PA_RequestDetail.php","PA_RequestDetailAMD.php","paymentrequestdetail","paymentrequestdetail",array("CommID","RequestNo","DetailNo","SupplierID"),"請款單身基本資料","center","table90");
		$a->setTableTitle("left","Arial18Bold bg_gray");
		$a->setFieldTitle("center","Arial16 bg_y");
		$a->setField("CommID","社區","left","Arial14","N","N","","","Y","N","Y","N","label",array(array(),""),$_SESSION['Community']);
		$a->setField("RequestNo","請款單號","left","Arial14","Y","Y","","","Y","N","Y","N","label",array(array(),""),"","");
		$a->setField("DetailNo","項次","left","Arial14","N","Y","","","Y","Y","Y","N","hidden",array(array(),""),0);
                $a->setField("RequestDate","請購日期","left","Arial14","N","N","","","Y","Y","Y","Y","date",array(array(),""),"");
                $a->setField("PaymentItemID","支出項目","left","Arial14","Y","N","","<em id=PaymentItemName></em>","Y","N","Y","N","love",array(array("CommID","out","PA02","code"),""),"");
                $a->setField("SupplierID","","left","Arial14","N","N","","","Y","N","Y","N","hidden",array(array(),""),"");
                $a->setField("Amount","數量","left","Arial14","N","Y","","","Y","Y","Y","Y","text",array(array(),""),0,"");
		$a->setField("UnitPrice","單價","left","Arial14","N","Y","","","Y","Y","Y","Y","text",array(array(),""),0,"");
                $a->setField("TotalAmount","複價","left","Arial14","Y","Y","","","Y","N","Y","N","text",array(array(),""),0);
                $a->setField("Paid","已付金額","left","Arial14","N","Y","","","N","N","Y","N","label",array(array(),""),0);
                $a->setField("Unpaid","未付金額","left","Arial14","Y","Y","","","N","N","Y","N","label",array(array(),""),0);
                $a->setField("Note","備註","left","Arial14","Y","N","","","Y","Y","Y","Y","text",array(array(),""),"","size=60");
                $a->setField("ModUser","修改人員","left","Arial14","N","N","","","N","N","Y","N","label",array(array(),""),$_SESSION['manageuser']);
		$a->setField("ModDate","修改時間","left","Arial14","N","N","","","N","N","Y","N","date",array(array(),""),"","",array("datetime","Y","N"));
		$a->setBeforeForm("Y","./PA_RequestJS.php");
                $a->setAfterWriteToDataBase('PA_Update_RequestMaster', array('CommID','RequestNo'),array("AW","MW","DW"));
		//$a->addBeforeDeleteCheck("community",array("ZoneID","ZoneID"),"已被社區基本資料叫用,不可刪除!");
		$a->showData();
	}
