<?php
include("../common/connectdb.php");	
session_start();
$CommID=$_SESSION['Community'];
$year = $_REQUEST['Year'];
$d2year=$year-2000;
$month = $_REQUEST['Month'];
$Current_YearMonth=$year*100+$month*1; //報表列印期間201504
$Lastyear = $year*1-1;
$d2Lastyear=$Lastyear-2000;
//標題
$sql = "Select CommName From community Where CommID ='".$CommID."'";
$rows=mysql_query($sql);
$row=mysql_fetch_array($rows);

$CommName=$row['CommName'];  //社區名
$html = "<h2 align=\"center\">".$CommName."社區結算至".$year."年".$month."月止管理費欠繳明細表</h2>";
$html .="
<table border=\"1\" cellpadding=\"4\" align=\"center\">
<thead>
	<tr bgcolor=\"#e6e6fa\" align=\"center\">
		<td width=\"55\">門牌戶號</td>		
    <td width=\"65\">繳款人</td>
    <td width=\"60\">每月管理費</td>
    <td width=\"60\">每月車位清潔費</td>
    <td width=\"60\">欠繳總額</td>
		<td width=\"250\">欠繳月份</td>
		<td width=\"100\">備註</td></tr></thead>";      

$Sum_TotalUnReceivedAmount=0;
$ARINFOsql = "select household.HouseHoldID, household.Payer as Payer, household.Owner as Owner, household.Renter as Renter, household.NetFee as HouseFee, sum(carspace.NetFee) as CarFee from household left join carspace on household.HouseHoldID=carspace.HouseHoldID and household.CommID=carspace.CommID where household.CommID='".$_SESSION['Community']."' group by household.HouseHoldID";
$ARINFOrows = mysql_query($ARINFOsql) ;
while($ARINFOrow = mysql_fetch_array($ARINFOrows))//while收款項目
{
	//近一年AR明細
  $TotalUnReceivedAmount=0; //該戶所有AR未繳總額
  $UnReceivedMemo='';       //該戶所有AR未繳備註
	//$ARsql = "select ar.ArNo, Year, Month, TotalAmount, sum(WriteOffAmount), Unreceived from ar left join  collectionsdetail on collectionsdetail.commid= ar.commid and collectionsdetail.ArNo=ar.ArNo where HouseHoldID='".$ARINFOrow["HouseHoldID"]."' and CommID='".$_SESSION['Community']."' and Year >= $d2Lastyear group by ar.ArNo";
  $ARsql = "select ArNo, Year, Month, 200000+Year*100+Month as AR_YearMonth, HouseFee, CarFee, TotalAmount from ar where HouseHoldID='".$ARINFOrow["HouseHoldID"]."' and CommID='".$_SESSION['Community']."' and Year >=$d2Lastyear and Year <=$d2year";
	$ARrows = mysql_query($ARsql) ;
 	while($ARrow = mysql_fetch_array($ARrows))//清查AR項目繳費狀況
  {
    $AR_YearMonth=$ARrow["AR_YearMonth"]*1;
    if ($AR_YearMonth <= $Current_YearMonth)
    {
      $ARAmount=$ARrow["TotalAmount"]*1;
      $Writeoff_Amount=0; //該筆AR所有沖消金額

      $sql_Detail = "select WriteOffAmount, Year(ModDate)*100+Month(ModDate) as WriteOff_YearMonth from collectionsdetail where ArNo='".$ARrow["ArNo"]."' and CommID='".$_SESSION['Community']."'";
	    $rows_Detail = mysql_query($sql_Detail) ;
      while($row_Detail = mysql_fetch_array($rows_Detail))//while沖消明細
      {  $WriteOff_YearMonth=$row_Detail["WriteOff_YearMonth"]*1;
        if ($WriteOff_YearMonth <= $Current_YearMonth)//早於報表選印日期才計入沖消 
        {$Writeoff_Amount=$Writeoff_Amount+$row_Detail["WriteOffAmount"]*1;}
      }//end while 沖消明細
      $TotalUnReceivedAmount= $TotalUnReceivedAmount+$ARAmount*1-$Writeoff_Amount*1;//預付款(當月前所有應收-當月前給付)
      $MemoInfo=200000+$ARrow["Year"]*100+$ARrow["Month"];    //AR資訊
      $MemoAmount=$ARAmount*1-$Writeoff_Amount*1;             //未結金額
      if($ARAmount*1-$Writeoff_Amount*1 >0)
      {$UnReceivedMemo.="$MemoInfo"."($"."$MemoAmount)";}     //記錄未收內容
    }
  }//end while 清查AR項目繳費狀況
      $Sum_TotalUnReceivedAmount=$Sum_TotalUnReceivedAmount+$TotalUnReceivedAmount;
  
  if($TotalUnReceivedAmount>0)
	{
    $html.="
	   <tr>
		    <td width=\"55\" align=\"right\">".$ARINFOrow["HouseHoldID"]."</td>";//戶別代號
        if ($ARINFOrow["Payer"] == '1')
        {$html.="<td width=\"65\">".$ARINFOrow["Owner"]."</td>";}
        else
        {$html.="<td width=\"65\">".$ARINFOrow["Renter"]."</td>";}

    $html.="<td width=\"60\" align=\"right\">".$ARINFOrow["HouseFee"]."</td>
              <td width=\"60\" align=\"right\">".$ARINFOrow["CarFee"]."</td>    
              <td width=\"60\" align=\"right\">".$TotalUnReceivedAmount."</td>
              <td width=\"250\" align=\"left\">".$UnReceivedMemo."</td>
              <td width=\"100\" align=\"left\"></td>";
	  $html.="</tr>";              
  }     
}//end while 收款項目
  
 $html.="
    <tr bgcolor=\"#e6e6fa\" align=\"center\">
		<td width=\"55\"></td>
    <td width=\"65\" align=\"right\"></td>
    <td width=\"60\" align=\"right\"></td>
		<td width=\"60\" align=\"right\">總計</td>
    <td width=\"60\" align=\"right\">".$Sum_TotalUnReceivedAmount."</td>
    <td width=\"250\" align=\"left\"></td>
    <td width=\"100\" align=\"left\"></td></tr>";            

$html.="</table>";

if ($_REQUEST['FORMAT']=="HTML")
{
	echo '<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="zh-tw" lang="zh-tw">';
	echo '<head>';
	echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
	echo '</head>';
	echo $html;
}

if ($_REQUEST['FORMAT']=="PDF")
{
require_once('../tcpdf/config/zho.php');
require_once('../tcpdf/config/tcpdf_config.php');
require_once('../tcpdf/tcpdf.php');
//客製頁首頁尾
// Extend the TCPDF class to create custom Header and Footer
class MYPDF extends TCPDF {

	//Page header
	/*
	public function Header() {
		// Logo
		$image_file = K_PATH_IMAGES.'logo_example.jpg';
		$this->Image($image_file, 10, 10, 15, '', 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);
		// Set font
		$this->SetFont('helvetica', 'B', 20);
		// Title
		$this->Cell(0, 15, '<< TCPDF Example 003 >>', 0, false, 'C', 0, '', 0, false, 'M', 'M');
	}  */

	// Page footer
	public function Footer() {
	/*	// Position at 15 mm from bottom
		$this->SetY(-15);
		// Set font
		$this->SetFont('droidsansfallback', 'I', 14);
		//$this->setFooterMargin(80);
		// Page number
		$this->Cell(0, 10, '主任委員：　 　                 監察委員:　 　                財務委員：　 　                現場主任:　 　                ', 0, false, 'C', 0, '', 0, false, 'T', 'M');
    */}        
}

//實體化PDF物件
$pdf = new MYPDF("L", PDF_UNIT,'A4', true, 'UTF-8', false);
//$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->setPrintHeader(false); //不要頁首
$pdf->setPrintFooter(true); //頁尾

// set margins
//$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
//$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);  //設定自動分頁

$pdf->setLanguageArray($l); //設定語言相關字串

$pdf->setFontSubsetting(true); //產生字型子集（有用到的字才放到文件中）

$pdf->SetFont('droidsansfallback', '', 10, '', true); //設定字型

$pdf->AddPage(); //新增頁面

$pdf->setTextShadow(array('enabled'=>false, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));//文字陰影
	
	
	$pdf->writeHTML($html);
	$pdf->Output('contact.pdf', 'I');	
}

if ($_REQUEST['FORMAT']=="EXCEL")
{
require_once '../Excel/PHPExcel.php';    //引入 PHPExcel 物件庫
require_once '../Excel/PHPExcel/IOFactory.php';    //引入 PHPExcel_IOFactory 物件庫
$objPHPExcel = new PHPExcel();  //實體化Excel

//----------內容-----------//
$objPHPExcel->setActiveSheetIndex(0);  //設定預設顯示的工作表
$objActSheet = $objPHPExcel->getActiveSheet(); //指定預設工作表為 $objActSheet
$objActSheet->setTitle("管理費欠繳明細表");  //設定標題
$objPHPExcel->createSheet(); //建立新的工作表，上面那三行再來一次，編號要改

$objActSheet->getColumnDimension('A')->setWidth(12);
$objActSheet->getColumnDimension('B')->setWidth(10);
$objActSheet->getColumnDimension('C')->setWidth(10);
$objActSheet->getColumnDimension('D')->setWidth(15);
$objActSheet->getColumnDimension('E')->setWidth(15);
$objActSheet->getColumnDimension('F')->setWidth(45);
$objActSheet->getColumnDimension('G')->setWidth(15);

$objActSheet-> getStyle('A1:G1')-> getFont()-> setName('SimHei')-> setSize('16');
$objActSheet-> getStyle('A1:G1')-> getFill()-> setFillType(PHPExcel_Style_Fill:: FILL_SOLID)-> getStartColor()-> setARGB('FFC9E3F3');
$objActSheet->mergeCells("A1:G1")->setCellValue("A1", $CommName."社區".$year."年".$month."月管理費欠繳明細表");
$objActSheet-> getStyle('A2:G2')-> getFont()-> setName('SimHei')-> setSize('12');
$objActSheet-> getStyle('A2:G2')-> getFill()-> setFillType(PHPExcel_Style_Fill:: FILL_SOLID)-> getStartColor()-> setARGB('FFC9E3F3');

$objActSheet->setCellValue("A2", '門牌戶號')
            ->setCellValue("B2", '繳款人')
            ->setCellValue("C2", '每月管理費')
            ->setCellValue("D2", '每月車位清潔費')
            ->setCellValue("E2", '欠繳總額')
            ->setCellValue("F2", '欠繳月份')
            ->setCellValue("G2", '備註');

$Sum_TotalUnReceivedAmount=0;
$i=3;
$ARINFOrows = mysql_query($ARINFOsql) ;
while($ARINFOrow = mysql_fetch_array($ARINFOrows))
{ //近一年AR明細
  $TotalUnReceivedAmount=0; //該戶所有AR未繳總額
  $UnReceivedMemo='';       //該戶所有AR未繳備註
  $ARsql = "select ArNo, Year, Month, 200000+Year*100+Month as AR_YearMonth, HouseFee, CarFee, TotalAmount from ar where HouseHoldID='".$ARINFOrow["HouseHoldID"]."' and CommID='".$_SESSION['Community']."' and Year >=$d2Lastyear and Year <=$d2year";
	$ARrows = mysql_query($ARsql) ;
 	while($ARrow = mysql_fetch_array($ARrows))//清查AR項目繳費狀況
  { $AR_YearMonth=$ARrow["AR_YearMonth"]*1;
    if ($AR_YearMonth <= $Current_YearMonth)
    {
      $ARAmount=$ARrow["TotalAmount"]*1;
      $Writeoff_Amount=0; //該筆AR所有沖消金額

      $sql_Detail = "select WriteOffAmount, Year(ModDate)*100+Month(ModDate) as WriteOff_YearMonth from collectionsdetail where ArNo='".$ARrow["ArNo"]."' and CommID='".$_SESSION['Community']."'";
	    $rows_Detail = mysql_query($sql_Detail) ;
      while($row_Detail = mysql_fetch_array($rows_Detail))//while沖消明細
      {  $WriteOff_YearMonth=$row_Detail["WriteOff_YearMonth"]*1;
        if ($WriteOff_YearMonth <= $Current_YearMonth)//早於報表選印日期才計入沖消 
        {$Writeoff_Amount=$Writeoff_Amount+$row_Detail["WriteOffAmount"]*1;}
      }//end while 沖消明細
      $TotalUnReceivedAmount= $TotalUnReceivedAmount+$ARAmount*1-$Writeoff_Amount*1;//預付款(當月前所有應收-當月前給付)
      $MemoInfo=200000+$ARrow["Year"]*100+$ARrow["Month"];    //AR資訊
      $MemoAmount=$ARAmount*1-$Writeoff_Amount*1;             //未結金額
      if($ARAmount*1-$Writeoff_Amount*1 >0)
      {$UnReceivedMemo.="$MemoInfo"."($"."$MemoAmount)";}     //記錄未收內容
    }
  }//end while 清查AR項目繳費狀況
      $Sum_TotalUnReceivedAmount=$Sum_TotalUnReceivedAmount+$TotalUnReceivedAmount;
 
  //if($TotalUnReceivedAmount>0)
	//{
      $objActSheet->  setCellValue("A{$i}", $ARINFOrow["HouseHoldID"]);
      if ($ARINFOrow["Payer"] == '1')
      {$objActSheet->setCellValue("B{$i}", $ARINFOrow["Owner"]);}
      else
      {$objActSheet->setCellValue("B{$i}", $ARINFOrow["Renter"]);}      

       $objActSheet->setCellValue("C{$i}", $ARINFOrow["HouseFee"])
                   ->setCellValue("D{$i}", $ARINFOrow["CarFee"])
                   ->setCellValue("E{$i}", $TotalUnReceivedAmount)
                   ->setCellValue("F{$i}", $UnReceivedMemo);
  //}     
  $i++;
}

$objActSheet-> getStyle("A{$i}:G{$i}")-> getFill()-> setFillType(PHPExcel_Style_Fill:: FILL_SOLID)-> getStartColor()-> setARGB('FFC9E3F3');
$objActSheet->mergeCells("A{$i}:D{$i}")->setCellValue("A{$i}", '總計');
$n=$i-1;
//check to here 20150504//  
$objActSheet->setCellValue("E{$i}", "=SUM(E3:E{$n})");

header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename='.iconv('UTF-8','Big5','欠繳明細').'.xls');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->setPreCalculateFormulas(false);
$objWriter->save('php://output');
exit;
}
?>