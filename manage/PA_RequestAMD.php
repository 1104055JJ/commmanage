<?php
	// 未登入則轉至首頁(登入頁)

	if (!isset($_SESSION)) { session_start(); }
	if (!(isset($_SESSION["manageuser"]))) {
		echo '<meta http-equiv=REFRESH CONTENT=1;url=index.php>';		
	} else {
		include("../common/connectdb.php");
		include("../common/AutoFormClass.php");
                
                //$Button='<Button type="Button" class="fa fa-search-plus" onclick="OpenWindow(\''.$_SESSION['Community'].'\')"></Button><em id=SupplierName> </em>';
                
		$a = new AutoFormClass("M","PA_Request.php","PA_RequestAMD.php","paymentrequestmaster","paymentrequestmaster",array("CommID","RequestNo","SupplierID"),"請款單頭基本資料","center","table90");
		$a->setTableTitle("left","Arial18Bold bg_gray");
		$a->setFieldTitle("center","Arial16 bg_y");
                $a->setField("CommID","社區","left","Arial14","N","N","","","N","N","N","N","text",array(array(),""),$_SESSION['Community']);
		$a->setField("RequestNo","請款單號","left","Arial14","N","N","","","N","N","Y","N","label",array(array(),""),"","",array("autoserial","Y","N"));
		
               // $a->setField("RequestDate","請購日期","left","Arial14","Y","N","","","Y","Y","Y","Y","date",array(array(),""),"","size=10");
                
                
                $a->setField("SupplierID","供應商","left","Arial14","N","N","","<em id=SupplierName></em>","Y","N","Y","N","love",array(array("CommID","out","PA01","code"),""),"","size=8");
                                                                                                                    //都要與TABLE欄位名稱相同//LOVName 為寫死於autodataclass button 後的text id,
                
                $a->setField("Petty","零用金支付","left","Arial14","Y","N","","","Y","Y","Y","Y","radio",array(array("是","Y","否","N"),""),"N");
                //$a->setField("PaymentItemID","支出項目","left","Arial14","Y","N","","<em id=PaymentItemName></em>","Y","Y","Y","Y","love",array(array("CommID","out","PaymentItemName","in","PaymentItemID","in","PA02","code"),""),"");
                $a->setField("TotalAmount","總金額","left","Arial14","N","Y","","","Y","N","Y","N","hidden",array(array(),""),0);
                $a->setField("Invoice","發票號碼","left","Arial14","Y","N","","","Y","Y","Y","Y","text",array(array(),""),"","");
                $a->setField("FlowStatus","狀態","left","Arial14","Y","N","","","Y","N","Y","N","hidden",array(array(),""),"");
                $a->setField("Note","備註","left","Arial14","Y","N","","","Y","Y","Y","Y","text",array(array(),""),"","size=40");
                //$a->setField("Void","有效否","left","Arial14","Y","N","","","Y","N","Y","N","radio",array(array("是","Y","否","N"),""),"");
                $a->setField("ModUser","修改人員","left","Arial14","N","N","","","N","N","Y","N","label",array(array(),""),$_SESSION['manageuser']);
                $a->setField("ModDate","修改時間","left","Arial14","N","N","","","N","N","Y","N","date",array(array(),""),"","",array("datetime","Y","Y"));
               // $a->setField($fieldName, $displayName, $align, $class, $wrap, $isNumeric, $beforeHTML, $afterHTML, $A_Display, $A_CanEdit, $M_Display, $M_CanEdit, $inputType);
		//$a->addBeforeDeleteCheck("community",array("ZoneID","ZoneID"),"已被社區基本資料叫用,不可刪除!");     
                $a->setBeforeForm("Y","./PA_RequestAMDJS.php");
                $a->showData();
	}
?>
   
