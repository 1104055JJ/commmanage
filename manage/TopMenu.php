<?php
/*
 * 程式名稱 :  
 * 程式功能 : 主要功能為於後台管理頁面於左邊顯示程式列表,點選列表程式後,於內容區顯示相應之程式頁面
 * 傳入參數 :
 * 參數檢查 :
 * 開發人員 : 
 * 開發日期 : 
 * 修改歷程 : 
 */
 
 	if (!isset($_SESSION)) { session_start(); }
	include_once("../common/connectdb.php");	
	
	if (isset($_SESSION["manageuser"])) {
		// Logout
		echo '<script language="JavaScript">';
		echo 'function logout() { window.location = "ManageLogout.php";	}';
		echo '</script>';
		echo '<p></p>';
	
		// 社區選單
		if (isset($_POST["Community"])) { $_SESSION["Community"] = $_POST["Community"]; }
		if (isset($_SESSION["Community"])) {
			echo '<div class="btn-group btn-group-lg">';
			echo '<button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-expanded="false">';
    		// 顯示目前所選之設區名稱
    		$strSQL = "SELECT CommName FROM community WHERE CommID='".$_SESSION["Community"]."'";
			$rows = mysql_query($strSQL);
			$commName = "";
			if (mysql_num_rows($rows) > 0) {
				$row = mysql_fetch_array($rows);
				$commName = $row[0];
			}
    		echo $commName.'<span class="caret"></span>';
  			echo '</button>';
  			echo '<ul class="dropdown-menu" role="menu">';
			// 社區
			$strSQL = "SELECT DISTINCT CommID,CommName FROM v_manage_user_comm_program WHERE UserID='".$_SESSION["manageuser"]."' ORDER BY CONVERT(CommName USING BIG5) ";
			$community_rows = mysql_query($strSQL);
			while ($community_row = mysql_fetch_array($community_rows)) {
				echo "<li onclick=\"refreshDataTopMenu('TopMenu.php','".$community_row["CommID"]."');\"><a href=\"#\">".$community_row["CommName"]."</a></li>";
			}
  			echo '</ul>';
			echo '</div>';
		}

		$j = 0;
		$CModuleID = "";
	 	$strSQL = "SELECT DISTINCT ModuleID,ModuleName,ModuleIcon,ModuleSort,ProgramName,ProgramFileName,ProgramSort FROM v_manage_user_module_program WHERE UserID='".$_SESSION["manageuser"]."'";
		$strSQL .= " UNION ";
		$strSQL .= "SELECT DISTINCT ModuleID,ModuleName,ModuleIcon,ModuleSort,ProgramName,ProgramFileName,ProgramSort FROM v_manage_user_comm_program WHERE UserID='".$_SESSION["manageuser"]."' AND CommID='".$_SESSION["Community"]."'";
	 	$strSQL .= " ORDER BY ModuleSort,ProgramSort";
	 	$module_rows = mysql_query($strSQL);
		while ($module_row = mysql_fetch_array($module_rows)) {
			// 程式模組
			if ($CModuleID == "" || $CModuleID != $module_row["ModuleID"]) {
				if ($CModuleID != "") {
  					echo '</ul>';
					echo '</div>';
				}
				$CModuleID = $module_row["ModuleID"];
				echo '<div class="btn-group btn-group-lg">';
				echo '<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false">';
    			echo $module_row["ModuleName"].'<span class="caret"></span>';
  				echo '</button>';
  				echo '<ul class="dropdown-menu" role="menu">';
			}
  			// 程式
			echo "<li onclick=\"refreshDataM('".$module_row["ProgramFileName"]."',10,10,1,'','','','','');\"><a href=\"#\">".$module_row["ProgramName"]."</a></li>";    				
		}
  		echo '</ul>';
		echo '</div>';

		// 個人設定	
		echo '<div class="btn-group btn-group-lg">';
		echo '<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false">';
	    echo "個人設定".'<span class="caret"></span>';
	  	echo '</button>';
	  	echo '<ul class="dropdown-menu" role="menu">';
		// 依是否已登入決定顯示值
		if (isset($_SESSION["manageuser"])) {
			echo '<li><a href="#">使用者:'.$_SESSION['manageusername'].'</a></li>';
			echo '<li class="divider"></li>';
			$strSQL = "SELECT IsLDAP FROM sysref";
			$rows = mysql_query($strSQL);
			$row = mysql_fetch_array($rows);
			if ($row["IsLDAP"] == "N") {
				echo "<li onclick=\"refreshDataM('"."changepass_w.php"."',10,10,1,'','','','','');\">"."<a href='#'>變更密碼</a></li>";
				echo '<li class="divider"></li>';				
			}
			echo '<li onclick="logout();"><a href="#">登出</a></li>';
		}
	  	echo '</ul>';
		echo '</div>';
	}
?>