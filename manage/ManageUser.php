<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
	// 未登入則轉至首頁(登入頁)
	if (!isset($_SESSION)) { session_start(); }
	if (!(isset($_SESSION["manageuser"]))) {
		echo '<meta http-equiv=REFRESH CONTENT=1;url=index.php>';		
	} else {
?>
		<script type="text/javascript" src="../js/MasterJSB.js"></script>
<?php
		include_once("../common/connectdb.php");
		include_once("../common/AutoDataClassB.php");
		include_once("../common/AutoPageSwitchClass.php");	

		// 建立及初使化頁數切換
		$a = new AutoPageSwitchClass("M","ManageUser.php","SELECT COUNT(*) FROM manage_user",array(10,15,20,25));			
		
		// 建立及初始化表格
		$b = new AutoDataClass("M","ManageUser.php","ManageUserAMD.php",array("UserID"),"","管理者基本資料單頭(列表)","center","");
		$b->setTableTitle("left","font-18-bold bg_gray");
		$b->setFieldTitle("center","font-16-bold bg_y");
		$b->setDefaultFieldAlign("left");
		$b->setDefaultFieldClass("font-14");		
		$b->setButtonDefaultClass("btn");
		// 配合頁數切換,設定表格資料來源
		$b->beginRowsNum = $a->beginRowsNum;
		$b->setQuery("UserID,UserName,RoleID,CommID,Tel,Email,Enable,Note,ModUser,ModDate","manage_user","","",$a->beginRowsNum.",".$a->perPageRows);
		// 設定表格欄位顯示
		$b->setField("UserID","帳號");
		$b->setField("UserName","名稱");
		$b->setField("RoleID","系統角色權限");
		$b->setField("CommID","預設社區代碼");		
		$b->setField("Tel","電話");
		$b->setField("Email","電子信箱");
		$b->setField("Enable","啟用","center");
		$b->setField("Note","備註");			
		$b->setField("ModUser","異動人員");
		$b->setField("ModDate","異動日期");
		// 增加按鈕及觸發事件
		// 設定表格底部顯示頁數切換
		$b->setTableBottom($a->getShowData());
		// 顯示表格資料
		$b->showData();
	}
?>
