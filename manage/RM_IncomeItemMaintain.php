<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
	// 未登入則轉至首頁(登入頁)
	session_start();
	if (!(isset($_SESSION["manageuser"]))) 
  {echo '<meta http-equiv=REFRESH CONTENT=1;url=index.php>';}
  //else if (!(isset($_POST['SubMenu']) && isset($_POST['ProgramItem']))) 
  //{echo '<meta http-equiv=REFRESH CONTENT=1;url=index.php>';}
  else
  {
?>
 		<script type="text/javascript" src="../js/MasterJSB.js"></script>
		<!--<script language="JavaScript">refreshDataMAMD('ProgramAMD.php','A','');</script>-->

<?php
		// 記錄點選了那個SubMenu中之程式
		//$_SESSION['SubMenu'] = $_POST['SubMenu'];
		//$_SESSION['ProgramItem'] = $_POST['ProgramItem'];
    //記錄社區
    $Society = "CommID='".$_SESSION['Community']."' and IncomeItemID <> '000'"; 

		include("../common/connectdb.php");
		include("../common/AutoDataClassB.php");
		include("../common/AutoPageSwitchClass.php");
		//include_once("../common/PublicFunction.php");	

		// 建立及初使化頁數切換
		//$a = new AutoPageSwitchClass("M","RM_IncomeItemMaintain.php","SELECT COUNT(*) FROM incomeitem where CommID='9BA0001'",array(5,10,15,20,25));			
    $a = new AutoPageSwitchClass("M","RM_IncomeItemMaintain.php","SELECT COUNT(*) FROM incomeitem where IncomeItemID <> '000' and CommID='".$_SESSION['Community']."' ",array(5,10,15,20,25));
		
		// 建立及初始化表格
		$b = new AutoDataClass("M","RM_IncomeItemMaintain.php","RM_IncomeItemMaintainAMD.php",array("CommID","IncomeItemID"),"","收入項目查詢","center","table90");
		$b->setTableTitle("left","Arial18Bold bg_gray");
		$b->setFieldTitle("center","Arial16 bg_y");	
		$b->setOddRow("","bg_light_blue");
		//$b->setEvenRow("","bg_y");
		// 配合頁數切換,設定表格資料來源
		$b->beginRowsNum = $a->beginRowsNum;
		$b->setQuery("*","incomeitem","$Society","",$a->beginRowsNum.",".$a->perPageRows);
		// 設定表格欄位顯示
		$b->setAllFieldAlign("center");
		$b->setAllFieldClass("Arial14");
		$b->setFieldDisplayName("CommID","社區代碼");
 		$b->setFieldDisplay("CommID","N");
		$b->setFieldDisplayName("IncomeItemID","收入代號");
		$b->setFieldDisplayName("IncomeItemName","收入項目名稱");
		$b->setFieldDisplayName("IncomeItemShow","收入顯示名稱");
		$b->setFieldDisplayName("AccountID","關聯帳戶");
		$b->setFieldDisplayName("WOrder","沖帳順序");
		$b->setFieldDisplayName("Enable","使用狀態");
 		$b->setFieldDisplayName("Note","備註");
		// 增加按鈕及觸發事件
		$b->addButton("匯入Excel","onclick=\"openDialogWindow('匯入Excel','./upload.php?url=RM_IncomeItemupload.php');\"","upexcel","btn btn-info");
		$b->addButton("複製收入項目","onclick=\"openDialogWindow('複製收入項目','./RM_IncomeItemCopy_w.php');\"","copyitem","btn btn-info");
		// 設定表格底部顯示頁數切換
		$b->setTableBottom($a->getShowData());
		// 顯示表格資料
		$b->showData();
	}
?>