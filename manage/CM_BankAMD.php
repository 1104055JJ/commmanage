<?php
	// 未登入則轉至首頁(登入頁)
	session_start();
	if (!(isset($_SESSION["manageuser"]))) {
		echo '<meta http-equiv=REFRESH CONTENT=1;url=index.php>';		
	} else {
		include("../common/connectdb.php");
		include("../common/AutoFormClass.php");

		$a = new AutoFormClass("M","CM_Bank.php","CM_BankAMD.php","accountbalance","accountbalance",array("CommID","AccountID","Year","Month"),"銀行帳戶餘額維護作業","center","table90");
		$a->setTableTitle("left","Arial18Bold bg_gray");
		$a->setFieldTitle("center","Arial16 bg_y");
       // $a->setField($fieldName, $displayName, $align, $class, $wrap, $isNumeric, $beforeHTML, $afterHTML, $A_Display, $A_CanEdit, $M_Display, $M_CanEdit, $inputType, $defaultValue)
        $FilterComm = "Select CommName,CommID From Community Where CommID='".$_SESSION['Community']."'"; 
	  //$a->setField($fieldName, $displayName, $align, $class, $wrap, $isNumeric, $beforeHTML, $afterHTML, $A_Display, $A_CanEdit, $M_Display, $M_CanEdit, $inputType, $defaultValue)
		$a->setField("CommID","社區代碼","left","Arial14","N","N","","","Y","Y","Y","N","select",array(array(),$FilterComm),"");
		//$a->setField("CommID","社區代碼","left","Arial14","N","N","","","Y","Y","Y","N","text",array(array(),""),"");
		$a->setField("AccountID","帳戶編號","left","Arial14","Y","N","","","Y","Y","Y","N","combobox",array(array(),"Select Distinct AccountName,AccountID from accountbalance where CommID ='".$_SESSION['Community']."'"),"");
		$a->setField("AccountType","帳戶類型","left","Arial14","Y","N","","","Y","Y","Y","Y","select",array(array(),"Select CodeName,CodeID from unifieddatacode where catagoryid='A06'"),"");
		$a->setField("Year","年度","left","Arial14","N","Y","","","Y",     "Y","Y","Y",    "combobox",array(array(),"select Distinct Year,Year from accountbalance where CommID ='".$_SESSION['Community']."'"),"");
		$a->setField("Month","月份","left","Arial14","Y","Y","","","Y","Y","Y","Y","combobox",array(array(1,1,2,2,3,3,4,4,5,5,6,6,7,7,8,8,9,9,10,10,11,11,12,12),""),"");
		$a->setField("AccountName","帳戶名稱","left","Arial14","N","N","","","Y","Y","Y","Y","text",array(array(),""),"");
		$a->setField("Balance","餘額","left","Arial14","Y","Y","","","Y","Y","Y","Y","text",array(array(),""),"");
		$a->setField("BalanceMod","調整餘額","left","Arial14","N","Y","","","Y","Y","Y","Y","text",array(array(),""),"");
		$a->setField("ModifyNote","調整事由","left","Arial14","Y","N","","","Y","Y","Y","Y","text",array(array(),""),"");
		//$a->setField("B","電話","left","Arial14","N","N","","","Y","Y","Y","Y","text",array(array(),""),"");
		//$a->setField("FAX","傳真","left","Arial14","Y","N","","","Y","Y","Y","Y","text",array(array(),""),"");
		//$a->setField("Address","地址","left","Arial14","Y","N","","","Y","Y","Y","Y","text",array(array(),""),"");
		//$a->setField("Note","備註","left","Arial14","Y","N","","","Y","Y","Y","Y","text",array(array(),""),"");
		//$a->setField("CashAllowZero","零用金允許為零","left","Arial14","Y","N","","","Y","Y","Y","Y","select",array(array(),"Select CodeID,CodeName from unifieddatacode where catagoryid='A02'"),"Y");
		//$a->setField("CoverMethod","沖銷順序","left","Arial14","N","N","","","Y","Y","Y","Y","select",array(array("先房後車","1","先車後房","2"),""),"1");
		//$a->setField("CashAllowZero","零用金允許為零","left","Arial14","Y","Y","","","Y","Y","Y","Y","text",array(array(),""),"");
		//$a->setField("CoverMethod","沖銷順序","left","Arial14","N","N","","","Y","Y","Y","Y","text",array(array(),""),"");			
		$a->setField("Close","關帳","left","Arial14","Y","N","","","Y","Y","Y","Y","select",array(array("Y","Y","N","N"),""),"");
		$a->setField("ModUser","更新者","left","Arial14","Y","N","","","Y","N","Y","N","text",array(array(),""),"",'readonly',array("loginuser","Y","Y"));
		$a->setField("ModDate","更新時間","left","Arial14","Y","N","","","Y","N","Y","N","text",array(array(),""),"",'readonly',array("datetime","Y","Y"));
	//	$a->addBeforeDeleteCheck("HouseHold",array("CommID","CommID"),"已被戶別基本資料叫用,不可刪除!");
		$a->showData();
		if (isset($_POST["f"]) && ($_POST["f"] == "A" || $_POST["f"] == "M")) {
			// 欄位及規則檢查
?>
			<script language="JavaScript">
				var form = "MasterForm";
				var fieldArray = [];
				fieldArray.push(["AccountID","Need","此欄位為必填"]);
				fieldArray.push(["Year","Need","此欄位為必填"]);
				fieldArray.push(["Month","Need","此欄位為必填"]);
				fieldArray.push(["AccountName","Need","此欄位為必填"]);
				fieldArray.push(["Balance","Need","此欄位為必填"]);
				checkRule(form,fieldArray);				
				function otherCheckRule() {
					return true;
				}			
			</script>
<?php
		}
	}
?>