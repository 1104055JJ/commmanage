﻿<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
	// 未登入則轉至首頁(登入頁)
	session_start();
	if (!(isset($_SESSION["manageuser"]))) {
		echo '<meta http-equiv=REFRESH CONTENT=1;url=index.php>';		
	} else {
?>
		<script type="text/javascript" src="../js/MasterJS.js"></script>
		<!--<script language="JavaScript">refreshDataMAMD('ProgramAMD.php','A','');</script>-->
<?php
		include("../common/connectdb.php");
		include("../common/AutoDataClass.php");
		include("../common/AutoPageSwitchClass.php");	
		
		// 建立及初使化頁數切換
		$a = new AutoPageSwitchClass("M","Comm.php","SELECT COUNT(*) FROM Community",array(3,6,9,12));			
		
		// 建立及初始化表格
		$b = new AutoDataClass("M","Comm.php","CommAMD.php",array("CommID"),"","社區基本資料維護作業","left","Arial18 bg_gray");
		$b->setTable("center","table90");
		$b->setFieldTitle("center","Arial16 bg_y");	
		$b->setOddRow("","bg_light_blue");
		//$b->setEvenRow("","bg_y");
		// 配合頁數切換,設定表格資料來源
		$b->beginRowsNum = $a->beginRowsNum;
		$b->setQuery("CommID,CommName,TEL,Address,CoverMethod,CashAllowZero","Community","","",$a->beginRowsNum.",".$a->perPageRows);
		// 設定表格欄位顯示
		$b->setAllFieldAlign("center");
		$b->setAllFieldClass("Arial14");
		$b->setFieldDisplayName("CommID","社區代碼");
		$b->setFieldDisplayName("CommName","社區名稱");
		$b->setFieldDisplayName("TEL","電話");
		$b->setFieldDisplayName("Address","地址");
		$b->setFieldDisplayName("CoverMethod","沖銷方式");
		$b->setFieldDisplayName("CashAllowZero","零用金允許<br>小於零");	
		// 增加按鈕及觸發事件
		$b->addButton("新增","onclick=\"handleMasterItem('CommAMD.php','A');\"");
		$b->addButton("修改","onclick=\"handleMasterItem('CommAMD.php','M');\"");
		$b->addButton("刪除","onclick=\"handleMasterItem('CommAMD.php','D');\"");		
		// 設定表格底部顯示頁數切換
		$b->setTableBottom($a->getShowData());
		// 顯示表格資料
		$b->showData();
	}
?>