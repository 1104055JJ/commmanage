<?php
	$err = "";
	// 未登入則轉至首頁(登入頁)
	session_start();
	if (!(isset($_SESSION["manageuser"]))) {
		echo '<meta http-equiv=REFRESH CONTENT=1;url=index.php>';		
	} else {
		include_once("../common/connectdb.php");
		include_once("../common/PublicFunction.php");
		//判斷要執行之功能 : Q->顯示資料  A->新增  AW->新增寫入  M->修改  MW->修改寫入  D->刪除  DW->刪除寫入  L->列表
		$f = "";
		$id = "";
		
		if (isset($_POST['f'])) {
			if ($_POST['f'] == "Q" || $_POST['f'] == "A" || $_POST['f'] == "M" || $_POST['f'] == "D" || $_POST['f'] == "AW" || $_POST['f'] == "MW" || $_POST['f'] == "DW") {
				$f = $_POST['f'];
			} else {
				$f = "L";
			}
		} else {
			$f = "L";
		}
		
		if ($f == "Q" || $f == "M" || $f == "D") {
			if (!isset($_POST['id'])) {
				$f = "L";
			} else {
				$id = $_POST['id'];
			}
		}
?>

<?php if ($f == "Q" || $f == "A" || $f == "M" || $f == "D") {
	if ($f != "A") {
		$strSQL = "SELECT COUNT(*) FROM user WHERE UserID='".$id."'";
		$rows = mysql_query($strSQL);
		list($i) = mysql_fetch_row($rows);
		if ($i == 0) {
			$err = "資料錯誤或不存在,請檢查後再執行 !";
		} else {
			$strSQL = "SELECT * FROM user WHERE UserID='".$id."'";
			$rows = mysql_query($strSQL);
			$row = mysql_fetch_array($rows);
		}
	}
	$fW = "";
	$fS = "";
	$fSS = "";
	$fV = "";
	if ($f == "Q") { $fW = "Q"; }
	if ($f == "A") { $fW = "AW"; $fS = "新增"; $fSS = "儲存"; }
	if ($f == "M") { $fW = "MW"; $fS = "修改"; $fSS = "儲存"; }
	if ($f == "D") { $fW = "DW"; $fS = "刪除"; $fSS = "刪除"; }
	if ($err != "") {
		echo $err;
	} else {	
?>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<form action="UserAMD.php" name="MasterForm" id="MasterForm" method="POST">
		<table align="center" class="table90">
			<tr class="bg_gray">
				<th colspan="8">
					<table width="100%">
						<tr>
							<th class="Arial18Bold align_left"><?php echo '<font color="red">'.$fS.'</font>'; ?>使用者基本資料<input name="f" id="f" value="<?php echo $fW; ?>" type="HIDDEN"></th>			
							<th class="Arial18Bold align_right">
							<?php
								if ($f != "Q") {
									if ($f == "D") {				
										//檢查是否已被使用
										//$err = "資料已被調用,不可刪除 !";
									}
									if ($err == "") {
										echo '<input type="Button" name="submit" value="'.$fSS.'" style="font-family: 標楷體 ,Arial, Helvetica, sans-serif;background-color: #FF8888;" class="Arial16Bold" onclick="refreshDataMaster(\'User.php\',\'UserAMD.php\');">';
									}
								}
							?>
							</th>
						</tr>
					</table>
				</th>
			</tr>
			<tr>
				<th class="Arial16Bold_Right">使用者帳號：</th>
				<td class="Arial16"><input name="UserID" type="text" id="UserID" style="font-family: 標楷體 ,Arial, Helvetica, sans-serif;" <?php if ($f != "A") { echo "readonly"; } ?> value="<?php if ($f != "A") { echo $row['UserID']; } ?>"></td>
				<th class="Arial16Bold_Right">使用者名稱：</th>
				<td class="Arial16"><input name="UserName" type="text" id="UserName" style="font-family: 標楷體 ,Arial, Helvetica, sans-serif;" <?php if ($f == "Q" || $f == "D") { echo "readonly"; } ?> value="<?php if ($f != "A") { echo $row['UserName']; } ?>"></td>
				<th class="Arial16Bold_Right">使用者密碼：</th>
				<td class="Arial16"><input name="Password" type="password" id="Password" style="font-family: 標楷體 ,Arial, Helvetica, sans-serif;" <?php if ($f == "Q" || $f == "D") { echo "readonly"; } ?> value="<?php if ($f != "A") { echo encrypt($row['Password'],"D"); } ?>"></td>				
				<th class="Arial16Bold_Right">有效否：</th>
				<td class="Arial16"><input name="Valid" type="checkbox" id="Valid" style="font-family: 標楷體 ,Arial, Helvetica, sans-serif;" <?php if ($f == "Q" || $f == "D") { echo "disabled"; } ?> value="Y" <?php if ($f != "A") { echo ($row['Valid']=="Y" ? "checked" : ""); } ?>></td>
			</tr>
			<tr>
				<th class="Arial16Bold_Right">角色權限：</th>
				<td colspan="7" align="left" class="Arial16">
					<table width="100%">
					<?php
						$j = 0;
						$strTemp = "SELECT * FROM user_role WHERE Valid='Y' ORDER BY RoleName";
						$rowsF = mysql_query($strTemp);
						while ($rowF = mysql_fetch_array($rowsF)) {
							$j = $j + 1;
							$strTemp = "SELECT COUNT(*) FROM user_roles WHERE UserID='".$id."' AND RoleID='".$rowF['RoleID']."'";
							$rowsTemp = mysql_query($strTemp);
							list($i) = mysql_fetch_row($rowsTemp);
							$html = "";
							if ($j % 4 == 1) { $html = $html.'<tr>'; }
							$html = '<td><input name="RoleID[]" type="checkbox" id="RoleID" style="font-family: 標楷體 ,Arial, Helvetica, sans-serif;" ';
							if ($f == "Q" || $f == "D") { $html = $html." disabled "; }
							$html = $html.' value="'.$rowF['RoleID'].'" ';
							if ($f != "A") {
								if ($i > 0) { $html = $html." checked "; }
							}
							$html = $html.'>'.$rowF['RoleName'].'</td>';
							if ($j % 4 == 0) { $html = $html.'</tr>'; }
							echo $html;
						}
						// 補完剩下空的<td></td>
						if ($j % 4 != 0) {
							for ($k = 1; $k <= (4 - ($j % 4)) ; $k++) { 
								echo '<td></td>';
							}
							echo '</tr>';
						}
					?>
					</table>
				</td>								
			</tr>
			<tr>
				<th colspan="8" class="Arial18Bold"><font color="red"><?php if ($err != "") { echo $err; } ?></font></th>
			</tr>
		</table>
	</form>
<?php }} ?>
				
<?php		
		//刪除資料
		if ($f == "DW") {
			if (isset($_POST['UserID'])) {
				// 刪除Table:user_roles中相關資料
				$strSQL = "DELETE FROM user_roles WHERE UserID='".$_POST['UserID']."'";
				mysql_query($strSQL);
				// 刪除Table:user資料				
				$strSQL = "DELETE FROM user WHERE UserID='".$_POST['UserID']."'";
				mysql_query($strSQL);
			} else {
				$err = "資料錯誤或不存在 ,請檢查後再執行!";
			}
			$f = "L";			
		}
		
		// 新增或修改儲存
		if ($f == "AW" || $f == "MW") {
			// 檢查新增或更改時之輸入值是否正確
			if (!isset($_POST['UserID']) || $_POST['UserID'] == "") {
				$err = $err.'使用者帳號不存在或輸入值不正確 !'.chr(13);
			}
			if (!isset($_POST['UserName']) || $_POST['UserName'] == "") {
				$err = $err.'使用者名稱不存在或輸入值不正確 !'.chr(13);
			}
			if (!isset($_POST['Password']) || $_POST['Password'] == "") {
				$err = $err.'使用者密碼不存在或輸入值不正確 !'.chr(13);
			}
			$Valid = "N";
			if (isset($_POST['Valid']) && $_POST['Valid'] == "Y") {
				$Valid = "Y";
			}
			// 新增或修改->寫入資料庫
			if ($err == "") {
				if ($f == "AW") {
					// 檢查Key值是否重覆
					$strSQL = "SELECT COUNT(*) FROM user WHERE UserID='".$_POST['UserID']."'";
					$rows = mysql_query($strSQL);
					list($i) = mysql_fetch_row($rows);
					if ($i > 0) {
						$err = "使用者帳號已存在,請檢查後再執行 !";
					} else {
						$strSQL = "INSERT INTO user (UserID,UserName,Password,Valid) VALUES ('".$_POST['UserID']."','".$_POST['UserName']."','".encrypt($_POST['Password'],"E")."','".$Valid."')";
						mysql_query($strSQL);
					}
				} else {
					$strSQL = "UPDATE user SET UserID='".$_POST['UserID']."',UserName='".$_POST['UserName']."',Password='".encrypt($_POST['Password'],"E")."',Valid='".$Valid."' WHERE UserID='".$_POST['UserID']."'";
					mysql_query($strSQL);
				}
				if ($err == "") {
					// 變更Table:user_roles資料(該人員可用 之角色)
					// 先刪除
					$strTemp = "DELETE FROM user_roles WHERE UserID='".$_POST['UserID']."'";
					mysql_query($strTemp);
					// 再新增
					if (isset($_POST['RoleID']) && sizeof($_POST['RoleID']) > 0) {
						for ($i = 0; $i < sizeof($_POST['RoleID']) ; $i++) {
							$strTemp = "SELECT COUNT(*) FROM user_role WHERE RoleID='".$_POST['RoleID'][$i]."'";
							$rowsTemp = mysql_query($strTemp);
							list($count) = mysql_fetch_row($rowsTemp);
							if ($count > 0) { 
								$strTemp = "INSERT INTO user_roles (UserID,RoleID) VALUES ('".$_POST['UserID']."','".$_POST['RoleID'][$i]."')";
								mysql_query($strTemp);
							}
						}
					}
				}		
			}
			$f = "L";
		}

		// L : 回列表 
		if ($f == "L") {
			echo $err;
		}
	}
?>