<?php
	$err = "";
	// 未登入則轉至首頁(登入頁)
	session_start();
	if (!(isset($_SESSION["manageuser"]))) {
		echo '<meta http-equiv=REFRESH CONTENT=1;url=index.php>';		
	} else {
		include("../common/connectdb.php");
		//判斷要執行之功能 : Q->顯示資料  M->修改  MW->修改寫入  L->列表
		$f = "";
		$id = "";
		
		if (isset($_POST['f'])) {
			if ($_POST['f'] == "Q" || $_POST['f'] == "M" || $_POST['f'] == "MW" || $_POST['f'] == "L") {
				$f = $_POST['f'];
			} else {
				$f = "L";
			}
		} else {
			$f = "L";
		}
		
		if ($f == "Q" || $f == "M") {
			if (!isset($_POST['id'])) {
				$f = "L";
			} else {
				$idArray = explode("^A", $_POST['id']);
				$id = $idArray[1];
			}
		}
?>

<?php if ($f == "Q" || $f == "M") {
	$RoleName = "";
	$Enable = "";
	
	$strSQL = "SELECT * FROM manage_user WHERE UserID='".$id."'";
	$rows = mysql_query($strSQL);
	$row = mysql_fetch_array($rows);
	
	global $mysql_link;
	if (mysql_errno($mysql_link) != 0) {
		$err .= mysql_errno($mysql_link).": ".mysql_error($mysql_link).chr(13);
	} else {
		$strSQL = "SELECT * FROM manage_user WHERE UserID='".$id."'";
		$userrows = mysql_query($strSQL);
		$userrow = mysql_fetch_array($userrows);
		$UserName = $userrow["UserName"];
		$Enable = $userrow["Enable"];
		if (mysql_errno($mysql_link) != 0) {
			$err .= mysql_errno($mysql_link).": ".mysql_error($mysql_link).chr(13);
		}
	}

	$fW = "";
	$fS = "";
	$fSS = "";
	$fV = "";
	if ($f == "Q") { $fW = "Q"; }
	if ($f == "M") { $fW = "MW"; $fS = "修改"; $fSS = "儲存"; }
	if ($err != "") {
		echo $err;
	} else {
?>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<form action="ManageUserRoleAMD.php" name="MasterForm" id="MasterForm" method="POST">
		<table id="MasterFormMainTable" align="center" class="table table-bordered table-condensed table-width-90 table-margin-0">
			<tr class="bg_gray">
				<td colspan="6">
					<table width="100%">
						<tr>
							<td class="font-18-bold align_left">
								<img id="m_FormTableImage" src="../images/icon/NT-Collapse.gif" onclick="foldTable('m_FormTable','m_FormTableDisplay','m_FormTableImage');">								
								<?php echo '<font color="red">'.$fS.'</font>'; ?>使用者社區角色權限維護作業(異動)
								<input name="f" id="f" value="<?php echo $fW; ?>" type="HIDDEN">
							</td>			
							<td class="font-18-bold align_right">
							<?php
								if ($f != "Q") {
									echo '<input type="Button" name="submit" value="'.$fSS.'" class="btn btn-danger" onclick="refreshDataMaster(\'ManageUserRole.php\',\'ManageUserRoleAMD.php\',\'MW\',\'N\');">';
								}
							?>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr><td><div id="m_FormTable" style="display:;"><table class="table table-bordered table-condensed table-width-100 table-margin-0">			
				<tr>
					<td class="font-16-bold bg_y align_center">帳號</td>
					<td class="font-16"><input name="UserID" type="text" id="UserID" value="<?php echo $id; ?>" readonly></td>
					<td class="font-16-bold bg_y align_center">名稱</td>
					<td class="font-16"><input name="UserName" type="text" id="UserName" value="<?php echo $UserName; ?>" readonly></td>
					<td class="font-16-bold bg_y align_center">啟用</td>
					<td class="font-16"><input name="Enable" type="text" id="Enable" value="<?php echo $Enable; ?>" readonly></td>
				</tr>
				<tr>
					<td class="font-16-bold bg_y align_center">角色權限</td>
					<td colspan="5" align="left" class="font-16">
						<table class="table-condensed table-width-100 table-margin-0">
						<?php
							$j = 0;
							$strTemp = "SELECT DISTINCT RoleID,RoleName FROM v_manage_role_comm_union_zone ORDER BY RoleID";
							$rowsR = mysql_query($strTemp);
							while ($rowR = mysql_fetch_array($rowsR)) {
								$j = $j + 1;
								$strTemp = "SELECT COUNT(*) FROM manage_user_role WHERE UserID='".$id."' AND RoleID='".$rowR['RoleID']."'";
								$rowsTemp = mysql_query($strTemp);
								list($i) = mysql_fetch_row($rowsTemp);
									
								$html = "";
								if ($j % 4 == 1) { $html = $html.'<tr>'; }
								$html = '<td width="25%"><input name="RoleID[]" type="checkbox" id="RoleID" ';
								if ($f == "Q") { $html = $html." disabled "; }
								$html .= ' value="'.$rowR['RoleID'].'" ';
								if ($i > 0) { $html = $html." checked "; }
								$html .= '>'.$rowR['RoleName'].'</td>';
								if ($j % 4 == 0) { $html = $html.'</tr>'; }
								echo $html;
							}
							// 補完剩下空的<td></td>
							if ($j % 4 != 0) {
								for ($k = 1; $k <= (4 - ($j % 4)) ; $k++) { 
									echo '<td></td>';
								}
								echo '</tr>';
							}
						?>
						</table>
					</td>								
				</tr>
			</table></div></td></tr>				
		</table>
	</form>
	<input id="m_FormTableDisplay" name="m_FormTableDisplay" value="Y" type="HIDDEN">	
<?php }} ?>
				
<?php		
		// 修改儲存
		if ($f == "MW") {
			// 變更Table:manage_user_role資料(該帳號可用 之角色)
			// 先刪除
			$strTemp = "DELETE FROM manage_user_role WHERE UserID='".$_POST['UserID']."'";
			mysql_query($strTemp);
			global $mysql_link;
			if (mysql_errno($mysql_link) != 0) {
				$err .= "刪除資料時發生錯誤 !".chr(13);
				$err .= mysql_errno($mysql_link).": ".mysql_error($mysql_link).chr(13);
			} else {
				// 再新增
				if (isset($_POST['RoleID']) && sizeof($_POST['RoleID']) > 0) {
					for ($i = 0; $i < sizeof($_POST['RoleID']); $i++) {
						$strTemp = "SELECT COUNT(*) FROM manage_role WHERE RoleID='".$_POST['RoleID'][$i]."'";
						$rowsTemp = mysql_query($strTemp);
						list($count) = mysql_fetch_row($rowsTemp);
						if ($count > 0) {
							$strTemp = "INSERT INTO manage_user_role (UserID,RoleID) VALUES ('".$_POST['UserID']."','".$_POST['RoleID'][$i]."')";
							mysql_query($strTemp);
							if (mysql_errno($mysql_link) != 0) {
								$err .= "新增資料時發生錯誤 !".chr(13);
								$err .= mysql_errno($mysql_link).": ".mysql_error($mysql_link).chr(13);
							}
						}
					}
				}
			}
			$f = "L";
		}

		// L : 回列表 
		if ($f == "L") {
			echo $err;
		}
	}
?>