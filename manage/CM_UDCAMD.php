<?php
	// 未登入則轉至首頁(登入頁)
	session_start();
	if (!(isset($_SESSION["manageuser"]))) {
		echo '<meta http-equiv=REFRESH CONTENT=1;url=index.php>';		
	} else {
		include("../common/connectdb.php");
		include("../common/AutoFormClass.php");

		$a = new AutoFormClass("M","CM_UDC.php","CM_UDCAMD.php","unifieddatacode","unifieddatacode",array("CatagoryID"),"選單列表維護作業","center","table90");
		$a->setTableTitle("left","Arial18Bold bg_gray");
		$a->setFieldTitle("center","Arial16 bg_y");
       // $a->setField($fieldName, $displayName, $align, $class, $wrap, $isNumeric, $beforeHTML, $afterHTML, $A_Display, $A_CanEdit, $M_Display, $M_CanEdit, $inputType, $defaultValue)
        
        
		$a->setField("CatagoryID","選單類別代碼","left","Arial14","N","N","","","Y","Y","Y","Y","select",array(array(),"Select CommName,CommID From Community"),"AA");
		$a->setField("CatagoryName","類別名稱","left","Arial14","N","N","","","Y","Y","Y","Y","text",array(array(),""),"");
		//$a->addBeforeDeleteCheck("HouseHold",array("CommID","CommID"),"已被戶別基本資料叫用,不可刪除!");
		$a->showData();
	}
?>