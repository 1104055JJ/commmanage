<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
	// 未登入則轉至首頁(登入頁)
	session_start();
	if (!(isset($_SESSION["manageuser"]))) {
		echo '<meta http-equiv=REFRESH CONTENT=1;url=index.php>';		
	} else {
?>
		<script type="text/javascript" src="../js/MasterJSB.js"></script>
<?php
		include("../common/connectdb.php");
		include("../common/AutoDataClassB.php");
		include("../common/AutoPageSwitchClass.php");	
		
		//社區過濾條件
		$where = "CommID='".$_SESSION['Community']."'"; 
		$countSQL="SELECT COUNT(*) FROM household where CommiD='".$_SESSION['Community']."'";

		// 建立及初使化頁數切換
		$a = new AutoPageSwitchClass("M","CM_HouseHold.php",$countSQL,array(5,10,15,20));			
		
		// 建立及初始化表格
		$b = new AutoDataClass("M","CM_HouseHold.php","CM_HouseHoldAMD.php",array("CommID","BuildingID","HouseHoldID"),"CM_HouseHoldD.php","門牌戶號資料維護作業","center","table90");
		$b->setTableTitle("left","Arial18Bold bg_gray");
		$b->setFieldTitle("center","Arial16 bg_y");	
		$b->setOddRow("","bg_light_blue");
		//$b->setEvenRow("","bg_y");
		// 配合頁數切換,設定表格資料來源
		$b->beginRowsNum = $a->beginRowsNum;
		$b->setQuery("*","HouseHold",$where,"",$a->beginRowsNum.",".$a->perPageRows);
		// 設定表格欄位顯示
		$b->setAllFieldAlign("center");
		$b->setAllFieldClass("Arial14");
		//$b->setField("ZoneID","區域代碼","center","Arial14","Y","Y","前置","後置","其他屬性");
		$b->setFieldDisplayName("CommID","社區代碼");
		$b->setFieldDisplayName("BuildingID","棟別代碼");
		$b->setFieldDisplayName("HouseHoldID","戶別代碼");
		$b->setFieldDisplayName("Address1","號");		
		$b->setFieldDisplayName("Address2","樓");	
		$b->setFieldDisplayName("Address3","之");	
		$b->setField("BankCoverCode","銀行銷帳碼","center","Arial14","Y","N","","","");	
		$b->setField("CStoreCoverCode","超商銷帳碼","center","Arial14","Y","N","","","");	
		$b->setField("Email","電郵","center","Arial14","Y","N","","","");	
		$b->setField("Payer","繳款人","center","Arial14","N","N","","","");	
		$b->setFieldDisplayName("Owner","所有權人");	
		$b->setFieldDisplayName("Renter","承租人");	
		$b->setField("Area","坪數","center","Arial14","Y","N","","","");	
		$b->setFieldDisplayName("Fee","管理費");	
		$b->setFieldDisplayName("Discount","折扣");	
		$b->setFieldDisplayName("NetFee","應繳金額");
		//$b->setFieldDisplayName("BillCycle","繳費週期");
		$b->setField("BillCycle","繳費週期","center","Arial14","N","N","","","");	
		$b->setField("FeePerMonths","繳費月份","center","Arial14","Y","N","","","");
		$b->setField("ModUser","資料更新人員","center","Arial14","Y","N","","","");
		$b->setField("ModDate","資料更新時間","center","Arial14","Y","N","","","");
		
		
		// 增加按鈕及觸發事件
		$b->addButton("匯入住戶","onclick=\"openDialogWindow('匯入住戶','./upload.php?url=CM_HouseHoldUpload.php');\"","upexcel","btn btn-info");
		$b->addButton("匯入車位","onclick=\"openDialogWindow('匯入車位','./upload.php?url=CM_House_Car_Upload.php');\"","upexcel_d","btn btn-info");
		$b->addButton("住戶報表","onclick=\"openreport();\"","","btn btn-info");
		// 設定表格底部顯示頁數切換
		$b->setTableBottom($a->getShowData());
		// 顯示表格資料
		$b->showData();
	}
?>