<?php
	$err = "";
	// 未登入則轉至首頁(登入頁)
	session_start();
	if (!(isset($_SESSION["manageuser"])))// && isset($_SESSION["SubMenu"]) && isset($_SESSION["ProgramItem"]))) 
  {
		echo '<meta http-equiv=REFRESH CONTENT=1;url=index.php>';		
	} 
  else
  {
		include("../common/connectdb.php");
		include("../common/PublicFunction.php");
		include("../common/AutoFormClass.php");    
		//$a = new AutoFormClass("M","ARGenerateMaintain.php","ARGenerateMaintainAMD.php","ar",array("CommID","ArNo"),"賬單查詢","center","table90");
    $a = new AutoFormClass("M","ARGenerateMaintain.php","ARGenerateMaintainAMD.php","ar","ar",array("CommID","ArNo"),"賬單查詢","center","table90");
		$a->setTableTitle("left","Arial18Bold bg_gray");
		$a->setFieldTitle("center","Arial16 bg_y");
 // $a->setField($fieldName   ,$displayName    ,$align,$class   ,$wrap,$isNumeric,$beforeHTML,$afterHTML,$A_Display,$A_CanEdit,$M_Display,$M_CanEdit,$inputType,$defaultValue)
    $Comm = "".$_SESSION['Community']."";
		$a->setField("CommID"     ,"社區代碼"      ,"left","Arial14","N"  ,"N"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"Y"       ,"Hidden"  ,array(array(),""),"$Comm", 'required');
		$FilterBuilding = "Select BuildingName,BuildingID From Building Where CommID='".$_SESSION['Community']."'";
		$a->setField("BuildingID" ,"棟別代碼"      ,"left","Arial14","N"  ,"N"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"N"       ,"select"  ,array(array(),$FilterBuilding),"",'size="8" maxlength="8"');
    //$a->setField("ArNo"       ,"賬單編號"      ,"left","Arial14","N"  ,"Y"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"N"       ,"text"    ,array(array(),""),"",'size="8" maxlength="8" required');
    $a->setField("ArNo"       ,"賬單編號"      ,"left","Arial14","N"  ,"N"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"Y"       ,"hidden"  ,array(array(),""),"","",array("autoserial","Y","Y"));
		$a->setField("Year"       ,"年度"          ,"left","Arial14","N"  ,"Y"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"N"       ,"text"    ,array(array(),""),"",'size="2" maxlength="2" required');
		$a->setField("Month"      ,"月份"          ,"left","Arial14","N"  ,"Y"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"N"       ,"text"    ,array(array(),""),"",'size="2" maxlength="2" required');
    $a->setField("ArDate"     ,"應收帳款產生日","left","Arial14","N"  ,"N"       ,""         ,""        ,"N"       ,"N"       ,"N"       ,"N"       ,"Hidden"  ,array(array(),""),"",'size="5" maxlength="5"');
		$FillterHouseHold = "Select HouseHoldID, HouseHoldID From household Where CommID ='".$_SESSION['Community']."'";
    $a->setField("HouseHoldID","門牌戶號"      ,"left","Arial14","Y"  ,"N"       ,"<div id='hid'> "         ,"</div>"        ,"Y"       ,"Y"       ,"Y"       ,"N"       ,"select"  ,array(array(),$FillterHouseHold),"",'size="5" maxlength="5" required');
		$a->setField("HouseFee"   ,"管理費"        ,"left","Arial14","N"  ,"Y"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"Y"       ,"text"    ,array(array(),""),"",'size="10" required maxlength="10"');
		$a->setField("CarFee"     ,"車位清潔費"    ,"left","Arial14","N"  ,"Y"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"Y"       ,"text"    ,array(array(),""),"",'size="10" required maxlength="10"');
		$a->setField("Discount"   ,"折扣總額"      ,"left","Arial14","N"  ,"Y"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"Y"       ,"text"    ,array(array(),""),"",'size="10" maxlength="10"');
		$a->setField("TotalAmount","總計金額"      ,"left","Arial14","Y"  ,"Y"       ,""         ,""        ,"Y"       ,"N"       ,"Y"       ,"N"       ,"text"    ,array(array(),""),"",'size="10" maxlength="10"');
		$a->setField("Received"   ,"已收金額"      ,"left","Arial14","N"  ,"Y"       ,""         ,""        ,"Y"       ,"N"       ,"Y"       ,"N"       ,"text"    ,array(array(),""),"0",'size="10" maxlength="10"');
		$a->setField("UnReceived" ,"未收金額"      ,"left","Arial14","N"  ,"Y"       ,""         ,""        ,"Y"       ,"N"       ,"Y"       ,"N"       ,"text"    ,array(array(),""),"",'size="10" maxlength="10"');
		$a->setField("Note"       ,"賬單說明"      ,"left","Arial14","N"  ,"N"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"Y"       ,"text"    ,array(array(),""),"",'size="20" maxlength="100"');
    $a->setField("Enable"     ,"啟用"          ,"left","Arial14","N"  ,"N"       ,""         ,""        ,"N"       ,"N"       ,"Y"       ,"Y"       ,"Hidden"  ,array(array(),""),"");
    //$a->setField("ModDate"    ,"修改日期"      ,"left","Arial14","N"  ,"N"       ,""         ,""        ,"N"       ,"N"       ,"N"       ,"N"       ,"Hidden"  ,array(array(),""),"");
    $a->setField("ModDate"    ,"異動日期"      ,"left","Arial14","N"  ,"N"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"Y"       ,"Hidden"  ,array(array(),""),"","",array("datetime","Y","Y"));
    //$User="".$_SESSION["manageuser"]."";
    //$a->setField("ModUser"    ,"修改人員"      ,"left","Arial14","N"  ,"N"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"N"     ,"Hidden"    ,array(array(),""),"$User", 'required');
    $a->setField("ModUser"    ,"異動人員"      ,"left","Arial14","N"  ,"N"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"Y"       ,"hidden"  ,array(array(),""),"","",array("loginuser","Y","Y"));    
    $a->setAfterForm("Y","./ARGenerateMaintainAMDJS.php");                
		$a->showData();
	}
?>