<?php
	$err = "";
	// 未登入則轉至首頁(登入頁)
	session_start();
	if (!(isset($_SESSION["manageuser"]))) {
		echo '<meta http-equiv=REFRESH CONTENT=1;url=index.php>';		
	} else {
		include("../common/connectdb.php");
		//判斷要執行之功能 : Q->顯示資料  A->新增  AW->新增寫入  M->修改  MW->修改寫入  D->刪除  DW->刪除寫入  L->列表
		$f = "";
		$id = "";
		$id1 = "";
		$id2 = "";
		
		if (isset($_POST['f'])) {
			if ($_POST['f'] == "Q" || $_POST['f'] == "A" || $_POST['f'] == "M" || $_POST['f'] == "D" || $_POST['f'] == "AW" || $_POST['f'] == "MW" || $_POST['f'] == "DW") {
				$f = $_POST['f'];
			} else {
				$f = "L";
			}
		} else {
			$f = "L";
		}
		
		if ($f == "Q" || $f == "A" || $f == "M" || $f == "D") {
			if (!isset($_POST['id']) || !isset($_POST['id1']) || !isset($_POST['id2'])) {
				$f = "L";
			} else {
				$id = $_POST['id'];
				$id1 = $_POST['id1'];
				$id2 = $_POST['id2'];				
			}
		}
?>

<?php if ($f == "Q" || $f == "A" || $f == "M" || $f == "D") {
	if ($f != "A") {
		$strSQL = "SELECT COUNT(*) FROM user_role_zone WHERE RoleID='".$id."' AND CompanyID='".$id1."' AND ZoneID='".$id2."'";
		$rows = mysql_query($strSQL);
		list($i) = mysql_fetch_row($rows);
		if ($i == 0) {
			$err = "資料錯誤或不存在,請檢查後再執行 !";
		}
	}
	$fW = "";
	$fS = "";
	$fSS = "";
	$fV = "";
	if ($f == "Q") { $fW = "Q"; }
	if ($f == "A") { $fW = "AW"; $fS = "新增"; $fSS = "儲存"; }
	if ($f == "M") { $fW = "MW"; $fS = "修改"; $fSS = "儲存"; }
	if ($f == "D") { $fW = "DW"; $fS = "刪除"; $fSS = "刪除"; }
	if ($err != "") {
		echo $err;
	} else {	
?>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<form action="RoleDAMD.php" name="DetailForm" id="DetailForm" method="POST">
		<table align="center" class="table90">
			<tr class="bg_gray">
				<th colspan="5">
					<table width="100%">
						<tr>
							<th class="Arial18Bold align_left"><?php echo '<font color="red">'.$fS.'</font>'; ?>功能設定-依區域<input name="f" id="f" value="<?php echo $fW; ?>" type="HIDDEN"></th>			
							<th class="Arial18Bold align_right">
							<?php
								if ($f != "Q") {
									if ($f == "D") {				
										//檢查是否已被使用
										//$err = "資料已被調用,不可刪除 !";
									}
									if ($err == "") {
										echo '<input type="Button" name="submit" value="'.$fSS.'" style="font-family: 標楷體 ,Arial, Helvetica, sans-serif;background-color: #FF8888;" class="Arial16Bold" onclick="refreshDataDetail(\'RoleD.php\',\'RoleDAMD.php\',\''.$id.'\');">';
									}
								}
							?>
							</th>
						</tr>
					</table>
				</th>
			</tr>
			<tr>
				<th class="Arial16Bold_Right">角色代碼：</th>
				<td class="Arial16"><input name="RoleID" type="text" id="RoleID" style="font-family: 標楷體 ,Arial, Helvetica, sans-serif;" readonly value="<?php echo $id; ?>"></td>
				<td rowspan="3" align="left" class="Arial16">
				<?php
					$strTemp = "SELECT * FROM function WHERE Valid='Y' ORDER BY FunctionSort";
					$rowsF = mysql_query($strTemp);
					while ($rowF = mysql_fetch_array($rowsF)) {
						$html = "";
						$html = '<input name="FunctionID[]" type="checkbox" id="FunctionID" style="font-family: 標楷體 ,Arial, Helvetica, sans-serif;" ';
						if ($f == "Q" || $f == "D") { $html = $html." disabled "; }
						$html = $html.' value="'.$rowF['FunctionID'].'" ';
						if ($f != "A") {
							$strTemp = "SELECT COUNT(*) FROM user_role_zone WHERE RoleID='".$id."' AND CompanyID='".$id1."' AND ZoneID='".$id2."' AND FunctionID='".$rowF['FunctionID']."'";
							$rowsTemp = mysql_query($strTemp);
							list($i) = mysql_fetch_row($rowsTemp);
							if ($i > 0) { $html = $html." checked "; }
						}
						$html = $html.'>'.$rowF['FunctionName'].'<br>';
						echo $html;
					}
				?>
				</td>							
			</tr>
			<tr>	
				<th class="Arial16Bold_Right">公司代碼：</th>
				<td class="Arial16">
					<select name="CompanyID" id="CompanyID" style="font-family: 標楷體 ,Arial, Helvetica, sans-serif;" <?php if ($f == "Q") { echo "disabled"; } ?>>
					<?php
						if ($f == "A") {
							$strTemp = "SELECT * FROM company WHERE Valid='Y' ORDER BY CompanySort";
						} else {
							$strTemp = "SELECT * FROM company ORDER BY CompanySort";	
						}
						$rowsTemp = mysql_query($strTemp);
						while ($rowTemp = mysql_fetch_array($rowsTemp)) {
							$selected = "";
							if ($f != "A") {
								if ($id1 == $rowTemp['CompanyID']) {
									$selected = "selected";
									echo '<option value="'.$rowTemp['CompanyID'].'" '.$selected.'>'.$rowTemp['CompanyName'].'</option>';
								}								
							} else {
								echo '<option value="'.$rowTemp['CompanyID'].'" '.$selected.'>'.$rowTemp['CompanyName'].'</option>';
							}
						} 
					?>
					</select>
				</td>
			</tr>
			<tr>				
				<th class="Arial16Bold_Right">區域代碼：</th>
				<td class="Arial16">
					<select name="ZoneID" id="ZoneID" style="font-family: 標楷體 ,Arial, Helvetica, sans-serif;" <?php if ($f == "Q") { echo "disabled"; } ?>>
					<?php
						if ($f == "A") {					
							$strTemp = "SELECT * FROM zone WHERE Valid='Y' ORDER BY ZoneSort";
						} else {
							$strTemp = "SELECT * FROM zone ORDER BY ZoneSort";	
						}
						$rowsTemp = mysql_query($strTemp);
						while ($rowTemp = mysql_fetch_array($rowsTemp)) {
							$selected = "";
							if ($f != "A") {
								if ($id2 == $rowTemp['ZoneID']) {
									$selected = "selected";
									echo '<option value="'.$rowTemp['ZoneID'].'" '.$selected.'>'.$rowTemp['ZoneName'].'</option>';
								}								
							} else {
								echo '<option value="'.$rowTemp['ZoneID'].'" '.$selected.'>'.$rowTemp['ZoneName'].'</option>';
							}
						} 
					?>
					</select>					
				</td>
			</tr>
			<tr>
				<th colspan="3" class="Arial18Bold"><font color="red"><?php if ($err != "") { echo $err; } ?></font></th>
			</tr>
		</table>
	</form>
<?php }} ?>
				
<?php		
		//刪除資料
		if ($f == "DW") {
			if (isset($_POST['RoleID']) && isset($_POST['CompanyID']) && isset($_POST['ZoneID'])) {
				$strSQL = "DELETE FROM user_role_zone WHERE RoleID='".$_POST['RoleID']."' AND CompanyID='".$_POST['CompanyID']."' AND ZoneID='".$_POST['ZoneID']."'";
				mysql_query($strSQL);
			} else {
				$err = "資料錯誤或不存在 ,請檢查後再執行!";
			}
			$f = "L";
		}
		
		// 新增或修改儲存
		if ($f == "AW" || $f == "MW") {
			// 檢查新增或更改時之輸入值是否正確
			if (!isset($_POST['RoleID']) || $_POST['RoleID'] == "") {
				$err = $err.'角色代碼不存在或輸入值不正確 !'.chr(13);
			}
			if (!isset($_POST['CompanyID']) || $_POST['CompanyID'] == "") {
				$err = $err.'公司代碼不存在或輸入值不正確 !'.chr(13);
			}
			if (!isset($_POST['ZoneID']) || $_POST['ZoneID'] == "") {
				$err = $err.'區域代碼不存在或輸入值不正確 !'.chr(13);
			}
			// 新增或修改->寫入資料庫
			if ($err == "") {
				if ($f == "AW") {
					// 檢查Key值是否重覆
					$strSQL = "SELECT COUNT(*) FROM user_role_zone WHERE RoleID='".$_POST['RoleID']."' AND CompanyID='".$_POST['CompanyID']."' AND ZoneID='".$_POST['ZoneID']."'";
					$rows = mysql_query($strSQL);
					list($i) = mysql_fetch_row($rows);
					if ($i > 0) {
						$err = "資料已存在,請檢查後再執行 !";
					}
				}
								
				if ($err == "") {
					// 變更Table:site_function資料(該工地可用 之Funcction)
					// 先刪除
					$strTemp = "DELETE FROM user_role_zone WHERE RoleID='".$_POST['RoleID']."' AND CompanyID='".$_POST['CompanyID']."' AND ZoneID='".$_POST['ZoneID']."'";
					mysql_query($strTemp);
					// 再新增
					if (isset($_POST['FunctionID']) && sizeof($_POST['FunctionID']) > 0) {
						for ($i = 0; $i < sizeof($_POST['FunctionID']) ; $i++) {
							$strTemp = "SELECT COUNT(*) FROM function WHERE FunctionID='".$_POST['FunctionID'][$i]."'";
							$rowsTemp = mysql_query($strTemp);
							list($count) = mysql_fetch_row($rowsTemp);
							if ($count > 0) { 
								$strTemp = "INSERT INTO user_role_zone (RoleID,CompanyID,ZoneID,FunctionID) VALUES ('".$_POST['RoleID']."','".$_POST['CompanyID']."','".$_POST['ZoneID']."','".$_POST['FunctionID'][$i]."')";
								mysql_query($strTemp);
							}
						}
					}
				}		
			}
			$f = "L";
		}

		// L : 回列表 
		if ($f == "L") {
			echo $err;
		}
	}
?>