
<script type="text/javascript">

//Step1. 資料計算確認
  var CommID = document.getElementById("DetailForm").elements.namedItem("CommID");
  var ArNo = document.getElementById("DetailForm").elements.namedItem("ArNo");
  var CollectionsNo = document.getElementById("MasterForm").elements.namedItem("CollectionsNo");
  var WriteOffAmount = document.getElementById("DetailForm").elements.namedItem("WriteOffAmount");
  var f = document.getElementById("DetailForm").elements.namedItem("f");
  var d_f = document.getElementById("DetailForm").elements.namedItem("d_f");

      $.ajax(
      {
				url: "RM_ARUpdateBack.php",
				type:"POST",
				dataType: "json",
				data:{type:'1',CommID:CommID.value, ArNo:ArNo.value, CollectionsNo:CollectionsNo.value, WriteOffAmount:WriteOffAmount.value},
				//成功執行並返回值
				success: function(data)
        { var Note = document.forms["DetailForm"].elements["Note"];
          //alert('succewss');
				},
				//發送請求之前會執行的函式
				beforeSend:function(){},
				//請求完成時執行的函式(不論結果是success或error)
				complete:function(){},//MoDFunction();
				error:function(xhr, ajaxOptions, thrownError)
        { 
					alert(xhr.status);
					alert(thrownError);
				}
			});     
//test procedure

//Step2. AR資料帶出
//Step3.初始化沖消金額預給
GetARDatabyArNo();

//After process
document.getElementById("DetailForm").elements.namedItem("ArNo").onchange = function(){GetARDatabyArNo()};
//document.getElementById("DetailForm").elements.namedItem("d_insertButton").onsubmit = function() {GetARDatabyArNo()};


function GetARDatabyArNo()
{
	var CommID = document.getElementById("DetailForm").elements.namedItem("CommID");
  var ArNo = document.getElementById("DetailForm").elements.namedItem("ArNo");
  var f = document.getElementById("DetailForm").elements.namedItem("f");
  var d_f = document.getElementById("DetailForm").elements.namedItem("d_f");
  var CollectionsNo = document.forms["MasterForm"].elements["CollectionsNo"]; 
                    
  if(f.value=="AW" & d_f.value=="AW" )
  {  
      $.ajax(
      {
				url: "RM_CollectionARBack.php",
				type:"POST",
				dataType: "json",
				data:{type:'2',CommID:CommID.value, ArNo:ArNo.value, CollectionsNo:CollectionsNo.value},
				//成功執行並返回值
				success: function(data)
        {
        var Year = document.getElementById("DetailForm").elements.namedItem("Year");
        var Month = document.getElementById("DetailForm").elements.namedItem("Month");     
        var HouseHoldID = document.getElementById("DetailForm").elements.namedItem("HouseHoldID");
	      var HouseFee = document.getElementById("DetailForm").elements.namedItem("HouseFee");  
        var CarFee = document.getElementById("DetailForm").elements.namedItem("CarFee");	
	      var Discount = document.getElementById("DetailForm").elements.namedItem("Discount");  
        var TotalAmount = document.getElementById("DetailForm").elements.namedItem("TotalAmount");
	      var Received = document.getElementById("DetailForm").elements.namedItem("Received");  
        var UnReceived = document.getElementById("DetailForm").elements.namedItem("UnReceived");
        var Note = document.getElementById("DetailForm").elements.namedItem("Note");
        var WriteOffAmount = document.forms["DetailForm"].elements["WriteOffAmount"];
        var IncomeAmount = document.forms["MasterForm"].elements["IncomeAmount"];
        var RemainAmount = document.forms["MasterForm"].elements["RemainAmount"]; 
        var Refund = document.forms["MasterForm"].elements["Refund"];           
   
					Year.value = data["Year"];
          Month.value = data["Month"];					
          HouseHoldID.value=data["HouseHoldID"];
          HouseFee.value = data["HouseFee"];
        	CarFee.value = data["CarFee"];          		
					Discount.value = data["Discount"];
        	TotalAmount.value = data["TotalAmount"];     
					Received.value = data["Received"];
        	UnReceived.value = data["UnReceived"];
          //Given Default Value and the corresponding change will calculate on next step
          if (IncomeAmount.value*1-data["TotWriteOffAmounts"]*1-Refund.value*1< UnReceived.value*1)
          {WriteOffAmount.value=IncomeAmount.value*1-data["TotWriteOffAmounts"]*1-Refund.value*1;}
          else
          {WriteOffAmount.value=UnReceived.value*1;}
          
          if(WriteOffAmount.value*1<=0)
          {document.getElementById('d_execButton').style.visibility = 'hidden';
           document.forms["DetailForm"].elements["WriteOffAmount"].style.border="2px Red dotted";
           document.forms["DetailForm"].elements["Note"].style.border="2px Red dotted";
           Note.value='沖銷金額不得為負!!';}
          else
          {document.getElementById('d_execButton').style.visibility = 'visible';
           document.forms["DetailForm"].elements["WriteOffAmount"].style.border="2px Blue dotted";
           document.forms["DetailForm"].elements["Note"].style.border="1px gray solid";
           Note.value='';}
          //Given Default Value          
          Note.value = data["ArNote"];
          //Note.value =IncomeAmount.value*1-data["TotWriteOffAmounts"]*1+'/'+UnReceived.value;
				},
				//發送請求之前會執行的函式
				beforeSend:function(){},
				//請求完成時執行的函式(不論結果是success或error)
				complete:function(){MoDFunction();},//MoDFunction();
				error:function(xhr, ajaxOptions, thrownError)
        { 
					alert(xhr.status);
					alert(thrownError);
				}
			});  
  }

  if(f.value=="MW" & d_f.value=="MW" )
  {MoDFunction();}  

  if(f.value=="DW" & d_f.value=="DW" )
  {MoDFunction();}   
}

//After process
document.getElementById("DetailForm").elements.namedItem("HouseHoldID").onchange = function(){GetARDatabyHouseHoldID()};
//取得帳單資訊
function GetARDatabyHouseHoldID()
{
	var CommID = document.getElementById("DetailForm").elements.namedItem("CommID");
  var HouseHoldID = document.getElementById("DetailForm").elements.namedItem("HouseHoldID");
  var f = document.getElementById("DetailForm").elements.namedItem("f");
  var d_f = document.getElementById("DetailForm").elements.namedItem("d_f");
  
  if(f.value=="AW" & d_f.value=="AW" )
  {  
      $.ajax(
      {
				url: "RM_CollectionARBack.php",
				type:"POST",
				dataType: "json",
				data:{type:'3',CommID:CommID.value, HouseHoldID:HouseHoldID.value},
				//成功執行並返回值
				success: function(data)
        {
          var ArNo = document.getElementById("DetailForm").elements.namedItem("ArNo");
    			ArNo.options.length=0;
  	   		for (var i=0;i<data.length ;i=i+1)
 			  	{ArNo.options.add(new Option(data[i],data[i]));}         
 				},
				//發送請求之前會執行的函式
				beforeSend:function(){},
				//請求完成時執行的函式(不論結果是success或error)
				complete:function(){GetARDatabyArNo();},
				error:function(xhr, ajaxOptions, thrownError)
        { 
					alert(xhr.status);
					alert(thrownError);
				}
			});  
  }
}  


//document.forms["DetailForm"].onsubmit = function(){GetARData()};
document.forms["DetailForm"].onsubmit = function(){UARColMasterData()};
//刪除時的餘額更新
function UARColMasterData()
{
  var CommID = document.getElementById("DetailForm").elements.namedItem("CommID");
  var ArNo = document.getElementById("DetailForm").elements.namedItem("ArNo");
  var CollectionsNo = document.getElementById("MasterForm").elements.namedItem("CollectionsNo");
  var WriteOffAmount = document.getElementById("DetailForm").elements.namedItem("WriteOffAmount");
  var f = document.getElementById("DetailForm").elements.namedItem("f");
  var d_f = document.getElementById("DetailForm").elements.namedItem("d_f");
      
  if(f.value=="DW" & d_f.value=="DW" )
  {  
      $.ajax(
      {
				url: "RM_ARUpdateBack.php",
				type:"POST",
				dataType: "json",
				data:{type:'3',CommID:CommID.value, ArNo:ArNo.value, CollectionsNo:CollectionsNo.value, WriteOffAmount:WriteOffAmount.value},
				//成功執行並返回值
				success: function(data)
        { //alert('succewss');
          var Note = document.forms["DetailForm"].elements["Note"];
          //Note.value= data["TotalAmount"];
          $virtual=data["TotalAmount"];
				},
				//發送請求之前會執行的函式
				beforeSend:function(){},
				//請求完成時執行的函式(不論結果是success或error)
				complete:function(){},
				error:function(xhr, ajaxOptions, thrownError)
        { 
					alert(xhr.status);
					alert(thrownError);
				}
			});  
  }
}

document.getElementById("DetailForm").elements.namedItem("WriteOffAmount").onchange = function(){MoDFunction()};
//沖銷金額AR與餘額判斷
function MoDFunction()
{
  var CommID = document.getElementById("DetailForm").elements.namedItem("CommID");     
  var ArNo = document.getElementById("DetailForm").elements.namedItem("ArNo");
  var ModDate = document.getElementById("DetailForm").elements.namedItem("ModDate");  
  var IncomeAmount = document.getElementById("MasterForm").elements.namedItem("IncomeAmount");
  var DetailNo = document.getElementById("DetailForm").elements.namedItem("DetailNo");
  var CollectionsNo = document.getElementById("MasterForm").elements.namedItem("CollectionsNo");  
  var WriteOffAmount = document.forms["DetailForm"].elements["WriteOffAmount"];
  var Received = document.forms["DetailForm"].elements["Received"];
  var UnReceived = document.forms["DetailForm"].elements["UnReceived"];
  var TotalAmount = document.forms["DetailForm"].elements["TotalAmount"];
  var IncomeAmount = document.forms["MasterForm"].elements["IncomeAmount"];
  var RemainAmount = document.forms["MasterForm"].elements["RemainAmount"];  
  var Refund = document.forms["MasterForm"].elements["Refund"];
  var Note = document.getElementById("DetailForm").elements.namedItem("Note");
  var f = document.getElementById("DetailForm").elements.namedItem("f");
  var d_f = document.getElementById("DetailForm").elements.namedItem("d_f");  
        var ModDateReForm = ModDate.value.substr(0,4) + ModDate.value.substr(5,2)+ModDate.value.substr(8,2);
        var ModDateYear=ModDate.value.substr(0,4);
        var ModDateMonth=ModDate.value.substr(5,2);
        //var currentdate = new Date();
        //var ThisYear=currentdate.getFullYear();
        //var ThisMonth=currentdate.getMonth()+1;  
      $.ajax(
      {
				url: "RM_ARUpdateBack.php",
				type:"POST",
				dataType: "json",
				data:{type:'4',CommID:CommID.value, ArNo:ArNo.value, DetailNo:DetailNo.value, CollectionsNo:CollectionsNo.value, ModDateYear:ModDateYear, ModDateMonth:ModDateMonth},
				//成功執行並返回值
				success: function(data)
        {  
            Received.value= data["ARTotWriteOffAmounts"]*1+WriteOffAmount.value*1;
            UnReceived.value=TotalAmount.value*1-data["ARTotWriteOffAmounts"]*1-WriteOffAmount.value*1; 
            RemainAmount.value=IncomeAmount.value*1-data["ColTotWriteOffAmounts"]*1-WriteOffAmount.value*1-Refund.value*1;
          //防呆措施

          if(f.value=="AW" & d_f.value=="AW" )
          { if(data["Close"]=='Y' || Received.value*1 > TotalAmount.value*1 || RemainAmount.value*1 < 0 || WriteOffAmount.value*1<=0 || isNaN(WriteOffAmount.value*1)==true)//是否收款為數值
            {   document.getElementById('d_execButton').style.visibility = 'hidden';
                document.forms["DetailForm"].elements["Note"].style.border="2px Red dotted";
                if(data["Close"]=='Y')
                {document.forms["DetailForm"].elements["WriteOffAmount"].style.border="2px Red dotted";
                 Note.value='已關帳無法變更!!';}
                else
                {document.forms["DetailForm"].elements["WriteOffAmount"].style.border="2px Red dotted";
                 if(RemainAmount.value*1 < 0)
                 {Note.value='收款可用餘額為負!!';}
                 if(WriteOffAmount.value*1<=0)
                 {Note.value='沖銷金額不得小於0!!';}
                 if(isNaN(WriteOffAmount.value*1)==true)
                 {Note.value='沖銷金額應為數值!!';}
                }
                
                if(Received.value*1 > TotalAmount.value*1)
                {document.forms["DetailForm"].elements["TotalAmount"].style.border="2px Red dotted";
                 document.forms["DetailForm"].elements["Note"].style.border="2px Red dotted";
                 Note.value='沖銷金額超過帳單餘額!!';
                }
                else
                {document.forms["DetailForm"].elements["TotalAmount"].style.border="none";}
            }
            else
            {  document.getElementById('d_execButton').style.visibility = 'visible';
               document.forms["DetailForm"].elements["Note"].style.border="1px gray solid";
               Note.value=''
               document.forms["DetailForm"].elements["TotalAmount"].style.border="none";
               document.forms["DetailForm"].elements["WriteOffAmount"].style.border="2px blue dotted";
            }
          }

          if(f.value=="MW" & d_f.value=="MW" )
          { if(data["Close"]=='Y' || Received.value*1 > TotalAmount.value*1 || RemainAmount.value*1 < 0 || WriteOffAmount.value*1<=0 || isNaN(WriteOffAmount.value*1)==true)//是否收款為數值
            {   document.getElementById('d_execButton').style.visibility = 'hidden';
                document.forms["DetailForm"].elements["Note"].style.border="2px Red dotted";
                if(data["Close"]=='Y')
                {document.forms["DetailForm"].elements["Note"].style.border="2px Red dotted";
                 Note.value='已關帳無法變更!!';}
                else
                {document.forms["DetailForm"].elements["WriteOffAmount"].style.border="2px Red dotted";
                 if(RemainAmount.value*1 < 0)
                 {Note.value='收款可用餘額為負!!';}
                 if(WriteOffAmount.value*1<=0)
                 {Note.value='沖銷金額不得小於0!!';}
                 if(isNaN(WriteOffAmount.value*1)==true)
                 {Note.value='沖銷金額應為數值!!';}
                }
                
                if(Received.value*1 > TotalAmount.value*1)
                {document.forms["DetailForm"].elements["TotalAmount"].style.border="2px Red dotted";
                 document.forms["DetailForm"].elements["Note"].style.border="2px Red dotted";
                 Note.value='沖銷金額超過帳單餘額!!';
                }
                else
                {document.forms["DetailForm"].elements["TotalAmount"].style.border="none";}
            }
            else
            {  document.getElementById('d_execButton').style.visibility = 'visible';
               document.forms["DetailForm"].elements["Note"].style.border="1px gray solid";
               Note.value=''
               document.forms["DetailForm"].elements["TotalAmount"].style.border="none";
               document.forms["DetailForm"].elements["WriteOffAmount"].style.border="2px blue dotted";               
            }
          }

          if(f.value=="DW" & d_f.value=="DW" )
          {
           if(data["Close"]=='Y')
           {document.getElementById('d_execButton').style.visibility = 'hidden';
            document.forms["DetailForm"].elements["Note"].style.border="2px Red dotted";
            Note.value='已關帳無法變更!!';
           }
          }          

          //Note.value=RemainAmount.value;          
				},
				//發送請求之前會執行的函式
				beforeSend:function(){},
				//請求完成時執行的函式(不論結果是success或error)
				complete:function(){},
				error:function(xhr, ajaxOptions, thrownError)
        { 
					alert(xhr.status);
					alert(thrownError);
				}
			}); 
} 
</script>

<script src="../js/jquery.validate.js" type="text/javascript"></script>
<script src="../js/message_tw.js" type="text/javascript"></script>
<script type="text/javascript">
$(function()
{
	//須與form表單ID名稱相同
	$("#DetailForm").validate
  (
    {
		  rules: 
      {
				WriteOffAmount: 
        {	digits: true,
          min:0,
				}, 
			},
	  }
	);
});

function padLeft(str, len) 
{
     str = '' + str;
          if (str.length >= len)
          {return str;}
          else
          {return padLeft("0" + str, len);} 
} 

</script>