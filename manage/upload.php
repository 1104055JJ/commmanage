<html>
	<head>
		<title>檔案上傳</title>
		<script src="upload.js"></script>
	</head>
	<body>
		<div class="main" align="left">
			<h3>資料匯入</h3>
			<form id="uploadimage" action="" method="post" enctype="multipart/form-data">
				<div id="selectImage">
					<label id="url" for=<?php echo $_GET['url']; ?> ></label>
					<br/>
					<label>選擇你的檔案 </label>
					<br/>
					<input type="file" name="file" id="file" required />
					<br/>
					<input type="submit" value="上傳並匯入" class="submit" />
				</div>
				<div id="message"></div>
			</form>
		</div>
	</body>
</html>