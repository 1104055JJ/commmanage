<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
	// 未登入則轉至首頁(登入頁)
	if (!isset($_SESSION)) { session_start(); }
	if (!(isset($_SESSION["manageuser"]))) {
		echo '<meta http-equiv=REFRESH CONTENT=1;url=index.php>';		
	} else {
?>
		<script type="text/javascript" src="../js/MasterJSB.js"></script>
<?php
		include("../common/connectdb.php");
		include("../common/AutoDataClassB.php");
		include("../common/AutoPageSwitchClass.php");	

		// 建立及初使化頁數切換
		$a = new AutoPageSwitchClass("M","Program.php","SELECT COUNT(*) FROM manage_program",array(10,15,20,25));			
		
		// 建立及初始化表格
		$b = new AutoDataClass("M","Program.php","ProgramAMD.php",array("ProgramID"),"","程式設定維護作業(列表)","center","");
		$b->setTableTitle("left","font-18-bold bg_gray");
		$b->setFieldTitle("center","font-16-bold bg_y");
		$b->setDefaultFieldAlign("left");
		$b->setDefaultFieldClass("font-14");
		$b->setButtonDefaultClass("btn");			
		// 配合頁數切換,設定表格資料來源
		$b->beginRowsNum = $a->beginRowsNum;
		$b->setQuery("*","v_manage_program","","",$a->beginRowsNum.",".$a->perPageRows);
		// 設定表格欄位顯示
		$b->setField("ProgramID","程式代碼");
		$b->setField("ProgramName","程式名稱");
		$b->setField("ProgramIcon","圖示","","","Y","N");			
		$b->setField("ModuleID","程式模組代碼","","","Y","N");
		$b->setField("ModuleName","程式模組");
		$b->setField("ProgramSort","排序","center");
		$b->setField("ProgramType","程式類型","center");			
		$b->setField("ProgramFileName","程式檔名");
		$b->setField("ProgramJSON","JSON","","","Y","N");	
		$b->setField("Enable","啟用","center");
		// 增加按鈕及觸發事件
		// 設定表格底部顯示頁數切換
		$b->setTableBottom($a->getShowData());
		// 顯示表格資料
		$b->showData();
	}
?>