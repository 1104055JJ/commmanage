<?php
	// 未登入則轉至首頁(登入頁)
	session_start();
	if (!(isset($_SESSION["manageuser"]))) {
		echo '<meta http-equiv=REFRESH CONTENT=1;url=index.php>';		
	} else {
		include("../common/connectdb.php");
		include("../common/AutoFormClass.php");

		$a = new AutoFormClass("D","CM_Comm2D.php","CM_Comm2DAMD.php","building","building",array("CommID","BuildingID"),"棟別代碼基本資料","center","table90");
		$a->setTableTitle("left","Arial18Bold bg_gray");
		$a->setFieldTitle("center","Arial16 bg_y");
	  	$a->setField("CommID","社區編號","left","Arial14","N","N","","","Y","N","Y","N","text",array(array(),""),"");
		$a->setField("BuildingID","棟別代碼","left","Arial14","N","N","","","Y","Y","Y","Y","text",array(array(),""),"");
		$a->setField("BuildingName","棟別名稱","left","Arial14","N","N","","","Y","Y","Y","Y","text",array(array(),""),"","onchange=\"CheckNum('Need','DetailForm','BuildingName','此欄位為必填')\"");
		
		$a->setField("Enable","有效否","left","Arial14","N","N","","","Y","Y","Y","Y","select",array(array("有效","Y","無效","N"),""),"Y");
		$a->setField("Note","備註","left","Arial14","N","N","","","Y","Y","Y","Y","text",array(array(),""),"");
		$a->addBeforeDeleteCheck("household",array("CommID","CommID","BuildingID","BuildingID"),"住戶資料存在,不可刪除!");
		//$a->setbeforeForm("Y","./CM_ManageHomeJS.php");
		$a->showData();
		if (isset($_POST["f"]) && ($_POST["f"] == "A" || $_POST["f"] == "M")) {
			// 欄位及規則檢查
?>
			<script language="JavaScript">
				var form = "DetailForm";
				var fieldArray = [];
				fieldArray.push(["BuildingID","Need","此欄位為必填"]);
				fieldArray.push(["BuildingName","Need","此欄位為必填"]);
				checkRule(form,fieldArray);				
				function otherCheckRule() {
					return true;
				}			
			</script>
<?php
		}
	}
?>