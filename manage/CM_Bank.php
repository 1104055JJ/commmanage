<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
	// 未登入則轉至首頁(登入頁)
	session_start();
	if (!(isset($_SESSION["manageuser"]))) {
		echo '<meta http-equiv=REFRESH CONTENT=1;url=index.php>';		
	} else {
?>
		<script type="text/javascript" src="../js/MasterJSB.js"></script>
		<!--<script language="JavaScript">refreshDataMAMD('ProgramAMD.php','A','');</script>-->


<?php
		$where = "CommID='".$_SESSION['Community']."'"; 
		$querystat = "SELECT COUNT(*) FROM v_accountbalance Where CommID ='".$_SESSION['Community']."'"; 
		include("../common/connectdb.php");
		include("../common/AutoDataClassB.php");
		include("../common/AutoPageSwitchClass.php");	
		//include("../common/PublicFunction.php");

		// 建立及初使化頁數切換
		$a = new AutoPageSwitchClass("M","CM_Bank.php",$querystat,array(5,10,15,20,25));			
		
		// 建立及初始化表格
		$b = new AutoDataClass("M","CM_Bank.php","CM_BankAMD.php",array("CommID","AccountID","Year","Month"),"","銀行帳戶餘額維護作業","center","table90");
		$b->setTableTitle("left","Arial18Bold bg_gray");
		$b->setFieldTitle("center","Arial16 bg_y");	
		$b->setOddRow("","bg_light_blue");
		//$b->setEvenRow("","bg_y");
		// 配合頁數切換,設定表格資料來源
		$b->beginRowsNum = $a->beginRowsNum;
		$b->setQuery("CommID,AccountType,CodeName,AccountID,AccountName,Year,Month,Balance,BalanceMod,ModifyNote,Close","v_accountbalance",$where,"",$a->beginRowsNum.",".$a->perPageRows);
		// 設定表格欄位顯示
		$b->setAllFieldAlign("center");
		$b->setAllFieldClass("Arial14");
		//$b->setFieldDisplayName("CommID","社區代碼");
		$b->setFieldDisplay("CommID","N");
		//$b->setFieldDisplayName("AccountType","帳號類型");
		$b->setFieldDisplay("AccountType","N");
		$b->setFieldDisplayName("AccountID","帳號代碼");
		$b->setFieldDisplayName("CodeName","帳號類型");
		$b->setFieldDisplayName("AccountName","帳戶名稱");
		$b->setFieldDisplayName("Year","年度");
		$b->setFieldDisplayName("Month","月份");
		
		$b->setFieldDisplayName("Balance","餘額");
		$b->setFieldDisplayName("BalanceMod","調整餘額");
		$b->setFieldDisplayName("ModifyNote","調整事由");
		$b->setFieldDisplayName("Close","關帳");
		// 增加按鈕及觸發事件
		$b->addButton("結轉載入","onclick=\"openDialogWindow('匯入Excel','./CM_Bank_closeAcc_w.php');\"","closeAcc","btn btn-info");
		// 設定表格底部顯示頁數切換
		$b->setTableBottom($a->getShowData());
		// 顯示表格資料
		$b->showData();
	}
?>
