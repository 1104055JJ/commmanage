<?php
	$err = "";
	// 未登入則轉至首頁(登入頁)
	session_start();
	if (!(isset($_SESSION["manageuser"]))) //&& isset($_SESSION["SubMenu"]) && isset($_SESSION["ProgramItem"])))
  {echo '<meta http-equiv=REFRESH CONTENT=1;url=index.php>';} 
  else
  {
		include("../common/connectdb.php");
		include("../common/PublicFunction.php");
		include("../common/AutoFormClass.php");    

		$a = new AutoFormClass("M","RM_AgencyReceiptMaintain.php","RM_AgencyReceiptMaintainAMD.php","collectionsmaster","collectionsmaster",array("CommID","CollectionsNo"),"代收款收款","center","table90");
		$a->setTableTitle("left","Arial18Bold bg_gray");
		$a->setFieldTitle("center","Arial16 bg_y");
 // $a->setField($fieldName      ,$displayName      ,$align,$class   ,$wrap,$isNumeric,$beforeHTML,$afterHTML,$A_Display,$A_CanEdit,$M_Display,$M_CanEdit,$inputType,$defaultValue      )
    $Comm = "".$_SESSION['Community']."";          
    $a->setField("CommID"          ,"社區代碼"      ,"left","Arial14","N"  ,"N"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"N"       ,"Hidden"  ,array(array(),""),"$Comm", 'required');    
    //預設值自動取號(開使)
           $serial = date("ym");
           $strSQL = "SELECT Max(CollectionsNo) FROM collectionsmaster Where CommID ='".$_SESSION['Community']."'";
           $rows = mysql_query($strSQL);
           if (mysql_num_rows($rows) > 0) 
           {			$row = mysql_fetch_array($rows);
									if (substr($row[0],0,4) == $serial) 
                  { // 不換年月最大號+1
                    $serial = $row[0] + 1;
									} 
                  else 
                  {	// 換年月重新給號
										$serial .= "0001";}
					} 
          else
          {$serial .= "0001";}
           $CollectionsNo= $serial;
    //預設值自動取號(結束)    
    $a->setField("CollectionsNo"   ,"收款編號"      ,"left","Arial14","N"  ,"N"       ,""         ,""        ,"Y"       ,"N"       ,"Y"       ,"N"       ,"text"    ,array(array(),""),"$CollectionsNo",'size="10" maxlength="10" ',array("autoserial","Y","Y"));
    $a->setField("CollectionDate"  ,""              ,"left","Arial14","N"  ,"N"       ,""         ,""        ,"N"       ,"N"       ,"N"       ,"N"       ,"Hidden"  ,array(array(),""),"");
    $FillterHouseHold = "Select HouseHoldID, HouseHoldID From household Where CommID ='".$_SESSION['Community']."'";
    $a->setField("HouseHoldID"     ,"門牌戶號"      ,"left","Arial14","N"  ,"N"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"Y"       ,"select"  ,array(array(),$FillterHouseHold),"",'size="5" maxlength="5" required');
	  $a->setField("PaymentType"     ,"收款方式"      ,"left","Arial14","N"  ,"N"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"Y"       ,"select"  ,array(array("現金","0","轉帳","1","支票","2"),""),"0", 'required');
		$FillterIncomeItemID = "Select IncomeItemShow, IncomeItemID From incomeitem Where IncomeItemId='004' and CommID ='".$_SESSION['Community']."'";
    $a->setField("IncomeItemID"    ,"收款用途"      ,"left","Arial14","Y"  ,"N"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"Y"       ,"select"  ,array(array(),$FillterIncomeItemID),"", 'required');
		$a->setField("BillNo"          ,"票據號碼"      ,"left","Arial14","N"  ,"N"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"Y"       ,"text"    ,array(array(),""),"",'size="10" maxlength="10"');
		$a->setField("BillDate"        ,"收票日期"      ,"left","Arial14","N"  ,"N"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"Y"       ,"date"    ,array(array(),""),"",'size="10" maxlength="10"');
    if ($_POST["f"] == "A")
    {$a->setField("DueDate"        ,"收款(到期)日期","left","Arial14","N"  ,"N"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"N"       ,"date"    ,array(array(),""),"",'size="10" maxlength="10" required');}
    else
    {$a->setField("DueDate"        ,"收款(到期)日期","left","Arial14","N"  ,"N"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"N"       ,"date"    ,array(array(),""),"",'size="10" maxlength="10"');}
		$a->setField("TurnToIncomeDate","回饋金轉收入日","left","Arial14","Y"  ,"N"       ,""         ,""        ,"Y"       ,"N"       ,"Y"       ,"N"       ,"label"    ,array(array(),""),"",'size="10" maxlength="10"');
    if ($_POST["f"] == "A")
    {$a->setField("IncomeAmount"   ,"收款金額"      ,"left","Arial14","N"  ,"Y"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"Y"       ,"text"    ,array(array(),""),"",'size="10" maxlength="10" required');}
    else
    {$a->setField("IncomeAmount"   ,"收款金額"      ,"left","Arial14","N"  ,"Y"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"Y"       ,"text"    ,array(array(),""),"",'size="10" maxlength="10"');}    
    $a->setField("CleanDeduction"  ,"清潔費扣款"    ,"left","Arial14","N"  ,"Y"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"Y"       ,"Hidden"  ,array(array(),""),"0",'size="10" maxlength="10"');
    $a->setField("RepairDeduction" ,"修繕費用扣款"  ,"left","Arial14","N"  ,"Y"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"Y"       ,"Hidden"  ,array(array(),""),"0",'size="10" maxlength="10"');
    $a->setField("PunishDeduction" ,"罰款扣款"      ,"left","Arial14","Y"  ,"Y"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"Y"       ,"Hidden"  ,array(array(),""),"0",'size="10" maxlength="10"');
    $a->setField("ReturnDeduction" ,"回饋金扣款"    ,"left","Arial14","N"  ,"Y"       ,""         ,""        ,"Y"       ,"N"       ,"Y"       ,"N"       ,"label"    ,array(array(),""),"0",'size="10" maxlength="10"');
		//$a->setField("Refund"          ,"退款共計"      ,"left","Arial14","N"  ,"Y"       ,""         ," <th align='center' class='Arial16 bg_y'>收款餘額</th> <td align='left' class='Arial14'><input type='text' name='TotalRes' id='TotalRes' size='10' maxlength='10' readonly='true' value='0'></td></tr> "         ,"Y"        ,"Y"        ,"Y"        ,"Y"        ,"text"     ,array(array(),""),"",'size="10" maxlength="10"');
    $a->setField("Refund"          ,"退款共計"      ,"left","Arial14","N"  ,"Y"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"Y"       ,"text"    ,array(array(),""),"0",'size="10" maxlength="10"');
    $a->setField("RemainAmount"    ,"剩餘可用金額"  ,"left","Arial14","Y"  ,"Y"       ,""         ,""        ,"Y"       ,"N"       ,"Y"       ,"N"       ,"label"    ,array(array(),""),"",'size="10" maxlength="10"');    
		$a->setField("Note"            ,"收款說明"      ,"left","Arial14","Y"  ,"N"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"Y"       ,"text"    ,array(array(),""),"",'size="140" maxlength="200"');
    $User="".$_SESSION["manageuser"]."";
		$a->setField("ModUser"         ,"修改人員"      ,"left","Arial14","N"  ,"N"       ,""         ,""        ,"Y"       ,"Y"       ,"N"       ,"Y"       ,"Hidden"  ,array(array(),""),"$User", 'readonly',array("loginuser","Y","Y"));
		$Today=date("Y-m-d H:i:s");
    $a->setField("ModDate"         ,"修改日期"      ,"left","Arial14","N"  ,"N"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"Y"       ,"Hidden"  ,array(array(),""),"$Today", 'readonly',array("datetime","Y","Y"));
    $a->setBeforeForm("Y","./RM_AgencyReceiptMaintainAMDJS.php");  
  	$a->showData();
  //reference :     
	//$a->setField("ModDate"      ,"修改日期"      ,"left","Arial14","N"  ,"N"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"Y"       ,"select"  ,array(array(),"Select CompanyName,CompanyID From Company Where Enable = 'Y'"),"PS1");
	//$a->setField("ModUser"      ,"修改人員"      ,"left","Arial14","Y"  ,"N"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"Y"       ,"select"  ,array(array(),"Select ZoneName,ZoneID From Zone Where Enable = 'Y'"),"02");
	//$a->setField("DueDate"      ,"收款(到期)日期","left","Arial14","Y"  ,"N"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"Y"       ,"select"  ,array(array(),"Select CodeName,CodeID from unifieddatacode where catagoryid='A02'"),"Y");
	//$a->setField("IncomeAmount" ,"收款金額"      ,"left","Arial14","N"  ,"N"       ,""         ,""        ,"Y"       ,"Y"       ,"Y"       ,"Y"       ,"select"  ,array(array(),"Select CodeName,CodeID from unifieddatacode where catagoryid='A01'"),"1");      		
	}
  ?>
