<!--廠商基本資料搜尋作業-->

<!DOCTYPE html>
<!--[if lt IE 9]>
<script type="text/javascript" src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<html>
<head>
  <meta charset="utf-8">
  <title>廠商選取</title>
  <link href="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.10.2/themes/hot-sneaks/jquery-ui.css" rel="stylesheet">
  <link href="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.10.2/themes/hot-sneaks/jquery-ui.css" rel="stylesheet">
  <!--<link href="..\bower_components\datatables-plugins\i18n\Chinese-traditional.lang" >-->
  <script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-2.0.0.min.js"></script>
  <script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.10.2/jquery-ui.min.js"></script>
  <script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/jquery.dataTables.min.js"></script>
  <script type="text/javascript" src="../js/ManageHomeJS.js"></script>
  <style>
    article,aside,figure,figcaption,footer,header,hgroup,menu,nav,section {display:block;}
    body {font: 62.5% "Trebuchet MS", sans-serif; margin: 50px;}
  </style>
</head>
<body>
     <FORM NAME="Search_form" METHOD=POST TARGET>  
        <div style="width:300px;">
    
            <table id="table1">
   
            </table>
        </div>
    </FORM>
  
  <script language="JavaScript">
    $(document).ready(function(){
   
                                          // Sets focus to the new window

        getSearchlist("get_search_list.php",<?php echo "'".$_GET['CommID']."'" ; ?>    );
        
       
        
      }); 
     
  
      
              
function getSearchlist(l_url,CommID) {
      
	$.ajax({
        type: "POST",
        url: l_url,
        data: {"CommID":CommID},
        dataType: "json",
        success: function(resultData) {
          var opt={"oLanguage":{"sProcessing":"處理中...",
                                     "sLengthMenu":"顯示 _MENU_ 項結果",
                                     "sZeroRecords":"沒有匹配結果",
                                     "sInfo":"顯示第 _START_ 至 _END_ 項結果，共 _TOTAL_ 項",
                                     "sInfoEmpty":"顯示第 0 至 0 項結果，共 0 項",
                                     "sInfoFiltered":"(從 _MAX_ 項結果過濾)",
                                     "sSearch":"搜索:",
                                     "oPaginate":{"sFirst":"首頁",
                                                          "sPrevious":"上頁",
                                                          "sNext":"下頁",
                                                          "sLast":"尾頁"}
                               },
                             
                   "bJQueryUI":true,
	           "aoColumns":[{"sTitle":"選取"},{"sTitle":"廠商名稱","sType":"string"},{"sTitle":"廠商代號","sType":"numeric"}],
                   "aaData": resultData
                   };     
                 
          //$("#table1").dataTable(opt);
         var oTable=$("#table1").dataTable(opt);
        oTable.on('click','tr',function()   {
               var row=oTable.fnGetData(this);
            oTable.on('dblclick','.sorting_1',function()
            {
              window.close();
             $('#SupplierID', opener.document).attr("value",row[2]);
              $('#SupplierName', opener.document).html(row[1]);
            });
	});    
    
    
    
    
    
    },
          error:function(xhr, ajaxOptions, thrownError){ 
                    alert(xhr.status); 
                    alert(thrownError); 
                 }
          
          
        });
        
        
        
        
        }

    
      
  </script>
  
  
  
  
</body>
</html>