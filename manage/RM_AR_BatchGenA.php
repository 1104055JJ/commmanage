<?php
		
		include("../common/connectdb.php");
		include("../common/PublicFunction.php");	
		session_start();
?>
<div style="text-align:center">
<p>整批產生管理費</p>
<p>請選擇所要產生管理費的年度/月份</p>
	<select id="s_year" name="s_year" class="Arial14">
		<option value ="15">2015年</option>
		<option value ="16">2016年</option>
		<option value ="17">2017年</option>
	</select>
	<select id="s_month" name="s_month" class="Arial14">
		<option value ='1'>1月</option>
		<option value ='2'>2月</option>
		<option value ='3'>3月</option>
		<option value ='4'>4月</option>
		<option value ='5'>5月</option>
		<option value ='6'>6月</option>
		<option value ='7'>7月</option>
		<option value ='8'>8月</option>
		<option value ='9'>9月</option>
		<option value ='10'>10月</option>
		<option value ='11'>11月</option>
		<option value ='12'>12月</option>
	</select>
	<select id="s_building" name="s_building" class="Arial14">
		<option value ='-'>不選擇</option>
		<?PHP
			//session_start();
			$where = "building where CommID = '".$_SESSION['Community']."'";
			SelectOption($where,'BuildingID','BuildingName','');
		?>
	</select>
	<select id="s_household" name="s_household" class="Arial14">
		<option value ='-'>不選擇</option>
	</select>
	<br><br>
	<button id="b_gen" >整批產生</button>
	<button id="b_cancel" >取消</button>
	<input id="l_comm" name="l_comm" value=<?php echo $_SESSION['Community'] ?> type="HIDDEN">
	<div id = "message"></div>
</div>

<script type="text/javascript">
	

$( "#b_cancel" ).click(function() {
    $( "#GroupGenDialog" ).dialog( "close" );
});

$( "#b_gen" ).click(function() {
    genAR();
});

function genAR(){
  			var e = document.getElementById("s_year");
			var s_year = e.options[e.selectedIndex].value;
  			var f = document.getElementById("s_month");
			var s_month = f.options[f.selectedIndex].value;
  			var g = document.getElementById("s_building");
			var s_building = g.options[g.selectedIndex].value;
			var i = document.getElementById("s_household");
			var s_household = i.options[i.selectedIndex].value;
			$.ajax({
				url: "RM_AR_BatchGen.php",
				type:"POST",
				dataType: "text",
				data:{s_year:s_year,s_month:s_month,s_building:s_building,s_household:s_household},
				//成功執行並返回值
				success: function(data){
					//$("#message").empty().append(data);
					refreshDataM('RM_ARGenerateMaintain.php',10,10,1,'','','','',''); 
				},
				//發送請求之前會執行的函式
				beforeSend:function(){
				},
				//請求完成時執行的函式(不論結果是success或error)
				complete:function(){		
				},
				error:function(xhr, ajaxOptions, thrownError){ 
					alert(xhr.status);
					alert(thrownError);
				}
			});
			$("#GroupGenDialog").dialog( "close" );
}

//取得棟別並組住戶資料
	var CommID1 = document.getElementById("l_comm").value;
	//alert(CommID1);
	document.getElementById("s_building").onchange = function(){
  			var h = document.getElementById("s_building");
			$.ajax({
				url: "RM_ARGenerateBack.php",
				type:"POST",
				dataType: "json",
				data:{type:'1',CommID:CommID1, BuildingID:h.options[h.selectedIndex].value },
				//data:$('#MasterForm').serialize(),
				//成功執行並返回值
				success: function(data){
					//$("#hid").empty().append(data); 
					var obj=document.getElementById("s_household");
  					obj.options.length=0;
  					obj.options.add(new Option('不選擇','-'));
  					for (var i=0;i<data.length ;i=i+1)
 					 {
    					obj.options.add(new Option(data[i],data[i]));
  					}           		
				},
				//發送請求之前會執行的函式
				beforeSend:function(){
					
				},
				//請求完成時執行的函式(不論結果是success或error)
				complete:function(){		
					//getHouseHoldData();
					},
				error:function(xhr, ajaxOptions, thrownError){ 
					alert(xhr.status);
					alert(thrownError);
				}
			});
			
	}
	//alert(CommID);

</script>
	

	