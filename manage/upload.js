$(document).ready(function (e) {
	$("#uploadimage").on('submit',(function(e) {
		e.preventDefault();
		$("#message").empty();
		//上傳資料
		$.ajax({
			url: "u_submit.php", // Url to which the request is send
			type: "POST",             // Type of request to be send, called as method
			data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
			contentType: false,       // The content type used when sending data to the server.
			cache: false,             // To unable request pages to be cached
			processData:false,        // To send DOMDocument or non processed data file it is set to false
			success: function(data)   // A function to be called if request succeeds
			{
				var u1 = document.getElementById("url").htmlFor;
				a(u1,data);
			},
			complete: function()
            {
            }
		});
	}));
	//匯入資料
function a(url,data){
			$.ajax({
				url: url,
				type:"POST",
				dataType: "text",
				data:{filename:data},
				//成功執行並返回值
				success: function(data){
					$("#message").empty().append(data);        		
				},
				//發送請求之前會執行的函式
				beforeSend:function(){
					
				},
				//請求完成時執行的函式(不論結果是success或error)
				complete:function(){		
				},
				error:function(xhr, ajaxOptions, thrownError){ 
					alert(xhr.status);
					alert(thrownError);
				}
			});
};
});