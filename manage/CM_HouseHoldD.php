<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
	// 未登入則轉至首頁(登入頁)
	session_start();
	if (!(isset($_SESSION["manageuser"]))) {
		echo '<meta http-equiv=REFRESH CONTENT=1;url=index.php>';		
	} else {
?>
		<script type="text/javascript" src="../js/DetailJSB.js"></script>		
<?php
		include("../common/connectdb.php");
		include("../common/AutoDataClassB.php");
		include("../common/AutoPageSwitchClass.php");	

		// 建立及初使化頁數切換
		$a = new AutoPageSwitchClass("D","CM_HouseHoldD.php","SELECT COUNT(*) FROM carspace",array(3,6,9,12));			
		
		// 建立及初始化表格
		$b = new AutoDataClass("D","CM_HouseHoldD.php","CM_HouseHoldDAMD.php",array("CommID","BuildingID","HouseHoldID","CarID"),"","車位資料維護作業","center","table90");
		$b->setTableTitle("left","Arial18Bold bg_gray");
		$b->setFieldTitle("center","Arial16 bg_y");	
		$b->setOddRow("","bg_light_blue");
		//$b->setEvenRow("","bg_y");
		// 配合頁數切換,設定表格資料來源
		$b->beginRowsNum = $a->beginRowsNum;
		$b->setQuery("*","carspace","","",$a->beginRowsNum.",".$a->perPageRows);
		// 設定表格欄位顯示
		$b->setAllFieldAlign("center");
		$b->setAllFieldClass("Arial14");
		$b->setFieldDisplayName("CommID","社區代碼");
		$b->setFieldDisplay("CommID", "N");
		//$b->setFieldDisplayName("BuildingID","棟別代碼");
		$b->setFieldDisplay("BuildingID", "N");
		$b->setField("HouseHoldID","戶別代碼");
		$b->setField("CarID","車位代碼");
		$b->setField("Area","坪數");
		$b->setField("Fee","管理費");
		$b->setField("Discount","折扣");
		$b->setField("NetFee","應收金額");
		$b->setField("IncomeItemID","對應收入項目");
		$b->setFieldDisplay("BillCycle", "N");
		$b->setFieldDisplay("FeePerMonths", "N");
		//$b->setField("BillCycle","繳費周期");
		//$b->setField("FeePerMonths","繳費月份");
		$b->setFieldDisplay("ModUser", "N");
		$b->setFieldDisplay("ModDate", "N");
		//$b->setField("ModUser","資料更新人員");
		//$b->setField("ModDate","資料更新時間");
		
		// 增加按鈕及觸發事件
		
		// 設定表格底部顯示頁數切換
		$b->setTableBottom($a->getShowData());
		// 顯示表格資料
		$b->showData();
	}
?>

