<?php
	// 未登入則轉至首頁(登入頁)
	if (!isset($_SESSION)) { session_start(); }
	if (!(isset($_SESSION["manageuser"]))) {
		echo '<meta http-equiv=REFRESH CONTENT=1;url=index.php>';		
	} else {
		include("../common/connectdb.php");
		include("../common/AutoFormClass.php");
		
		$a = new AutoFormClass("M","Module.php","ModuleAMD.php","manage_module","manage_module",array("ModuleID"),"程式模組維護作業(異動)","center","");
		$a->setTableTitle("left","font-18-bold bg_gray");
		$a->setFieldTitle("center","font-16-bold bg_y");
		$a->setButtonDefaultClass("btn btn-warning");
		$a->setField("ModuleID","程式模組代碼","left","font-14","N","N","","","Y","Y","Y","N","text",array(array(),""),"","");
		$a->setField("ModuleName","程式模組名稱","left","font-14","N","N","","","Y","Y","Y","Y","text",array(array(),""),"","");
		$a->setField("ModuleIcon","圖示","left","font-14","N","N","","","Y","Y","Y","Y","hidden",array(array(),""),"NT-Expand.gif","");
		$a->setField("ModuleSort","排序","left","font-14","N","Y","","","Y","Y","Y","Y","combobox",array(array(),"SELECT DISTINCT ModuleSort,ModuleSort FROM manage_module ORDER BY ModuleSort"),"");
		$a->setField("Enable","啟用否","left","font-14","N","N","","","Y","Y","Y","Y","radio",array(array("啟用","Y","停用","N"),""),"Y");			
		$a->addBeforeDeleteCheck("manage_program",array("ModuleID","ModuleID"),"已被【程式設定維護作業】調用,不可刪除!");
		$a->showData();
		
		if (isset($_POST["f"]) && ($_POST["f"] == "A" || $_POST["f"] == "M")) {
			// 欄位及規則檢查
?>
			<script language="JavaScript">
				var form = "MasterForm";
				var fieldArray = [];
				fieldArray.push(["ModuleID","Need","此欄位為必填"]);
				fieldArray.push(["ModuleName","Need","此欄位為必填"]);
				fieldArray.push(["ModuleSort","Num","此欄位為整數"]);
				checkRule(form,fieldArray);			
				
				function otherCheckRule() {
					return true;
				}
			</script>
<?php
		}
	}
?>