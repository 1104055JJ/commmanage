<!--廠商基本資料搜尋作業-->

<!DOCTYPE html>
<!--[if lt IE 9]>
<script type="text/javascript" src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<html>
<head>
  <meta charset="utf-8">
  <title>資料選取</title>
  <link href="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.10.2/themes/redmond/jquery-ui.css" rel="stylesheet">
  <link href="http://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/css/jquery.dataTables_themeroller.css" rel="stylesheet">
  <script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-2.0.0.min.js"></script>
  <script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.10.2/jquery-ui.min.js"></script>
  <script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/jquery.dataTables.min.js"></script>
  <script type="text/javascript" src="../js/ManageHomeJS.js"></script>
  
  <style>
    article,aside,figure,figcaption,footer,header,hgroup,menu,nav,section {display:block;}
    body {font: 90% "Arial", sans-serif; margin: 30px;}
  </style>
</head>
<body>
     <FORM NAME="Search_form" METHOD=POST TARGET>  
        <div style="width:600px;">
    
            <table id="table1">
   
            </table>
        </div>
    </FORM>
  
  <script language="JavaScript">
    $(document).ready(function(){
    
                                          // Sets focus to the new window
      
       
        getSearchlist("get_search_list.php",<?php echo "'".$_GET['passout']."'" ; ?>,<?php echo "'".$_GET['formID']."'" ; ?>,<?php echo "'".$_GET['code']."'" ; ?> );
       function getSearchlist(l_url,passout,formID,code) {
      
	$.ajax({
        type: "get",
        url: l_url,
        data: {"passout":passout,"code":code},
        dataType: "json",
        success: function(resultData) {
          var opt={"oLanguage":{"sProcessing":"處理中...",
                                     "sLengthMenu":"顯示 _MENU_ 項結果",
                                     "sZeroRecords":"沒有匹配結果",
                                     "sInfo":"顯示第 _START_ 至 _END_ 項結果，共 _TOTAL_ 項",
                                     "sInfoEmpty":"顯示第 0 至 0 項結果，共 0 項",
                                     "sInfoFiltered":"(從 _MAX_ 項結果過濾)",
                                     "sSearch":"搜索:",
                                     "oPaginate":{"sFirst":"首頁",
                                                          "sPrevious":"上頁",
                                                          "sNext":"下頁",
                                                          "sLast":"尾頁"}
                               },
                               "bDeferRender": true,
                       "bJQueryUI":true,
                      "bProcessing":true,
                     "bAutoWidth": false,
                      "aoColumnDefs": [
                     { "sWidth": "5%", "aTargets": [ 0 ] }
                                      ],
                   "bPaginate":false,
                   "sScrollY":"250px",
                   "bScrollCollapse":true,
                  
                   //show value
	          // "aoColumns":[{"sTitle":"選取"},{"sTitle":"廠商名稱"},{"sTitle":"廠商代號"}],//ao" 為一物件陣列 
                   "aoColumns":resultData.columns,
                   "aaData": resultData.Data//aa" 可知此為二維陣列
                   };     
  
         var oTable=$("#table1").dataTable(opt);
  
         
        oTable.on('click','tr',function()   {
              var row=oTable.fnGetData(this);
              // alert(resultData.PassinID);

               
               
               
               //alert(row);
            oTable.on('dblclick','#select',function()
            {
               // var passinArray=passin.split(",");
             //alert(row);
             
              var count=0;
             
             $.each(resultData.PassinID, function(k, v) {
    //display the key and value pair
                count++;
           if(v.indexOf('#') !== -1)
          
           { 
             var vs=  v.substr(1,v.length-2);//去頭去尾
             var vsarray=vs.split("#");
           
           //  alert(k + ' order ' + count  +' is '+vs );
            for(var i=0;i<vsarray.length;i++)
            {
                 if(vsarray[i].match(/.Name.*/))
                 { $('#'+vsarray[i], opener.document.forms[formID]).html(row[count+1]);   }
                else{$('#'+vsarray[i], opener.document.forms[formID]).attr("value",row[count]); }
            }
            
            }
            
        
           else{
                    if(v.match(/.Name.*/))
                 { $('#'+v, opener.document.forms[formID]).html(row[count]);   }
                 else{$('#'+v, opener.document.forms[formID]).attr("value",row[count]); }
              }
  
});
                //  $('#SupplierID', opener.document).attr("value",row[2]);
                 //$('#SupplierName', opener.document).html(row[1]);
                            window.close();
                          
	   
            }        );
                                                 });
                                                 
                                                 },//success
          error:function(xhr, ajaxOptions, thrownError){ 
                    alert(xhr.status); 
                    alert(thrownError); 
                 }
          
          
        });//ajax
  
}  //getsearchlist
       
        }); //ready

       
  </script>
  </body>
</html>