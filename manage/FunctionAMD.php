<?php
	$err = "";
	// 未登入則轉至首頁(登入頁)
	session_start();
	if (!(isset($_SESSION["manageuser"]))) {
		echo '<meta http-equiv=REFRESH CONTENT=1;url=index.php>';		
	} else {
		include("../common/connectdb.php");
		//判斷要執行之功能 : Q->顯示資料  A->新增  AW->新增寫入  M->修改  MW->修改寫入  D->刪除  DW->刪除寫入  L->列表
		$f = "";
		$id = "";
		
		if (isset($_POST['f'])) {
			if ($_POST['f'] == "Q" || $_POST['f'] == "A" || $_POST['f'] == "M" || $_POST['f'] == "D" || $_POST['f'] == "AW" || $_POST['f'] == "MW" || $_POST['f'] == "DW") {
				$f = $_POST['f'];
			} else {
				$f = "L";
			}
		} else {
			$f = "L";
		}
		
		if ($f == "Q" || $f == "M" || $f == "D") {
			if (!isset($_POST['id'])) {
				$f = "L";
			} else {
				$id = $_POST['id'];
			}
		}
?>

<?php if ($f == "Q" || $f == "A" || $f == "M" || $f == "D") {
	if ($f != "A") {
		$strSQL = "SELECT COUNT(*) FROM Function WHERE FunctionID='".$id."'";
		$rows = mysql_query($strSQL);
		list($i) = mysql_fetch_row($rows);
		if ($i == 0) {
			$err = "資料錯誤或不存在,請檢查後再執行 !";
		} else {
			$strSQL = "SELECT * FROM Function WHERE FunctionID='".$id."'";
			$rows = mysql_query($strSQL);
			$row = mysql_fetch_array($rows);
		}
	}
	$fW = "";
	$fS = "";
	$fSS = "";
	$fV = "";
	if ($f == "Q") { $fW = "Q"; }
	if ($f == "A") { $fW = "AW"; $fS = "新增"; $fSS = "儲存"; }
	if ($f == "M") { $fW = "MW"; $fS = "修改"; $fSS = "儲存"; }
	if ($f == "D") { $fW = "DW"; $fS = "刪除"; $fSS = "刪除"; }
	if ($err != "") {
		echo $err;
	} else {	
?>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<form action="FunctionAMD.php" name="MasterForm" id="MasterForm" method="POST">
		<table align="center" class="table90">
			<tr class="bg_gray">
				<th colspan="6">
					<table width="100%">
						<tr>
							<th class="Arial18Bold align_left"><?php echo '<font color="red">'.$fS.'</font>'; ?>功能基本資料<input name="f" id="f" value="<?php echo $fW; ?>" type="HIDDEN"></th>			
							<th class="Arial18Bold align_right">
							<?php
								if ($f != "Q") {
									if ($f == "D") {				
										//檢查是否已被使用
										$strTemp = "SELECT COUNT(*) FROM site_function WHERE FunctionID='".$id."'";
										$rowsTemp = mysql_query($strTemp);
										$strTemp1 = "SELECT COUNT(*) FROM user_role_zone WHERE FunctionID='".$id."'";
										$rowsTemp1 = mysql_query($strTemp1);
										$strTemp2 = "SELECT COUNT(*) FROM user_role_site WHERE FunctionID='".$id."'";
										$rowsTemp2 = mysql_query($strTemp2);
										list($countTemp) = mysql_fetch_row($rowsTemp);
										list($countTemp1) = mysql_fetch_row($rowsTemp1);
										list($countTemp2) = mysql_fetch_row($rowsTemp2);
										if ($countTemp > 0 or $countTemp1 > 0 or $countTemp2 > 0) {
											$err = "資料已被程式設定維護作業調用,不可刪除 !";
										}
									}
									if ($err == "") {
										echo '<input type="Button" name="submit" value="'.$fSS.'" style="font-family: 標楷體 ,Arial, Helvetica, sans-serif;background-color: #FF8888;" class="Arial16Bold" onclick="refreshDataMaster(\'Function.php\',\'FunctionAMD.php\');">';
									}
								}
							?>
							</th>
						</tr>
					</table>
				</th>
			</tr>
			<tr>
				<th class="Arial16Bold_Right">功能代碼：</th>
				<td class="Arial16"><input name="FunctionID" type="text" id="FunctionID" style="font-family: 標楷體 ,Arial, Helvetica, sans-serif;" <?php if ($f != "A") { echo "readonly"; } ?> value="<?php if ($f != "A") { echo $row['FunctionID']; } ?>"></td>				
				<th class="Arial16Bold_Right">功能名稱：</th>
				<td class="Arial16"><input name="FunctionName" type="text" id="FunctionName" style="font-family: 標楷體 ,Arial, Helvetica, sans-serif;"<?php if ($f == "Q" || $f == "D") { echo "readonly"; } ?> value="<?php if ($f != "A") { echo $row['FunctionName']; } ?>"></td>
				<th class="Arial16Bold_Right">有效否：</th>				
				<td class="Arial16"><input name="Enable" type="checkbox" id="Enable" style="font-family: 標楷體 ,Arial, Helvetica, sans-serif;" <?php if ($f == "Q" || $f == "D") { echo "disabled"; } ?> value="Y" <?php if ($f != "A") { echo ($row['Enable']=="Y" ? "checked" : ""); } ?>></td>								
			</tr>
			<tr>	
				<th class="Arial16Bold_Right">排序：</th>
				<td class="Arial16"><input name="FunctionSort" type="text" id="FunctionSort" style="font-family: 標楷體 ,Arial, Helvetica, sans-serif;" <?php if ($f == "Q" || $f == "D") { echo "readonly"; } ?> value="<?php if ($f != "A") { echo $row['FunctionSort']; } ?>"></td>				
				<th class="Arial16Bold_Right">功能檔名：</th>
				<td class="Arial16"><input name="FunctionFileName" type="text" id="FunctionFileName" style="font-family: 標楷體 ,Arial, Helvetica, sans-serif;" <?php if ($f == "Q" || $f == "D") { echo "readonly"; } ?> value="<?php if ($f != "A") { echo $row['FunctionFileName']; } ?>"></td>			
			</tr>
			<tr>
				<th colspan="6" class="Arial18Bold"><font color="red"><?php if ($err != "") { echo $err; } ?></font></th>
			</tr>
		</table>
	</form>
<?php }} ?>
				
<?php		
		//刪除資料
		if ($f == "DW") {
			if (isset($_POST['FunctionID'])) {
				$strSQL = "DELETE FROM Function WHERE FunctionID='".$_POST['FunctionID']."'";
				mysql_query($strSQL);
				$f = "L";
			} else {
				$err = "資料錯誤或不存在 ,請檢查後再執行!";
				$f = "L";
			}
		}
		
		// 新增或修改儲存
		if ($f == "AW" || $f == "MW") {
			// 檢查新增或更改時之輸入值是否正確
			if (!isset($_POST['FunctionID']) || $_POST['FunctionID'] == "") {
				$err = $err.'功能代碼不存在或輸入值不正確 !'.chr(13);
			}
			if (!isset($_POST['FunctionName']) || $_POST['FunctionName'] == "") {
				$err = $err.'功能名稱不存在或輸入值不正確 !'.chr(13);
			}
			if (!isset($_POST['FunctionFileName']) || $_POST['FunctionFileName'] == "") {
				$err = $err.'功能檔名不存在或輸入值不正確 !'.chr(13);
			}
			if (!isset($_POST['FunctionSort']) || $_POST['FunctionSort'] == "") {
				$err = $err.'功能代碼排序值不存在或輸入值不正確 !'.chr(13);
			}
			$Enable = "N";
			if (isset($_POST['Enable']) && $_POST['Enable'] == "Y") {
				$Enable = "Y";
			}
			// 新增或修改->寫入資料庫
			if ($err == "") {
				if ($f == "AW") {
					// 檢查Key值是否重覆
					$strSQL = "SELECT COUNT(*) FROM Function WHERE FunctionID='".$_POST['FunctionID']."'";
					$rows = mysql_query($strSQL);
					list($i) = mysql_fetch_row($rows);
					if ($i > 0) {
						$err = "功能代碼已存在,請檢查後再執行 !";
						$f = "L";
					} else {
						$strSQL = "INSERT INTO Function (FunctionID,FunctionName,FunctionFileName,FunctionSort,Enable) VALUES ('".$_POST['FunctionID']."','".$_POST['FunctionName']."','".$_POST['FunctionFileName']."',".$_POST['FunctionSort'].",'".$Enable."')";
						mysql_query($strSQL);
						$f = "L";
					}
				} else {
					$strSQL = "UPDATE Function SET FunctionID='".$_POST['FunctionID']."',FunctionName='".$_POST['FunctionName']."',FunctionFileName='".$_POST['FunctionFileName']."',FunctionSort=".$_POST['FunctionSort'].",Enable='".$Enable."' WHERE FunctionID='".$_POST['FunctionID']."'";
					mysql_query($strSQL);
					$f = "L";
				}
			} else {
				$f = "L";
			}
		}

		// L : 回列表 
		if ($f == "L") {
			echo $err;
		}
	}
?>