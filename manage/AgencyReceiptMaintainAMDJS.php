
<script type="text/javascript">
   var IniIncomeAmount = document.forms["MasterForm"].elements["IncomeAmount"];
   var IniRefund = document.forms["MasterForm"].elements["Refund"];
   var IniReturnDeduction = document.forms["MasterForm"].elements["ReturnDeduction"];
   var IniTotalRes = document.forms["MasterForm"].elements["TotalRes"];
   IniTotalRes.value = IniIncomeAmount.value*1 - IniRefund.value*1 - IniReturnDeduction.value*1;

document.getElementById("MasterForm").elements.namedItem("CollectionsNo").onchange = function() {NewFunction()};
document.getElementById("MasterForm").elements.namedItem("DueDate").onchange = function() {RecalFunction()};
document.getElementById("MasterForm").elements.namedItem("TurnToIncomeDate").onchange = function() {RecalFunction()};
document.getElementById("MasterForm").elements.namedItem("IncomeAmount").onchange = function() {RecalFunction()};
document.getElementById("MasterForm").elements.namedItem("Refund").onchange = function(){RecalFunction()};
document.getElementById("MasterForm").elements.namedItem("CleanDeduction").onchange = function(){RecalFunction()};
document.getElementById("MasterForm").elements.namedItem("RepairDeduction").onchange = function(){RecalFunction()};
document.getElementById("MasterForm").elements.namedItem("PunishDeduction").onchange = function(){RecalFunction()};
document.getElementById("MasterForm").elements.namedItem("ReturnDeduction").onchange = function(){RecalFunction()};
document.getElementById("MasterForm").elements.namedItem("Note").onchange = function(){RecalFunction()};
function NewFunction() 
{
  //var CommID = document.forms["MasterForm"].elements["CommID"];
  //CommID.value = "9BA0001";
  //ModUser.value = "1111057";
  var ModDate = document.forms["MasterForm"].elements["ModDate"];
  var currentdate = new Date(); 
  var datetime =  currentdate.getFullYear() + "-"
                + (currentdate.getMonth()+1)  + "-" 
                + currentdate.getDate() + " "  
                + currentdate.getHours() + ":"  
                + currentdate.getMinutes() + ":" 
                + currentdate.getSeconds();
  ModDate.value = datetime; 
  var CleanDeduction = document.forms["MasterForm"].elements["CleanDeduction"];
  CleanDeduction.value = 0;
  var RepairDeduction = document.forms["MasterForm"].elements["RepairDeduction"];
  RepairDeduction.value = 0;
  var PunishDeduction = document.forms["MasterForm"].elements["PunishDeduction"];
  PunishDeduction.value = 0;
  var ReturnDeduction = document.forms["MasterForm"].elements["ReturnDeduction"];
  ReturnDeduction.value = 0;
  TurnToIncomeDate.value=DueDate.value;
  Refund.value=0;
  
}

function RecalFunction()
{   
  var ModUser = document.forms["MasterForm"].elements["ModUser"];
  ModUser.value = "1111057";
  var ModDate = document.forms["MasterForm"].elements["ModDate"];
  var currentdate = new Date(); 
  var datetime =  currentdate.getFullYear() + "-"
                + (currentdate.getMonth()+1)  + "-" 
                + currentdate.getDate() + " "  
                + currentdate.getHours() + ":"  
                + currentdate.getMinutes() + ":" 
                + currentdate.getSeconds();
  ModDate.value = datetime; 
  //var WriteOffAmount = document.forms["DetailForm"].elements["WriteOffAmount"];
  //TotalArs.value=WriteOffAmount.value*1;
  var IncomeAmount = document.forms["MasterForm"].elements["IncomeAmount"];
  var WriteOffAmount = document.forms["MasterForm"].elements["WriteOffAmount"];
  var Refund = document.forms["MasterForm"].elements["Refund"];
  var CleanDeduction = document.forms["MasterForm"].elements["CleanDeduction"];
  var RepairDeduction = document.forms["MasterForm"].elements["RepairDeduction"];
  var PunishDeduction = document.forms["MasterForm"].elements["PunishDeduction"];
  var ReturnDeduction = document.forms["MasterForm"].elements["ReturnDeduction"];
  
  var TotalRes = document.forms["MasterForm"].elements["TotalRes"];
  TotalRes.value = IncomeAmount.value*1 - Refund.value*1 - ReturnDeduction.value*1;

}

</script>

<script src="../js/jquery.validate.js" type="text/javascript"></script>
<script src="../js/message_tw.js" type="text/javascript"></script>
<script type="text/javascript">
$(function()
{
	//須與form表單ID名稱相同
	$("#DetailForm").validate
  (
    {
		  rules: 
      {
	 			DueDate:
        {Required: true},
  			IncomeAmount: 
        {	digits: true,
          min:0,
          Required: true
				},       
        CleanDeduction:   
        {	digits: true,
          //max:IncomeAmount-TotalArs
          max:IncomeAmount.value*1,
          min:0,
          Required: true 
				},
        RepairDeduction:   
        {	digits: true,
          //max:IncomeAmount-TotalArs
          max:IncomeAmount.value*1,
          min:0,
          Required: true 
				},            
        PunishDeduction:   
        {	digits: true,
          //max:IncomeAmount-TotalArs
          max:IncomeAmount.value*1,
          min:0,
          Required: true 
				},  
        Refund:   
        {	digits: true,
          //max:IncomeAmount-TotalArs
          max:IncomeAmount.value*1,
          min:0,
          Required: true 
				},  
  		},
	  }
	);
});

function padLeft(str, len) 
{
     str = '' + str;
          if (str.length >= len)
          {return str;}
          else
          {return padLeft("0" + str, len);} 
} 

</script>