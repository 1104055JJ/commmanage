<?php
	// 未登入則轉至首頁(登入頁)

	if (!isset($_SESSION)) { session_start(); }
	if (!(isset($_SESSION["manageuser"]))) {
		echo '<meta http-equiv=REFRESH CONTENT=1;url=index.php>';		
	} else {
		include("../common/connectdb.php");
		include("../common/AutoFormClass.php");
                
                //$Button='<Button type="Button" class="fa fa-search-plus" onclick="OpenWindow(\''.$_SESSION['Community'].'\')"></Button><em id=SupplierName> </em>';
                
		$a = new AutoFormClass("M","PA_PettyCashSup.php","PA_PettyCashSupAMD.php","pettycash","pettycash",array("CommID","RequestNo"),"零用金撥補作業","center","table90");
		$a->setTableTitle("left","Arial18Bold bg_gray");
		$a->setFieldTitle("center","Arial16 bg_y");
                $a->setField("CommID","社區","left","Arial14","Y","N","","","Y","N","Y","N","text",array(array(),""),$_SESSION['Community']);
		$a->setField("RequestNo","撥補單號","left","Arial14","Y","N","","","N","N","Y","N","text",array(array(),""),"","",array("autoserial","Y","N"));
		
                $a->setField("RequestDate","撥補日期","left","Arial14","N","N","","","Y","Y","Y","N","date",array(array(),""),"");
                $a->setField("BelongDate","歸屬月份","left","Arial14","Y","N","","","Y","Y","Y","Y","date",array(array(),""),"");
                $a->setField("Amount","撥補金額","left","Arial14","Y","Y","","","Y","Y","Y","Y","text",array(array(),""),0);
                $a->setField("Note","備註","left","Arial14","Y","N","","","Y","Y","Y","Y","text",array(array(),""),"");
                $a->setField("ModUser","修改人員","left","Arial14","N","N","","","N","N","Y","N","text",array(array(),""),$_SESSION['manageuser']);
                $a->setField("ModDate","修改時間","left","Arial14","N","N","","","N","N","Y","N","date",array(array(),""),"","",array("datetime","Y","Y"));
               
               // $a->setField($fieldName, $displayName, $align, $class, $wrap, $isNumeric, $beforeHTML, $afterHTML, $A_Display, $A_CanEdit, $M_Display, $M_CanEdit, $inputType);
		//$a->addBeforeDeleteCheck("community",array("ZoneID","ZoneID"),"已被社區基本資料叫用,不可刪除!");
		//$a->setBeforeForm("Y","./PA_SearchFunction.php");
                $a->setBeforeForm("Y","./PA_PettyCashSupAMDJS.php");
                $a->showData();
	}
?>


