<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
	// 未登入則轉至首頁(登入頁)
	session_start();
	if (!(isset($_SESSION["manageuser"]))) {
		echo '<meta http-equiv=REFRESH CONTENT=1;url=index.php>';		
	} else {
?>
		<script type="text/javascript" src="../js/DetailJS.js"></script>
		<script language="JavaScript">handleDetailItem('SiteDAMD.php','A');</script>		
		<table align="center" class="table90">
			<tr class="bg_gray">
				<th colspan="6">
					<table width="100%">
						<tr>
							<!-- <th class="Arial18Bold align_left"> -->
								<!-- <input type="Button" class="Arial16Bold" style="font-family: 標楷體 ,Arial, Helvetica, sans-serif;background-color: #88FFFF" value="功能設定-依區域" onclick="refreshDataD('RoleD.php','<?php echo $_POST['id']; ?>',10,10,1);handleDetailItem('RoleDAMD.php','A');"> -->
								<!-- <input type="Button" class="Arial16Bold" style="font-family: 標楷體 ,Arial, Helvetica, sans-serif;" value="功能設定-依工地" onclick="refreshDataD('RoleDS.php','<?php echo $_POST['id']; ?>',10,10,1);handleDetailItem('RoleDSAMD.php','A');"> -->								
							<!-- </th> -->
							<th class="Arial18Bold align_right">
								<input type="Button" style="font-family: 標楷體 ,Arial, Helvetica, sans-serif;" value="新增" onclick="handleDetailItem('SiteDAMD.php','A');">
								<input type="Button" style="font-family: 標楷體 ,Arial, Helvetica, sans-serif;" value="修改" onclick="handleDetailItem('SiteDAMD.php','M');">
								<input type="Button" style="font-family: 標楷體 ,Arial, Helvetica, sans-serif;" value="刪除" onclick="handleDetailItem('SiteDAMD.php','D');">								
							</th>
						</tr>
					</table>
				</th>
			</tr>
			<tr class="bg_y">
				<th colspan="2" class="Arial16Bold align_center">項次</th>
				<th class="Arial16Bold">工地代碼</th>
				<th class="Arial16Bold">主機位置</th>
				<th class="Arial16Bold">對應埠號</th>
				<th class="Arial16Bold">設備類型</th>
			<tr>
<?php
		include("../common/connectdb.php");
		// 計算筆數及頁數
		$d_TotalCount = 0;
		$d_PageCount = 10;
		$d_PrePageCount = 10;
		$d_Page = 1;
		$d_TotalPage = 1;
		$strSQL = "SELECT COUNT(DISTINCT CONCAT(SiteID,Host,Port,Type)) FROM site_cctv WHERE SiteID='".$_POST['id']."'";
		$rows = mysql_query($strSQL);
		list($d_TotalCount) = mysql_fetch_row($rows);
		if (isset($_POST['d_PageCount'])) { $d_PageCount = $_POST['d_PageCount']; }
		if (isset($_POST['d_PrePageCount'])) { $d_PrePageCount = $_POST['d_PrePageCount']; } 		 
		if (isset($_POST['d_Page'])) { $d_Page = $_POST['d_Page']; }
		if ($d_TotalCount % $d_PageCount == 0) { $d_TotalPage = $d_TotalCount/$d_PageCount; } else { $d_TotalPage = intval($d_TotalCount/$d_PageCount) + 1; } 
		$d_Page = intval(((($d_Page-1)*$d_PrePageCount)+1) / $d_PageCount) + 1;
		//

		$i = 0;
		$j = 0;
		$k = ($d_Page-1)*$d_PageCount;
		$strSQL = "SELECT DISTINCT SiteID,Host,Port,Type FROM site_cctv WHERE SiteID='".$_POST['id']."' ORDER BY SiteID,Host,Port,Type LIMIT ".($d_Page-1)*$d_PageCount.','.$d_PageCount;
		$rows = mysql_query($strSQL);
		while ($row = mysql_fetch_array($rows)) {
			$i++;
			$j++;
			$k++;
			if ($i % 2 == 1) {
				echo '<tr id="d_tr_'.$j.'" class="bg_light_blue" onclick="showSelectDetailItem(\'SiteDAMD.php\','.$j.',\''.$row['SiteID'].'\',\''.$row['Host'].'\',\'\');">';
			} else {
				echo '<tr id="d_tr_'.$j.'" class="bg_light_gray" onclick="showSelectDetailItem(\'SiteDAMD.php\','.$j.',\''.$row['SiteID'].'\',\''.$row['Host'].'\',\'\');">';
			}
			echo '<td width="30" class="Arial14 align_center" id="d_item_'.$j.'"></td>';
			echo '<td width="45" class="Arial14 align_center">'.$k.'</td>';						
			echo '<td class="Arial14 align_center">'.$row['SiteID'].'</td>';
			echo '<td class="Arial14 align_center">'.$row['Host'].'</td>';
			echo '<td class="Arial14 align_center">'.$row['Port'].'</td>';
			echo '<td class="Arial14 align_center">'.$row['Type'].'</td>';
			echo '</tr>';
		}

		// 顯示筆數及頁數
		echo '<tr><td colspan="6"><form action="" name="DetailFormPage" id="DetailFormPage" method="POST">
		<br>每頁顯示';
		echo '<select name="d_PageCount" id="d_PageCount" style="font-family: 標楷體 ,Arial, Helvetica, sans-serif;" onchange="refreshDataDPage(\'SiteD.php\',\''.$_POST['id'].'\',\'\');">';
		$d_PageCountArray = array("5","5","10","10","15","15","20","20");
		for ($i = 0; $i < sizeof($d_PageCountArray); $i = $i + 2) {
			$selected = "";
			if ($d_PageCount == $d_PageCountArray[$i+1]) {
				$selected = "selected";
			}
			echo '<option value="'.$d_PageCountArray[$i+1].'" '.$selected.'>'.$d_PageCountArray[$i].'</option>';			
		}
		echo '</select>';
		echo '共'.$d_TotalCount.'筆    ';							
		echo '<input type="Button" style="font-family: 標楷體 ,Arial, Helvetica, sans-serif;" value="<<" '.($d_Page==1?"disabled":"").' onclick="refreshDataDPage(\'SiteD.php\',\''.$_POST['id'].'\',\'f\');">';
		echo '<input type="Button" style="font-family: 標楷體 ,Arial, Helvetica, sans-serif;" value="<" '.($d_Page==1?"disabled":"").' onclick="refreshDataDPage(\'SiteD.php\',\''.$_POST['id'].'\',\'p\');">';
		echo '<input type="Button" style="font-family: 標楷體 ,Arial, Helvetica, sans-serif;" value=">" '.($d_Page==$d_TotalPage?"disabled":"").' onclick="refreshDataDPage(\'SiteD.php\',\''.$_POST['id'].'\',\'n\');">';
		echo '<input type="Button" style="font-family: 標楷體 ,Arial, Helvetica, sans-serif;" value=">>" '.($d_Page==$d_TotalPage?"disabled":"").' onclick="refreshDataDPage(\'SiteD.php\',\''.$_POST['id'].'\',\'l\');">';
		echo '    跳至';
		echo '<select name="d_Page" id="d_Page" style="font-family: 標楷體 ,Arial, Helvetica, sans-serif;" onchange="refreshDataDPage(\'SiteD.php\',\''.$_POST['id'].'\',\'\');">';
		for ($i = 1; $i <= $d_TotalPage; $i++) {
			$selected = "";
			if ($d_Page == $i) {
				$selected = "selected";
			}
			echo '<option value="'.$i.'" '.$selected.'>'.$i.'</option>';			
		}
		echo '</select>';
		echo '共'.$d_TotalPage.'頁';
		echo '<input id="d_PrePageCount" name="d_PrePageCount" value="'.$d_PageCount.'" type="HIDDEN">';
		echo '<input id="d_TotalPage" name="d_TotalPage" value="'.$d_TotalPage.'" type="HIDDEN">';		
		echo '</form></td></tr>';
		//

		echo '</table>';
		echo '<input id="d_id" name="d_id" value="" type="HIDDEN">';
		echo '<input id="d_id1" name="d_id1" value="" type="HIDDEN">';
		echo '<input id="d_id2" name="d_id2" value="" type="HIDDEN">';		
		echo '<input id="d_tr_count" name="d_tr_count" value="'.$j.'" type="HIDDEN">';		
	}
?>
