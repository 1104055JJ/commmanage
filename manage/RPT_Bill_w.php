<?php
		
		include("../common/connectdb.php");
		include("../common/PublicFunction.php");	
		session_start();
?>
<div style="text-align:center">
<p>管理費通知單<p>
<p>請選擇所要列印的管理費範圍</p>
	<br>
	<select id="s_year" name="s_year" class="Arial14">
		<option value ="2015">2015年</option>
		<option value ="2016">2016年</option>
	</select>
	<select id="s_month" name="s_month" class="Arial14">
		<option value ='1'>1月</option>
		<option value ='2'>2月</option>
		<option value ='3'>3月</option>
		<option value ='4'>4月</option>
		<option value ='5'>5月</option>
		<option value ='6'>6月</option>
		<option value ='7'>7月</option>
		<option value ='8'>8月</option>
		<option value ='9'>9月</option>
		<option value ='10'>10月</option>
		<option value ='11'>11月</option>
		<option value ='12'>12月</option>
	</select>
	<br><br>
	<select id="s_building_f" name="s_building_f" class="Arial14">
		<option value ="-">不選擇</option>
		<?PHP
			$where = "building where CommID = '".$_SESSION['Community']."' ";
			SelectOption($where,'BuildingID','BuildingName','');
		?>
	</select>
	<select id="s_household_f" name="s_household_f" class="Arial14">
		<option value ="-">不選擇</option>
		<?PHP
			$where = "household where CommID = '".$_SESSION['Community']."' ";
			SelectOption($where,"HouseHoldID","owner",'');
		?>
	</select>
	<br>
	<select id="s_building_t" name="s_building_t" class="Arial14">
		<option value ="-">不選擇</option>
		<?PHP
			$where = "building where CommID = '".$_SESSION['Community']."' ";
			SelectOption($where,'BuildingID','BuildingName','');
		?>
	</select>
	<select id="s_household_t" name="s_household_t" class="Arial14">
		<option value ="-">不選擇</option>
		<?PHP
			$where = "household where CommID = '".$_SESSION['Community']."' ";
			SelectOption($where,'HouseHoldID','HouseHoldID','');
		?>
	</select>
	<br>
	<button id="b_html" >HTML報表</button>
	<button id="b_pdf" >pdf報表</button>
	<button id="b_csv" >excel報表</button>
	<input id="l_comm" name="l_comm" value=<?php echo $_SESSION['Community'] ?> type="HIDDEN">
	<div id = "msg"></div>
</div>
<script type="text/javascript">
	

$( "#b_pdf" ).click(function() {
	call_rpt('PDF');
});

$( "#b_html" ).click(function() {
	call_rpt('HTML');
});

$( "#b_csv" ).click(function() {
	call_rpt('EXCEL');
});

function call_rpt(format){
	var commID = document.getElementById("l_comm").value;
   	var e = document.getElementById("s_year");
	var year = e.options[e.selectedIndex].value;
  	var f = document.getElementById("s_month");
	var month = f.options[f.selectedIndex].value;
  	var g = document.getElementById("s_building_f");
	var b_f = g.options[g.selectedIndex].value;
  	var h = document.getElementById("s_building_t");
	var b_t = h.options[h.selectedIndex].value;
  	var i = document.getElementById("s_household_f");
	var h_f = i.options[i.selectedIndex].value;
  	var j = document.getElementById("s_household_t");
	var h_t = j.options[j.selectedIndex].value;
	if (format == 'PDF' || format == 'EXCEL'){
    	window.open("RPT_Bill.php?CommID="+commID+"&Year="+year+"&Month="+month+"&FORMAT="+format+"&b_f="+b_f+"&b_t="+b_t+"&h_f="+h_f+"&h_t="+h_t,"w",config="width=1000,height=800");
    	$( "#dialogWindow" ).dialog( "close" );
	}else{
			//alert(format);
			$.ajax({
				url: "RPT_Bill.php",
				type:"POST",
				dataType: "text",
				data:{CommID:commID,Year:year,Month:month,FORMAT:format,b_f:b_f,b_t:b_t,h_f:h_f,h_t:h_t},
				//成功執行並返回值
				success: function(data){
					$("#msg").empty().append(data); 
					$( "#dialogWindow" ).dialog( "close" );
				},
				//發送請求之前會執行的函式
				beforeSend:function(){
				},
				//請求完成時執行的函式(不論結果是success或error)
				complete:function(){
				},
				error:function(xhr, ajaxOptions, thrownError){ 
					alert(xhr.status);
					alert(thrownError);
				}
			});
	}
}

function getHouseHoldID(BuildingID,input){
	var CommID = document.getElementById("l_comm").value;
	//alert(ID);
	//var BuildingID = document.getElementById("MasterForm").elements.namedItem("BuildingID");
			$.ajax({
				url: "RM_ARGenerateBack.php",
				type:"POST",
				dataType: "json",
				data:{type:'1',CommID:CommID, BuildingID:BuildingID },
				//data:$('#MasterForm').serialize(),
				//成功執行並返回值
				success: function(data){
					//$("#hid").empty().append(data); 
					var obj=document.getElementById(input);
  					obj.options.length=0;
  					obj.options.add(new Option('不選擇','-'));
  					for (var i=0;i<data.length ;i=i+1)
 					 {
    					obj.options.add(new Option(data[i],data[i]));
  					}           		
				},
				//發送請求之前會執行的函式
				beforeSend:function(){
				},
				//請求完成時執行的函式(不論結果是success或error)
				complete:function(){		
					},
				error:function(xhr, ajaxOptions, thrownError){ 
					alert(xhr.status);
					alert(thrownError);
				}
			});
}
document.getElementById("s_building_f").onchange= function(){
  	var g = document.getElementById("s_building_f");
	var b_f = g.options[g.selectedIndex].value;
		getHouseHoldID(b_f,'s_household_f');
};

	document.getElementById("s_building_t").onchange = function() {
  	var g = document.getElementById("s_building_t");
	var b_t = g.options[g.selectedIndex].value;
		getHouseHoldID(b_t,'s_household_t');
	}
</script>
	

	