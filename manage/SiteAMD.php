<?php
	$err = "";
	// 未登入則轉至首頁(登入頁)
	session_start();
	if (!(isset($_SESSION["manageuser"]))) {
		echo '<meta http-equiv=REFRESH CONTENT=1;url=index.php>';		
	} else {
		include("../common/connectdb.php");
		//判斷要執行之功能 : Q->顯示資料  A->新增  AW->新增寫入  M->修改  MW->修改寫入  D->刪除  DW->刪除寫入  L->列表
		$f = "";
		$id = "";
		
		if (isset($_POST['f'])) {
			if ($_POST['f'] == "Q" || $_POST['f'] == "A" || $_POST['f'] == "M" || $_POST['f'] == "D" || $_POST['f'] == "AW" || $_POST['f'] == "MW" || $_POST['f'] == "DW") {
				$f = $_POST['f'];
			} else {
				$f = "L";
			}
		} else {
			$f = "L";
		}
		
		if ($f == "Q" || $f == "M" || $f == "D") {
			if (!isset($_POST['id'])) {
				$f = "L";
			} else {
				$id = $_POST['id'];
			}
		}
?>

<?php if ($f == "Q" || $f == "A" || $f == "M" || $f == "D") {
	if ($f != "A") {
		$strSQL = "SELECT COUNT(*) FROM site WHERE SiteID='".$id."'";
		$rows = mysql_query($strSQL);
		list($i) = mysql_fetch_row($rows);
		if ($i == 0) {
			$err = "資料錯誤或不存在,請檢查後再執行 !";
		} else {
			$strSQL = "SELECT * FROM site WHERE SiteID='".$id."'";
			$rows = mysql_query($strSQL);
			$row = mysql_fetch_array($rows);
		}
	}
	$fW = "";
	$fS = "";
	$fSS = "";
	$fV = "";
	if ($f == "Q") { $fW = "Q"; }
	if ($f == "A") { $fW = "AW"; $fS = "新增"; $fSS = "儲存"; }
	if ($f == "M") { $fW = "MW"; $fS = "修改"; $fSS = "儲存"; }
	if ($f == "D") { $fW = "DW"; $fS = "刪除"; $fSS = "刪除"; }
	if ($err != "") {
		echo $err;
	} else {	
?>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<form action="SiteAMD.php" name="MasterForm" id="MasterForm" method="POST">
		<table align="center" class="table90">
			<tr class="bg_gray">
				<th colspan="5">
					<table width="100%">
						<tr>
							<th class="Arial18Bold align_left"><?php echo '<font color="red">'.$fS.'</font>'; ?>工地基本資料<input name="f" id="f" value="<?php echo $fW; ?>" type="HIDDEN"></th>			
							<th class="Arial18Bold align_right">
							<?php
								if ($f != "Q") {
									if ($f == "D") {				
										//檢查是否已被角色權限維護作業使用
										$strTemp = "SELECT COUNT(*) FROM user_role_site WHERE SiteID='".$id."' LIMIT 1";
										$rowsTemp = mysql_query($strTemp);
										list($countTemp) = mysql_fetch_row($rowsTemp);
										if ($countTemp > 0) {
											$err = "此工地已被角色權限維護作業(依工地)調用,不可刪除 !";
										}
										//檢查是否已有刷卡歷史資料										
										if ($err == "") {
											$strTemp = "SELECT COUNT(*) FROM access WHERE SiteID='".$id."' LIMIT 1";
											$rowsTemp = mysql_query($strTemp);
											list($countTemp) = mysql_fetch_row($rowsTemp);
											if ($countTemp > 0) {
												$err = "此工地已有刷卡資料,不可刪除 !";
											}
										}
										//檢查是否已有人員歷史資料										
										if ($err == "") {
											$strTemp = "SELECT COUNT(*) FROM allemployee WHERE SiteID='".$id."' LIMIT 1";
											$rowsTemp = mysql_query($strTemp);
											list($countTemp) = mysql_fetch_row($rowsTemp);
											if ($countTemp > 0) {
												$err = "此工地已有刷卡人員資料,不可刪除 !";
											}
										}																														
									}
									if ($err == "") {
										echo '<input type="Button" name="submit" value="'.$fSS.'" style="font-family: 標楷體 ,Arial, Helvetica, sans-serif;background-color: #FF8888;" class="Arial16Bold" onclick="refreshDataMaster(\'Site.php\',\'SiteAMD.php\');">';
									}
								}
							?>
							</th>
						</tr>
					</table>
				</th>
			</tr>
			<tr>
				<th class="Arial16Bold_Right">工地代碼：</th>
				<td class="Arial16"><input name="SiteID" type="text" id="SiteID" style="font-family: 標楷體 ,Arial, Helvetica, sans-serif;" <?php if ($f != "A") { echo "readonly"; } ?> value="<?php if ($f != "A") { echo $row['SiteID']; } ?>"></td>
				<th class="Arial16Bold_Right">工地名稱：</th>
				<td class="Arial16"><input name="SiteName" type="text" id="SiteName" style="font-family: 標楷體 ,Arial, Helvetica, sans-serif;" <?php if ($f == "Q" || $f == "D") { echo "readonly"; } ?> value="<?php if ($f != "A") { echo $row['SiteName']; } ?>"></td>
				<td rowspan="4" align="left" class="Arial16">
				<?php
					$strTemp = "SELECT * FROM function WHERE Valid='Y' ORDER BY FunctionSort";
					$rowsF = mysql_query($strTemp);
					while ($rowF = mysql_fetch_array($rowsF)) {
						$strTemp = "SELECT COUNT(*) FROM site_function WHERE SiteID='".$id."' AND FunctionID='".$rowF['FunctionID']."'";
						$rowsTemp = mysql_query($strTemp);
						list($i) = mysql_fetch_row($rowsTemp);
						$html = "";
						$html = '<input name="FunctionID[]" type="checkbox" id="FunctionID" style="font-family: 標楷體 ,Arial, Helvetica, sans-serif;" ';
						if ($f == "Q" || $f == "D") { $html = $html." disabled "; }
						$html = $html.' value="'.$rowF['FunctionID'].'" ';
						if ($f != "A") {
							if ($i > 0) { $html = $html." checked "; }
						}
						$html = $html.'>'.$rowF['FunctionName'].'<br>';
						echo $html;
					}
				?>
				</td>							
			</tr>
			<tr>	
				<th class="Arial16Bold_Right">公司代碼：</th>
				<td class="Arial16">
					<select name="CompanyID" id="CompanyID" style="font-family: 標楷體 ,Arial, Helvetica, sans-serif;" <?php if ($f == "Q" || $f == "D") { echo "disabled"; } ?>>
					<?php
						$strTemp = "SELECT * FROM company WHERE Valid='Y' ORDER BY CompanySort";
						$rowsTemp = mysql_query($strTemp);
						while ($rowTemp = mysql_fetch_array($rowsTemp)) {
							$selected = "";
							if ($f != "A") {
								if ($row['CompanyID'] == $rowTemp['CompanyID']) {$selected = "selected";}
							}	
							echo '<option value="'.$rowTemp['CompanyID'].'" '.$selected.'>'.$rowTemp['CompanyName'].'</option>';
						} 
					?>
					</select>
				</td>				
				<th class="Arial16Bold_Right">區域代碼：</th>
				<td class="Arial16">
					<select name="ZoneID" id="ZoneID" style="font-family: 標楷體 ,Arial, Helvetica, sans-serif;" <?php if ($f == "Q" || $f == "D") { echo "disabled"; } ?>>
					<?php
						$strTemp = "SELECT * FROM zone WHERE Valid='Y' ORDER BY ZoneSort";
						$rowsTemp = mysql_query($strTemp);
						while ($rowTemp = mysql_fetch_array($rowsTemp)) {
							$selected = "";
							if ($f != "A") {
								if ($row['ZoneID'] == $rowTemp['ZoneID']) {$selected = "selected";}
							}
							echo '<option value="'.$rowTemp['ZoneID'].'" '.$selected.'>'.$rowTemp['ZoneName'].'</option>';
						} 
					?>
					</select>					
				</td>
			</tr>
			<tr>			
				<th class="Arial16Bold_Right">主機位址：</th>
				<td class="Arial16"><input name="Host" type="text" id="Host" style="font-family: 標楷體 ,Arial, Helvetica, sans-serif;" <?php if ($f == "Q" || $f == "D") { echo "readonly"; } ?> value="<?php if ($f != "A") { echo $row['Host']; } ?>"></td>				
				<th class="Arial16Bold_Right">連接埠號：</th>
				<td class="Arial16"><input name="Port" type="text" id="Port" style="font-family: 標楷體 ,Arial, Helvetica, sans-serif;" <?php if ($f == "Q" || $f == "D") { echo "readonly"; } ?> value="<?php if ($f != "A") { echo $row['Port']; } ?>"></td>
			</tr>
			<tr>
				<th class="Arial16Bold_Right">資料庫主機位址：</th>
				<td class="Arial16"><input name="MySQL_Host" type="text" id="MySQL_Host" style="font-family: 標楷體 ,Arial, Helvetica, sans-serif;" <?php if ($f == "Q" || $f == "D") { echo "readonly"; } ?> value="<?php if ($f != "A") { echo $row['MySQL_Host']; } ?>"></td>												
				<th class="Arial16Bold_Right">有效否：</th>
				<td class="Arial16"><input name="Valid" type="checkbox" id="Valid" style="font-family: 標楷體 ,Arial, Helvetica, sans-serif;" <?php if ($f == "Q" || $f == "D") { echo "disabled"; } ?> value="Y" <?php if ($f != "A") { echo ($row['Valid']=="Y" ? "checked" : ""); } ?>></td>
			</tr>
			<tr>
				<th colspan="5" class="Arial18Bold"><font color="red"><?php if ($err != "") { echo $err; } ?></font></th>
			</tr>
		</table>
	</form>
<?php }} ?>
				
<?php		
		//刪除資料
		if ($f == "DW") {
			if (isset($_POST['SiteID'])) {
				// 刪site_cctv
				$strSQL = "DELETE FROM site_cctv WHERE SiteID='".$_POST['SiteID']."'";
				mysql_query($strSQL);
				// 刪site_function
				$strSQL = "DELETE FROM site_function WHERE SiteID='".$_POST['SiteID']."'";
				mysql_query($strSQL);				
				// 刪主檔
				$strSQL = "DELETE FROM site WHERE SiteID='".$_POST['SiteID']."'";
				mysql_query($strSQL);				
			} else {
				$err = "資料錯誤或不存在 ,請檢查後再執行!";
			}
			$f = "L";			
		}
		
		// 新增或修改儲存
		if ($f == "AW" || $f == "MW") {
			// 檢查新增或更改時之輸入值是否正確
			if (!isset($_POST['SiteID']) || $_POST['SiteID'] == "") {
				$err = $err.'工地代碼不存在或輸入值不正確 !'.chr(13);
			}
			if (!isset($_POST['SiteName']) || $_POST['SiteName'] == "") {
				$err = $err.'工地名稱不存在或輸入值不正確 !'.chr(13);
			}
			if (!isset($_POST['CompanyID']) || $_POST['CompanyID'] == "") {
				$err = $err.'公司代碼不存在或輸入值不正確 !'.chr(13);
			}
			if (!isset($_POST['ZoneID']) || $_POST['ZoneID'] == "") {
				$err = $err.'區域代碼不存在或輸入值不正確 !'.chr(13);
			}
			if (!isset($_POST['Host']) || $_POST['Host'] == "") {
				$err = $err.'主機位址不存在或輸入值不正確 !'.chr(13);
			}
			if (!isset($_POST['Port']) || $_POST['Port'] == "") {
				$err = $err.'連接埠號不存在或輸入值不正確 !'.chr(13);
			}
			$Valid = "N";
			if (isset($_POST['Valid']) && $_POST['Valid'] == "Y") {
				$Valid = "Y";
			}
			// 新增或修改->寫入資料庫
			if ($err == "") {
				if ($f == "AW") {
					// 檢查Key值是否重覆
					$strSQL = "SELECT COUNT(*) FROM Site WHERE SiteID='".$_POST['SiteID']."'";
					$rows = mysql_query($strSQL);
					list($i) = mysql_fetch_row($rows);
					if ($i > 0) {
						$err = "工地代碼已存在,請檢查後再執行 !";
					} else {
						$strSQL = "INSERT INTO Site (SiteID,SiteName,CompanyID,ZoneID,Host,Port,MySQL_Host,Valid) VALUES ('".$_POST['SiteID']."','".$_POST['SiteName']."','".$_POST['CompanyID']."','".$_POST['ZoneID']."','".$_POST['Host']."','".$_POST['Port']."','".$_POST['MySQL_Host']."','".$Valid."')";
						mysql_query($strSQL);
					}
				} else {
					$strSQL = "UPDATE Site SET SiteID='".$_POST['SiteID']."',SiteName='".$_POST['SiteName']."',CompanyID='".$_POST['CompanyID']."',ZoneID='".$_POST['ZoneID']."',Host='".$_POST['Host']."',Port='".$_POST['Port']."',MySQL_Host='".$_POST['MySQL_Host']."',Valid='".$Valid."' WHERE SiteID='".$_POST['SiteID']."'";
					mysql_query($strSQL);
				}
				if ($err == "") {
					// 變更Table:site_function資料(該工地可用 之Funcction)
					// 先刪除
					$strTemp = "DELETE FROM site_function WHERE SiteID='".$_POST['SiteID']."'";
					mysql_query($strTemp);
					// 再新增
					if (isset($_POST['FunctionID']) && sizeof($_POST['FunctionID']) > 0) {
						for ($i = 0; $i < sizeof($_POST['FunctionID']) ; $i++) {
							$strTemp = "SELECT COUNT(*) FROM function WHERE FunctionID='".$_POST['FunctionID'][$i]."'";
							$rowsTemp = mysql_query($strTemp);
							list($count) = mysql_fetch_row($rowsTemp);
							if ($count > 0) { 
								$strTemp = "INSERT INTO site_function (SiteID,FunctionID) VALUES ('".$_POST['SiteID']."','".$_POST['FunctionID'][$i]."')";
								mysql_query($strTemp);
							}
						}
					}
				}		
			}
			$f = "L";
		}

		// L : 回列表 
		if ($f == "L") {
			echo $err;
		}
	}
?>