<?php
$locale_arr = array (
"language" => "Chinese (traditional)",
"template" => array (
        "T_CHOOSE_LANGUAGE" => "選擇語言",
        "T_GO" => "執行",
        "T_LOGGED_IN_AS" => "登入為",
        "T_LOGIN" => "登入",
        "T_LOGOFF" => "登出",
        "T_ADMIN_HOME" => "管理主頁",
        "T_CONFIG_PROJECT" => "設定專案",
        "T_CREATE_REPORT" => "新增專案",
        "T_ENTER_PROJECT_PASSWORD" => "輸入項目密碼",
        "T_ENTER_PROJECT_PASSWORD_DEMO" => "輸入項目密碼。這些教程的密碼是 <b>reportico</b>",
        "T_UNABLE_TO_CONTINUE" => "無法繼續",
		"T_PASSWORD_ERROR" => "不正確的密碼。再試一次。",
),
);
?>