<?php
$locale_arr = array (
"language" => "Chinese (traditional)",
"template" => array (
        "T_en_gb" => "英語（英國）",
        "T_en_us" => "英語（美國）",
        "T_zh_cn" => "中文（簡體）",
		"T_zh_tw" => "中文（繁體）",
        "T_fr_fr" => "法國",
        "T_it_it" => "意大利的",
        "T_ar_ar" => "阿拉伯語",
        "T_es_es" => "西班牙語",
),
);