<?php
$locale_arr = array (
"language" => "Chinese (traditional)",
"template" => array (
        // Maintenance Buttons
		"T_GO_BACK" => "返回",
		"T_NO_DATA_FOUND" => "未找到數據匹配您的標準",
		"T_UNABLE_TO_CONTINUE" => "無法繼續",
		"T_INFORMATION" => "訊息",
        "T_GO_REFRESH" => "重整",
        "T_GO_PRINT" => "列印",
        )
);
?>