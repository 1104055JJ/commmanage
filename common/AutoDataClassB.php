<?php
	/**
	 * 
	 */
	
	include_once("../common/PublicFunction.php");	 
	class AutoDataClass {
		// 成員屬性
		private $DataFormKey;			// 列表,修改及key欄位
		private $masterKeyValue;		// Master傳給Detail的Key及Value
		
		public $table_Title;			// 表格標題
		public $table_Title_Align;		// 表格標題對齊方式
		public $table_Title_Class;		// 表格標題class
		public $table_Align;			// 表格對齊方式
		public $table_Class;			// 表格class
		public $tableBottom;			// 表格底部區域要顯示的資料
		
		public $field_Title_Align;		// 欄位標題對齊方式
		public $field_Title_Class;		// 欄位標題class
		public $row_Odd_Align;			// 奇數列對齊方式
		public $row_Odd_Class;			// 奇數列class
		public $row_Even_Align;			// 偶數列對齊方式
		public $row_Even_Class;			// 偶數列class
		private $fields;				// 欄位相關值陣列
		private $totalCols;				// 總欄位數
		private $rows;					// 表格內容資料
		private $pageRows;				// 本頁查詢之筆數
		public $beginRowsNum;			// 起始項次號碼(已減1)
		
		public $field_Default_Align;	// 欄位值預設對齊方式
		public $field_Default_Class;	// 欄位值預設class
		
		private $buttons;				// 按鈕
		public $FAMDButtonDisplay;		// 是否顯示FAMDE按鈕,array(F,A,M,D,E) => array("Y","Y","Y","Y","Y")
		public $buttons_Align;			// 按鈕對齊方式
		public $button_Default_Class;	// 按鈕預設class
		
		private $orderBy;				// 排序欄位
		private $orderByDESC;			// 升降冪
		private $filterString;			// 查詢過濾條件
		private $prefix;				// 依Master或Detail給定"m_"或"d_"
		
		private $querySQL;				// 記錄查詢之SQL語法(不含LIMIT)
		private $queryFields;			// 記錄相應於$this->querySQL中之欄位(fieldname,displayName,display)
		
		// 建構子
		function __construct($MasterOrDetail,$dataFile,$formFile,$key,$dataFileDetail,$table_Title,$table_Align,$table_Class) {
			$this->DataFormKey = array("","","",array(),"");
			$this->setDataFormKey($MasterOrDetail,$dataFile,$formFile,$key,$dataFileDetail);
							
			$this->table_Title = $table_Title;
			$this->table_Title_Align = "left";
			$this->table_Title_Class = "";			
			$this->table_Align = $table_Align;
			$this->table_Class = $table_Class;
			$this->tableBottom = "";
			
			$this->field_Title_Align = "";
			$this->field_Title_Class = "";
			
			$this->row_Odd_Align = "";
			$this->row_Odd_Class = "";
			$this->row_Even_Align = "";
			$this->row_Even_Class = "";
			
			$this->pageRows = 0;
			$this->beginRowsNum = 0;
			
			$this->fields = array();
			$this->totalCols = 0;
			$this->field_Default_Align = "";
			$this->field_Default_Class = "";
			
			$this->buttons = array();
			$this->buttons_Align = "right";
			$this->button_Default_Class = "btn";
			$this->FAMDButtonDisplay = array("Y","Y","Y","Y","Y");
			$this->queryFields = array();

			if ($this->DataFormKey[0] == "M") {
				$this->prefix = "m_";
			} else {
				$this->prefix = "d_";
				if (isset($_POST["id"]) && $_POST["id"] != "") {
					$this->masterKeyValue = explode("^A",$_POST["id"]);
				} else {
					$this->masterKeyValue = array();
				}	
			}
									
			isset($_POST[$this->prefix.'orderby']) ? $this->orderBy = $_POST[$this->prefix.'orderby'] : $this->orderBy = "";
			isset($_POST[$this->prefix.'orderbydesc']) ? $this->orderByDESC = $_POST[$this->prefix.'orderbydesc'] : $this->orderByDESC = "";
			isset($_POST[$this->prefix.'filter']) ? $this->filterString = $_POST[$this->prefix.'filter'] : $this->filterString = "";					
		}
		
		// 解建構子
		function __destruct() {
		}

		// 設定AutoData,AutoForm及key欄位
		private function setDataFormKey($MasterOrDetail,$dataFile,$formFile,$key,$dataFileDetail) {
			$MasterOrDetail = strtoupper($MasterOrDetail);
			if ($MasterOrDetail == "M" || $MasterOrDetail == "D") {
				$this->DataFormKey[0] = $MasterOrDetail;
			} else {
				$this->DataFormKey[0] = "M";
			}
			$this->DataFormKey[1] = $dataFile;
			$this->DataFormKey[2] = $formFile;
			$this->DataFormKey[3] = $key;
			$this->DataFormKey[4] = $dataFileDetail;
		}
		
		// 設定表格屬性
		function setTable($align,$class) {
			$align = strtolower($align);
			if ($align == "" || $align == "left" || $align == "center" || $align == "right") {
				$this->table_Align = $align;
			}
			$this->table_Class = $class;
		}

		// 設定表格標題屬性
		function setTableTitle($align,$class) {
			$align = strtolower($align);
			if ($align == "" || $align == "left" || $align == "center" || $align == "right") {
				$this->table_Title_Align = $align;
			}
			$this->table_Title_Class = $class;
		}

		// 設定欄位標題屬性
		function setFieldTitle($align,$class) {
			$align = strtolower($align);
			if ($align == "" || $align == "left" || $align == "center" || $align == "right") {
				$this->field_Title_Align = $align;
			}
			$this->field_Title_Class = $class;
		}
		
		// 設定表格底部要顯示的資料
		function setTableBottom($tableBottom) {
			$this->tableBottom = $tableBottom;
		}

		// 設定奇數列對齊方式及class
		function setOddRow($align,$class) {
			$align = strtolower($align);
			if ($align == "" || $align == "left" || $align == "center" || $align == "right") {
				$this->row_Odd_Align = $align;
			}
			$this->row_Odd_Class = $class;						
		}

		// 設定偶數列對齊方式及class
		function setEvenRow($align,$class) {
			$align = strtolower($align);
			if ($align == "" || $align == "left" || $align == "center" || $align == "right") {
				$this->row_Even_Align = $align;
			}
			$this->row_Even_Class = $class;
		}

		// 設定預設欄位對齊方式
		function setDefaultFieldAlign($align) {
			$align = strtolower($align);
			if ($align == "" || $align == "left" || $align == "center" || $align == "right") {
				$this->field_Default_Align = $align;
			}			
		}

		// 設定預設欄位class
		function setDefaultFieldClass($class) {
			$this->field_Default_Class = $class;
		}

		// 設定按鈕預設之class
		function setButtonDefaultClass($class) {
			$this->button_Default_Class = $class;
			// 已存在之按鈕如果沒設定class,則給預設值
			for ($i = 0; $i < count($this->buttons); $i++) { 
				if ($this->buttons[$i]["class"] == "") {
					$this->buttons[$i]["class"] = $this->button_Default_Class;
				}
			}
		}
		
		// 執行SQL查詢,並產生相應之欄位屬性
		function setQuery($SELECT,$FROM,$WHERE,$ORDERBY,$LIMIT) {
			if ($SELECT != "" && $FROM != "") {
				// 組SQL
				$strSQL = "SELECT ".$SELECT." FROM ".$FROM;
				
				// 由key值產生查詢條件
				$keyToWhere = $this->keyToWhere();
				if ($WHERE != "") {
					$strSQL .= " WHERE ".$WHERE;
					if ($keyToWhere != "") { $strSQL .= " AND ".$keyToWhere; }
				} else {
					if ($keyToWhere != "") { $strSQL .= " WHERE ".$keyToWhere; }
				}
				
				// 由輸入之條件值加入查詢條件
				$filterToWhere = $this->filterToWhere();
				if (strpos($strSQL,"WHERE")) {
					$strSQL .= ($filterToWhere != "" ? " AND ".$filterToWhere : "");
				} else {
					if ($WHERE != "") {
						$strSQL .= " WHERE ".$WHERE;
						if ($filterToWhere != "") { $strSQL .= " AND ".$filterToWhere; }
					} else {
						if ($filterToWhere != "") { $strSQL .= " WHERE ".$filterToWhere; }
					}
				}

				// 排序ORDER BY
				if ($this->orderBy != "") {
					$strSQL .= " ORDER BY ".$this->orderBy." ".$this->orderByDESC;
				} else {
					if ($ORDERBY != "") { $strSQL .= " ORDER BY ".$ORDERBY; }
				}
				
				// 記錄不含LIMIT之SQL語法
				$this->querySQL = $strSQL;
				
				if ($LIMIT != "") { $strSQL .= " LIMIT ".$LIMIT; }
				
				// 讀欄位屬性
				$this->rows = mysql_query($strSQL);
				$this->pageRows = mysql_num_rows($this->rows);
				$this->totalCols = mysql_num_fields($this->rows);
				for ($i = 0; $i < $this->totalCols; $i ++) {
					// 設定單一欄位屬性
					$field = array();
					$field["fieldName"] = mysql_field_name($this->rows,$i);				
					$field["displayName"] = mysql_field_name($this->rows,$i);
					$field["align"] = $this->field_Default_Align;
					$field["class"] = $this->field_Default_Class;
					$field["orderby"] = "Y";
					$field["display"] = "Y";
					$field["beforeField"] = "";
					$field["afterField"] = "";
					$field["otherProperty"] = "";
					// 加入欄位陣列裡
					$this->fields[$i] = $field;
					
					// 記錄$this->queryFields
					$field1 = array();
					$field1["fieldName"] = $field["fieldName"];
					$field1["displayName"] = $field["displayName"];
					$field1["display"] = $field["display"];
					$this->queryFields[$i] = $field1;
				}
			}
		}
				
		// 設定欄位所有屬性
		function setField($fieldName,$displayName,$align = "",$class = "",$orderby = "Y",$display = "Y",$beforeField = "",$afterField = "",$otherProperty = "") {
			for ($i = 0; $i < $this->totalCols; $i++) {
				if ($this->fields[$i]["fieldName"] == $fieldName) {
					$this->fields[$i]["displayName"] = $displayName;
					$align = strtolower($align);
					if ($align == "left" || $align == "center" || $align == "right") {
						$this->fields[$i]["align"] = $align;
					} else {
						$this->fields[$i]["align"] = $this->field_Default_Align;
					}
					if ($class != "") {
						$this->fields[$i]["class"] = $class;
					} else {
						$this->fields[$i]["class"] = $this->field_Default_Class;
					}
					$this->fields[$i]["orderby"] = $orderby;
					$display = strtoupper($display);
					if ($display == "Y" || $display == "N") {
						$this->fields[$i]["display"] = $display;
					}
					$this->fields[$i]["beforeField"] = $beforeField;
					$this->fields[$i]["afterField"] = $afterField;
					$this->fields[$i]["otherProperty"] = $otherProperty;
					
					$this->queryFields[$i]["displayName"] = $this->fields[$i]["displayName"];
					$this->queryFields[$i]["display"] = $this->fields[$i]["display"];
					break;
				}
			}
		}

		// 設定欄位顯示
		function setFieldDisplayName($fieldName,$displayName) {
			for ($i = 0; $i < $this->totalCols; $i++) {
				if ($this->fields[$i]["fieldName"] == $fieldName) {
					$this->fields[$i]["displayName"] = $displayName;
					$this->queryFields[$i]["displayName"] = $displayName;
					break;
				}
			}
		}

		// 設定所有欄位對齊方式
		function setAllFieldAlign($align) {
			$align = strtolower($align);
			if ($align == "" || $align == "left" || $align == "center" || $align == "right") {
				for ($i = 0; $i < $this->totalCols; $i++) {
					$this->fields[$i]["align"] = $align;
				}
			}
		}

		// 設定欄位對齊方式
		function setFieldAlign($fieldName,$align) {
			$align = strtolower($align);
			if ($align == "" || $align == "left" || $align == "center" || $align == "right") {			
				for ($i = 0; $i < $this->totalCols; $i++) {
					if ($this->fields[$i]["fieldName"] == $fieldName) {
						$this->fields[$i]["align"] = $align;
						break;
					}
				}
			}
		}

		// 設定所有欄位class
		function setAllFieldClass($class) {
			for ($i = 0; $i < $this->totalCols; $i++) {
				$this->fields[$i]["class"] = $class;
			}
		}

		// 設定欄位class
		function setFieldClass($fieldName,$class) {
			for ($i = 0; $i < $this->totalCols; $i++) {
				if ($this->fields[$i]["fieldName"] == $fieldName) {
					$this->fields[$i]["class"] = $class;
					break;
				}
			}
		}

		// 設定所有欄位orderby
		function setAllFieldOrderBy($orderby) {
			if (strtoupper($orderby) == "Y" || strtoupper($orderby) == "N") {
				for ($i = 0; $i < $this->totalCols; $i++) { 
					$this->fields[$i]["orderby"] = $orderby;
				}
			}
		}

		// 設定欄位orderby
		function setFieldOrderBy($fieldName,$orderby) {
			if (strtoupper($orderby) == "Y" || strtoupper($orderby) == "N") {			
				for ($i = 0; $i < $this->totalCols; $i++) {
					if ($this->fields[$i]["fieldName"] == $fieldName) {
						$this->fields[$i]["orderby"] = $orderby;
						break;
					}
				}
			}
		}

		// 設定所有欄位display
		function setAllFieldDisplay($display) {
			if (strtoupper($display) == "Y" || strtoupper($display) == "N") {
				for ($i = 0; $i < $this->totalCols; $i++) { 
					$this->fields[$i]["display"] = $display;
					$this->queryFields[$i]["display"] = $display;
				}
			}
		}

		// 設定欄位display
		function setFieldDisplay($fieldName,$display) {
			if (strtoupper($display) == "Y" || strtoupper($display) == "N") {			
				for ($i = 0; $i < $this->totalCols; $i++) {
					if ($this->fields[$i]["fieldName"] == $fieldName) {
						$this->fields[$i]["display"] = $display;
						$this->queryFields[$i]["display"] = $display;
						break;
					}
				}
			}
		}
		
		// 增加Button及功能
		private function addDefaultButton($serial,$name,$event,$id = "",$class = "") {
			if ($name != "") {
				$button = array();
				$button["name"] = $name;
				$button["event"] = $event;
				$button["id"] = $id;				
				$button["class"] = ($class != "" ? $class : $this->button_Default_Class);
				$this->buttons[$serial] = $button;
			}
		}
		
		function addButton($name,$event,$id = "",$class = "") {
			if ($name != "") {
				$button = array();
				$button["name"] = $name;
				$button["event"] = $event;
				$button["id"] = $id;				
				$button["class"] = ($class != "" ? $class : $this->button_Default_Class);
				$i = count($this->buttons);
				if ($i == 0) {
					$this->buttons[5] = $button;
				} else {
					$this->buttons[] = $button;
				}
			}
		}

		private function addFAMDButton() {
			$queryFieldsString = "";
			for ($i = 0; $i < count($this->queryFields); $i++) {
				$queryFieldsString .= ($i == 0 ? "" : "^A");
				$queryFieldsString .= $this->queryFields[$i]["fieldName"].','.$this->queryFields[$i]["displayName"].','.$this->queryFields[$i]["display"];
			}
			$querySQL = $this->querySQL;
			$exportExcelEvent = 'onclick="location.href=\'RPT_Excel.php?t='.urlencode(encrypt($this->table_Title,'E')).'&f='.urlencode(encrypt($queryFieldsString,'E')).'&q='.urlencode(encrypt($querySQL,'E')).'\'"';

			if ($this->DataFormKey[0] == "M") {
				$this->addDefaultButton(0,"查詢","onclick=\"handleMasterItem('".$this->DataFormKey[2]."','F');\"",$this->prefix."queryButton","btn btn-info");				
				$this->addDefaultButton(1,"新增","onclick=\"handleMasterItem('".$this->DataFormKey[2]."','A');\"",$this->prefix."insertButton","btn btn-info");
				$this->addDefaultButton(2,"修改","onclick=\"handleMasterItem('".$this->DataFormKey[2]."','M');\"",$this->prefix."updateButton","btn btn-info");
				$this->addDefaultButton(3,"刪除","onclick=\"handleMasterItem('".$this->DataFormKey[2]."','D');\"",$this->prefix."deleteButton","btn btn-info");
				//$this->addDefaultButton(4,"匯出Excel","onclick=\"exportExcel('../manage/RPT_Excel.php','".$this->table_Title."','".$queryFieldsString."','".$querySQL."');\"",$this->prefix."exportExcelButton","btn btn-info");
				$this->addDefaultButton(4,"匯出Excel",$exportExcelEvent,$this->prefix."exportExcelButton","btn btn-info");
			} else {
				$this->addDefaultButton(0,"查詢","onclick=\"handleDetailItem('".$this->DataFormKey[2]."','F');\"",$this->prefix."queryButton","btn btn-info");				
				$this->addDefaultButton(1,"新增","onclick=\"handleDetailItem('".$this->DataFormKey[2]."','A');\"",$this->prefix."insertButton","btn btn-info");
				$this->addDefaultButton(2,"修改","onclick=\"handleDetailItem('".$this->DataFormKey[2]."','M');\"",$this->prefix."updateButton","btn btn-info");
				$this->addDefaultButton(3,"刪除","onclick=\"handleDetailItem('".$this->DataFormKey[2]."','D');\"",$this->prefix."deleteButton","btn btn-info");				
				//$this->addDefaultButton(4,"匯出Excel","onclick=\"exportExcel('../manage/RPT_Excel.php','".$this->table_Title."','".$queryFieldsString."','".$querySQL."');\"",$this->prefix."exportExcelButton","btn btn-info");
				$this->addDefaultButton(4,"匯出Excel",$exportExcelEvent,$this->prefix."exportExcelButton","btn btn-info");			
			}			
		}

		// 設定是否顯示FAMD按鈕
		function setFAMDButtonDisplay($query,$insert,$update,$delete,$exportExcel) {
			$query = strtoupper($query);
			$insert = strtoupper($insert);
			$update = strtoupper($update);
			$delete = strtoupper($delete);
			$exportExcel = strtoupper($exportExcel);
			
			if ($query == "Y" || $query == "N") { $this->FAMDButtonDisplay[0] = $query; }
			if ($insert == "Y" || $insert == "N") { $this->FAMDButtonDisplay[1] = $insert; }
			if ($update == "Y" || $update == "N") { $this->FAMDButtonDisplay[2] = $update; }
			if ($delete == "Y" || $delete == "N") { $this->FAMDButtonDisplay[3] = $delete; }
			if ($exportExcel == "Y" || $exportExcel == "N") { $this->FAMDButtonDisplay[4] = $exportExcel; }						
		}

		// 設定排序
		function setOrderBy($orderBy,$orderByDESC) {
			$this->orderBy = $orderBy;
			$this->orderByDESC = $orderByDESC;
		}

		// 取得欄位數
		function getTotalCols() {
			return $this->totalCols;
		}
		
		// 取得本頁查詢筆數
		function getPageRows() {
			return $this->pageRows;
		}
		
		// 取得所有資料列
		function getData() {
			return $this->rows;
		}
		
		// 將key值轉成where條件
		private function keyToWhere() {
			$where = "";
			for ($i = 0; $i < count($this->masterKeyValue); $i += 3) {
				$value = $this->masterKeyValue[$i+1];
				if ($this->masterKeyValue[$i+2] == "N") {
					$value = "'".$value."'";
				}
				if ($i != 0) { $where .= " AND "; }
				$where .= $this->masterKeyValue[$i]."=".$value;
			}
			return $where;
		}		

		// 將filterString值轉成條件,需檢查輸入值是否合理
		private function filterToWhere() {
			$where = "";
			if ($this->filterString != "") {
				$filters = explode("^A",$this->filterString);
				for ($i = 0; $i < count($filters); $i += 3) {
					$fieldName = $filters[$i];
					$filterValue = str_replace(" ","",trim($filters[$i+1]));
					$quotes = ($filters[$i+2] == "Y" ? "" : "'"); 

					if (substr($filterValue,0,2) == "<>") {
						if ($quotes == "" && !is_numeric(substr($filterValue,2))) { continue; }						
						$where .= ($i > 0 ? " AND (" : " (").$fieldName." <> ".$quotes.substr($filterValue,2).$quotes.") ";																
					} elseif (substr($filterValue,0,2) == ">=") {
						if ($quotes == "" && !is_numeric(substr($filterValue,2))) { continue; }
						$where .= ($i > 0 ? " AND (" : " (").$fieldName." >= ".$quotes.substr($filterValue,2).$quotes.") ";
					} elseif (substr($filterValue,0,1) == ">") {
						if ($quotes == "" && !is_numeric(substr($filterValue,1))) { continue; }
						$where .= ($i > 0 ? " AND (" : " (").$fieldName." > ".$quotes.substr($filterValue,1).$quotes.") ";					
					} elseif (substr($filterValue,0,2) == "<=") {
						if ($quotes == "" && !is_numeric(substr($filterValue,2))) { continue; }							
						$where .= ($i > 0 ? " AND (" : " (").$fieldName." <= ".$quotes.substr($filterValue,2).$quotes.") ";
					} elseif (substr($filterValue,0,1) == "<") {
						if ($quotes == "" && !is_numeric(substr($filterValue,1))) { continue; }							
						$where .= ($i > 0 ? " AND (" : " (").$fieldName." < ".$quotes.substr($filterValue,1).$quotes.") ";					
					} elseif (substr($filterValue,0,2) == "!=") {
						if ($quotes == "" && !is_numeric(substr($filterValue,2))) { continue; }						
						$where .= ($i > 0 ? " AND (" : " (").$fieldName." != ".$quotes.substr($filterValue,2).$quotes.") ";
					} else {
						$isExist = false;
						$filterValueArray = explode("|",$filterValue);
						if (count($filterValueArray) >= 2) {
							if ($quotes == "") {
								$isBreak = false;
								for ($j = 0; $j < Count($filterValueArray); $j++) {
									if (!is_numeric($filterValueArray[$j])) {
										$isBreak = true;
										break;
									}
								}
								if ($isBreak) { continue; }
							}
							
							$isExist = true;	 
							$where .= ($i > 0 ? " AND (" : " (");
							for ($j = 0; $j < Count($filterValueArray); $j++) { 
								$where .= ($j > 0 ? " OR " : " ").$fieldName." = ".$quotes.$filterValueArray[$j].$quotes;
							}
							$where .= ") ";
						}
						if (!$isExist) {
							$filterValueArray = explode(":",$filterValue);
							if (count($filterValueArray) >= 2) {
								if ($quotes == "" && (!is_numeric($filterValueArray[0]) || !is_numeric($filterValueArray[count($filterValueArray)-1]))) { continue; }
								$isExist = true;
								$where .= ($i > 0 ? " AND (".$fieldName." BETWEEN " : " (".$fieldName." BETWEEN ");
								$where .= $quotes.$filterValueArray[0].$quotes." AND ".$quotes.$filterValueArray[count($filterValueArray)-1].$quotes;
								$where .= ") ";
							}
						}
						if (!$isExist) {							
							$filterValueArray1 = explode("_",$filterValue);
							$filterValueArray2 = explode("%",$filterValue);
							$not = "";
							$startPos = 0;
							if (substr($filterValue,0,1) == "!") { $not = " NOT ";$startPos = 1; }
					 		if (count($filterValueArray1) >= 2 || count($filterValueArray2) >= 2) {
					 			if ($quotes == "") { continue; }
						 		$isExist = true;
								$where .= ($i > 0 ? " AND (" : " (").$fieldName.$not." LIKE ".$quotes.substr($filterValue,$startPos).$quotes.") ";
							}
						}
						if (!$isExist) {
							if ($quotes == "" && !is_numeric(substr($filterValue,0))) { continue; }							
							$isExist = true;
							$where .= ($i > 0 ? " AND (" : " (").$fieldName." = ".$quotes.substr($filterValue,0).$quotes.") ";											
						}
					}
				}				
			}
			return $where;
		}
		
		// 產生資料列表
		function showData() {
			// 增加FAMD按鈕
			$this->addFAMDButton();
			
			$dataTable = $this->prefix."DataTable";
			$dataTableDisplay = $dataTable."Display";
			$dataTableImage = $dataTable."Image";			
			// 設定表格
			$align = ($this->table_Align != "" ? ' align="'.$this->table_Align.'"' : "");
			echo '<table '.$align.'" class="table table-bordered table-condensed table-width-90 table-margin-0 '.$this->table_Class.'">';
			// 設定表格名稱
			if ($this->table_Title != "" || count($this->buttons) > 0) {
				$align = ($this->table_Title_Align != "") ? ' align="'.$this->table_Title_Align.'"' : '';
				$class = ($this->table_Title_Class != "") ? ' class="'.$this->table_Title_Class.'"' : '';				
				echo '<tr '.$class.'><td>';
				echo '<table class="table-condensed table-width-100 table-margin-0">';
				echo '<tr>';
				echo '<td width="100%" '.$align.$class.'"><img id="'.$dataTableImage.'" src="../images/icon/NT-Collapse.gif" onclick="foldTable(\''.$dataTable.'\',\''.$dataTableDisplay.'\',\''.$dataTableImage.'\');">'.$this->table_Title.'</td>';
				// 增加按鈕
				$align = ($this->buttons_Align != "") ? ' align="'.$this->buttons_Align.'"' : '';				
				echo '<td nowrap>';
				for ($i = 0; $i < count($this->buttons); $i++) {
					$FAMDButtonDisplay = "Y";
					// 按鈕FAMDE
					if ($i < 5) {
						$FAMDButtonDisplay = $this->FAMDButtonDisplay[$i];
					}
					$style = "";
					if ($FAMDButtonDisplay == "N") {
						$style = ' style="display:none;" ';
					}
					$class = ($this->buttons[$i]["class"] != "") ? ' class="'.$this->buttons[$i]["class"].'"' : $this->button_Default_Class;
					$event = ($this->buttons[$i]["event"] != "") ? ' '.$this->buttons[$i]["event"] : '';
					$id = ($this->buttons[$i]["id"] != "") ? ' id="'.$this->buttons[$i]["id"].'" ' : '';
					echo '<button type="button"'.$id.$style.$class.' value="" '.$event.' >'.$this->buttons[$i]["name"].'</button>';
				}
				//
				echo '</td>';
				echo '</tr>';
				echo '</table>';
				echo '</td>';
				echo '</tr>';
			}

			// 控制內容是否顯示
			echo '<tr><td><div id="'.$dataTable.'" style="display:;"><table class="table table-bordered table-striped table-hover table-condensed table-margin-0"><thead>';
			// 設定欄位名稱		
			$align = ($this->field_Title_Align != "") ? ' align="'.$this->field_Title_Align.'"' : '';
			$class = ($this->field_Title_Class != "") ? ' class="'.$this->field_Title_Class.'"' : '';						
			echo '<tr'.$align.$class.'>';
			echo '<td colspan="2">項次</td>';
			$i = 0;
			foreach ($this->fields as $key => $value) {
				$i++;
				$event = "";
				if ($value["orderby"] == "Y") {
					if ($this->DataFormKey[0] == "M") {
						$event = " onclick=\"setMDataOrderBy('".$value["fieldName"]."','".$this->DataFormKey[1]."','');\"";
					} else {
						$event = " onclick=\"setDDataOrderBy('".$value["fieldName"]."','".$this->DataFormKey[1]."','".$_POST["id"]."','');\"";						
					}
				}
				$orderbyimg = "";
				if ($value["fieldName"] == $this->orderBy) {
					if ($this->orderByDESC == "") {
						$orderbyimg = '<img src="../images/icon/NT-Ascend.gif">';
					} else {
						$orderbyimg = '<img src="../images/icon/NT-Descend.gif">';
					}
				}
				echo '<td'.($value["display"] == "Y" ? "" : ' style="display:none;" ').$event.'>'.($value["orderby"] == "Y" ? '<a href="#" style="text-decoration:none;">' : '').$value["displayName"].$orderbyimg.($value["orderby"] == "Y" ? '</a>' : '').'</td>';				
			}
			if ($this->DataFormKey[4] != "") { echo '<td width="45"></td>'; }			
			echo '</tr></thead><tbody>';
			// 設定欄位值
			$j = 0;
			$k = $this->beginRowsNum;
			while ($row = mysql_fetch_array($this->rows)) {
				// 判斷奇/偶列以進行不同之顯示	
				$j++;
				$k++;
				$align = "";
				$class = "";
				if ($j % 2 == 1) {
					$align = ($this->row_Odd_Align != "") ? ' align="'.$this->row_Odd_Align.'"' : '';
					$class = ($this->row_Odd_Class != "") ? ' class="'.$this->row_Odd_Class.'"' : '';				
				} else {
					$align = ($this->row_Even_Align != "") ? ' align="'.$this->row_Even_Align.'"' : '';
					$class = ($this->row_Even_Class != "") ? ' class="'.$this->row_Even_Class.'"' : '';					
				}
				
				$event = "";
				$keyvalue = "";
				for ($i = 0; $i < count($this->DataFormKey[3]); $i++) {
					$keyvalue .= ($i == 0 ? "" : "^A").$this->DataFormKey[3][$i]."^A".$row[$this->DataFormKey[3][$i]]."^A".(is_numeric($row[$this->DataFormKey[3][$i]]) ? "Y" : "N");
				}
				if ($this->DataFormKey[0] == "M") {
					// Master
					$event = " onclick=\"showSelectMasterItem('".$this->DataFormKey[2]."',".$j.",'".$keyvalue."');\"";
				} else {
					// Detail
					$event = " onclick=\"showSelectDetailItem('".$this->DataFormKey[2]."',".$j.",'".$keyvalue."');\"";					
				}
				echo '<tr id="'.$this->prefix.'tr_'.$j.'"'.$align.$class.'>';				
				// 項次
				echo '<td width="30" align="center" id="'.$this->prefix.'item_'.$j.'">';
				// 加上radio button及onclick事件
				echo '<input type="radio" id="'.$this->prefix.'radioitem_'.$j.'" name="'.$this->prefix.'item" value="'.$j.'" '.$event.'>';
				echo '</td>';
				echo '<td width="45" align="center">'.$k.'</td>';				
				// 欄位值
				for ($i = 0; $i < $this->totalCols; $i++) {
					$align = ($this->fields[$i]["align"] != "") ? ' align="'.$this->fields[$i]["align"].'"' : '';
					$class = ($this->fields[$i]["class"] != "") ? ' class="'.$this->fields[$i]["class"].'"' : '';					
					echo '<td '.$this->fields[$i]["otherProperty"].($this->fields[$i]["display"] == "Y" ? " " : ' style="display:none;" ').$align.$class.'>'.$this->fields[$i]["beforeField"].$row[$i].$this->fields[$i]["afterField"].'</td>';
				}
				// 加上顯示DetailData之按鈕
				if ($this->DataFormKey[4] != "") {
					$dataFileDetail = " refreshDataD('".$this->DataFormKey[4]."','".$keyvalue."',0,0,1);";					
					$event = " onclick=\"".$dataFileDetail."\"";
					echo '<td><button type="button" id="m_editdetail_'.$j.'" class="btn btn-default btn-sm" disabled="disabled" '.$event.'><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></button></td>';
				}
				echo '</tr>';
			}
			// 表格底部可插入如頁數切換等資訊
			echo '<tr>';
			echo '<td colspan="'.($this->totalCols + 2).'">'.$this->tableBottom.'</td>';
			echo '</tr>';
			// 控制內容是否顯示
			echo '</tbody></table></div></td></tr>';
			//			
			echo '</table>';
			// 配合Master或Detail表格放置不同之隱藏欄位
			echo '<input id="'.$this->prefix.'tr_count" name="'.$this->prefix.'tr_count" value="'.$j.'" type="HIDDEN">';
			echo '<input id="'.$this->prefix.'totalCols" name="'.$this->prefix.'totalCols" value="'.$this->totalCols.'" type="HIDDEN">';				
			echo '<input id="'.$this->prefix.'orderby" name="'.$this->prefix.'orderby" value="'.$this->orderBy.'" type="HIDDEN">';
			echo '<input id="'.$this->prefix.'orderbydesc" name="'.$this->prefix.'orderbydesc" value="'.$this->orderByDESC.'" type="HIDDEN">';
			echo '<input id="'.$this->prefix.'filter" name="'.$this->prefix.'filter" value="'.$this->filterString.'" type="HIDDEN">';
			
			if ($this->DataFormKey[0] == "M") {
				// Master
				echo '<input id="m_id" name="m_id" value="" type="HIDDEN">';
				echo '<input id="m_programM" name="m_programM" value="'.$this->DataFormKey[1].'" type="HIDDEN">';
				echo '<input id="m_programMAMD" name="m_programMAMD" value="'.$this->DataFormKey[2].'" type="HIDDEN">';
			} else {
				// Detail
				echo '<input id="d_id" name="d_id" value="" type="HIDDEN">';
				echo '<input id="d_id1" name="d_id1" value="" type="HIDDEN">';
				echo '<input id="d_id2" name="d_id2" value="" type="HIDDEN">';
				echo '<input id="d_programD" name="d_programD" value="'.$this->DataFormKey[1].'" type="HIDDEN">';
				echo '<input id="d_programDAMD" name="d_programDAMD" value="'.$this->DataFormKey[2].'" type="HIDDEN">';						
			}
			echo '<input id="'.$dataTableDisplay.'" name="'.$dataTableDisplay.'" value="Y" type="HIDDEN">';					
		}
	}
?>