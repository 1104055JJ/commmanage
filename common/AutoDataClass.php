<?php
	/**
	 * 
	 */
	class AutoDataClass {
		// 成員屬性
		public $DataFormKey;			// 列表,修改及key欄位
		
		public $table_Title;			// 表格標題
		public $table_Title_Align;		// 表格標題對齊方式
		public $table_Title_Class;		// 表格標題class
		public $table_Align;			// 表格對齊方式
		public $table_Class;			// 表格class
		public $tableBottom;			// 表格底部區域要顯示的資料
		
		public $field_Title_Align;		// 欄位標題對齊方式
		public $field_Title_Class;		// 欄位標題class
		public $row_Odd_Align;			// 奇數列對齊方式
		public $row_Odd_Class;			// 奇數列class
		public $row_Even_Align;			// 偶數列對齊方式
		public $row_Even_Class;			// 偶數列class
		private $fields;				// 欄位相關值陣列
		private $totalCols;				// 總欄位數
		private $rows;					// 表格內容資料
		private $pageRows;				// 本頁查詢之筆數
		public $beginRowsNum;			// 起始項次號碼(已減1)
		
		public $field_Default_Align;	// 欄位值預設對齊方式
		public $field_Default_Class;	// 欄位值預設class
		
		private $buttons;				// 按鈕
		public $buttons_Align;			// 按鈕對齊方式
		public $button_Default_Class;	// 按鈕預設class
		
		private $orderBy;				// 排序欄位
		private $orderByDESC;			// 升降冪
		private $prefix;				// 依Master或Detail給定"_m"或"_d"
		
		// 建構子
		function __construct($MasterOrDetail,$dataFile,$formFile,$key,$dataFileDetail = "",$table_Title = "",$table_Align = "",$table_Class = "") {
			$this->DataFormKey = array("","","",array(),"");
			$this->setDataFormKey($MasterOrDetail,$dataFile,$formFile,$key,$dataFileDetail);
				
			$this->table_Title = $table_Title;
			$this->table_Title_Align = "left";
			$this->table_Title_Class = "";			
			$this->table_Align = $table_Align;
			$this->table_Class = $table_Class;
			$this->tableBottom = "";
			
			$this->field_Title_Align = "";
			$this->field_Title_Class = "";
			
			$this->row_Odd_Align = "";
			$this->row_Odd_Class = "";
			$this->row_Even_Align = "";
			$this->row_Even_Class = "";
			
			$this->pageRows = 0;
			$this->beginRowsNum = 0;
			
			$this->fields = array();
			$this->totalCols = 0;
			$this->field_Default_Align = "";
			$this->field_Default_Class = "";
			
			$this->buttons = array();
			$this->buttons_Align = "right";
			$this->button_Default_Class = "";
			
			if ($this->DataFormKey[0] == "M") {
				$this->prefix = "m_";
			} else {
				$this->prefix = "d_";
			}
			
			isset($_POST[$this->prefix.'orderby']) ? $this->orderBy = $_POST[$this->prefix.'orderby'] : $this->orderBy = "";
			isset($_POST[$this->prefix.'orderbydesc']) ? $this->orderByDESC = $_POST[$this->prefix.'orderbydesc'] : $this->orderByDESC = "";		
		}
		
		// 解建構子
		function __destruct() {
		}

		// 設定AutoData,AutoForm及key欄位
		private function setDataFormKey($MasterOrDetail,$dataFile,$formFile,$key,$dataFileDetail) {
			$MasterOrDetail = strtoupper($MasterOrDetail);
			if ($MasterOrDetail != "M" || $MasterOrDetail != "D") {
				$this->DataFormKey[0] = "M";
			} else {
				$this->DataFormKey[0] = $MasterOrDetail;
			}
			$this->DataFormKey[1] = $dataFile;
			$this->DataFormKey[2] = $formFile;
			$this->DataFormKey[3] = $key;
			$this->DataFormKey[4] = $dataFileDetail;
		}
		
		// 設定表格屬性
		function setTable($align = "",$class = "") {
			$align = strtolower($align);
			if ($align == "" || $align == "left" || $align == "center" || $align == "right") {
				$this->table_Align = $align;
			}
			$this->table_Class = $class;
		}

		// 設定表格標題屬性
		function setTableTitle($title,$align = "",$class = "") {
			$this->table_Title = $title;
			$align = strtolower($align);
			if ($align == "" || $align == "left" || $align == "center" || $align == "right") {
				$this->table_Title_Align = $align;
			}
			$this->table_Title_Class = $class;
		}

		// 設定欄位標題屬性
		function setFieldTitle($align = "",$class = "") {
			$align = strtolower($align);
			if ($align == "" || $align == "left" || $align == "center" || $align == "right") {
				$this->field_Title_Align = $align;
			}
			$this->field_Title_Class = $class;
		}
		
		// 設定表格底部要顯示的資料
		function setTableBottom($tableBottom) {
			$this->tableBottom = $tableBottom;
		}

		// 設定奇數列對齊方式及class
		function setOddRow($align,$class = "") {
			$align = strtolower($align);
			if ($align == "" || $align == "left" || $align == "center" || $align == "right") {
				$this->row_Odd_Align = $align;
			}
			$this->row_Odd_Class = $class;						
		}

		// 設定偶數列對齊方式及class
		function setEvenRow($align,$class = "") {
			$align = strtolower($align);
			if ($align == "" || $align == "left" || $align == "center" || $align == "right") {
				$this->row_Even_Align = $align;
			}
			$this->row_Even_Class = $class;
		}

		// 設定預設欄位對齊方式
		function setDefaultFieldAlign($align) {
			$align = strtolower($align);
			if ($align == "" || $align == "left" || $align == "center" || $align == "right") {
				$this->field_Default_Align = $align;
			}			
		}

		// 設定預設欄位class
		function setDefaultFieldClass($class) {
			$this->field_Default_Class = $class;
		}

		// 設定按鈕預設之class
		function setButtonDefaultClass($class) {
			$this->button_Default_Class = $class;
		}
		
		// 執行SQL查詢,並產生相應之欄位屬性
		function setQuery($SELECT,$FROM,$WHERE,$ORDERBY,$LIMIT) {
			if ($SELECT != "" && $FROM != "") {
				// 組SQL
				$strSQL = "SELECT ".$SELECT." FROM ".$FROM;
				if ($WHERE != "") { $strSQL .= " WHERE ".$WHERE; }
				if ($this->orderBy != "") {
					$strSQL .= " ORDER BY ".$this->orderBy." ".$this->orderByDESC;
				} else {
					if ($ORDERBY != "") { $strSQL .= " ORDER BY ".$ORDERBY; }
				}
				if ($LIMIT != "") { $strSQL .= " LIMIT ".$LIMIT; }
				// 讀欄位屬性
				$this->rows = mysql_query($strSQL);
				$this->pageRows = mysql_num_rows($this->rows);
				$this->totalCols = mysql_num_fields($this->rows);
				for ($i = 0; $i < $this->totalCols; $i ++) {
					// 設定單一欄位屬性
					$field = array();
					$field["fieldname"] = mysql_field_name($this->rows,$i);				
					$field["displayname"] = mysql_field_name($this->rows,$i);
					$field["align"] = $this->field_Default_Align;
					$field["class"] = $this->field_Default_Class;
					$field["orderby"] = "Y";
					// 加入欄位陣列裡
					$this->fields[$i] = $field;
				}
			}
		}
				
		// 設定欄位所有屬性
		function setField($fieldname,$displayname,$align = "",$class = "",$orderby = "Y") {
			for ($i = 0; $i < $this->totalCols; $i++) {
				if ($this->fields[$i]["fieldname"] == $fieldname) {
					$this->fields[$i]["displayname"] = $displayname;
					$align = strtolower($align);
					if ($align == "" || $align == "left" || $align == "center" || $align == "right") {
						$this->fields[$i]["align"] = $align;
					}
					$this->fields[$i]["class"] = $class;
					$this->fields[$i]["orderby"] = $orderby;
					break;
				}
			}
		}

		// 設定欄位顯示
		function setFieldDisplayName($fieldname,$displayname) {
			for ($i = 0; $i < $this->totalCols; $i++) {
				if ($this->fields[$i]["fieldname"] == $fieldname) {
					$this->fields[$i]["displayname"] = $displayname;
					break;
				}
			}
		}

		// 設定所有欄位對齊方式
		function setAllFieldAlign($align) {
			$align = strtolower($align);
			if ($align == "" || $align == "left" || $align == "center" || $align == "right") {
				for ($i = 0; $i < $this->totalCols; $i++) {
					$this->fields[$i]["align"] = $align;
				}
			}
		}

		// 設定欄位對齊方式
		function setFieldAlign($fieldname,$align) {
			$align = strtolower($align);
			if ($align == "" || $align == "left" || $align == "center" || $align == "right") {			
				for ($i = 0; $i < $this->totalCols; $i++) {
					if ($this->fields[$i]["fieldname"] == $fieldname) {
						$this->fields[$i]["align"] = $align;
						break;
					}
				}
			}
		}

		// 設定所有欄位class
		function setAllFieldClass($class) {
			for ($i = 0; $i < $this->totalCols; $i++) {
				$this->fields[$i]["class"] = $class;
			}
		}

		// 設定欄位class
		function setFieldClass($fieldname,$class) {
			for ($i = 0; $i < $this->totalCols; $i++) {
				if ($this->fields[$i]["fieldname"] == $fieldname) {
					$this->fields[$i]["class"] = $class;
					break;
				}
			}
		}

		// 設定所有欄位orderby
		function setAllFieldOrderBy($orderby = "Y") {
			if (strtoupper($orderby) == "Y" || strtoupper($orderby) == "N") {
				for ($i = 0; $i < $this->totalCols; $i++) { 
					$this->fields[$i]["orderby"] = $orderby;
				}
			}
		}

		// 設定欄位orderby
		function setFieldOrderBy($fieldname,$orderby = "Y") {
			if (strtoupper($orderby) == "Y" || strtoupper($orderby) == "N") {			
				for ($i = 0; $i < $this->totalCols; $i++) {
					if ($this->fields[$i]["fieldname"] == $fieldname) {
						$this->fields[$i]["orderby"] = $orderby;
						break;
					}
				}
			}
		}
		
		// 增加Button及功能
		function addButton($name,$event = "",$class = "") {
			if ($name != "") {
				$button = array();
				$button["name"] = $name;
				$button["event"] = $event;				
				$button["class"] = ($class != "" ? $class : $this->button_Default_Class);
				$this->buttons[] = $button;
			}
		}

		//設定排序
		function setOrderBy($orderBy,$orderByDESC) {
			$this->orderBy = $orderBy;
			$this->orderByDESC = $orderByDESC;
		}

		// 取得欄位數
		function getTotalCols() {
			return $this->totalCols;
		}
		
		// 取得本頁查詢筆數
		function getPageRows() {
			return $this->pageRows;
		}
		
		// 取得所有資料列
		function getData() {
			return $this->rows;
		}
		
		// 產生資料列表
		function showData() {
			// 設定表格
			$align = ($this->table_Align != "") ? ' align="'.$this->table_Align.'"' : '';
			$class = ($this->table_Class != "") ? ' class="'.$this->table_Class.'"' : '';			
			echo '<table'.$align.$class.'>';
			// 設定表格名稱
			if ($this->table_Title != "" || count($this->buttons) > 0) {
				$class = ($this->table_Title_Class != "") ? ' class="'.$this->table_Title_Class.'"' : '';				
				echo '<tr>';
				echo '<th colspan="'.($this->totalCols + 2).'">';
				echo '<table width="100%"'.$class.'>';
				echo '<tr>';
				$align = ($this->table_Title_Align != "") ? ' align="'.$this->table_Title_Align.'"' : '';
				echo '<th'.$align.'>'.$this->table_Title.'</th>';
				$align = ($this->buttons_Align != "") ? ' align="'.$this->buttons_Align.'"' : '';				
				echo '<th'.$align.'>';
				for ($i = 0; $i < count($this->buttons); $i++) {
					$class = ($this->buttons[$i]["class"] != "") ? ' class="'.$this->buttons[$i]["class"].'"' : '';
					$event = ($this->buttons[$i]["event"] != "") ? ' '.$this->buttons[$i]["event"] : '';					
					echo '<input type="Button"'.$class.' value="'.$this->buttons[$i]["name"].'"'.$event.' />';
				}
				echo '</th>';
				echo '</tr>';
				echo '</table>';
				echo '</th>';
				echo '</tr>';
			}
			// 設定欄位名稱			
			$align = ($this->field_Title_Align != "") ? ' align="'.$this->field_Title_Align.'"' : '';
			$class = ($this->field_Title_Class != "") ? ' class="'.$this->field_Title_Class.'"' : '';						
			echo '<tr'.$align.$class.'>';
			echo '<th colspan="2">項次</th>';
			$i = 0;
			foreach ($this->fields as $key => $value) {
				$i++;
				$event = "";
				if ($value["orderby"] == "Y") {
					$event = " onclick=\"setMDataOrderBy('".$value["fieldname"]."','".$this->DataFormKey[1]."','');\"";
				}
				$orderbyimg = "";
				if ($value["fieldname"] == $this->orderBy) {
					if ($this->orderByDESC == "") {
						$orderbyimg = '<img src="../images/icon/NT-Ascend.gif">';
					} else {
						$orderbyimg = '<img src="../images/icon/NT-Descend.gif">';
					}
				}
				echo '<th'.$event.'>'.($value["orderby"] == "Y" ? '<a href="#" style="text-decoration:none;">' : '').$value["displayname"].$orderbyimg.($value["orderby"] == "Y" ? '</a>' : '').'</th>';				
			}
			echo '</tr>';
			// 設定欄位值
			$j = 0;
			$k = $this->beginRowsNum;
			while ($row = mysql_fetch_array($this->rows)) {
				// 判斷奇/偶列以進行不同之顯示	
				$j++;
				$k++;
				$align = "";
				$class = "";
				if ($j % 2 == 1) {
					$align = ($this->row_Odd_Align != "") ? ' align="'.$this->row_Odd_Align.'"' : '';
					$class = ($this->row_Odd_Class != "") ? ' class="'.$this->row_Odd_Class.'"' : '';				
				} else {
					$align = ($this->row_Even_Align != "") ? ' align="'.$this->row_Even_Align.'"' : '';
					$class = ($this->row_Even_Class != "") ? ' class="'.$this->row_Even_Class.'"' : '';					
				}
				
				$event = "";
				$keyvalue = "";
				for ($i = 0; $i < count($this->DataFormKey[3]); $i++) { 
					$keyvalue .= "'".$row[$this->DataFormKey[3][$i]]."'";
				}
				$dataFileDetail = "";
				if ($this->DataFormKey[4] != "") {
					$dataFileDetail = " refreshDataD('".$this->DataFormKey[4]."',".$keyvalue.",0,0,1);";
				}
				if ($this->DataFormKey[0] == "M") {
					// Master
					$event = " onclick=\"showSelectMasterItem('".$this->DataFormKey[2]."',".$j.",".$keyvalue.");".$dataFileDetail."\"";
				} else {
					// Detail
					$event = " onclick=\"showSelectDetailItem('".$this->DataFormKey[2]."',".$j.",".$keyvalue.");".$dataFileDetail."\"";					
				}
				echo '<tr id="'.$this->prefix.'tr_'.$j.'"'.$align.$class.$event.'>';				
				// 項次
				echo '<td width="30" align="center" id="'.$this->prefix.'item_'.$j.'"></td>';
				echo '<td width="45" align="center">'.$k.'</td>';				
				// 欄位值
				for ($i = 0; $i < $this->totalCols; $i++) {
					$align = ($this->fields[$i]["align"] != "") ? ' align="'.$this->fields[$i]["align"].'"' : '';
					$class = ($this->fields[$i]["class"] != "") ? ' class="'.$this->fields[$i]["class"].'"' : '';					
					echo '<td'.$align.$class.'>'.$row[$i].'</td>';	
				}
				echo '</tr>';
			}
			// 表格底部可插入如頁數切換等資訊
			echo '<tr>';
			echo '<td colspan="'.($this->totalCols + 2).'">'.$this->tableBottom.'</td>';
			echo '</tr>';
			echo '</table>';
			// 配合Master或Detail表格放置不同之隱藏欄位
			echo '<input id="'.$this->prefix.'tr_count" name="'.$this->prefix.'tr_count" value="'.$j.'" type="HIDDEN">';
			echo '<input id="'.$this->prefix.'totalCols" name="'.$this->prefix.'totalCols" value="'.$this->totalCols.'" type="HIDDEN">';				
			echo '<input id="'.$this->prefix.'orderby" name="'.$this->prefix.'orderby" value="'.$this->orderBy.'" type="HIDDEN">';
			echo '<input id="'.$this->prefix.'orderbydesc" name="'.$this->prefix.'orderbydesc" value="'.$this->orderByDESC.'" type="HIDDEN">';
			
			if ($this->DataFormKey[0] == "M") {
				// Master
				echo '<input id="m_id" name="m_id" value="" type="HIDDEN">';
			} else {
				// Detail
				echo '<input id="d_id" name="d_id" value="" type="HIDDEN">';
				echo '<input id="d_id1" name="d_id1" value="" type="HIDDEN">';
				echo '<input id="d_id2" name="d_id2" value="" type="HIDDEN">';		
			}					
		}
	}
?>