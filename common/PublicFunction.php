<?php
/*

函数 : encrypt($string,$operation,$key)
參數 : $string:需要加解密的字串; $operation:E表示加密,D表示解密; $key:密鑰
範例 :
$str = '要加解密的字串'; 
$key = 'www.prince.com.tw'; 
echo '加密:'.encrypt($str, 'E', $key); 
echo '解密:'.encrypt($str, 'D', $key);

*/ 
function encrypt($string,$operation,$key=''){
	$key="icms.prince.com.tw";
	$key=md5($key);     
	$key_length=strlen($key);       
	$string=$operation=='D'?base64_decode($string):substr(md5($string.$key),0,8).$string;     
	$string_length=strlen($string);     
	$rndkey=$box=array();     
	$result='';     
	for($i=0;$i<=255;$i++){            
		$rndkey[$i]=ord($key[$i%$key_length]);         
		$box[$i]=$i;     
	}     
	for($j=$i=0;$i<256;$i++){         
		$j=($j+$box[$i]+$rndkey[$i])%256;         
		$tmp=$box[$i];         
		$box[$i]=$box[$j];         
		$box[$j]=$tmp;     
	}     
	for($a=$j=$i=0;$i<$string_length;$i++){         
		$a=($a+1)%256;         
		$j=($j+$box[$a])%256;         
		$tmp=$box[$a];         
		$box[$a]=$box[$j];         
		$box[$j]=$tmp;         
		$result.=chr(ord($string[$i])^($box[($box[$a]+$box[$j])%256]));     
	}     
	if($operation=='D'){         
		if(substr($result,0,8)==substr(md5(substr($result,8).$key),0,8)){             
			return substr($result,8);         
		}else{             
			return '';         
		}     
	}else{         
		return str_replace('=','',base64_encode($result));     
	} 
}

function check_m_f() {
	//判斷要執行之功能 : Q->顯示資料  A->新增  AW->新增寫入  M->修改  MW->修改寫入  D->刪除  DW->刪除寫入  L->列表	
	$f = "L";
	$id = "";
	
	if (isset($_POST['f'])) {
		if ($_POST['f'] == "Q" || $_POST['f'] == "A" || $_POST['f'] == "M" || $_POST['f'] == "D" || $_POST['f'] == "AW" || $_POST['f'] == "MW" || $_POST['f'] == "DW") {
			$f = $_POST['f'];
		}
	}
	
	if ($f == "Q" || $f == "M" || $f == "D") {
		if (!isset($_POST['id'])) {
			$f = "L";
		}
	}
	
	if ($f == "Q" || $f == "M" || $f == "D") {
		$id = $_POST['id'];
	}
	
	return array($f,$id);
}

function set_fW($f) {
	//依要執行之功能,設定變數值
	$fW = "";
	$fS = "";
	$fSS = "";
	if ($f == "Q") { $fW = "Q"; }
	if ($f == "A") { $fW = "AW"; $fS = "新增"; $fSS = "儲存"; }
	if ($f == "M") { $fW = "MW"; $fS = "修改"; $fSS = "儲存"; }
	if ($f == "D") { $fW = "DW"; $fS = "刪除"; $fSS = "刪除"; }
	
	return array($fW,$fS,$fSS);
}

function check_id_exist($from,$where) {
	//檢查傳入之$id(即Key值)是否存在,並傳回資料列
	$err = "";
	$row = NULL;
	$strSQL = "SELECT COUNT(*) FROM ".$from." WHERE ".$where;
	$rows = mysql_query($strSQL);
	list($i) = mysql_fetch_row($rows);
	if ($i == 0) {
		$err = "資料錯誤或不存在,請檢查後再執行 !";
	} else {
		$strSQL = "SELECT * FROM ".$from." WHERE ".$where;
		$rows = mysql_query($strSQL);
		$row = mysql_fetch_array($rows);
	}
	return array($err,$row);
}

function check_can_delete($from,$where) {
	$err = "";
	//檢查是否已被使用,如已被使用則不可刪除
	$strSQL = "SELECT COUNT(*) FROM ".$from." WHERE ".$where;
	$rows = mysql_query($strSQL);
	list($count) = mysql_fetch_row($rows);
	if ($count > 0) {
		$err = "資料已被調用,不可刪除 !";
	}
	return $err;
}

function set_submit_button($fSS,$url,$amdurl) {
	//設定submit按鈕
	echo '<input type="Button" name="submit" value="'.$fSS.'" style="background-color: #FF8888;" class="Arial16Bold" onclick="refreshDataMaster(\''.$url.'\',\''.$amdurl.'\');">';
}

function insert_update_data($f,$from,$where,$field) {
	//檢查是否重覆,新增或修改資料
	$err = "";
	if ($f == "AW") {
		// 檢查Key值是否重覆
		$strSQL = "SELECT COUNT(*) FROM ".$from." WHERE ".$where;
		$rows = mysql_query($strSQL);
		list($i) = mysql_fetch_row($rows);
		if ($i > 0) {
			$err = "資料已存在,請檢查後再執行 !";
		} else {
			$strSQL = "INSERT INTO ".$from." (";
			for ($j = 0; $j < sizeof($field); $j += 3) { 
				if ($j != 0) { $strSQL .= ","; }
				$strSQL .= $field[$j];
			}
			$strSQL .= ") VALUES (";
			for ($j = 1; $j < sizeof($field); $j += 3) {
				if ($j != 1) { $strSQL .= ","; }
				$strSQL .= $field[$j+1].$field[$j].$field[$j+1];
			}
			$strSQL .= ")"; 
			mysql_query($strSQL);
		}
	} else {
		$strSQL = "UPDATE ".$from." SET ";
		for ($j = 0; $j < sizeof($field); $j += 3) {
			if ($j != 0) { $strSQL .= ","; }
			$strSQL .= $field[$j]."=".$field[$j+2].$field[$j+1].$field[$j+2];
		}
		$strSQL .= " WHERE ".$where;		
		mysql_query($strSQL);
	}
	
	return $err;
}

function Data($strSQL) {
	$rows = mysql_query($strSQL);
	echo '<table class="testtable">';
	echo '<tr>';
	for ($i = 0; $i < mysql_num_fields($rows); $i++) {
		$FieldName = mysql_field_name($rows, $i);
		$strTemp = "SELECT * FROM language WHERE English='".mysql_field_name($rows, $i)."'";
		$temp_rows = mysql_query($strTemp);
		if (mysql_num_rows($temp_rows) > 0) {
			$temp_row = mysql_fetch_array($temp_rows);
			$FieldName = $temp_row['Chinese'];
		}
		echo '<td>'.$FieldName.'</td>';
	}
	while ($row = mysql_fetch_array($rows)) {
		echo '<tr>';
		for ($i = 0; $i < mysql_num_fields($rows); $i++) {
			echo '<td>'.$row[$i].'</td>';
		}
		echo '</tr>';
	}
	echo '</tr>';
	echo '</table>';
}

function Tree($Parent = '',$space = 0) {
	$strSQL = "SELECT * FROM tree WHERE Parent='".$Parent."'";
	$rows = mysql_query($strSQL);
	echo '<table>';
	while ($row = mysql_fetch_array($rows)) {
		$j = $space;
		echo '<tr onclick="foldMenu(\''.$row['Child'].'\');';
		if ($row['Link'] != '') {
			echo 'refreshData(\''.$row['Link'].'\');">';
		} else {
			echo '">';
		}
		for ($i = 0; $i <= $j; $i++) {
			echo '<td width="10"></td>';
		}
		if ($row['Link'] != '') {
			echo '<td onclick="refreshData(\''.$row['Link'].'\');">'.$row['Child'].'</td>';
		} else {
			echo '<td><img src=../images/icon/icon_arrow.png>'.$row['Child'].'</td>';
		}
		echo '</tr>';
		echo '<tr>';
		for ($i = 0; $i <= $j; $i++) {
			echo '<td width="10"></td>';
		}		
		echo'<td>';
		echo '<div id="'.$row['Child'].'" style="display:none;">';
		Tree($row['Child'],$j++);
		echo '</div>';
		echo '</td></tr>';
	}
	echo '</table>';
}
//Select 選單
function SelectOption_udc($cata,$currValue){
	$strSQL = "Select CodeID,CodeName from unifieddatacode where CatagoryID = '".$cata."'";
	$rows = mysql_query($strSQL);
	while ($row = mysql_fetch_array($rows)) {
		echo '<option value="'.$row['CodeID'].'"';
		if ($row['CodeID'] == $currValue){
			echo 'selected';
		}
	echo '>'.$row['CodeName'].'</option>';
	
	}
}

function SelectOption($table,$value,$name,$currValue){
	$strSQL = "Select distinct ".$value.",".$name." From ".$table;
	//echo '<script language="javascript">';
	//echo 'alert("'.$strSQL.'")';
	//echo '</script>';
	$rows = mysql_query($strSQL);
	while ($row = mysql_fetch_array($rows)) {
		echo '<option value="'.$row[$value].'"';
		if ($row[$value] == $currValue){
			echo 'selected';
		}
	echo '>'.$row[$value].'-'.$row[$name].'</option>';
	
	}
}

 function excelDate($days){
 if(is_numeric($days)){
  //based on 1900-1-1
  $jd = GregorianToJD(1, 1, 1970);
  $gregorian = JDToGregorian($jd+intval($days)-25569);
  $myDate = explode('/',$gregorian);
  $myDateStr = str_pad($myDate[2],4,'0', STR_PAD_LEFT)
    ."-".str_pad($myDate[0],2,'0', STR_PAD_LEFT)
    ."-".str_pad($myDate[1],2,'0', STR_PAD_LEFT);
  return $myDateStr;
 }
 return $days;
}
// 從$_POST["id"]中取得相對應之欄位值,其中$_POST["id"]為"欄位名稱,欄位值,是否為數值,..." =>三個欄位為一組
function getIdValueFromPost($fieldName) {
	if (isset($_POST["id"])) {
		$id = explode("^A", $_POST["id"]);
		for ($i = 0; $i < count($id); $i += 3) {
			if ($id[$i] == $fieldName) {
				return $id[$i+1];
				break;
			}
		}
	}
	return "";
}

function checkInjection() {
	if( !get_magic_quotes_gpc() )
	{
	    if( is_array($_GET) )
	    {
	        while( list($k, $v) = each($_GET) )
	        {
	            if( is_array($_GET[$k]) )
	            {
	                while( list($k2, $v2) = each($_GET[$k]) )
	                {
	                    $_GET[$k][$k2] = addslashes($v2);
	                }
	                @reset($_GET[$k]);
	            }
	            else
	            {
	                $_GET[$k] = addslashes($v);
	            }
	        }
	        @reset($_GET);
	    }
	 
	    if( is_array($_POST) )
	    {
	        while( list($k, $v) = each($_POST) )
	        {
	            if( is_array($_POST[$k]) )
	            {
	                while( list($k2, $v2) = each($_POST[$k]) )
	                {
	                    $_POST[$k][$k2] = addslashes($v2);
	                }
	                @reset($_POST[$k]);
	            }
	            else
	            {
	                $_POST[$k] = addslashes($v);
	            }
	        }
	        @reset($_POST);
	    }
	 
	    if( is_array($_COOKIE) )
	    {
	        while( list($k, $v) = each($_COOKIE) )
	        {
	            if( is_array($_COOKIE[$k]) )
	            {
	                while( list($k2, $v2) = each($_COOKIE[$k]) )
	                {
	                    $_COOKIE[$k][$k2] = addslashes($v2);
	                }
	                @reset($_COOKIE[$k]);
	            }
	            else
	            {
	                $_COOKIE[$k] = addslashes($v);
	            }
	        }
	        @reset($_COOKIE);
	    }
	}
}
?>