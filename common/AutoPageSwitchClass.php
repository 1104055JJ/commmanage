<?php
	/**
	 * 
	 */
	class AutoPageSwitchClass {
		// 成員屬性
		private $totalRows;			// 資料總列數
		public $perPageRows;		// 目前每頁顯示列數
		private $perPrePageRows;	// 前一次每頁顯示列數
		private $showPage;			// 目前顯示頁
		private $totalPages;		// 總頁數
		private $pageRows;			// 可選擇之每頁列數陣列
		private $MasterOrDetail;	// 用在Master(M)或Detail(D)
		private $dataFile;			// AutoData檔名
		public $beginRowsNum;		// 起始項次號碼(已減1)
		private $pagePrefix;		// 依Master或Detail給定"m_"或"d"
		private $filterString;		// 查詢過濾條件
						
		// 建構子
		function __construct($MasterOrDetail,$dataFile,$strSQL,$pageRows = array(10,20,30,40,50)) {
			if ($MasterOrDetail == "M") {
				$this->pagePrefix = "m_";
			} else {
				$this->pagePrefix = "d_";
			}
			
			$this->MasterOrDetail = $MasterOrDetail;
			$this->dataFile = $dataFile;

			if (is_array($pageRows)) {
				if (count($pageRows) > 0) {
					$isNumeric = true;
					for ($i = 0; $i < count($pageRows); $i++) { 
						if (!is_numeric($pageRows[$i])) {
							$isNumeric = false;
							break;
						}
					}
					// 只有數字才有效
					if ($isNumeric) {
						$this->pageRows = $pageRows;
					} else {
						$this->pageRows = array(10,20,30,40,50);
					}
				}
			}
			
			$this->totalRows = 0;
			$this->perPageRows = $this->pageRows[0];
			$this->perPrePageRows = $this->perPageRows;
			$this->showPage = 1;
			$this->totalPages = 1;

			// 加上key值之查詢條件
			$whereArray = (isset($_POST["id"]) ? explode("^A",$_POST["id"]) : array());
			$WHERE = "";
			if (count($whereArray) > 0) {
				for ($i = 0; $i < count($whereArray); $i += 3) { 
					$WHERE .= ($i == 0 ? "" : " AND ").$whereArray[$i]."=".($whereArray[$i+2] == "Y" ? "" : "'").$whereArray[$i+1].($whereArray[$i+2] == "Y" ? "" : "'");
				}
			}
			if ($WHERE != "") { $strSQL .= " WHERE ".$WHERE; }
			
			// 由輸入之條件值加入查詢條件
			$this->filterString = (isset($_POST[$this->pagePrefix."filter"]) ? $_POST[$this->pagePrefix."filter"] : "");
			$filterToWhere = $this->filterToWhere();						
			if (strpos(strtoupper($strSQL),"WHERE")) {
				$strSQL .= ($filterToWhere != "" ? " AND ".$filterToWhere : "");
			} else {
				if ($WHERE != "") {
					$strSQL .= " WHERE ".$WHERE;
					if ($filterToWhere != "") { $strSQL .= " AND ".$filterToWhere; }
				} else {
					if ($filterToWhere != "") { $strSQL .= " WHERE ".$filterToWhere; }
				}
			}
	
			$rows = mysql_query($strSQL);
		 	list($this->totalRows) = mysql_fetch_row($rows);
		 							
			if (isset($_POST[$this->pagePrefix.'PageCount'])) { $this->perPageRows = $_POST[$this->pagePrefix.'PageCount']; }
			for ($i = 0; $i < count($this->pageRows); $i++) { 
				$isExist = false;
				if ($this->pageRows[$i] == $this->perPageRows) {
					$isExist = true;
					break;
				}
			}
			if ($isExist) {
				if (isset($_POST[$this->pagePrefix.'PrePageCount'])) { $this->perPrePageRows = $_POST[$this->pagePrefix.'PrePageCount']; }
			} else {
				$this->perPageRows = $this->pageRows[0];
				$this->perPrePageRows = $this->perPageRows;
			}
						 		 
			if (isset($_POST[$this->pagePrefix.'Page'])) { $this->showPage = $_POST[$this->pagePrefix.'Page']; }
			if ($this->totalRows % $this->perPageRows == 0) {
				$this->totalPages = $this->totalRows / $this->perPageRows;
			} else {
				$this->totalPages = intval($this->totalRows / $this->perPageRows) + 1;
			} 
			$this->showPage = intval(((($this->showPage - 1) * $this->perPrePageRows) + 1) / $this->perPageRows) + 1;
			$this->beginRowsNum = ($this->showPage - 1) * $this->perPageRows;
		}
		
		// 解建構子
		function __destruct() {
		}

		// 將filterString值轉成條件,需檢查輸入值是否合理
		private function filterToWhere() {
			$where = "";
			if ($this->filterString != "") {
				$filters = explode("^A",$this->filterString);
				for ($i = 0; $i < count($filters); $i += 3) {
					$fieldName = $filters[$i];
					$filterValue = str_replace(" ","",trim($filters[$i+1]));
					$quotes = ($filters[$i+2] == "Y" ? "" : "'"); 

					if (substr($filterValue,0,2) == "<>") {
						if ($quotes == "" && !is_numeric(substr($filterValue,2))) { continue; }						
						$where .= ($i > 0 ? " AND (" : " (").$fieldName." <> ".$quotes.substr($filterValue,2).$quotes.") ";																
					} elseif (substr($filterValue,0,2) == ">=") {
						if ($quotes == "" && !is_numeric(substr($filterValue,2))) { continue; }
						$where .= ($i > 0 ? " AND (" : " (").$fieldName." >= ".$quotes.substr($filterValue,2).$quotes.") ";
					} elseif (substr($filterValue,0,1) == ">") {
						if ($quotes == "" && !is_numeric(substr($filterValue,1))) { continue; }
						$where .= ($i > 0 ? " AND (" : " (").$fieldName." > ".$quotes.substr($filterValue,1).$quotes.") ";					
					} elseif (substr($filterValue,0,2) == "<=") {
						if ($quotes == "" && !is_numeric(substr($filterValue,2))) { continue; }							
						$where .= ($i > 0 ? " AND (" : " (").$fieldName." <= ".$quotes.substr($filterValue,2).$quotes.") ";
					} elseif (substr($filterValue,0,1) == "<") {
						if ($quotes == "" && !is_numeric(substr($filterValue,1))) { continue; }							
						$where .= ($i > 0 ? " AND (" : " (").$fieldName." < ".$quotes.substr($filterValue,1).$quotes.") ";					
					} elseif (substr($filterValue,0,2) == "!=") {
						if ($quotes == "" && !is_numeric(substr($filterValue,2))) { continue; }						
						$where .= ($i > 0 ? " AND (" : " (").$fieldName." != ".$quotes.substr($filterValue,2).$quotes.") ";
					} else {
						$isExist = false;
						$filterValueArray = explode("|",$filterValue);
						if (count($filterValueArray) >= 2) {
							if ($quotes == "") {
								$isBreak = false;
								for ($j = 0; $j < Count($filterValueArray); $j++) {
									if (!is_numeric($filterValueArray[$j])) {
										$isBreak = true;
										break;
									}
								}
								if ($isBreak) { continue; }
							}
							
							$isExist = true;								 
							$where .= ($i > 0 ? " AND (" : " (");
							for ($j = 0; $j < Count($filterValueArray); $j++) { 
								$where .= ($j > 0 ? " OR " : " ").$fieldName." = ".$quotes.$filterValueArray[$j].$quotes;
							}
							$where .= ") ";
						}
						if (!$isExist) {
							$filterValueArray = explode(":",$filterValue);
							if (count($filterValueArray) >= 2) {
								if ($quotes == "" && (!is_numeric($filterValueArray[0]) || !is_numeric($filterValueArray[count($filterValueArray)-1]))) { continue; }
								$isExist = true;
								$where .= ($i > 0 ? " AND (".$fieldName." BETWEEN " : " (".$fieldName." BETWEEN ");
								$where .= $quotes.$filterValueArray[0].$quotes." AND ".$quotes.$filterValueArray[count($filterValueArray)-1].$quotes;
								$where .= ") ";
							}
						}
						if (!$isExist) {
							$filterValueArray1 = explode("_",$filterValue);
							$filterValueArray2 = explode("%",$filterValue);
							$not = "";
							$startPos = 0;
							if (substr($filterValue,0,1) == "!") { $not = " NOT ";$startPos = 1; }
					 		if (count($filterValueArray1) >= 2 || count($filterValueArray2) >= 2) {
								if ($quotes == "") { continue; }												 			
						 		$isExist = true;
								$where .= ($i > 0 ? " AND (" : " (").$fieldName.$not." LIKE ".$quotes.substr($filterValue,$startPos).$quotes.") ";
							}
						}
						if (!$isExist) {
							if ($quotes == "" && !is_numeric(substr($filterValue,0))) { continue; }							
							$isExist = true;
							$where .= ($i > 0 ? " AND (" : " (").$fieldName." = ".$quotes.substr($filterValue,0).$quotes.") ";											
						}
					}
				}				
			}
			return $where;
		}

		// 顯示筆數及頁數
		function getShowData() {
			$html = "";
			$formName = "";
			$events = "";
			$eventf = "";
			$eventp = "";
			$eventn = "";
			$eventl = "";
			$eventj = "";
			
			if ($this->MasterOrDetail == "M") {
				$formName = "MasterFormPage";
				$events = " onchange=\"refreshDataMPage('".$this->dataFile."','');\"";
				$eventf = " onclick=\"refreshDataMPage('".$this->dataFile."','f');\"";
				$eventp = " onclick=\"refreshDataMPage('".$this->dataFile."','p');\"";
				$eventn = " onclick=\"refreshDataMPage('".$this->dataFile."','n');\"";
				$eventl = " onclick=\"refreshDataMPage('".$this->dataFile."','l');\"";
 				$eventj = " onchange=\"refreshDataMPage('".$this->dataFile."','');\"";							
			} else {
				$formName = "DetailFormPage";
				$events = " onchange=\"refreshDataDPage('".$this->dataFile."','".$_POST['id']."','');\"";
				$eventf = " onclick=\"refreshDataDPage('".$this->dataFile."','".$_POST['id']."','f');\"";				
				$eventp = " onclick=\"refreshDataDPage('".$this->dataFile."','".$_POST['id']."','p');\"";				
				$eventn = " onclick=\"refreshDataDPage('".$this->dataFile."','".$_POST['id']."','n');\"";				
				$eventl = " onclick=\"refreshDataDPage('".$this->dataFile."','".$_POST['id']."','l');\"";				
				$eventj = " onchange=\"refreshDataDPage('".$this->dataFile."','".$_POST['id']."','');\"";								
			}

			$html .= '<form action="" name="'.$formName.'" id="'.$formName.'" method="POST">';			
			$html .= '<table><tr><td>';
			$html .= '<br>每頁顯示';			
			$html .= '<select name="'.$this->pagePrefix.'PageCount" id="'.$this->pagePrefix.'PageCount" '.$events.'>';
			for ($i = 0; $i < count($this->pageRows); $i++) {
				$selected = "";
				if ($this->perPageRows == $this->pageRows[$i]) {
					$selected = "selected";
				}
				$html .= '<option value="'.$this->pageRows[$i].'" '.$selected.'>'.$this->pageRows[$i].'</option>';			
			}
			$html .= '</select>';
			$html .= '共'.$this->totalRows.'筆    ';							
			$html .= '<input type="Button" value="<<" '.($this->showPage==1?"disabled":"").$eventf.'>';
			$html .= '<input type="Button" value="<" '.($this->showPage==1?"disabled":"").$eventp.'>';
			$html .= '<input type="Button" value=">" '.($this->showPage==$this->totalPages?"disabled":"").$eventn.'>';
			$html .= '<input type="Button" value=">>" '.($this->showPage==$this->totalPages?"disabled":"").$eventl.'>';
			$html .= '    跳至';
			$html .= '<select name="'.$this->pagePrefix.'Page" id="'.$this->pagePrefix.'Page" '.$eventj.'>';
			for ($i = 1; $i <= $this->totalPages; $i++) {
				$selected = "";
				if ($this->showPage == $i) {
					$selected = "selected";
				}
				$html .= '<option value="'.$i.'" '.$selected.'>'.$i.'</option>';			
			}
			$html .= '</select>';
			$html .= '共'.$this->totalPages.'頁';
			$html .= '<input id="'.$this->pagePrefix.'PrePageCount" name="'.$this->pagePrefix.'PrePageCount" value="'.$this->perPageRows.'" type="HIDDEN">';
			$html .= '<input id="'.$this->pagePrefix.'TotalPage" name="'.$this->pagePrefix.'TotalPage" value="'.$this->totalPages.'" type="HIDDEN">';		
			$html .= '</td></tr></table></form>';
			
			return $html;
		}
	}
?>