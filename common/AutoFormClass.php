﻿<?php
	/**
	 * 
	 */
	class AutoFormClass {
		// 成員屬性
		private $databaseName;				// 資料庫名稱
		private $tableName;					// 真實表格名稱(for insert,update)
		private $viewTableName;				// 顯示用之表格名稱(for 編修畫面)
		private $tableKey;					// 表格key值(array)				
		
		private $beforeForm;				// 在Form之前增加的HTML或其他程式碼array(isIncludeFile,fileNameOrHtmlString)
		private $afterForm;					// 在Form之後要增加的HTML或其他程式array(isIncludeFile,fileNameOrHtmlString)
				
		private $DataFormKey;				// 列表,修改及key欄位
		private $prefix;					// 前置字元,用來分辨是Master還是Detail
		public $table_Align;				// 表格對齊方式
		public $table_Class;				// 表格class		
		public $table_Title;				// 表格標題
		public $table_Title_Align;			// 表格標題對齊方式
		public $table_Title_Class;			// 表格標題class
		public $tableBottom;				// 表格底部區域要顯示的資料

		public $field_Title_Align;			// 欄位標題對齊方式
		public $field_Title_Class;			// 欄位標題class
		private $field_Default_Align;		// 預設欄位對齊方式
		private $field_Default_Class;		// 預設欄位class
		private $fields;					// 顯示欄位相關值陣列(for $this->viewTableName)
		private $tableFields;				// 真實欄位之陣列(for $this->tableName)
		private $totalCols;					// 總欄位數
		private $perRowCols;				// 每列欄位數(含欄位及值))
												
		private $buttons;					// 按鈕
		private $execButtonDisplay;			// 設定$buttons[0]是否顯示
		public $buttons_Align;				// 按鈕對齊方式
		public $button_Default_Class;		// 按鈕預設class
		private $afterExecButtonDisplayAMD;	//
														
		private $funcID;					// 要執行之功能 : Q->顯示資料  A->新增  AW->新增寫入  M->修改  MW->修改寫入  D->刪除  DW->刪除寫入  L->列表		
		
		private $beforeDeleteCheck;			// 刪除前需檢查是否有被其他程式叫用,如有則不可刪除,array
		private $otherDelete;				// 除主table資料以外,還需刪除之table資料,array
		
		private $hiddenHTML;				// 存放隱藏input之HTML
		
		private $filters;					// 存放查詢條件,array(array(fieldName,filterValue),...)
		
		private $afterWriteToDataBase;		// 主Table執行Insert或Update或Delete後,執行額外之StoreProcedure,Ex:array('sp_update_ar',array('CommID','ArNo'),array('AW','MW','DW'))
						
		// 建構子
		function __construct($MasterOrDetail,$dataFile,$formFile,$tableName,$viewTableName,$tableKey,$table_Title,$table_Align,$table_Class) {
			$this->databaseName = "icms";
			$this->tableName = $tableName;
			$this->viewTableName = $viewTableName;
			$this->tableKey = $tableKey;
			
			$this->beforeForm = array("isIncludeFile"=>"N","fileNameOrHtmlString"=>"");
			$this->afterForm = array("isIncludeFile"=>"N","fileNameOrHtmlString"=>"");
			
			$this->table_Title = $table_Title;
			$this->table_Title_Align = "left";
			$this->table_Title_Class = "";			
			$this->table_Align = $table_Align;
			$this->table_Class = $table_Class;
			$this->tableBottom = "";
			
			$this->field_Title_Align = "right";
			$this->field_Title_Class = "";
			$this->field_Default_Align = "left";
			$this->field_Default_Class = "";

			$this->buttons = array();
			$this->buttons_Align = "right";
			$this->button_Default_Class = "btn";

			$this->perRowCols = 0;
			
			$this->DataFormKey = array("","","",array(),"");
			$this->setDataFormKey($MasterOrDetail,$dataFile,$formFile);
			// 初始化所有欄位
			$this->setFields();

			if ($this->funcID == "A" || $this->funcID == "F") {
				if (isset($_POST["id"]) && $_POST["id"] != "") {
					$this->DataFormKey[4] = $_POST["id"];
					$this->DataFormKey[3] = explode("^A",$_POST["id"]);
				}
			}

			// 設定查詢之過濾條件array
			$this->setFilterArray();
			// 預留給execButton
			$this->addButton(" ","","","");
			$this->execButtonDisplay = "Y";
			$this->afterExecButtonDisplayAMD = "N";
			
			$this->otherDelete = array();
			$this->hiddenHTML = "";
			
			$this->afterWriteToDataBase = array();
		}
		
		// 解建構子
		function __destruct() {
		}

		// 設定AutoData,AutoForm及key欄位
		private function setDataFormKey($MasterOrDetail,$dataFile,$formFile) {
			$MasterOrDetail = strtoupper($MasterOrDetail);
			if ($MasterOrDetail == "M" || $MasterOrDetail == "D") {
				$this->DataFormKey[0] = $MasterOrDetail;
			} else {
				$this->DataFormKey[0] = "M";
			}
			$this->prefix = $this->DataFormKey[0];
			$this->DataFormKey[1] = $dataFile;
			$this->DataFormKey[2] = $formFile;
			list($this->DataFormKey[3],$this->DataFormKey[4]) = $this->setFunction();
		}
		
		// 設定beforeForm要插入之程式碼(HTML,JavaScript等)
		public function setBeforeForm($isIncludeFile,$fileNameOrHtmlString) {
			$isIncludeFile = strtoupper($isIncludeFile);
			if ($isIncludeFile == "Y" || $isIncludeFile == "N") {
				$this->beforeForm["isIncludeFile"] = $isIncludeFile;
			} else {
				$this->beforeForm["isIncludeFile"] = "N";
			}
			$this->beforeForm["fileNameOrHtmlString"] = $fileNameOrHtmlString;
		}
		
		// 設定afterForm要插入之程式碼(HTML,JavaScript等)
		public function setAfterForm($isIncludeFile,$fileNameOrHtmlString) {
			$isIncludeFile = strtoupper($isIncludeFile);
			if ($isIncludeFile == "Y" || $isIncludeFile == "N") {
				$this->afterForm["isIncludeFile"] = $isIncludeFile;
			} else {
				$this->afterForm["isIncludeFile"] = "N";
			}
			$this->afterForm["fileNameOrHtmlString"] = $fileNameOrHtmlString;
		}		

		// 設定表格屬性
		function setTable($align = "",$class = "") {
			$align = strtolower($align);
			if ($align == "" || $align == "left" || $align == "center" || $align == "right") {
				$this->table_Align = $align;
			}
			$this->table_Class = $class;
		}

		// 設定表格標題屬性
		function setTableTitle($align = "",$class = "") {
			$align = strtolower($align);
			if ($align == "" || $align == "left" || $align == "center" || $align == "right") {
				$this->table_Title_Align = $align;
			}
			$this->table_Title_Class = $class;
		}

		// 設定欄位標題屬性
		function setFieldTitle($align = "",$class = "") {
			$align = strtolower($align);
			if ($align == "" || $align == "left" || $align == "center" || $align == "right") {
				$this->field_Title_Align = $align;
			}
			$this->field_Title_Class = $class;
		}
		
		// 設定表格底部要顯示的資料
		function setTableBottom($tableBottom) {
			$this->tableBottom = $tableBottom;
		}

		// 設定預設欄位對齊方式
		function setDefaultFieldAlign($align) {
			$align = strtolower($align);
			if ($align == "" || $align == "left" || $align == "center" || $align == "right") {
				$this->field_Default_Align = $align;
			}			
		}

		// 設定預設欄位class
		function setDefaultFieldClass($class) {
			$this->field_Default_Class = $class;
		}

		// 設定按鈕預設之class
		function setButtonDefaultClass($class) {
			$this->button_Default_Class = $class;
			for ($i = 0; $i < count($this->buttons); $i++) { 
				if ($this->buttons[$i]["class"] == "") { $this->buttons[$i]["class"] = $this->button_Default_Class; }
			}
		}

		// 設定execButton按下後,是否顯示AMD畫面
		function setAfterExecButtonDisplayAMD($displayAMD) {
			$displayAMD = strtoupper($displayAMD);
			if ($displayAMD == "Y" || $displayAMD == "N") { $this->afterExecButtonDisplayAMD = $displayAMD; }
		}
		
		// 由表格名稱產生相關欄位
		private function setFields() {
			// 讀欄位屬性
			$strSQL = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE table_schema='".$this->databaseName."' AND table_name='".$this->viewTableName."'";
			$rows = mysql_query($strSQL);
			$this->totalCols = mysql_num_rows($rows);
			while ($row = mysql_fetch_array($rows)) {
				// 設定單一欄位屬性
				$field = array();
				$field["fieldName"] = $row[0];				
				$field["displayName"] = $row[0];
				$field["align"] = $this->field_Default_Align;
				$field["class"] = $this->field_Default_Class;
				$field["wrap"] = "N";
				$field["isNumeric"] = "N";
				$field["beforeHTML"] = "";
				$field["afterHTML"] = "";
				$field["A_Display"] = "Y";
				$field["A_CanEdit"] = "Y";
				$field["M_Display"] = "Y";
				$field["M_CanEdit"] = "Y";
				$field["inputType"] = "text";
				$field["item"] = array(array(),"");
				$field["defaultValue"] = "";
				$field["otherProperty"] = "";
				$field["autoValue"] = array();
				// 加入欄位陣列裡
				$this->fields[] = $field;
			}
			
			// 儲存真實欄位
			$strSQL = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE table_schema='".$this->databaseName."' AND table_name='".$this->tableName."'";
			$rows = mysql_query($strSQL);
			while ($row = mysql_fetch_array($rows)) {
				$this->tableFields[] = $row[0];
			}
		}

		// 設定欄位所有屬性
		function setField($fieldName,$displayName,$align,$class,$wrap,$isNumeric,$beforeHTML,$afterHTML,$A_Display,$A_CanEdit,$M_Display,$M_CanEdit,$inputType,$item = array(),$defaultValue,$otherProperty = "",$autoValue = array()) {
			for ($i = 0; $i < $this->totalCols; $i++) {
				if ($this->fields[$i]["fieldName"] == $fieldName) {
					$this->fields[$i]["displayName"] = $displayName;
					
					$align = strtolower($align);
					if ($align == "" || $align == "left" || $align == "center" || $align == "right") {
						$this->fields[$i]["align"] = $align;
					}
					
					$this->fields[$i]["class"] = $class;
					
					$wrap = strtoupper($wrap);
					if ($wrap == "Y" || $wrap == "N") {
						$this->fields[$i]["wrap"] = $wrap;
					}

					$isNumeric = strtoupper($isNumeric);
					if ($isNumeric == "Y" || $isNumeric == "N") {
						$this->fields[$i]["isNumeric"] = $isNumeric;
					}
										
					$this->fields[$i]["beforeHTML"] = $beforeHTML;
					$this->fields[$i]["afterHTML"] = $afterHTML;
					
					$A_Display = strtoupper($A_Display);
					if ($A_Display == "Y" || $A_Display == "N") {
						$this->fields[$i]["A_Display"] = $A_Display;
					}
					
					$A_CanEdit = strtoupper($A_CanEdit);
					if ($A_CanEdit == "Y" || $A_CanEdit == "N") {
						$this->fields[$i]["A_CanEdit"] = $A_CanEdit;
					}
					
					$M_Display = strtoupper($M_Display);
					if ($M_Display == "Y" || $M_Display == "N") {
						$this->fields[$i]["M_Display"] = $M_Display;
					}
					
					$M_CanEdit = strtoupper($M_CanEdit);
					if ($M_CanEdit == "Y" || $M_CanEdit == "N") {
						$this->fields[$i]["M_CanEdit"] = $M_CanEdit;
					}
					
					$inputType = strtolower($inputType);

					if ($inputType == "hidden" || $inputType == "label" || $inputType == "date" || $inputType == "text" || $inputType == "password" || $inputType == "lov" || $inputType == "love"|| $inputType == "textarea" || $inputType == "combobox" || $inputType == "checkbox" || $inputType == "select" || $inputType = "radio") {
						$this->fields[$i]["inputType"] = $inputType;
					} else {
						$this->fields[$i]["inputType"] = "text";	
					}
					$this->fields[$i]["item"] = $item;
					$this->fields[$i]["defaultValue"] = $defaultValue;
					$this->fields[$i]["otherProperty"] = $otherProperty;
					$this->fields[$i]["autoValue"] = $autoValue;
										
					break;
				}
			}
		}
		
		// 計算每列有幾個欄位
		private function setPerRowCols() {
			$this->perRowCols = 0;
			$tempCols = 0;
			for ($i = 0; $i < count($this->fields); $i++) {
				// 當變成隱藏欄位時,不需計算
				if (!($this->fields[$i]["inputType"] == "hidden" || ($this->funcID == "A" && $this->fields[$i]["A_Display"] == "N") || ($this->funcID == "M" && $this->fields[$i]["M_Display"] == "N"))) {
					$tempCols++;
					if ($this->fields[$i]["wrap"] == "Y") {
						if ($tempCols > $this->perRowCols) { $this->perRowCols = $tempCols; }
						$tempCols = 0;
					}
				}
			}
			if ($tempCols > $this->perRowCols) { $this->perRowCols = $tempCols; }
			$this->perRowCols = $this->perRowCols * 2;
		}		
		
		// 判斷設定要執行之功能
		private function setFunction() {
			//判斷要執行之功能 : F->查詢  Q->顯示資料  A->新增  AW->新增寫入  M->修改  MW->修改寫入  D->刪除  DW->刪除寫入  L->列表	
			$f = "L";
			$key = "";
	
			if (isset($_POST["f"])) {
				if ($_POST["f"] == "Q" || $_POST["f"] == "F" || $_POST["f"] == "A" || $_POST["f"] == "M" || $_POST["f"] == "D" || $_POST["f"] == "FW" || $_POST["f"] == "AW" || $_POST["f"] == "MW" || $_POST["f"] == "DW") {
					$f = $_POST["f"];
				}
			}

			if ($_POST["f"] == "Q" || $_POST["f"] == "M" || $_POST["f"] == "D" || $_POST["f"] == "MW" || $_POST["f"] == "DW") { 
				if (isset($_POST["id"])) {
					$key = $_POST["id"];
				} else {
					$f = "L";
				}
			}
			
			$this->funcID = $f;
			return array(($key == "" ? array() : explode("^A",$key)),$key);
		}
		
		// 將key值轉成where條件
		private function keyToWhere() {
			$where = "";
			for ($i = 0; $i < count($this->DataFormKey[3]); $i += 3) {
				for ($j = 0; $j < count($this->fields); $j++) { 
					if ($this->DataFormKey[3][$i] == $this->fields[$j]["fieldName"]) {
						$value = $this->DataFormKey[3][$i+1];
						if ($this->fields[$j]["isNumeric"] == "N") {
							$value = "'".$value."'";
						}
						if ($i != 0) { $where .= " AND "; }
						$where .= $this->DataFormKey[3][$i]."=".$value;
						break;
					}
				}
			}
			return $where;
		}
						
		// 增加Button及功能
		function addButton($name,$event,$id = "",$class = "") {
			if ($name != "") {
				$button = array();
				$button["name"] = $name;
				$button["event"] = $event;
				$button["id"] = $id;				
				$button["class"] = ($class != "" ? $class : $this->button_Default_Class);
				$this->buttons[] = $button;
			}
		}

		private function addExecButton() {
			// 增加F時之重置按鈕
			$execButton = array();
			$event = "";
			$id = "";
			$class = "btn btn-danger";
			
			if ($this->DataFormKey[0] == "M") {
				$id = "m_resetButton";
				$event = "onclick=\"document.getElementById('m_filter').value='';handleMasterItem('".$this->DataFormKey[2]."','F');\"";
			} else {
				$id = "d_resetButton";
				$event = "onclick=\"document.getElementById('d_filter').value='';handleDetailItem('".$this->DataFormKey[2]."','F');\"";
			}
			
			$execButton["name"] = " ";
			$execButton["event"] = $event;
			$execButton["id"] = $id;
			$execButton["class"] = $class;
			
			$this->buttons[0] = $execButton;
						
			// 增加A,M,D時之按鈕,原本event之格式為onclick="...;",為了配合submit故改為不加onclick="",其他按鈕則以原格式處理
			$execButton = array();
			$event = "";
			$id = "";
			$class = "btn btn-danger";
			
			if ($this->DataFormKey[0] == "M") {
				$f = "";
				if ($this->funcID == "A") {$f = "AW";}
				if ($this->funcID == "M") {$f = "MW";}
				if ($this->funcID == "D") {$f = "DW";}
				if ($this->funcID == "F") {$f = "FW";}				
				$event = " refreshDataMaster('".$this->DataFormKey[1]."','".$this->DataFormKey[2]."','".$f."','".$this->afterExecButtonDisplayAMD."');";
				$id = "m_execButton";
			} else {
				$event = " refreshDataDetail('".$this->DataFormKey[1]."','".$this->DataFormKey[2]."','".$this->DataFormKey[4]."','".($this->funcID == "F" ? "FW" : "")."','".$this->afterExecButtonDisplayAMD."');";
				$id = "d_execButton";
			}
			
			$execButton["name"] = " ";
			$execButton["event"] = $event;
			$execButton["id"] = $id;
			$execButton["class"] = $class;
			
			$this->buttons[1] = $execButton;
		}
		
		// 設定execButton(儲存'刪除等)是否顯示,即$button[0]
		function setExecButtonDisplay($execButton) {
			$execButton = strtoupper($execButton);
			if ($execButton == "Y" || $execButton == "N") { $this->execButtonDisplay = $execButton; }
		}
		
		function setAMDW() {
			//依要執行之功能,設定變數值
			$fW = "";
			$fS = "";
			$fSS = "";
			if ($this->funcID == "F") { $fW = "FW"; $fS = "查詢"; $fSS = "查詢"; }			
			if ($this->funcID == "Q") { $fW = "Q";  $fS = "";     $fSS = "";    }
			if ($this->funcID == "A") { $fW = "AW"; $fS = "新增"; $fSS = "儲存"; }
			if ($this->funcID == "M") { $fW = "MW"; $fS = "修改"; $fSS = "儲存"; }
			if ($this->funcID == "D") { $fW = "DW"; $fS = "刪除"; $fSS = "刪除"; }
	
			return array($fW,$fS,$fSS);
		}		
		
		// 於Insert或Update後執行之StoreProcedure
		function setAfterWriteToDataBase($spName,$paramArray,$funcArray) {
			if ($spName != "") {
				$spArray = array();
				$spArray["spName"] = $spName;
				$spArray["paramArray"] = $paramArray;
				$spArray["funcArray"] = $funcArray;
				$this->afterWriteToDataBase[] = $spArray;
			}
		}
		
		// 判斷為何種inputType
		private function selectInputType($fieldIndex,$value,$colspan) {
			$inputType = $this->fields[$fieldIndex]["inputType"];
			// 新增時不顯示
			if ($this->funcID == "A" && $this->fields[$fieldIndex]["A_Display"] == "N") {
				$inputType = "hidden";
			}
			// 修改時不顯示
			if ($this->funcID == "M" && $this->fields[$fieldIndex]["M_Display"] == "N") {
				$inputType = "hidden";
			}
			// 查詢時如果是password則不顯示(不當查詢條件)
			if ($this->funcID == "F" && $this->fields[$fieldIndex]["inputType"] == "password") {
				$inputType = "hidden";
			}			
			// 查詢時將date改成text,才能輸入多種條件
			if ($this->funcID == "F" && ($inputType == "date" OR $inputType == "label")) {
				$inputType = "text";
			}
			
			switch ($inputType) {
				case 'hidden':
					$this->inputHidden($fieldIndex,$value,$colspan);
					break;
				case 'date':
					$this->inputDate($fieldIndex,$value,$colspan);
					break;					
				case 'text':
					$this->inputText($fieldIndex,$value,$colspan,'text');
					break;
				case 'password':
					$this->inputText($fieldIndex,$value,$colspan,'password');
					break;
				case 'label':
					$this->inputText($fieldIndex,$value,$colspan,'label');
					break;										
				case 'lov':
					$this->inputLov($fieldIndex,$value,$colspan);
					break;
				case 'love':
					$this->inputLove($fieldIndex,$value,$colspan);
					break;						
				case 'textarea':
					$this->inputTextArea($fieldIndex,$value,$colspan);
					break;
				case 'checkbox':
					$this->inputCheckbox($fieldIndex,$value,$colspan);
					break;
				case 'select':
					$this->inputSelect($fieldIndex,$value,$colspan);
					break;
				case 'radio':
					$this->inputRadio($fieldIndex,$value,$colspan);
					break;
				case 'combobox':
					$this->inputCombobox($fieldIndex,$value,$colspan);
					break;													
				default:
					$this->inputText($fieldIndex,$value,$colspan);
					break;
			}
		}
		
		// 判斷並返回是否為readonly
		private function isReadonly($fieldIndex) {
			$readonly = false;
			if ($this->funcID == "Q" || $this->funcID == "D") {
				$readonly = true;
			} elseif ($this->funcID == "A") {
				if ($this->fields[$fieldIndex]["A_CanEdit"] == "N") {
					$readonly = true;
				}	
			} elseif ($this->funcID == "M") {
				if ($this->fields[$fieldIndex]["M_CanEdit"] == "N") {
					$readonly = true;
				}					
			}
			return $readonly;		
		}
		
		// 判斷是否可編輯
		private function canEdit($fieldIndex) {
			$canEdit = true;
			if ($this->funcID == "Q" || $this->funcID == "D") {
				$canEdit = false;
			} elseif ($this->funcID == "A") {
				if ($this->fields[$fieldIndex]["A_CanEdit"] == "N") {
					$canEdit = false;
				}	
			} elseif ($this->funcID == "M") {
				if ($this->fields[$fieldIndex]["M_CanEdit"] == "N") {
					$canEdit = false;
				}
			}			
			return $canEdit;
		}

		// 判斷是否可編輯
		private function isDisabled($fieldIndex) {
			$disabled = false;
			if ($this->funcID == "Q" || $this->funcID == "D") {
				$disabled = true;
			} elseif ($this->funcID == "A") {
				if ($this->fields[$fieldIndex]["A_CanEdit"] == "N") {
					$disabled = true;
				}	
			} elseif ($this->funcID == "M") {
				if ($this->fields[$fieldIndex]["M_CanEdit"] == "N") {
					$disabled = true;
				}					
			}
			return $disabled;			
		}
		
		// 取預設值
		private function getDefaultValue($fieldIndex,$value) {
			// 找預設值及修改值
			if ($this->funcID == "A") {
				for ($z = 0; $z < count($this->DataFormKey[3]); $z += 3) { 
					if ($this->DataFormKey[3][$z] == $this->fields[$fieldIndex]["fieldName"]) {
						$value = $this->DataFormKey[3][$z+1];
					}
				}
				if ($value == "") {
					$value = $this->fields[$fieldIndex]["defaultValue"];
				}
			}
			return $value;
		}
		
		// 從POST讀入filter
		private function setFilterArray() {
			$this->filters = array();
			$prefix = ($this->DataFormKey[0] == "M" ? "m_" : "d_");
			if (isset($_POST[$prefix."filter"])) {
				if ($_POST[$prefix."filter"] != "") {
					$filterArray = explode("^A",$_POST[$prefix."filter"]);
					for ($i = 0; $i < count($filterArray); $i += 3) {	 
						$filter = array();
						$filter["fieldName"] = $filterArray[$i];
						$filter["filterValue"] = $filterArray[$i+1];
						$filter["isNumeric"] = $filterArray[$i+2];
						$this->filters[] = $filter;
					}
				}
			}
		}
		
		// 過濾條件用之運算子
		private function addFilter($fieldIndex) {
			// 找出之前的查詢條件
			$filterValue = "";
			for ($i = 0; $i < count($this->filters); $i++) { 
				if ($this->fields[$fieldIndex]["fieldName"] == $this->filters[$i]["fieldName"]) {
					$filterValue = $this->filters[$i]["filterValue"];
					break;
				}
			}
			$readonly = false;
			for ($z = 0; $z < count($this->DataFormKey[3]); $z += 3) { 
				if ($this->DataFormKey[3][$z] == $this->fields[$fieldIndex]["fieldName"]) {
					$readonly = true;
					$filterValue = $this->DataFormKey[3][$z+1];
					break;
				}
			}			
			return array($filterValue,$readonly);
		}
		
		// inputType = hidden
		private function inputHidden($fieldIndex,$value,$colspan) {
			// 找預設值及修改值
			$value = $this->getDefaultValue($fieldIndex,$value);
			$this->hiddenHTML .= $this->fields[$fieldIndex]["beforeHTML"];
			$this->hiddenHTML .= '<input type="hidden" name="'.$this->fields[$fieldIndex]["fieldName"].'" id="'.$this->fields[$fieldIndex]["fieldName"].'" '.$this->fields[$fieldIndex]["otherProperty"].' value="'.$value.'">';
			$this->hiddenHTML .= $this->fields[$fieldIndex]["afterHTML"];
		}
		
		// inputType = date
		private function inputDate($fieldIndex,$value,$colspan) {
			// 顯示欄位名稱
			$align = ($this->field_Title_Align != "") ? ' align="'.$this->field_Title_Align.'"' : '';
			$class = ($this->field_Title_Class != "") ? ' class="'.$this->field_Title_Class.'"' : '';										
			echo '<td'.$align.$class.'>'.$this->fields[$fieldIndex]["displayName"].'</td>';					
			
			$align = ($this->fields[$fieldIndex]["align"] != "") ? ' align="'.$this->fields[$fieldIndex]["align"].'"' : '';
			$class = ($this->fields[$fieldIndex]["class"] != "") ? ' class="'.$this->fields[$fieldIndex]["class"].'"' : '';	

			// 判斷是否為唯讀			
			$readonly = ($this->isReadonly($fieldIndex) ? ' readonly="true" ' : '');
			
			// 觸發日期開窗
			if ($this->canEdit($fieldIndex)) {
				echo '<script language="JavaScript" type="text/javascript">';
				echo '$(function() {';
				echo '$("#'.$this->fields[$fieldIndex]["fieldName"].'").datepicker({dateFormat: "yy-mm-dd",showAnim:"slideDown",changeMonth: true,changeYear: true});';		
				echo '});';
				echo '</script>';
			}
			// 找預設值及修改值
			$value = $this->getDefaultValue($fieldIndex,$value);
			
			echo '<td'.$colspan.$align.$class.'>';
			echo $this->fields[$fieldIndex]["beforeHTML"];
			// 查詢條件
			if ($this->funcID == "F") {
				list($value,$readonly) = $this->addFilter($fieldIndex);
				// 判斷是否為唯讀			
				$readonly = ($readonly ? ' readonly="true" ' : '');
			}
			//			
			echo '<input type="text" name="'.$this->fields[$fieldIndex]["fieldName"].'" id="'.$this->fields[$fieldIndex]["fieldName"].'" '.$this->fields[$fieldIndex]["otherProperty"].' '.$readonly.' value="'.$value.'" '.$class.'>';
			echo $this->fields[$fieldIndex]["afterHTML"];
			echo '</td>';							
		}
		
		// inputType = text || inputType = password || inputType = label
		private function inputText($fieldIndex,$value,$colspan,$inputType) {
			// 顯示欄位名稱
			$align = ($this->field_Title_Align != "") ? ' align="'.$this->field_Title_Align.'"' : '';
			$class = ($this->field_Title_Class != "") ? ' class="'.$this->field_Title_Class.'"' : '';										
			echo '<td'.$align.$class.'>'.$this->fields[$fieldIndex]["displayName"].'</td>';					
			
			$align = ($this->fields[$fieldIndex]["align"] != "") ? ' align="'.$this->fields[$fieldIndex]["align"].'"' : '';
			$class = ($this->fields[$fieldIndex]["class"] != "") ? ' class="'.$this->fields[$fieldIndex]["class"].'"' : '';	
			
			// 判斷是否為唯讀			
			$readonly = ($this->isReadonly($fieldIndex) ? ' readonly="true" ' : '');
			// 找預設值及修改值
			$value = $this->getDefaultValue($fieldIndex,$value);
						
			echo '<td'.$colspan.$align.$class.'>';
			echo $this->fields[$fieldIndex]["beforeHTML"];
			// 查詢條件
			if ($this->funcID == "F") {
				list($value,$readonly) = $this->addFilter($fieldIndex);
				// 判斷是否為唯讀			
				$readonly = ($readonly ? ' readonly="true" ' : '');
			} else {
				// 密碼型態其他請況讀取後要解密
				if ($inputType == "password" && $this->funcID != "A") {
					$value = encrypt($value,"D");
				}
			}
			//
			if ($inputType == "password") {
				echo '<input type="password" name="'.$this->fields[$fieldIndex]["fieldName"].'" id="'.$this->fields[$fieldIndex]["fieldName"].'" '.$this->fields[$fieldIndex]["otherProperty"].' '.$readonly.' value="'.$value.'" '.$class.'>';				
			} elseif ($inputType == "label") {
				$style = "";
				if ($readonly != '') { $style = ' style="background:rgba(0,0,0,0);border:none;"'; }
				echo '<input type="text" name="'.$this->fields[$fieldIndex]["fieldName"].'" id="'.$this->fields[$fieldIndex]["fieldName"].'" '.$this->fields[$fieldIndex]["otherProperty"].' '.$readonly.' value="'.$value.'" '.$class.' '.$style.'>';				
			} else {			
				echo '<input type="text" name="'.$this->fields[$fieldIndex]["fieldName"].'" id="'.$this->fields[$fieldIndex]["fieldName"].'" '.$this->fields[$fieldIndex]["otherProperty"].' '.$readonly.' value="'.$value.'" '.$class.'>';
			}
			echo $this->fields[$fieldIndex]["afterHTML"];
			echo '</td>';							
		}

		// inputType = lov (List Of Value)
		private function inputLov($fieldIndex,$value,$colspan) {
			// 顯示欄位名稱
			$align = ($this->field_Title_Align != "") ? ' align="'.$this->field_Title_Align.'"' : '';
			$class = ($this->field_Title_Class != "") ? ' class="'.$this->field_Title_Class.'"' : '';										
			echo '<td'.$align.$class.'>'.$this->fields[$fieldIndex]["displayName"].'</td>';					
			
			$align = ($this->fields[$fieldIndex]["align"] != "") ? ' align="'.$this->fields[$fieldIndex]["align"].'"' : '';
			$class = ($this->fields[$fieldIndex]["class"] != "") ? ' class="'.$this->fields[$fieldIndex]["class"].'"' : '';	
			
			// 判斷是否為唯讀			
			$readonly = ($this->isReadonly($fieldIndex) ? ' readonly="true" ' : '');

			// 找預設值及修改值
			$value = $this->getDefaultValue($fieldIndex,$value);
						
			echo '<td'.$colspan.$align.$class.'>';
			echo $this->fields[$fieldIndex]["beforeHTML"];
			// 查詢條件
			if ($this->funcID == "F") {
				list($value,$readonly) = $this->addFilter($fieldIndex);
				// 判斷是否為唯讀			
				$readonly = ($readonly ? ' readonly="true" ' : '');
			}
			//			
			echo '<input type="text" name="'.$this->fields[$fieldIndex]["fieldName"].'" id="'.$this->fields[$fieldIndex]["fieldName"].'" '.$this->fields[$fieldIndex]["otherProperty"].' '.$readonly.' value="'.$value.'" '.$class.'>';
			if (!($readonly || $this->funcID == "F")) {
				$item = $this->fields[$fieldIndex]["item"][0];
				$jsonID = $this->fields[$fieldIndex]["item"][1];
				echo '<button type="button" class="btn btn-default btn-sm" onclick="openLOV(\''.$this->DataFormKey[0].'\',\''.$jsonID.'\');">';
				echo '<span class="glyphicon glyphicon-search" aria-hidden="true"></span>';
				echo '</button>';				
			}
			echo $this->fields[$fieldIndex]["afterHTML"];
			echo '</td>';							
		}

		// inputType = lov (List Of Value)
		private function inputLove($fieldIndex,$value,$colspan) {
			// 顯示欄位名稱
			$align = ($this->field_Title_Align != "") ? ' align="'.$this->field_Title_Align.'"' : '';
			$class = ($this->field_Title_Class != "") ? ' class="'.$this->field_Title_Class.'"' : '';										
			echo '<td'.$align.$class.'>'.$this->fields[$fieldIndex]["displayName"].'</td>';					
			$align = ($this->fields[$fieldIndex]["align"] != "") ? ' align="'.$this->fields[$fieldIndex]["align"].'"' : '';
			$class = ($this->fields[$fieldIndex]["class"] != "") ? ' class="'.$this->fields[$fieldIndex]["class"].'"' : '';	
			// 判斷是否為唯讀			
			$readonly = ($this->isReadonly($fieldIndex) ? ' readonly="true" ' : '');

			// 找預設值及修改值
			$value = $this->getDefaultValue($fieldIndex,$value);
						
			echo '<td'.$colspan.$align.$class.'>';
			echo $this->fields[$fieldIndex]["beforeHTML"];
			// 查詢條件
			if ($this->funcID == "F") {
				list($value,$readonly) = $this->addFilter($fieldIndex);
				// 判斷是否為唯讀			
				$readonly = ($readonly ? ' readonly="true" ' : '');
			}
			//			
			echo '<input type="text" name="'.$this->fields[$fieldIndex]["fieldName"].'" id="'.$this->fields[$fieldIndex]["fieldName"].'" '.$this->fields[$fieldIndex]["otherProperty"].' '.$readonly.' value="'.$value.'">';
			if (!($this->funcID == "F")) {
				$item = $this->fields[$fieldIndex]["item"][0];
				$in = "";
				$out = "";
                                $code = "";
				for ($i = 0; $i < count($item); $i += 2) {
					if ($item[$i+1] == "in") {
						$in .= ($in == "" ? "" : ",").$item[$i];
					} else if($item[$i+1] == "out") {
						$out .= ($out == "" ? "" : ",").$item[$i];						
                                        }else{$code .= ($code == "" ? "" : ",").$item[$i];                                          }
				}
				echo '<Button type="Button" id="SearchBut" class="btn btn-default btn-sm" onclick="openLOVE(\''.$this->DataFormKey[0].'\',\''.$in.'\',\''.$out.'\',\''.$code.'\');">';
				echo '<span class="glyphicon glyphicon-search" aria-hidden="true"></span>';
				echo '</button>';	
			}
			echo $this->fields[$fieldIndex]["afterHTML"];
			echo '</td>';							
		}

		// inputType = textarea
		private function inputTextArea($fieldIndex,$value,$colspan) {
			// 顯示欄位名稱
			$align = ($this->field_Title_Align != "") ? ' align="'.$this->field_Title_Align.'"' : '';
			$class = ($this->field_Title_Class != "") ? ' class="'.$this->field_Title_Class.'"' : '';										
			echo '<td'.$align.$class.'>'.$this->fields[$fieldIndex]["displayName"].'</td>';					
			
			$align = ($this->fields[$fieldIndex]["align"] != "") ? ' align="'.$this->fields[$fieldIndex]["align"].'"' : '';
			$class = ($this->fields[$fieldIndex]["class"] != "") ? ' class="'.$this->fields[$fieldIndex]["class"].'"' : '';	
			
			// 判斷是否為唯讀			
			$readonly = ($this->isReadonly($fieldIndex) ? ' readonly="true" ' : '');
			// 找預設值及修改值
			$value = $this->getDefaultValue($fieldIndex,$value);
						
			echo '<td'.$colspan.$align.$class.'>';
			echo $this->fields[$fieldIndex]["beforeHTML"];
			$rows = (isset($this->fields[$fieldIndex]["item"][0][0]) ? $this->fields[$fieldIndex]["item"][0][0] : 1);
			$cols = (isset($this->fields[$fieldIndex]["item"][0][1]) ? $this->fields[$fieldIndex]["item"][0][1] : 10);
			// 查詢條件
			if ($this->funcID == "F") {
				list($value,$readonly) = $this->addFilter($fieldIndex);
				// 判斷是否為唯讀			
				$readonly = ($readonly ? ' readonly="true" ' : '');
			}
			//			
			echo '<textarea name="'.$this->fields[$fieldIndex]["fieldName"].'" id="'.$this->fields[$fieldIndex]["fieldName"].'" '.$this->fields[$fieldIndex]["otherProperty"].' '.$readonly.' rows="'.$rows.'" cols="'.$cols.'" '.$class.'>'.$value.'</textarea>';
			echo $this->fields[$fieldIndex]["afterHTML"];
			echo '</td>';							
		}
				
		// inputType = combobox
		private function inputCombobox($fieldIndex,$value,$colspan) {
			// 顯示欄位名稱
			$align = ($this->field_Title_Align != "") ? ' align="'.$this->field_Title_Align.'"' : '';
			$class = ($this->field_Title_Class != "") ? ' class="'.$this->field_Title_Class.'"' : '';										
			echo '<td'.$align.$class.'>'.$this->fields[$fieldIndex]["displayName"].'</td>';					
			
			
			$align = ($this->fields[$fieldIndex]["align"] != "") ? ' align="'.$this->fields[$fieldIndex]["align"].'"' : '';
			$class = ($this->fields[$fieldIndex]["class"] != "") ? ' class="'.$this->fields[$fieldIndex]["class"].'"' : '';	
			
			// 判斷是否為唯讀			
			$readonly = ($this->isReadonly($fieldIndex) ? ' readonly="true" ' : '');
			// 判斷是否可修改
			$canEdit = $this->canEdit($fieldIndex);			
			// 找預設值及修改值
			$value = $this->getDefaultValue($fieldIndex,$value);
						
			echo '<td'.$colspan.$align.$class.'>';
			echo $this->fields[$fieldIndex]["beforeHTML"];
			// 查詢條件
			if ($this->funcID == "F") {
				list($value,$readonly) = $this->addFilter($fieldIndex);
				// 判斷是否為唯讀			
				$readonly = ($readonly ? ' readonly="true" ' : '');
			}
			//			
			echo '<input type="text" list="list_'.$this->fields[$fieldIndex]["fieldName"].'" name="'.$this->fields[$fieldIndex]["fieldName"].'" id="'.$this->fields[$fieldIndex]["fieldName"].'" '.$this->fields[$fieldIndex]["otherProperty"].' '.$readonly.' value="'.$value.'" '.$class.'>';
			
			// item = array(自訂array,from SQL的array)
			echo '<datalist id="list_'.$this->fields[$fieldIndex]["fieldName"].'" name="list_'.$this->fields[$fieldIndex]["fieldName"].'">';
			if (count($this->fields[$fieldIndex]["item"][0]) > 0) {
				for ($i = 0; $i < count($this->fields[$fieldIndex]["item"][0]); $i += 2) {
					if ($canEdit) {
						echo '<option value="'.$this->fields[$fieldIndex]["item"][0][$i+1].'" '.($this->fields[$fieldIndex]["item"][0][$i+1] === $value ? " selected" : "").'>'.$this->fields[$fieldIndex]["item"][0][$i].'</option>';
					} else {
						if ($this->fields[$fieldIndex]["item"][0][$i+1] == $value) {
							echo '<option value="'.$this->fields[$fieldIndex]["item"][0][$i+1].'" '.($this->fields[$fieldIndex]["item"][0][$i+1] === $value ? " selected" : "").'>'.$this->fields[$fieldIndex]["item"][0][$i].'</option>';
							break;
						}
					}
				}
			}
			
			if ($this->fields[$fieldIndex]["item"][1] != "") {
				$rows = mysql_query($this->fields[$fieldIndex]["item"][1]);
				while ($row = mysql_fetch_array($rows)) {
					if ($canEdit) {
						echo '<option value="'.$row[1].'" '.($row[1] === $value ? " selected" : "").'>'.$row[0].'</option>';
					} else {
						if ($row[1] == $value) {
							echo '<option value="'.$row[1].'" '.($row[1] === $value ? " selected" : "").'>'.$row[0].'</option>';
							break;
						}						
					}
				}
			}
			echo '</datalist>';
			//
			echo $this->fields[$fieldIndex]["afterHTML"];
			echo '</td>';							
		}
				
		// inputType = checkbox
		private function inputCheckbox($fieldIndex,$value,$colspan) {
			// 顯示欄位名稱
			$align = ($this->field_Title_Align != "") ? ' align="'.$this->field_Title_Align.'"' : '';
			$class = ($this->field_Title_Class != "") ? ' class="'.$this->field_Title_Class.'"' : '';										
			echo '<td'.$align.$class.'>'.$this->fields[$fieldIndex]["displayName"].'</td>';					
			
			$align = ($this->fields[$fieldIndex]["align"] != "") ? ' align="'.$this->fields[$fieldIndex]["align"].'"' : '';
			$class = ($this->fields[$fieldIndex]["class"] != "") ? ' class="'.$this->fields[$fieldIndex]["class"].'"' : '';	
			
			// 判斷是否需disabled			
			$disabled = ($this->isDisabled($fieldIndex) ? ' disabled="true" ' : '');			
			// 找預設值及修改值
			$value = $this->getDefaultValue($fieldIndex,$value);
			
			echo '<td'.$colspan.$align.$class.'>';
			echo $this->fields[$fieldIndex]["beforeHTML"];
			// 查詢條件
			if ($this->funcID == "F") {
				list($value,$disabled) = $this->addFilter($fieldIndex);
				// 判斷是否需disabled			
				$disabled = ($disabled ? ' disabled="true" ' : '');			
			}
			//			
			// item = array(自訂array,from SQL的array)
			if (count($this->fields[$fieldIndex]["item"][0]) > 0) {
				for ($i = 0; $i < count($this->fields[$fieldIndex]["item"][0]); $i += 2) {
					echo '<input type="checkbox" id="'.$this->fields[$fieldIndex]["fieldName"].'" name="'.$this->fields[$fieldIndex]["fieldName"].'[]" value="'.$this->fields[$fieldIndex]["item"][0][$i+1].'" '.($this->fields[$fieldIndex]["item"][0][$i+1] === $value ? " checked" : "").' '.$disabled.'>'.$this->fields[$fieldIndex]["item"][0][$i];
				}
			}
			
			if ($this->fields[$fieldIndex]["item"][1] != "") {
				$rows = mysql_query($this->fields[$fieldIndex]["item"][1]);
				while ($row = mysql_fetch_array($rows)) {
					echo '<input type="checkbox" id="'.$row[0].'" name="'.$row[0].'" value="'.$row[1].'" '.($row[1] === $value ? " checked" : "").' '.$disabled.'>'.$row[0];
				}
			}
			echo $this->fields[$fieldIndex]["afterHTML"];
			echo '</td>';			
		}		

		// inputType = select
		private function inputSelect($fieldIndex,$value,$colspan) {
			// 顯示欄位名稱
			$align = ($this->field_Title_Align != "") ? ' align="'.$this->field_Title_Align.'"' : '';
			$class = ($this->field_Title_Class != "") ? ' class="'.$this->field_Title_Class.'"' : '';										
			echo '<td'.$align.$class.'>'.$this->fields[$fieldIndex]["displayName"].'</td>';					
			
			$align = ($this->fields[$fieldIndex]["align"] != "") ? ' align="'.$this->fields[$fieldIndex]["align"].'"' : '';
			$class = ($this->fields[$fieldIndex]["class"] != "") ? ' class="'.$this->fields[$fieldIndex]["class"].'"' : '';	
			
			// 判斷是否可修改
			$canEdit = $this->canEdit($fieldIndex);
			// 找預設值及修改值
			$value = $this->getDefaultValue($fieldIndex,$value);
			
			echo '<td'.$colspan.$align.$class.'>';
			echo $this->fields[$fieldIndex]["beforeHTML"];
			// 查詢條件
			if ($this->funcID == "F") {
				list($value,$readonly) = $this->addFilter($fieldIndex);
				// 判斷是否需disabled			
				$canEdit = ($readonly ? false : true);			
			}
			//			
			// item = array(自訂array,from SQL的array)
			echo '<select id="'.$this->fields[$fieldIndex]["fieldName"].'" name="'.$this->fields[$fieldIndex]["fieldName"].'" '.$class.'>';
			// 作查詢時,加上"不篩選":""當作條件之一
			if ($this->funcID == "F") {
				echo '<option value="" '.($value === "" ? " selected" : "").'>不篩選</option>';				
			}
			//
			if (count($this->fields[$fieldIndex]["item"][0]) > 0) {
				for ($i = 0; $i < count($this->fields[$fieldIndex]["item"][0]); $i += 2) {
					if ($canEdit) {
						echo '<option value="'.$this->fields[$fieldIndex]["item"][0][$i+1].'" '.($this->fields[$fieldIndex]["item"][0][$i+1] === $value ? " selected" : "").'>'.$this->fields[$fieldIndex]["item"][0][$i].'</option>';
					} else {
						if ($this->fields[$fieldIndex]["item"][0][$i+1] === $value) {
							echo '<option value="'.$this->fields[$fieldIndex]["item"][0][$i+1].'" '.($this->fields[$fieldIndex]["item"][0][$i+1] === $value ? " selected" : "").'>'.$this->fields[$fieldIndex]["item"][0][$i].'</option>';
							break;
						}
					}
				}
			}
			
			if ($this->fields[$fieldIndex]["item"][1] != "") {
				$rows = mysql_query($this->fields[$fieldIndex]["item"][1]);
				while ($row = mysql_fetch_array($rows)) {
					if ($canEdit) {
						echo '<option value="'.$row[1].'" '.($row[1] === $value ? " selected" : "").'>'.$row[0].'</option>';
					} else {
						if ($row[1] === $value) {
							echo '<option value="'.$row[1].'" '.($row[1] === $value ? " selected" : "").'>'.$row[0].'</option>';
							break;
						}						
					}
				}
			}
			echo '</select>';
			echo $this->fields[$fieldIndex]["afterHTML"];
			echo '</td>';			
		}
		
		// inputType = radio
		private function inputRadio($fieldIndex,$value,$colspan) {
			// 顯示欄位名稱
			$align = ($this->field_Title_Align != "") ? ' align="'.$this->field_Title_Align.'"' : '';
			$class = ($this->field_Title_Class != "") ? ' class="'.$this->field_Title_Class.'"' : '';										
			echo '<td'.$align.$class.'>'.$this->fields[$fieldIndex]["displayName"].'</td>';					
			
			$align = ($this->fields[$fieldIndex]["align"] != "") ? ' align="'.$this->fields[$fieldIndex]["align"].'"' : '';
			$class = ($this->fields[$fieldIndex]["class"] != "") ? ' class="'.$this->fields[$fieldIndex]["class"].'"' : '';	
			
			// 判斷是否為唯讀			
			$readonly = $this->isReadonly($fieldIndex);
			$event = "";
			if ($readonly) {
				// 以JavaScript來控制
				$event = " onclick=\"readonlyRadioButton('".$this->prefix."','".$this->fields[$fieldIndex]["fieldName"]."',".($this->fields[$fieldIndex]["isNumeric"] == "Y" ? "" : "'").$value.($this->fields[$fieldIndex]["isNumeric"] == "Y" ? "" : "'").")\"";
			}
			// 找預設值及修改值
			$value = $this->getDefaultValue($fieldIndex,$value);
			
			echo '<td'.$colspan.$align.$class.'>';
			echo $this->fields[$fieldIndex]["beforeHTML"];
			// 查詢條件
			if ($this->funcID == "F") {
				list($value,$readonly) = $this->addFilter($fieldIndex);
				// 判斷是否需disabled			
				if ($readonly) {
					// 以JavaScript來控制
					$event = " onclick=\"readonlyRadioButton('".$this->prefix."','".$this->fields[$fieldIndex]["fieldName"]."',".($this->fields[$fieldIndex]["isNumeric"] == "Y" ? "" : "'").$value.($this->fields[$fieldIndex]["isNumeric"] == "Y" ? "" : "'").")\"";
				} else {
					$event = "";
				}
			}
			// 作查詢時,加上"不篩選":""當作條件之一
			if ($this->funcID == "F") {
				echo '<input type="radio" id="'.$this->fields[$fieldIndex]["fieldName"].'" name="'.$this->fields[$fieldIndex]["fieldName"].'" value="" '.($value === "" ? " checked=\"true\" " : "").$event.'>不篩選';
			}
						
			// item = array(自訂array,from SQL的array)
			if (count($this->fields[$fieldIndex]["item"][0]) > 0) {
				for ($i = 0; $i < count($this->fields[$fieldIndex]["item"][0]); $i += 2) {
					echo '<input type="radio" id="'.$this->fields[$fieldIndex]["fieldName"].'" name="'.$this->fields[$fieldIndex]["fieldName"].'" value="'.$this->fields[$fieldIndex]["item"][0][$i+1].'" '.($this->fields[$fieldIndex]["item"][0][$i+1] === $value ? " checked=\"true\" " : "").$event.'>'.$this->fields[$fieldIndex]["item"][0][$i];
				}
			}
			
			if ($this->fields[$fieldIndex]["item"][1] != "") {
				$rows = mysql_query($this->fields[$fieldIndex]["item"][1]);
				while ($row = mysql_fetch_array($rows)) {
					echo '<input type="radio" id="'.$this->fields[$fieldIndex]["fieldName"].'" name="'.$this->fields[$fieldIndex]["fieldName"].'" value="'.$row[1].'" '.($row[1] === $value ? " checked=\"true\" " : "").$event.'>'.$row[1];
				}
			}
			
			echo $this->fields[$fieldIndex]["afterHTML"];
			echo '</td>';			
		}
		
		// 除主table資料外,其他在刪除前需檢查之資料
		function addBeforeDeleteCheck($table,$where,$errormessage) {
			if ($table != "" && count($where) > 0 && (count($where) % 2 == 0)) {
				$beforeDelete = array();
				$beforeDelete["table"] = $table;
				$beforeDelete["where"] = $where;	//array($table的欄位,$this->viewTableName的欄位)
				$beforeDelete["errormessage"] = $errormessage;
				$this->beforeDeleteCheck[] = $beforeDelete;
			}
		}		
		
		// 除主table資料外,其他需連動刪除之資料
		function addOtherDelete($table,$where) {
			if ($table != "" && count($where) > 0 && (count($where) % 2 == 0)) {
				$otherDelete = array();
				$otherDelete["table"] = $table;
				$otherDelete["where"] = $where;		//array($table的欄位,$this->viewTableName的欄位)
				$this->otherDelete[] = $otherDelete;
			}
		}
		
		// 改變AutoData之按鈕背景色
		private function setAutoDataButtonBackgroundColor() {
			$prefix = ($this->DataFormKey[0] == "M" ? "m_" : "d_");
			$id = "";
			if ($this->funcID == "F") {
				$id = $prefix."queryButton";
			} elseif ($this->funcID == "A") {
				$id = $prefix."insertButton";
			} elseif ($this->funcID == "M") {
				$id = $prefix."updateButton";
			} elseif ($this->funcID == "D") {
				$id = $prefix."deleteButton";
			}
			echo "<script language=\"JavaScript\">selectButton('".$this->DataFormKey[0]."','".$id."');</script>";
		}
		
		// 儲存時自動給值
		private function autoValue($fieldIndex,$value) {
			$autoValue = $this->fields[$fieldIndex]["autoValue"];
			if (!is_null($autoValue) && count($autoValue) == 3) {
				$autoType = strtolower($autoValue[0]);
				$changeValue = false;
				// 判斷是否套用自動給值
				if ($this->funcID == "AW" && strtoupper($autoValue[1]) == "Y") { $changeValue = true; }
				if ($this->funcID == "MW" && strtoupper($autoValue[2]) == "Y") { $changeValue = true; }
				if ($autoType != "") {
					if ($changeValue) {
						switch ($autoType) {
							case 'date':
								// 自動給日期
								return date("Y-m-d");
								break;							
							case 'datetime':
								// 自動給日期時間
								return date("Y-m-d H:i:s");
								break;
							case 'loginuser':
								// 自動給登入者
								return $_SESSION["manageuser"];
								break;
							case 'autoserial':
								// 自動編號(西元年後2碼+月份2碼+流水號4碼)
								$serial = date("ym");
								$strSQL = "SELECT MAX(".$this->fields[$fieldIndex]["fieldName"].") FROM ".$this->viewTableName." WHERE CommID='".$_SESSION["Community"]."'";
								$rows = mysql_query($strSQL);
								if (mysql_num_rows($rows) > 0) {
									$row = mysql_fetch_array($rows);
									if (substr($row[0],0,4) == $serial) {
										// 不換年月最大號+1
										$serial = $row[0] + 1;
									} else {
										// 換年月重新給號
										$serial .= "0001";
									}
								} else {
									$serial .= "0001";
								}
								return $serial;
								break;
							default:
								break;
						}
					}
				}
			}
			return $value;
		}

		// 額外執行之StoreProcedure
		private function execOtherSP($funcID) {
			$message = "";
			$spArray = $this->afterWriteToDataBase;
			for ($i = 0; $i < count($spArray); $i++) {
				$funcArray = $spArray[$i]["funcArray"];
				$execFunc = false;
				for ($j = 0; $j < count($funcArray); $j++) { 
					if ($funcArray[$j] == $funcID) {
						$execFunc = true;
						break;
					}
				}
				if ($execFunc == true) {
					$strSQL = "CALL ".$spArray[$i]["spName"]."(";
					$paramArray = $spArray[$i]["paramArray"];
					for ($j = 0; $j < count($paramArray); $j++) {
						$isNumeric = "N";
						for ($k = 0; $k < count($this->fields); $k++) { 
							if ($paramArray[$j] == $this->fields[$k]["fieldName"]) {
								$isNumeric = $this->fields[$k]["isNumeric"];
								break;
							}
						} 
						$strSQL .= ($j == 0 ? "" : ",").($isNumeric == "N" ? "'" : "").$_POST[$paramArray[$j]].($isNumeric == "N" ? "'" : "");
					}
					$strSQL .= ")";
					
					$success = mysql_query($strSQL);
					if (!$success) {
						$message = "儲存時發生錯誤,請檢查後再執行 !".chr(13).chr(13);
						global $mysql_link;
						$message .= mysql_errno($mysql_link).": ".mysql_error($mysql_link).chr(13);								
					}
				}
			}
			return $message;
		}
		
		// 檢查是否為日期欄位
		private function isDate($fieldName) {
			$strSQL = "SELECT DATA_TYPE FROM INFORMATION_SCHEMA.COLUMNS WHERE table_schema='icms' AND table_name='".$this->tableName."' AND column_name='".$fieldName."'";
			$rows = mysql_query($strSQL);
			$row = mysql_fetch_array($rows);
			if ($row[0] == "date" || $row[0] == "datetime") {
				return true;
			} else {
				return false;
			}
		}
				
		// 產生新增修改表格
		function showData() {
			// 增加execButton(儲存...)
			$this->addExecButton();
			//
			$err = "";
			$formTable = ($this->DataFormKey[0] == "M" ? "m_" : "d_")."FormTable";
			$formTableDisplay = $formTable."Display";
			$formTableImage = $formTable."Image";
			if ($this->funcID == "F" || $this->funcID == "Q" || $this->funcID == "A" || $this->funcID == "M" || $this->funcID == "D") {
				// 改變AutoData之按鈕背景色
				$this->setAutoDataButtonBackgroundColor();
				//
				if ($this->funcID != "A" && $this->funcID != "F") {
					$where = ($this->keyToWhere() != "" ? " WHERE ".$this->keyToWhere() : "");
					$strSQL = "SELECT COUNT(*) FROM ".$this->viewTableName.$where;
					$rows = mysql_query($strSQL);
					list($i) = mysql_fetch_row($rows);
					if ($i == 0) {
						$err = "資料錯誤或不存在,請檢查後再執行 !";
					} else {
					// 讀取該筆資料
						$where = ($this->keyToWhere() != "" ? " WHERE ".$this->keyToWhere() : "");				
						$strSQL = "SELECT * FROM ".$this->viewTableName.$where;
						$rows = mysql_query($strSQL);
						$row = mysql_fetch_array($rows);						
					}
				}
				list($fW,$fS,$fSS) = $this->setAMDW();
							
				if ($err != "") {
					echo $err;
				} else {
					// 設定每列幾個欄位
					$this->setPerRowCols();
					echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8">';
					// 在form之前插入程式碼,非查詢(F)時才生效
					if ($this->funcID != "F") {
						if ($this->beforeForm["isIncludeFile"] == "Y") {
							include_once($this->beforeForm["fileNameOrHtmlString"]);
						} else {
							echo $this->beforeForm["fileNameOrHtmlString"];
						}
					}
					// 產生表格
					$formName = ($this->DataFormKey[0] == "M" ? "MasterForm" : "DetailForm");
					//echo '<form action="'.$this->DataFormKey[2].'" name="'.$formName.'" id="'.$formName.'" method="POST">';
					$action = ($this->funcID != "Q" ? 'javascript:'.$this->buttons[1]["event"] : '');
					echo '<form action="'.$action.'" name="'.$formName.'" id="'.$formName.'" method="POST">';					
					// 設定表格
					$align = ($this->table_Align != "") ? ' align="'.$this->table_Align.'"' : '';
					echo '<table id="'.$formName.'MainTable" '.$align.' class="table table-bordered table-condensed table-width-90 table-margin-0 "'.$this->table_Class.'>';		
					// 設定表格名稱
					if ($this->table_Title != "" || count($this->buttons) > 0) {
						$align = ($this->table_Title_Align != "") ? ' align="'.$this->table_Title_Align.'"' : '';
						$class = ($this->table_Title_Class != "") ? ' class="'.$this->table_Title_Class.'"' : '';						
						echo '<tr '.$class.'>';
						echo '<td colspan="'.$this->perRowCols.'">';
						echo '<table class="table-condensed table-width-100 table-margin-0">';
						echo '<tr>';
						echo '<td width="100%" '.$align.$class.'><img id="'.$formTableImage.'" src="../images/icon/NT-Collapse.gif" onclick="foldTable(\''.$formTable.'\',\''.$formTableDisplay.'\',\''.$formTableImage.'\');"><font color="red">'.$fS.'</font>'.$this->table_Title;
						echo '<input name="f" id="f" value="'.$fW.'" type="HIDDEN">';
						$f_id = ($this->DataFormKey[0] == "M" ? "m_f" : "d_f");
						echo '<input name="'.$f_id.'" id="'.$f_id.'" value="'.$fW.'" type="HIDDEN">';						
						echo '<input name="id" id="id" value="'.$this->DataFormKey[4].'" type="HIDDEN">';
						echo '</td>';
						// 按鈕
						$align = ($this->buttons_Align != "") ? ' align="'.$this->buttons_Align.'"' : '';				
						echo '<td nowrap>';
						if ($this->funcID != "Q") {
							// 刪除時應檢查是否已被使用
							if ($this->funcID == "D") {
								for ($i = 0; $i < count($this->beforeDeleteCheck); $i++) {
									if ($err == "") {
										$where = ""; 
										for ($j = 0; $j < count($this->beforeDeleteCheck[$i]["where"]); $j += 2) { 
											for ($k = 0; $k < count($this->fields); $k++) {
												if ($this->fields[$k]["fieldName"] == $this->beforeDeleteCheck[$i]["where"][$j + 1]) {
													$where .= ($j != 0 ? " AND " : "").$this->beforeDeleteCheck[$i]["where"][$j]."=".($this->fields[$k]["isNumeric"] == "N" ? "'" : "").$row[$this->fields[$k]["fieldName"]].($this->fields[$k]["isNumeric"] == "N" ? "'" : "");
													break;
												}
											}
										}
										if ($where != "") {
											$strSQL = "SELECT COUNT(*) FROM ".$this->beforeDeleteCheck[$i]["table"]." WHERE ".$where;
											$countRows = mysql_query($strSQL);
											list($count) = mysql_fetch_row($countRows);
											if ($count > 0) {
												$err = $this->beforeDeleteCheck[$i]["errormessage"];
											}
										}
									}
								}				
							}
						}
						if ($err == "") {
							for ($i = 0; $i < count($this->buttons); $i++) {
								$value = $this->buttons[$i]["name"];
								$class = ($this->buttons[$i]["class"] != "") ? ' class="'.$this->buttons[$i]["class"].'"' : '';
								$event = ($this->buttons[$i]["event"] != "") ? ' '.$this->buttons[$i]["event"] : '';					
								$id = ($this->buttons[$i]["id"] != "") ? ' id="'.$this->buttons[$i]["id"].'"' : '';
								if ($i == 0) {
									// 預設按鈕:form的reset
									if ($this->funcID == "F") {
										if ($this->execButtonDisplay == "Y") {
											echo '<button type="button" '.$id.' '.$class.' value="" '.$event.'>重置</button>';											
										}
									}									
								} elseif ($i == 1) {
									// 預設按鈕:form的submit
									if ($this->funcID != "Q") {
										if ($this->execButtonDisplay == "Y") {
											echo '<button type="submit" '.$id.' '.$class.' value="">'.$fSS.'</button>';											
										}
									}
								} else {
									// 其他按鈕
									echo '<button type="button" '.$id.' '.$class.' value="" '.$event.'>'.$value.'</button>';
								}
							}
						}				
						echo '</td>';
						echo '</tr>';
						echo '</table>';
						echo '</td>';
						echo '</tr>';
					}
					
					// 控制內容是否顯示
					echo '<tr><td><div id="'.$formTable.'" style="display:;"><table class="table table-bordered table-condensed table-margin-0">';
					//	
					// 欄位及值
					$j = 0;
					$k = $this->perRowCols;
					for ($i = 0; $i < count($this->fields); $i++) {
						// 當變成隱藏欄位時,不需再作折行等判斷
						if ($this->fields[$i]["inputType"] == "hidden" || ($this->funcID == "A" && $this->fields[$i]["A_Display"] == "N") || ($this->funcID == "M" && $this->fields[$i]["M_Display"] == "N")) {
							// 依inputType呼叫對應之function => 此段會是隱藏欄位
							$this->selectInputType($i,($this->funcID != "A" && $this->funcID != "F" ? $row[$this->fields[$i]["fieldName"]] : ""),"");							
						} else {							
							if ($j == 0) {	echo '<tr>'; }						
							// 判斷是否折行
							$colspan = "";
							if ($this->fields[$i]["wrap"] == "Y") {
								$l = ($k - $j) - 1;
								if ($l > 1) {
									$colspan = ' colspan="'.$l.'"';
								}
								$j = $j + $l + 1;
							} else {
								$j += 2;
							}
							// 依inputType呼叫對應之function
							$this->selectInputType($i,($this->funcID != "A" && $this->funcID != "F" ? $row[$this->fields[$i]["fieldName"]] : ""),$colspan);
							//
							if ($j == $k) { echo '</tr>'; $j = 0; }
						}
					}
					// 表格底部顯示訊息
					if ($err != "") {
						echo '<tr>';
						echo '<td align="center" class="font-16-bold" colspan="'.$this->perRowCols.'"><font color="red">'.$err.'</font></td>';
						echo '</tr>';
					}
					// 控制內容是否顯示
					echo '</table></div></td></tr>';
					//
					echo '</table>';
					// 顯示隱藏input欄位
					echo $this->hiddenHTML;
					echo '</form>';
					echo '<input id="'.$formTableDisplay.'" name="'.$formTableDisplay.'" value="Y" type="HIDDEN">';
					// 在form之後插入程式碼,非查詢時才生效
					if ($this->funcID != "F") {
						if ($this->afterForm["isIncludeFile"] == "Y") {
							include_once($this->afterForm["fileNameOrHtmlString"]);
						} else {
							echo $this->afterForm["fileNameOrHtmlString"];
						}
					}
				}
			}
			
			//WHERE條件值
			if ($this->funcID == "AW") {
				$isset = true;
				for ($i = 0; $i < count($this->tableKey); $i++) {
					if (!isset($_POST[$this->tableKey[$i]])) {
						$isset = false;
						break;
					}
				}
				if ($isset) {
					$key = "";
					for ($i = 0; $i < count($this->tableKey); $i++) {
						$isNumeric = "";
						for ($j = 0; $j < count($this->fields); $j++) { 
							if ($this->fields[$j]["fieldName"] == $this->tableKey[$i]) {
								$isNumeric = $this->fields[$j]["isNumeric"];
								break;
							}
						}
						$key .= ($i == 0 ? "" : "^A").$this->tableKey[$i]."^A".$_POST[$this->tableKey[$i]]."^A".$isNumeric;
					}
					$this->DataFormKey[4] = $key;
					$this->DataFormKey[3] = ($key == "" ? array() : explode("^A",$key));
				}
			}
			$where = ($this->keyToWhere() != "" ? " WHERE ".$this->keyToWhere() : "");

			//查詢條件
			if ($this->funcID == "FW") {
				// 將查詢條件存入隱藏欄位,再由AutoDataClass讀取成where的過濾條件
				$filterString = "";
				$j = 0;
				for ($i = 0; $i < count($this->fields); $i++) {
					$field = $this->fields[$i];
					$fieldName = $field["fieldName"];
					$isNumeric = $field["isNumeric"];
					if (isset($_POST[$fieldName])) {
						if ($_POST[$fieldName] != "") {
							if (is_array($_POST[$fieldName])) {
								// 如果是array,則用|作為分隔符號
								$filterString .= ($j == 0 ? "" : "^A").$fieldName."^A".implode("|",$_POST[$fieldName])."^A".$isNumeric;
							} else {
								$filterString .= ($j == 0 ? "" : "^A").$fieldName."^A".$_POST[$fieldName]."^A".$isNumeric;
							}
							$j++;
						}
					}
				}
				$err = $filterString;
				$this->funcID = "L";
			}

			//刪除資料
			if ($this->funcID == "DW") {
				if ($err == "") {
					// 起始交易
					$strSQL = "BEGIN";
					$success = mysql_query($strSQL);
					if (!$success) {
						$err .= "起始交易(BEGIN)錯誤,請檢查後再執行 !".chr(13).chr(13);
						global $mysql_link;
						$err .= mysql_errno($mysql_link).": ".mysql_error($mysql_link).chr(13);
					}
					// 刪其他相關table
					if ($err == "") {
						for ($i = 0; $i < count($this->otherDelete); $i++) {
							if ($err == "") {
								$otherWhere = ""; 
								for ($j = 0; $j < count($this->otherDelete[$i]["where"]); $j += 2) { 
									for ($k = 0; $k < count($this->fields); $k++) {
										if ($this->fields[$k]["fieldName"] == $this->otherDelete[$i]["where"][$j + 1]) {
											$otherWhere .= ($j != 0 ? " AND " : "").$this->otherDelete[$i]["where"][$j]."=".($this->fields[$k]["isNumeric"] == "N" ? "'" : "").$row[$this->fields[$k]["fieldName"]].($this->fields[$k]["isNumeric"] == "N" ? "'" : "");
											break;
										}
									}
								}
								if ($otherWhere != "") {
									$strSQL = "DELETE FROM ".$this->otherDelete[$i]["table"]." WHERE ".$otherWhere;
									$success = mysql_query($strSQL);
									if (!$success) {
										$err = $this->otherDelete[$i]["errormessage"];
										global $mysql_link;
										$err .= mysql_errno($mysql_link).": ".mysql_error($mysql_link).chr(13);									
									}
								}
							}
						}
					}				
								
					// 刪主table
					if ($err == "") {
						$isset = true;
						for ($i = 0; $i < count($this->DataFormKey[3]); $i += 3) {
							if (!isset($_POST[$this->DataFormKey[3][$i]])) {
								$isset = false;
								break;
							}
						}
						if ($isset) {
							if ($where != "") {
								$strSQL = "DELETE FROM ".$this->tableName.$where;
								$success = mysql_query($strSQL);
								if (!$success) {
									$err = "刪除資料時發生錯誤,請檢查後再執行 !".chr(13).chr(13);
									global $mysql_link;
									$err .= mysql_errno($mysql_link).": ".mysql_error($mysql_link).chr(13);
								}
							} else {
								$err .= "條件值不存在,請檢查後再執行 !".chr(13);
							}
						} else {
							$err .= "資料錯誤或不存在 ,請檢查後再執行!".chr(13);
						}
					}
					
					// 執行其他StoreProcedure
					if ($err == "") {
						$err .= $this->execOtherSP($this->funcID);
					}
					
					// 取消或完成交易
					if ($err == "") {
						$strSQL = "COMMIT";
					} else {
						$strSQL = "ROLLBACK";
					}
					$success = mysql_query($strSQL);
					if (!$success) {
						$err .= "取消或完成交易(".$strSQL.")錯誤,請檢查後再執行 !".chr(13).chr(13);
						global $mysql_link;
						$err .= mysql_errno($mysql_link).": ".mysql_error($mysql_link).chr(13);
					}
				}
				$this->funcID = "L";
			}
		
			// 新增或修改儲存
			if ($this->funcID == "AW" || $this->funcID == "MW") {
				// 檢查新增或更改時之輸入值是否正確
				// 檢查如果是數值,則輸入值是否為數值
				/*for ($i = 0; $i < count($this->fields); $i++) { 
					if ($this->fields[$i]["isNumeric"] == "Y" && isset($_POST[$this->fields[$i]["fieldName"]])) {
						if (!is_numeric($_POST[$this->fields[$i]["fieldName"]])) {
							$err .= "【".$this->fields[$i]["displayName"]."】之輸入值應為數值,請檢查後再儲存!".chr(13);
						}
					}
				}*/
				
				// 新增或修改->寫入資料庫
				// MasterForm新增或修改後,列表要過濾停在那一筆
				$s_filters = array();
				for ($i = 0; $i < count($this->tableKey); $i++) { 
					for ($j = 0; $j < count($this->fields); $j++) { 
						if ($this->tableKey[$i] == $this->fields[$j]["fieldName"]) {
							$s_filter = array();
							$s_filter["fieldName"] = $this->tableKey[$i];
							$s_filter["value"] = ($this->funcID == "AW" ? null : $_POST[$this->tableKey[$i]]);
							$s_filter["isNumeric"] = $this->fields[$j]["isNumeric"];
							$s_filters[] = $s_filter;
							break;
						}
					}
				}
				//
				if ($err == "") {
					// 起始交易
					$strSQL = "BEGIN";
					$success = mysql_query($strSQL);
					if (!$success) {
						$err .= "起始交易(BEGIN)錯誤,請檢查後再執行 !".chr(13).chr(13);
						global $mysql_link;
						$err .= mysql_errno($mysql_link).": ".mysql_error($mysql_link).chr(13);
					}
					
					if ($this->funcID == "AW") {
						$j = 0;
						$strSQL = "INSERT INTO ".$this->tableName." (";
						for ($i = 0; $i < count($this->fields); $i++) {
							$field = $this->fields[$i];
							if (isset($_POST[$field["fieldName"]])) {
								// 檢查是否為真實table欄位
								for ($x = 0; $x < count($this->tableFields); $x++) { 
									if ($this->tableFields[$x] == $field["fieldName"]) {
										$strSQL .= ($j == 0 ? "" : ",").$field["fieldName"];
										$j++;
										break;
									}
								}
							}
						}
						$j = 0;
						$strSQL .= ") VALUES (";
						for ($i = 0; $i < count($this->fields); $i++) {
							$field = $this->fields[$i];
							if (isset($_POST[$field["fieldName"]])) {
								$value = $_POST[$field["fieldName"]];
								if ($field["isNumeric"] == "Y" && $value == "") { $value = 0; }
								$value = ($field["inputType"] == "password" ? encrypt($value,'E') : $value);
								$value = $this->autoValue($i, $value);
								// 檢查是否為key值欄位
								for ($z = 0; $z < count($s_filters); $z++) { 
									if ($s_filters[$z]["fieldName"] == $field["fieldName"]) {
										$s_filters[$z]["value"] = $value;
										break;
									}
								}
								// 檢查是否為真實table欄位
								for ($x = 0; $x < count($this->tableFields); $x++) { 
									if ($this->tableFields[$x] == $field["fieldName"]) {
										// 檢查是否為日期且為空值,如果是,則儲存時要改為null
										if ($value == "" && $this->isDate($field["fieldName"])) {
											$strSQL .= ($j == 0 ? "" : ",")."null";											
										} else {
											$strSQL .= ($j == 0 ? "" : ",").($field["isNumeric"] == "Y" ? "" : "'").$value.($field["isNumeric"] == "Y" ? "" : "'");
										}
										$j++;
										break;
									}
								}
							}
						}
						$strSQL .= ")";
						$success = mysql_query($strSQL);
						if (!$success) {
							$err .= "儲存時發生錯誤,請檢查後再執行 !".chr(13).chr(13);
							global $mysql_link;
							$err .= mysql_errno($mysql_link).": ".mysql_error($mysql_link).chr(13);
						}
					} else {
						if ($where != "") {
							$j = 0;
							$strSQL = "UPDATE ".$this->tableName." SET ";
							for ($i = 0; $i < count($this->fields); $i++) {
								$field = $this->fields[$i];
								if (isset($_POST[$field["fieldName"]])) {
									$value = $_POST[$field["fieldName"]];
									if ($field["isNumeric"] == "Y" && $value == "") { $value = 0; }
									$value = ($field["inputType"] == "password" ? encrypt($value,'E') : $value);
									$value = $this->autoValue($i, $value);
									// 檢查是否為真實table欄位
									for ($x = 0; $x < count($this->tableFields); $x++) { 
										if ($this->tableFields[$x] == $field["fieldName"]) {
											$isKey = false;
											// 檢查是否為key,若是,則不更新
											/*for ($k = 0; $k < count($this->tableKey); $k++) { 
												if ($this->tableKey[$k] == $field["fieldName"]) {
													$isKey = true;
													break;
												}
											}*/
											if (!$isKey) {
												// 檢查是否為日期且為空值,如果是,則儲存時要改為null
												if ($value == "" && $this->isDate($field["fieldName"])) {
													$strSQL .= ($j == 0 ? "" : ",").$field["fieldName"]."=null";											
												} else {				
													$strSQL .= ($j == 0 ? "" : ",").$field["fieldName"]."=".($field["isNumeric"] == "Y" ? "" : "'").$value.($field["isNumeric"] == "Y" ? "" : "'");
												}
												$j++;
												break;
											}
										}
									}
								}
							}
							$strSQL .= $where;
							$success = mysql_query($strSQL);
							if (!$success) {
								$err .= "儲存時發生錯誤,請檢查後再執行 !".chr(13).chr(13);
								global $mysql_link;
								$err .= mysql_errno($mysql_link).": ".mysql_error($mysql_link).chr(13);								
							}
						} else {
							$err .= "條件值不存在,請檢查後再執行 !".chr(13);
						}
					}

					// 執行其他StoreProcedure
					if ($err == "") {
						$err .= $this->execOtherSP($this->funcID);
					}
					
					// 取消或完成交易
					if ($err == "") {
						$strSQL = "COMMIT";
					} else {
						$strSQL = "ROLLBACK";
					}
					$success = mysql_query($strSQL);
					if (!$success) {
						$err .= "取消或完成交易(".$strSQL.")錯誤,請檢查後再執行 !".chr(13).chr(13);
						global $mysql_link;
						$err .= mysql_errno($mysql_link).": ".mysql_error($mysql_link).chr(13);
					}
				}
				if ($err == "" && $this->DataFormKey[0] == "M") {
					// 回傳要變更m_filter的值
					for ($z = 0; $z < count($s_filters); $z++) { 
						$err .= ($z == 0 ? "" : "^A").$s_filters[$z]["fieldName"]."^A".$s_filters[$z]["value"]."^A".$s_filters[$z]["isNumeric"];
					}
				}
				$this->funcID = "L";
			}

			// L : 回列表 
			if ($this->funcID == "L") {
				echo $err;
			}
		}
	}
?>