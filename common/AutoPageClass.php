<?php
	/**
	 * 
	 */
	class AutoPageClass {
		// 成員屬性
		public $totalRows;			// 資料總列數
		public $perPageRows;		// 目前每頁顯示列數
		public $perPrePageRows;		// 前一次每頁顯示列數
		public $showPage;			// 目前顯示頁
		public $totalPages;			// 總頁數
		public $pageRows;			// 可選擇之每頁列數陣列
						
		// 建構子
		function __construct($perPageRows = 10) {
			$this->totalRows = 0;
			$this->perPageRows = $perPageRows;
			$this->perPrePageRows = $this->perPageRows;
			$this->showPage = 1;
			$this->totalPages = 1;
			$pageRows = array(10,20,30,40,50);
		}
		
		// 解建構子
		function __destruct() {
		}

		// 設定可選擇之每頁列數
		function setPageRows($pageRows) {
			$pageRows = natsort($pageRows);	// 重新排序(文數字混合時)
			if (is_array($pageRows)) {
				if (count($papRows) > 0) {
					$isNumeric = true;
					for ($i = 0; $i < count($pageRows); $i++) { 
						if (!is_numeric($pageRows[$i])) {
							$isNumeric = false;
							break;
						}
					}
					// 只有數字才有效
					if ($isNumeric) {
						$this->pageRows = $pageRows;
					}
				}
			}
		}

		// 顯示筆數及頁數
		function showData() {
			echo '<table><tr><td colspan="6"><form action="" name="MasterFormPage" id="MasterFormPage" method="POST">
		<br>每頁顯示';
		echo '<select name="m_PageCount" id="m_PageCount" style="font-family: 標楷體 ,Arial, Helvetica, sans-serif;" onchange="refreshDataMPage(\'Company.php\',\''.$_POST['SubMenu'].'\',\''.$_POST['ProgramItem'].'\',\'\');">';
		$m_PageCountArray = array("5","5","10","10","15","15","20","20");
		for ($i = 0; $i < sizeof($m_PageCountArray); $i = $i + 2) {
			$selected = "";
			if ($m_PageCount == $m_PageCountArray[$i+1]) {
				$selected = "selected";
			}
			echo '<option value="'.$m_PageCountArray[$i+1].'" '.$selected.'>'.$m_PageCountArray[$i].'</option>';			
		}
		echo '</select>';
		echo '共'.$m_TotalCount.'筆    ';							
		echo '<input type="Button" style="font-family: 標楷體 ,Arial, Helvetica, sans-serif;" value="<<" '.($m_Page==1?"disabled":"").' onclick="refreshDataMPage(\'Company.php\',\''.$_POST['SubMenu'].'\',\''.$_POST['ProgramItem'].'\',\'f\');">';
		echo '<input type="Button" style="font-family: 標楷體 ,Arial, Helvetica, sans-serif;" value="<" '.($m_Page==1?"disabled":"").' onclick="refreshDataMPage(\'Company.php\',\''.$_POST['SubMenu'].'\',\''.$_POST['ProgramItem'].'\',\'p\');">';
		echo '<input type="Button" style="font-family: 標楷體 ,Arial, Helvetica, sans-serif;" value=">" '.($m_Page==$m_TotalPage?"disabled":"").' onclick="refreshDataMPage(\'Company.php\',\''.$_POST['SubMenu'].'\',\''.$_POST['ProgramItem'].'\',\'n\');">';
		echo '<input type="Button" style="font-family: 標楷體 ,Arial, Helvetica, sans-serif;" value=">>" '.($m_Page==$m_TotalPage?"disabled":"").' onclick="refreshDataMPage(\'Company.php\',\''.$_POST['SubMenu'].'\',\''.$_POST['ProgramItem'].'\',\'l\');">';
		echo '    跳至';
		echo '<select name="m_Page" id="m_Page" style="font-family: 標楷體 ,Arial, Helvetica, sans-serif;" onchange="refreshDataMPage(\'Company.php\',\''.$_POST['SubMenu'].'\',\''.$_POST['ProgramItem'].'\',\'\');">';
		for ($i = 1; $i <= $m_TotalPage; $i++) {
			$selected = "";
			if ($m_Page == $i) {
				$selected = "selected";
			}
			echo '<option value="'.$i.'" '.$selected.'>'.$i.'</option>';			
		}
		echo '</select>';
		echo '共'.$m_TotalPage.'頁';
		echo '<input id="m_PrePageCount" name="m_PrePageCount" value="'.$m_PageCount.'" type="HIDDEN">';
		echo '<input id="m_TotalPage" name="m_TotalPage" value="'.$m_TotalPage.'" type="HIDDEN">';		
		echo '</form></td></tr>';
		//
		}
	}
?>