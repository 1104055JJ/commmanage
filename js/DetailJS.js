function handleDetailItem(l_amdurl,f) {
	var m_id = $("#m_id").val();			
	var id = $("#d_id").val();
	var id1 = $("#d_id1").val();
	var id2 = $("#d_id2").val();
	if (f == "A") { refreshDataDAMD(l_amdurl,'A',m_id,'',''); showSelectDetailItem(l_amdurl,0,m_id,'',''); }
	if (id != "") {
		if (f == "M") { refreshDataDAMD(l_amdurl,'M',id,id1,id2); }
		if (f == "D") { refreshDataDAMD(l_amdurl,'D',id,id1,id2); }
	}
}
			
function showSelectDetailItem(l_amdurl,x,id,id1,id2) {
	var j = $("#d_tr_count").val();
	if (j > 0) {
		for (var i = 1; i <= j; i++) {
			document.getElementById("d_item_"+i).innerHTML = "";
		}
		if (x > 0 && id != "" && id1 != "") {
			document.getElementById("d_item_"+x).innerHTML = "<img src=\"../images/icon/icon_arrow.png\">";
			document.getElementById("d_id").value = id;
			document.getElementById("d_id1").value = id1;
			document.getElementById("d_id2").value = id2;					
			refreshDataDAMD(l_amdurl,'Q',id,id1,id2);
		} else {
			document.getElementById("d_id").value = "";
			document.getElementById("d_id1").value = "";
			document.getElementById("d_id2").value = "";					
			refreshDataDAMD(l_amdurl,'A',id,'','');						
		}
	} else {
		document.getElementById("d_id").value = "";
		document.getElementById("d_id1").value = "";
		document.getElementById("d_id2").value = "";				
		refreshDataDAMD(l_amdurl,'A',id,'','');
	}
}
				
function refreshDataDetail(l_durl,l_amdurl,id) {
	$.ajax({
		url: l_amdurl,
        type:"POST",
        data:$('#DetailForm').serialize(),
        success: function(data){
            var data = $.trim(data);
            if (data != "") {
            	alert(data);
            } else {
				var strObj = $("#DetailFormPage").serialize();
				strObj = strObj.replace(/\&/g,",\"");
				strObj = "{\"" + strObj.replace(/\=/g,"\":") + "}";
				eval('var obj = ' + strObj);
				refreshDataD(l_durl,id,obj.d_PrePageCount,obj.d_PageCount,obj.d_Page);
				refreshDataDAMD(l_amdurl,'A',id,'','');
			}            		
         }
	});
}								

function refreshDataDPage(l_murl,id,p) {
	var strObj = $("#DetailFormPage").serialize();
	strObj = strObj.replace(/\&/g,",\"");
	strObj = "{\"" + strObj.replace(/\=/g,"\":") + "}";
	eval('var obj = ' + strObj);
	var page = obj.d_Page;
	var totalpage = obj.d_TotalPage;
	if (p == "f") {page = 1;}
	if (p == "p") {if ((page-1) <= 0) {page = 1;} else {page = page - 1;}}
	if (p == "n") {if ((page+1) > totalpage) {page = totalpage;} else {page = page + 1;}}
	if (p == "l") {page = totalpage;}
	refreshDataD(l_murl,id,obj.d_PrePageCount,obj.d_PageCount,page);	
}
