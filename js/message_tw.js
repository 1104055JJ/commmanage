jQuery.extend(jQuery.validator.messages, {
  required: "這是必填欄位",
  remote: "請修正該欄位",
  email: "E-Mail格式不符",
  url: "請輸入正確網址",
  date: "請輸入正確的日期",
  dateISO: "請輸入正確的日期 (ISO).",
  number: "請輸入0~9的阿拉伯數字",
  digits: "請輸入0~9的阿拉伯數字(不可有小數點)",
  creditcard: "請輸入合法信用卡號",
  equalTo: "請再次輸入相同的資料",
  accept: "请输入拥有合法后缀名的字符串",
  maxlength: jQuery.validator.format("請輸入一個長度最多是 {0} 的字串"),
  minlength: jQuery.validator.format("請輸入一個長度最少是 {0} 的字串"),
  rangelength: jQuery.validator.format("請輸入一個長度介於 {0} 和 {1} 之間的字串"),
  range: jQuery.validator.format("請輸入一個介於 {0} 和 {1} 之間的值"),
  max: jQuery.validator.format("請輸入一個最大為{0} 的值"),
  min: jQuery.validator.format("請輸入一個最小為{0} 的值")
});