function foldSiteMenu(x) {
	if (document.getElementById("SiteMenu"+x).style.display == "none") {
		document.getElementById("SiteMenu"+x).style.display = "";
	} else {
		document.getElementById("SiteMenu"+x).style.display = "none";					
	}
}
		
function SelectSite(x,y) {
	j = document.getElementById("SiteCount").value;
	for (i=1 ; i <= j; i++) {
		document.getElementById("SelectSite"+i).style.display="none";
	}
	document.getElementById("SiteMenu"+x).style.display = "";
	document.getElementById("SelectSite"+y).style.display="";
}

function refreshDataFunction(l_url,siteid,functionid,functionlink) {
	$.ajax({
		url: l_url,
			type:"POST",
			data:{"SiteID":siteid,"FunctionID":functionid},
			success: function(data){
				$("#CenterZoneF").empty().append(data);
				if (functionid == "") {
					$("#CenterZoneQ").empty().append("");
				} else {
					//var iframe = '<iframe id="FunctionIframe" src="" width="100%" height="500px" marginwidth="0" marginheight="0" scrolling="yes" frameborder="0" align="center"></iframe>';
					//$("#CenterZoneQ").empty().append(data);
					//document.getElementById("FunctionIframe").src = functionlink;
					//refreshDataQ(functionlink);
				}				
			}
	});
}

function ChangeFunctionColor(i) {
	var function_count = document.getElementById("function_count").value;
	for (j=1;j<=function_count;j++) {
		if (j == i) {
			document.getElementById("Function"+j).style.backgroundColor = "#88FFFF";
		} else {
			document.getElementById("Function"+j).style.backgroundColor = "";			
		}
	}
}

function refreshDataQ(l_url,siteid) {
	$.ajax({
		url: l_url,
			type:"POST",
			data:{"SiteID":siteid},
			success: function(data){
				$("#CenterZoneQ").empty().append(data);
			}
	});
}

function refreshDataIECCTV(l_url,siteid,host) {
	$.ajax({
		    url: l_url,
			type:"POST",
			data:{"SiteID":siteid,"Host":host},
			success: function(data){
				$("#CCTVZone").empty().append(data);
			}
	});		
}	

function refreshDataFromForm(l_url,formid,appendid) {
	$.ajax({
		    url: l_url,
			type:"POST",
			data:$("#"+formid).serialize(),
			success: function(data){
				$("#"+appendid).empty().append(data);
			}
	});
}

function refreshDataFromObj(l_url,objdata,appendid) {
	$.ajax({
		    url: l_url,
			type:"POST",
			data:objdata,
			success: function(data){
				$("#"+appendid).empty().append(data);
			}
	});
}