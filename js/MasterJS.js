function handleMasterItem(l_amdurl,f) {
	var m_id = $("#m_id").val();
	if (f == "A") {
		$("#CenterZoneD").empty().append("");             	
        $("#CenterZoneDAMD").empty().append("");             	                 	            	
		refreshDataMAMD(l_amdurl,'A','');
		showSelectMasterItem(l_amdurl,0,'');
	}
	if (m_id != "") {
		if (f == "M") { refreshDataMAMD(l_amdurl,'M',m_id); }
		if (f == "D") { refreshDataMAMD(l_amdurl,'D',m_id); }
	}
}
			
function showSelectMasterItem(l_amdurl,x,m_id) {
	var j = $("#m_tr_count").val();
	if (j > 0) {
		for (var i = 1; i <= j; i++) {
			document.getElementById("m_item_"+i).innerHTML = "";
		}
		if (x > 0 && m_id != "") {
			document.getElementById("m_item_"+x).innerHTML = "<img src=\"../images/icon/icon_arrow.png\">";
			document.getElementById("m_id").value = m_id;
			refreshDataMAMD(l_amdurl,'Q',m_id);
		} else {
			document.getElementById("m_id").value = "";
			refreshDataMAMD(l_amdurl,'A','');						
		}
	} else {
		document.getElementById("m_id").value = "";
		refreshDataMAMD(l_amdurl,'A','');
	}
}
				
function refreshDataMaster(l_murl,l_amdurl) {
	$.ajax({
		url: l_amdurl,
        type:"POST",
        data:$('#MasterForm').serialize(),
        success: function(data){
          	var data = $.trim(data);
          	if (data != "") {
          		alert(data);
          	} else {
				var strObj = $("#MasterFormPage").serialize();
				strObj = strObj.replace(/\&/g,",\"");
				strObj = "{\"" + strObj.replace(/\=/g,"\":") + "}";
				eval('var obj = ' + strObj);
				refreshDataM(l_murl,obj.m_PrePageCount,obj.m_PageCount,obj.m_Page);
				refreshDataMAMD(l_amdurl,'A','');
			}            		
       	}
	});
}

function refreshDataMPage(l_murl,p) {
	var strObj = $("#MasterFormPage").serialize();
	strObj = strObj.replace(/\&/g,",\"");
	strObj = "{\"" + strObj.replace(/\=/g,"\":") + "}";
	eval('var obj = ' + strObj);
	var page = obj.m_Page;
	var totalpage = obj.m_TotalPage;
	if (p == "f") {page = 1;}
	if (p == "p") {if ((page-1) <= 0) {page = 1;} else {page = page - 1;}}
	if (p == "n") {if ((page+1) > totalpage) {page = totalpage;} else {page = page + 1;}}
	if (p == "l") {page = totalpage;}
	refreshDataM(l_murl,obj.m_PrePageCount,obj.m_PageCount,page,$('#m_orderby').val(),$('#m_orderbydesc').val());	
}

function setMDataOrderBy(fieldName,l_murl,p) {
	if (document.getElementById("m_orderbydesc").value == "") {
		document.getElementById("m_orderbydesc").value = "DESC";
	} else {
		document.getElementById("m_orderbydesc").value = "";
	}
	document.getElementById("m_orderby").value = fieldName;
	refreshDataMPage(l_murl,x,y,p);
}
