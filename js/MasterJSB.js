// 控制開窗
$(function() {
	$("#CenterZoneMAMD").dialog({
		autoOpen: false,
		modal: true,
		width: screen.availWidth*0.9,
		height: screen.availHeight/2
	});
});		

// Master之DataList中FAMD之event
function handleMasterItem(l_amdurl,f) {
	var m_id = $("#m_id").val();
	if (f == "A" || f == "F") {
		$("#CenterZoneD").empty().append("");             	
        $("#CenterZoneDAMD").empty().append("");             	                 	            	
		//if (f == "A") { refreshDataMAMD(l_amdurl,'A','',$('#m_filter').val()); showSelectMasterItem(l_amdurl,0,'',f); }		
		//if (f == "F") { refreshDataMAMD(l_amdurl,'F','',$('#m_filter').val()); showSelectMasterItem(l_amdurl,0,'',f); }		
		if (f == "A") { showSelectMasterItem(l_amdurl,0,'',f); }		
		if (f == "F") { showSelectMasterItem(l_amdurl,0,'',f); }		
	}
	if (m_id != "") {
		if (f == "M") { refreshDataMAMD(l_amdurl,'M',m_id,$('#m_filter').val()); }
		if (f == "D") { refreshDataMAMD(l_amdurl,'D',m_id,$('#m_filter').val()); }
	}
}

// Master之DataList中選擇某項次後之標示及event			
function showSelectMasterItem(l_amdurl,x,m_id,f) {	
	var j = $("#m_tr_count").val();
	if (j > 0) {
		for (var i = 1; i <= j; i++) {
			$("#m_tr_"+i).removeAttr("class");
			$("#m_editdetail_"+i).attr("disabled","disabled");			
			$("#m_editdetail_"+i).attr("class","btn btn-default btn-sm");
		}
		if (x > 0 && m_id != "") {
			$("#m_tr_"+x).attr("class","info");
			$("#m_editdetail_"+x).removeAttr("disabled");			
			$("#m_editdetail_"+x).attr("class","btn btn-success btn-sm");
			selectButton('M','');			
			document.getElementById("m_id").value = m_id;
			refreshDataMAMD(l_amdurl,'Q',m_id,$('#m_filter').val());
		} else {
			for (var i = 1; i <= j; i++) {
				$("#m_radioitem_"+i).removeAttr("checked");
			}
			document.getElementById("m_id").value = "";
			refreshDataMAMD(l_amdurl,f,m_id,$('#m_filter').val());						
		}
	} else {
		document.getElementById("m_id").value = "";
		refreshDataMAMD(l_amdurl,f,m_id,$('#m_filter').val());
	}
	// 點選後關閉Detail之顯示(清空)
	$("#CenterZoneD").empty().append("");
	$("#CenterZoneDAMD").empty().append("");	
}

// MasterForm之按鈕event
function refreshDataMaster(l_murl,l_amdurl,f,displayMAMD) {
	f = f || "";
	displayMAMD = displayMAMD || "Y";

	$.ajax({
		url: l_amdurl,
        type:"POST",
        data:$('#MasterForm').serialize(),
        success: function(data){
          	var data = $.trim(data);
          	// 如果是AW或MW儲存後無誤,則會傳回過濾值,只列表該筆資料
          	if ((f == "AW" || f == "MW") && data != "" && data.indexOf("^A") != -1) {
          		f = "FW";
          	}
          	if (data != "" && f != "FW") {
          		alert(data);
          	} else {      			
				// 關窗
				$("#CenterZoneMAMD").dialog("close");
				
          		// page相關值變成object
				var strObj = $("#MasterFormPage").serialize();
				strObj = strObj.replace(/\&/g,",\"");
				strObj = "{\"" + strObj.replace(/\=/g,"\":") + "}";
				eval('var obj = ' + strObj);
				if (f == "FW") {
					document.getElementById("m_filter").value = data;
				}
				refreshDataMPage(l_murl,'f');
				if (displayMAMD == "Y") {
					refreshDataMAMD(l_amdurl,'A','',$('#m_filter').val());
				}
			}            		
       	}
	});
}

// 重整page
function refreshDataMPage(l_murl,p) {
	var strObj = $("#MasterFormPage").serialize();
	strObj = strObj.replace(/\&/g,",\"");
	strObj = "{\"" + strObj.replace(/\=/g,"\":") + "}";
	eval('var obj = ' + strObj);
	var page = obj.m_Page;
	var totalpage = obj.m_TotalPage;
	if (p == "f") {page = 1;}
	if (p == "p") {if ((page-1) <= 0) {page = 1;} else {page = page - 1;}}
	if (p == "n") {if ((page+1) > totalpage) {page = totalpage;} else {page = page + 1;}}
	if (p == "l") {page = totalpage;}
	refreshDataM(l_murl,obj.m_PrePageCount,obj.m_PageCount,page,$('#m_orderby').val(),$('#m_orderbydesc').val(),$('#m_filter').val(),$("#jsonID").val(),$("#formID").val());	
}

// 設定Master之DataList的排序
function setMDataOrderBy(fieldName,l_murl,p) {
	if (document.getElementById("m_orderbydesc").value == "") {
		document.getElementById("m_orderbydesc").value = "DESC";
	} else {
		document.getElementById("m_orderbydesc").value = "";
	}
	document.getElementById("m_orderby").value = fieldName;
	refreshDataMPage(l_murl,p);
}