// 控制開窗
$(function() {
	$("#CenterZoneDAMD").dialog({
		autoOpen: false,
		modal: true,
		width: screen.availWidth*0.9,
		height: screen.availHeight/2
	});
});		

// Detail之DataList中FAMD之event
function handleDetailItem(l_amdurl,f) {
	var m_id = $("#m_id").val();
	var id = $("#d_id").val();
	var id1 = $("#d_id1").val();
	var id2 = $("#d_id2").val();
	//if (f == "A") { refreshDataDAMD(l_amdurl,'A',m_id,'','',$('#d_filter').val()); showSelectDetailItem(l_amdurl,0,m_id,'','',f); }
	//if (f == "F") { refreshDataDAMD(l_amdurl,'F',m_id,'','',$('#d_filter').val()); showSelectDetailItem(l_amdurl,0,m_id,'','',f); }	
	if (f == "A") { showSelectDetailItem(l_amdurl,0,m_id,'','',f); }
	if (f == "F") { showSelectDetailItem(l_amdurl,0,m_id,'','',f); }	
	if (id != "") {
		if (f == "M") { refreshDataDAMD(l_amdurl,'M',id,id1,id2,$('#d_filter').val()); }
		if (f == "D") { refreshDataDAMD(l_amdurl,'D',id,id1,id2,$('#d_filter').val()); }
	}
}

// Detail之DataList中選擇某項次後之標示及event			
function showSelectDetailItem(l_amdurl,x,id,id1,id2,f) {
	var j = $("#d_tr_count").val();
	if (j > 0) {
		for (var i = 1; i <= j; i++) {
			$("#d_tr_"+i).removeAttr("class");			
		}
		if (x > 0 && id != "") {
			$("#d_tr_"+x).attr("class","info");
			document.getElementById("d_id").value = id;
			document.getElementById("d_id1").value = id1;
			document.getElementById("d_id2").value = id2;					
			refreshDataDAMD(l_amdurl,'Q',id,id1,id2,$('#d_filter').val());
		} else {
			for (var i = 1; i <= j; i++) {
				$("#d_radioitem_"+i).removeAttr("checked");
			}			
			document.getElementById("d_id").value = "";
			document.getElementById("d_id1").value = "";
			document.getElementById("d_id2").value = "";					
			refreshDataDAMD(l_amdurl,f,id,'','',$('#d_filter').val());
		}
	} else {
		document.getElementById("d_id").value = "";
		document.getElementById("d_id1").value = "";
		document.getElementById("d_id2").value = "";				
		refreshDataDAMD(l_amdurl,f,id,'','',$('#d_filter').val());
	}
}

// DetailForm之按鈕event				
function refreshDataDetail(l_durl,l_amdurl,id,f,displayDAMD) {
	f = f || "";
	displayDAMD = displayDAMD || "Y";
	var m_id = $("#m_id").val();
	$.ajax({
		url: l_amdurl,
        type:"POST",
        data:$('#DetailForm').serialize(),
        success: function(data){
            var data = $.trim(data);
            if (data != "" && f != "FW") {
            	alert(data);
            } else {
				// 關窗
				$("#CenterZoneDAMD").dialog("close");
            	
				var strObj = $("#DetailFormPage").serialize();
				strObj = strObj.replace(/\&/g,",\"");
				strObj = "{\"" + strObj.replace(/\=/g,"\":") + "}";
				eval('var obj = ' + strObj);
				if (f == "FW") {
					document.getElementById("d_filter").value = data;
				}
				refreshDataDPage(l_durl,m_id,'f');
				if (displayDAMD == "Y") {
					refreshDataDAMD(l_amdurl,'A',m_id,'','',$('#d_filter').val());
				}
			}            		
         }
	});
}								

// 重整page
function refreshDataDPage(l_murl,id,p) {
	var strObj = $("#DetailFormPage").serialize();
	strObj = strObj.replace(/\&/g,",\"");
	strObj = "{\"" + strObj.replace(/\=/g,"\":") + "}";
	eval('var obj = ' + strObj);
	var page = obj.d_Page;
	var totalpage = obj.d_TotalPage;
	if (p == "f") {page = 1;}
	if (p == "p") {if ((page-1) <= 0) {page = 1;} else {page = page - 1;}}
	if (p == "n") {if ((page+1) > totalpage) {page = totalpage;} else {page = page + 1;}}
	if (p == "l") {page = totalpage;}
	refreshDataD(l_murl,id,obj.d_PrePageCount,obj.d_PageCount,page,$('#d_orderby').val(),$('#d_orderbydesc').val(),$('#d_filter').val());	
}

// 設定Detail之DataList的排序
function setDDataOrderBy(fieldName,l_murl,id,p) {
	if (document.getElementById("d_orderbydesc").value == "") {
		document.getElementById("d_orderbydesc").value = "DESC";
	} else {
		document.getElementById("d_orderbydesc").value = "";
	}
	document.getElementById("d_orderby").value = fieldName;
	refreshDataDPage(l_murl,id,p);
}