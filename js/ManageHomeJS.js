$(function() {
	$("#dialogWindow").dialog({
		autoOpen: false,
		height:550,
		width:600,
		modal:true,
		close: function(ev, ui) { $(this).empty().append(""); }
	});
});

function openDialogWindow(title,url) {
	$.ajax({
		url: url,
        type:"POST",
        data:"",
        success: function(data){
        	var data = '<div id="div_dialogWindow">'+data+'</div>';
			$("#dialogWindow").empty().append(data);
			$("#dialogWindow").dialog("open");
			$("#dialogWindow").dialog("option","title",title);
			if ($("#div_dialogWindow").height() > 0) {
				$("#dialogWindow").dialog("option","height",$("#div_dialogWindow").height()+80);
			} else {
				$("#dialogWindow").dialog("option","height",600);
			}		             	
		}
	});	
}

function login(l_url) {
	$.ajax({
		url: l_url,
        type:"POST",
        data:$("#manageLoginForm").serialize(),
        success: function(data){
        	//if (data != "") {
        		$("#manageLoginMessage").empty().append(data);
            //}             	                 	            	
		}
	});	
}

function refreshDataTopMenu(l_url,Community) {
	$.ajax({
		url: l_url,
        type:"POST",
        data:{"Community":Community},
        success: function(data){
			$("#TopMenu").empty().append(data);             	
			$("#CenterZoneM").empty().append("");
            $("#CenterZoneMAMD").empty().append("");             	
            $("#CenterZoneD").empty().append("");             	
            $("#CenterZoneDAMD").empty().append("");             	                 	            	
		}
	});	
}

function foldMainMenu() {
	if (document.getElementById("LeftZone").style.display == "none") {
		document.getElementById("LeftZone").style.display = "";
		document.getElementById("foldMainMenuImage").src = "../images/icon/Menu-Hide.gif";
	} else {
		document.getElementById("LeftZone").style.display = "none";
		document.getElementById("foldMainMenuImage").src = "../images/icon/Menu-Show.gif";		
	}
}

function foldMenu(menu) {
	if (document.getElementById(menu).style.display == "none") {
		document.getElementById(menu).style.display = "";
	} else {
		document.getElementById(menu).style.display = "none";
	}	
}

function foldSubMenu(x) {
	if (document.getElementById("subMenu"+x).style.display == "none") {
		document.getElementById("subMenu"+x).style.display = "";
		document.getElementById("subMenuImage"+x).src = "../images/icon/NT-Collapse.gif";		
	} else {
		document.getElementById("subMenu"+x).style.display = "none";
		document.getElementById("subMenuImage"+x).src = "../images/icon/NT-Expand.gif";
	}
}

// 標示目前所選之程式
function SelectProgram(x,y) {
	var j = document.getElementById("ProgramCount").value;
	for (var i=1 ; i <= j; i++) {
		document.getElementById("SelectProgram"+i).style.display="none";
	}
	document.getElementById("subMenu"+x).style.display = "";
	document.getElementById("SelectProgram"+y).style.display="";
}

// 重整Master的DataList
function refreshDataM(l_url,m_PrePageCount,m_PageCount,m_Page,orderby,orderbydesc,m_filter,jsonID,formID) {
	orderby = orderby || "";
	orderbydesc = orderbydesc || "";
	m_filter = m_filter || "";
	jsonID = jsonID || "";
	formID = formID || "";
	document.getElementById("jsonID").value = jsonID;
	document.getElementById("formID").value = formID;
	$.ajax({
		url: l_url,
        type:"POST",
        data:{"jsonID":jsonID,"formID":formID,"m_PrePageCount":m_PrePageCount,"m_PageCount":m_PageCount,"m_Page":m_Page,"m_orderby":orderby,"m_orderbydesc":orderbydesc,"m_filter":m_filter},
        success: function(data){
			document.getElementById("tr_CenterZoneM").style.display = "";
			document.getElementById("tr_CenterZoneMAMD").style.display = "";                	
			$("#CenterZoneM").empty().append(data);
            $("#CenterZoneMAMD").empty().append("");             	
            $("#CenterZoneD").empty().append("");             	
            $("#CenterZoneDAMD").empty().append("");
            initSelectButton("M");
		}
	});
}

// 重整Master的Form
function refreshDataMAMD(l_url,f,id,m_filter) {
	m_filter = m_filter || "";
	$.ajax({
		url: l_url,
        type:"POST",
        data:{"jsonID":$("#jsonID").val(),"formID":$("#formID").val(),"f":f,"id":id,"m_filter":m_filter},
        success: function(data){
        	document.getElementById("tr_CenterZoneM").style.display = "";
            document.getElementById("tr_CenterZoneMAMD").style.display = "";
            $("#CenterZoneMAMD").empty().append(data);                        	            		
			// 點選查資料後MasterForm先不展開
			if (f != "Q") {
				// 開窗
				$("#CenterZoneMAMD").dialog("open");
				$("#CenterZoneMAMD").dialog("option","height",$("#MasterFormMainTable").height()+100);
			}			
		}
	});
}

// 重整Detail的DataList
function refreshDataD(l_url,id,d_PrePageCount,d_PageCount,d_Page,orderby,orderbydesc,d_filter) {
	orderby = orderby || "";
	orderbydesc = orderbydesc || "";
	d_filter = d_filter || "";	
	$.ajax({
		url: l_url,
        type:"POST",
        data:{"jsonID":$("#jsonID").val(),"formID":$("#formID").val(),"id":id,"d_PrePageCount":d_PrePageCount,"d_PageCount":d_PageCount,"d_Page":d_Page,"d_orderby":orderby,"d_orderbydesc":orderbydesc,"d_filter":d_filter},
        success: function(data){
			document.getElementById("tr_CenterZoneD").style.display = "";
            document.getElementById("tr_CenterZoneDAMD").style.display = "";
            $("#CenterZoneD").empty().append(data);
            $("#CenterZoneDAMD").empty().append("");
            initSelectButton("D");
        }
	});
}

// 重整Detail的Form
function refreshDataDAMD(l_url,f,id,id1,id2,d_filter) {
	d_filter = d_filter || "";
	$.ajax({
		url: l_url,
        type:"POST",
        data:{"jsonID":$("#jsonID").val(),"formID":$("#formID").val(),"f":f,"id":id,"id1":id1,"id2":id2,"d_filter":d_filter},
        success: function(data){
        	document.getElementById("tr_CenterZoneD").style.display = "";
            document.getElementById("tr_CenterZoneDAMD").style.display = "";                	            		
            $("#CenterZoneDAMD").empty().append(data);
			// 點選查資料後DetailForm先不展開
			if (f != "Q") {
				// 開窗
				$("#CenterZoneDAMD").dialog("open");
				$("#CenterZoneDAMD").dialog("option","height",$("#DetailFormMainTable").height()+100);
			}			
		}
	});
}

// 當Radio Button為唯讀時,自動返回原值
function readonlyRadioButton(form,id,value) {
	var formname = "MasterForm";
	if (form != "M") { formname = "DetailForm"; }
	var obj = document.forms[formname].elements[id];
	for (var i = 0; i < obj.length; i++) {
		if (obj[i].value == value) {
			obj[i].checked = true;
		} else {
			obj[i].checked = false;
		}
	}
}

// 預設按鈕設定
function initSelectButton(MasterOrDetail) {
	var prefix = "m_";
	if (MasterOrDetail == "D") {
		prefix = "d_";
	}
	$("#"+prefix+"queryButton").attr("class","btn btn-info");
	$("#"+prefix+"insertButton").attr("class","btn btn-info");
	$("#"+prefix+"updateButton").attr("class","btn btn-info");
	$("#"+prefix+"deleteButton").attr("class","btn btn-info");
	$("#"+prefix+"updateButton").attr("disabled","disabled");
	$("#"+prefix+"deleteButton").attr("disabled","disabled");
}

// 改變AutoData之按鈕顏色
function selectButton(MasterOrDetail,id) {
	var prefix = "m_";
	if (MasterOrDetail == "D") {
		prefix = "d_";
	}
	$("#"+prefix+"queryButton").attr("class","btn btn-info");
	$("#"+prefix+"insertButton").attr("class","btn btn-info");
	$("#"+prefix+"updateButton").attr("class","btn btn-info");
	$("#"+prefix+"deleteButton").attr("class","btn btn-info");
	$("#"+id).attr("class","btn btn-primary");	

	$("#"+prefix+"queryButton").removeAttr("disabled");
	$("#"+prefix+"insertButton").removeAttr("disabled");
	$("#"+prefix+"updateButton").removeAttr("disabled");
	$("#"+prefix+"deleteButton").removeAttr("disabled");
	
	if (id == prefix+"queryButton" || id == prefix+"insertButton") {
		$("#"+prefix+"updateButton").attr("disabled","disabled");
		$("#"+prefix+"deleteButton").attr("disabled","disabled");		
	}
}

function openLOV(MasterOrDetail,jsonID) {
	var formID = "MasterForm";
	if (MasterOrDetail == "D") { formID = "DetailForm"; }
	var left = (screen.width/2)-(500/2);
	var top = (screen.height/2)-(500/2);
	var w = window.open("Lov.php?formID=" + formID + "&jsonID=" + jsonID + "&" + $("#"+formID).serialize(),"小幫手搜尋","toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=500, height=500, top="+ top +", left=" + left);	
}

//1104055 version
function openLOVE(MasterOrDetail,passin,passout,code) {     
       //passin 目前用不到 
       var formID = "MasterForm";
        if (MasterOrDetail == "D") { formID = "DetailForm"; }
	var passoutarray = passout.split(",");
	var passoutIDvalue = "";
	for(var i = 0; i < passoutarray.length; i++) {
		//抓ID的值
	passoutIDvalue += passoutarray[i] + ',' + $('#'+passoutarray[i]+'',document.forms[formID]).val() + (i === passoutarray.length - 1 ? '' : ',');
	}
	var uri = './PA_Search.php?passout=' + passoutIDvalue + '&passin=' + passin + '&formID=' + formID + '&code=' + code;
	window.open(uri, '搜尋', config='height=400,width=650,left=450,top=200');
}

//點選查資料後MasterForm或DetailForm先不展開
function foldTable(div_id,display_id,image_id) {
	if ($("#"+display_id).val() == "Y") {
		document.getElementById(display_id).value = "N";
		document.getElementById(div_id).style.display = "none";
		document.getElementById(image_id).src = "../images/icon/NT-Expand.gif";
	} else {
		document.getElementById(display_id).value = "Y";
		document.getElementById(div_id).style.display = "";
		document.getElementById(image_id).src = "../images/icon/NT-Collapse.gif";			
	}
}

//檢察傳入值(類型(Need必填,Float可帶小數點之數字，Num整數,Rec發票),Formname,fieldname,說明文字)
function checkFieldValue(type,form,field,show){
    var f = document.forms[form].elements["f"].value;   
	if (f != "FW") {	
		document.forms[form].elements[field].style.border = "";
		document.forms[form].elements[field].placeholder="";
		var con = $.trim(document.forms[form].elements[field].value); // 去除前後空白
		var event = true;
		
		//必填
		if(type == "Need"){
			if (con == "") {
				event = false;
			}		
		}
			
		// 可帶小數點之數字
		if (type == "Float"){
			var re = /^\d+[.]?\d*$/;
			if (con == "" || con.value <= 0 || !re.test(con)) {
				event = false;
			}
		}
		
		// 檢查整數
		if (type == "Num"){
			var re = /^\d+$/;		
			if (con.value == "" || con.value <=0 || !re.test(con)) {
				event = false;
			}
		}
		
		// 檢查發票
		if (type == "Rec"&&con != ""){
			var conl=con.length;
			var con2=con.substr(0,1);
			var con3=con.substr(1,1);				
			if (conl != 10 ) {
				event = false;
			} else {
				re = /[a-zA-Z]/;
				if(!re.test(con2) || !re.test(con3)) {				
	  				event = false;
				}
			}			
	    }
	    //檢察公司統一編號
	    if (type == "TaxId"){
	    	//$radio=$('input[name="Type"]:checked').val();
	    	var radio=document.forms[form].elements["Type"].value;	    	
			if (con != ""){			
			if(radio==='1'){
  	 			if(!isValidGUI(con)){event = false;}
  				else{event = true;}       
			}
			else{  
				if(!checkTwID(con)){event = false;}
  				else{event = true;}  
			}
			}
		}

	    if (event == false) {
	    	if (type != "Rec"||type !="TaxId"){     	   	
	    	document.forms[form].elements[field].value="";
	    	document.forms[form].elements[field].style.border="2px red dotted";
	    	document.forms[form].elements[field].placeholder=show;
	    	}
	    	else{
	    	document.forms[form].elements[field].style.border="2px red dotted";
	    	document.forms[form].elements[field].placeholder=show;	
	    	} 
	    }

	    return event;
	}
}
		//統編檢查呼叫程式	    
function isValidGUI(taxId) {
    var invalidList = "00000000,11111111";
    if (/^\d{8}$/.test(taxId) == false || invalidList.indexOf(taxId) != -1) {
        return false;
    }

    var validateOperator = [1, 2, 1, 2, 1, 2, 4, 1],
        sum = 0,
        calculate = function(product) { // 個位數 + 十位數
            var ones = product % 10,
                tens = (product - ones) / 10;
            return ones + tens;
        };
    for (var i = 0; i < validateOperator.length; i++) {
        sum += calculate(taxId[i] * validateOperator[i]);
    }

    return sum % 10 == 0 || (taxId[6] == "7" && (sum + 1) % 10 == 0);
};

//身分證字號檢查呼叫程式
function checkTwID(id){
//建立字母分數陣列(A~Z)
    var city = new Array(
    1,10,19,28,37,46,55,64,39,73,82, 2,11,
    20,48,29,38,47,56,65,74,83,21, 3,12,30
    )
    id = id.toUpperCase();
// 使用「正規表達式」檢驗格式
    if (id.search(/^[A-Z](1|2)\d{8}$/i) == -1) {
    return false;
    } else {
    //將字串分割為陣列(IE必需這麼做才不會出錯)
    id = id.split('');
    //計算總分
    var total = city[id[0].charCodeAt(0)-65];
    for(var i=1; i<=8; i++){
    total += eval(id[i]) * (9 - i);
    }
    //補上檢查碼(最後一碼)
    total += eval(id[9]);
    //檢查比對碼(餘數應為0);
    return ((total%10 == 0 ));
    }
    }	    
	    
function disabledExecButton(form,event) {
	if (event == false) {     	   	
		if (form == "MasterForm"){
	    	document.forms[form].m_execButton.disabled = true;	    		
	    } else {
	    	document.forms[form].d_execButton.disabled = true;
	    }
	} else {
	    if (form == "MasterForm"){
	    	document.forms[form].m_execButton.disabled = false;	    		
	    } else {
	    	document.forms[form].d_execButton.disabled = false;	    	
	    }
	}
}

function checkRule(form,fieldArray) {
	// 觸發onchange
	for (var i = 0; i < fieldArray.length; i++) {
		document.getElementById(form).elements.namedItem(fieldArray[i][0]).onchange = function() {MFunction();};					
	}
	document.forms[form].onsubmit = function() {return MFunction();};  
				
	// 一開始就先檢查並顯示提示
	MFunction();

	function MFunction() {
		var event = true;
		// 欄位檢查
		for (var i = 0; i < fieldArray.length; i++) {
			if (!checkFieldValue(fieldArray[i][1],form,fieldArray[i][0],fieldArray[i][2])) {event = false;}						
		}
		// 其他檢查
		if (!otherCheckRule()) {event = false;}
		// 控制儲存按鈕是否有效
		disabledExecButton(form,event);
		return event;
	}	
}
